$(document).ready(function() {
	var Ingredient = 0;
	var HowTo = 0;
	
//----------------------------------------------------
	$("#post-recipe-button").click(function(){
		Ingredient = 0;
		HowTo = 0;
	})
	
	$("#add-ingredients0").click(function addIngredient(){
		$("#add-ingredients" + Ingredient).hide()
		Ingredient+=1;
		var ingredient = document.createElement('div');
		ingredient.id = "ingredients" + Ingredient;
		var input1 = document.createElement('input');
		input1.className = "pcs-class";
		input1.type = "text";
		input1.name = "ingredient[" + Ingredient + "][pcs]";
		input1.setAttribute("placeholder", "Pcs");
		var input2 = document.createElement('input');
		input2.className = "ingredients-class";
		input2.type = "text";
		input2.name = "ingredient[" + Ingredient + "][name]";
		input2.setAttribute("placeholder", "Type ingredient");
		input2.style.position = "relative";
		input2.style.marginLeft = "4px";
		var input3 = document.createElement('input');
		input3.id = "add-ingredients" + Ingredient;
		input3.className = "add-posting-button";
		input3.type = "button";
		input3.value = "+";
		input3.style.position = "relative";
		input3.style.marginLeft = "4px";
		input3.onclick = addIngredient;
		ingredient.appendChild(input1);
		ingredient.appendChild(input2);
		ingredient.appendChild(input3); 
		var table = document.getElementById("td_ingredients");
		table.appendChild(ingredient);
	});

//----------------------------------------------------
	$("#add-howto0").click(function addHowTo(){
		$("#add-howto" + HowTo).hide()
		HowTo+=1;
		var howto = document.createElement('div');
		howto.id = "howto" + HowTo;
		var input1 = document.createElement('input');
		input1.className = "howto-class";
		input1.type = "text";
		input1.name = "howto[" + HowTo + "][desc]";
		HowTo+=1;
		input1.setAttribute("placeholder", "Step " + HowTo);
		HowTo-=1;
		var attach = document.createElement('span');
		attach.textContent = "Attach Image: ";
		var image = document.createElement('input');
		image.name = "howto[" + HowTo + "][pict]";
		image.type = "file";
		var input3 = document.createElement('input');
		input3.id = "add-howto" + HowTo;
		input3.className = "add-posting-button";
		input3.type = "button";
		input3.value = "+";
		input3.style.position = "relative";
		input3.style.marginLeft = "4px";
		input3.onclick = addHowTo;
		howto.appendChild(input1);
		howto.appendChild(input3);
		howto.appendChild(attach);
		howto.appendChild(image);
		var table = document.getElementById("td_howto");
		table.appendChild(howto);
	})
	
});