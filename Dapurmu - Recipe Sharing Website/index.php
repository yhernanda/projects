<?php
    include "koneksi.php";
	include "src/header.php";
    include "src/upside.php";
	include "src/navbar-home.php";
?>
		<style>
			body {font-family:Arial, Helvetica, sans-serif; font-size:12px;}
			
			.fadein { 
				position:relative; height:400px; width:960px; margin:0 auto;
				background: url("slideshow-bg.png") repeat-x scroll left top transparent;
				padding: 10px;
			 }
			.fadein img { position:absolute; left:0px; top:5px; height:400px; width:960px; margin-bottom: 10px; }
        </style>
        
        <script>
        $(function(){
            $('.fadein img:gt(0)').hide();
            setInterval(function(){$('.fadein :first-child').fadeOut().next('img').fadeIn().end().appendTo('.fadein');}, 3000);
        });
        </script> 
       
    <div class="fadein">
          <img src="img/post/sushi-food-2.jpg">
          <img src="img/post/tasty-fast-food-recipes-7.jpg">
          <img src="img/post/Food HD Wallpapers (3).jpg">
          <img src="img/post/desktop-backgrounds-pictures-of-steak-foods-for-mac.jpg">
          <img src="img/post/Fast-Food-Wallpapers-7.jpg">
	  <img src="img/post/d5a3498cfc9e53130b5f815ef44713b7_Jet.jpg">
	  <img src="img/post/traditional_mexican_foods_wallpaper_cool_hd.jpg">
	  <img src="img/post/spaghetti+with+meat+sauce11.jpg">
	  <img src="img/post/beijing_food.jpg">
	  <img src="img/post/24ChickenSalad.jpg">


    </div>
    
	<div id="layout">
   		<div id="content_index">
            
        <div id="isiindex">
            <?php
		

        if (isset($_GET["category"])){ 
                $category = $_GET["category"];
            if (isset($_GET["page"])){
                    $page=$_GET["page"];
                }
            else{
                $page=1;
            }    

            $limit = 3;
            $start = $limit*($page-1);

            $sqldb = "select * from resep where kategori = '".$category."' ORDER BY id DESC limit $start,$limit";
            $tot = mysql_query("select * from resep where kategori = '".$category."'");
            $total = mysql_num_rows($tot);
            $num_page = ceil($total/$limit);

            $getresep = mysql_query($sqldb,$con);?>
            <div id="bungkusresepindex">  
            
            <?php while ($resep = mysql_fetch_array($getresep)) { ?>
                    
                        <div class="resep">
                            <div class="gambarresep"><a href="showresep.php?idresep=<?php echo $resep['id'] ?>"><?php echo '<img class="gambar" src="data:image/jpeg;base64,'.base64_encode($resep["picture"]).'" onerror="this.src='.$defaultresep.'" alt="Recipe Photo">' ?> </a> </div>
                            <div class="judulresep"><a href="showresep.php?idresep=<?php echo $resep['id'] ?>" class="judulresepp" ><?php echo $resep["judul_resep"] ?></a></div>
                        </div>       
            <?php }  ?>
            </div>

            <div id="page">
            <?php
            if($num_page>=1){
                for ($i=1; $i <= $num_page ; $i++) { 
                    if ($i==$page) {
                        echo '<a href="index.php?category='.$category.'&page='.$i.'" class="pagee"">'.$i.'</a>';
                    }
                    else{
                        echo '<a href="index.php?category='.$category.'&page='.$i.'" class="pageee"">'.$i.'</a>';
                    }

                }
                echo"</div>";
            }
            ?>
            </div>
        
        <?php }

        else {
            if (isset($_GET["page"])){
                    $page=$_GET["page"];
                }
            else{
                $page=1;
            }    

            $limit = 3;
            $start = $limit*($page-1);

            $sqldb = "select * from resep ORDER BY id DESC limit $start,$limit";
            $tot = mysql_query("select * from resep");
            $total = mysql_num_rows($tot);
            $num_page = ceil($total/$limit);

            $getresep = mysql_query($sqldb,$con);?>
            <div id="bungkusresepindex">
            <?php while ($resep = mysql_fetch_array($getresep)) { ?>
                    
                        <div class="resep">
                            <div class="gambarresep"><a href="showresep.php?idresep=<?php echo $resep['id'] ?>"><?php echo '<img class="gambar" src="data:image/jpeg;base64,'.base64_encode($resep["picture"]).'" onerror="this.src='.$defaultresep.'" alt="Recipe Photo">' ?> </a> </div>
                            <div class="judulresep"><a href="showresep.php?idresep=<?php echo $resep['id'] ?>" class="judulresepp" ><?php echo $resep["judul_resep"] ?></a></div>
                        </div>       
            <?php }  ?>
            </div>

            <div id="page">
            <?php
            if($num_page>=1){
                for ($i=1; $i <= $num_page ; $i++) { 
                    if ($i==$page) {
                        echo '<a href="index.php?page='.$i.'" class="pagee"">'.$i.'</a>';
                    }
                    else{
                        echo '<a href="index.php?page='.$i.'" class="pageee"">'.$i.'</a>';
                    }

                }
                echo"</div>";
            }
            ?>
            </div>

        <?php } ?>        
            
            </div>

   		</div>
   		<div id="rightbar">
     		<div id="tasteoftheweek">
                <h2 class="title">Taste of the Week</h2>
                <?php
                $sqldb = "select * from resep ORDER BY like_count DESC limit 0,4";
                $getresep = mysql_query($sqldb,$con)
                ?>

                <div id="bungkustasteoftheweek">
                    <?php while ($resep = mysql_fetch_array($getresep)) { ?>
                                <div class="reseptasteoftheweek">
                                    <div class="judulreseptasteoftheweek"><a href="showreseps.php?idresep=<?php echo $resep['id'] ?>" class="judulresepptasteoftheweek" ><?php echo $resep["judul_resep"] ?></a></div>
                                </div>
                    <?php } ?>
                </div>

            </div>
     		<div id="category">
     			<h2 class="title">Category</h2>
     			<ul>
     				<li><a href="index.php?category=sayuran">Sayuran</a></li>
                    <li><a href="index.php?category=seafood">Ikan dan Seafood</a></li>
                    <li><a href="index.php?category=daging">Daging</a></li>
                    <li><a href="index.php?category=kedelai">Olahan Kedelai</a></li>
                    <li><a href="index.php?category=umbi">Umbi-umbian</a></li>
                    <li><a href="index.php?category=kuah">Makanan berkuah</a></li> 
                    <li><a href="index.php?category=pendamping">Makanan pendamping</a></li>
                    <li><a href="index.php?category=mie">Mie dan Pasta</a></li>
                    <li><a href="index.php?category=kue">Kue</a></li>
                    <li><a href="index.php?category=roti">Roti dan Pastri</a></li>
                    <li><a href="index.php?category=minuman">Minuman</a></li>
     			</ul>
     		</div>
   		</div>
	</div>

    <?php if(isset($_GET["err"])){ ?>
         <?php if($_GET["err"] == 2) { ?>
                    <script>
                        alert("Email already in use !");
                    </script>
        <?php }else if ($_GET["err"] == 1) { ?>
                     <script>
                        alert("Email or password is incorrect ");
                    </script>
        <?php }else if ($_GET["err"] == 3) { ?>
                     <script>
                        alert("The confirmed password is not same as password or secret not selected");
                    </script>
        <?php }else if ($_GET["err"] == 4) { ?>
                     <script>
                        alert("Email, secret or answer not correct");
                    </script>   
        <?php }else if ($_GET["scs"] == 10) { ?>
                     <script>
                        alert("Registration Successfull");
                    </script>   
        <?php }else if ($_GET["err"] == 5) { ?>
                     <script>
                        alert("Email or password is incorrect ");
                    </script>   
        <?php }
    } else if (isset($_GET["scs"])) {?>
        <?php if ($_GET["scs"] == 10) { ?>
                     <script>
                        alert("Registration Successfull");
                    </script>   
        <?php } 

    }?>



<?php	
	include "src/footer.php";
?>