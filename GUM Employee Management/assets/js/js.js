$(document).ready(function () {
	$('.dropMenu').click(function(){
		$('.navigation').toggleClass('visible');
		$('body').toggleClass('opacity');
	});

	$('[id="tool"]').tooltip();

	$('.tool').tooltip();

	$('[id="picture"]').tooltip();

	$('#tabelpegawai').DataTable();

	$('#tabelpelamar').DataTable();

	$('#tabelpegawai').on('click', 'tr td a.edit_pegawai', function (e) {
		e.preventDefault();
		var nip = $(this).closest('tr').find('td.nip').text();
		$.ajax({
			type: "POST",
			url: '',
			success: function (data) {
				window.location.replace("http://localhost/database_pegawai/view/edit_pegawai/"+nip);
				/*alert(data);*/
			},
			error: function  (data) {
				alert('terjadi kesalahan, gagal');
				return false;
			}						
		})
	});

	$('#tabelpelamar').on('click', 'tr td a.edit_pelamar', function (e) {
		e.preventDefault();
		var np = $(this).closest('tr').find('td.np').text();
		$.ajax({
			type: "POST",
			url: '',
			success: function (data) {
				window.location.replace("http://localhost/database_pegawai/view/edit_pelamar/"+np);
				/*alert(data);*/
			},
			error: function  (data) {
				alert('terjadi kesalahan, gagal');
				return false;
			}						
		})
	});

	$('#tabelpegawai').on('click', 'tr td a.hapus_pegawai', function (e) {
		e.preventDefault();
		var d = confirm('Hapus Data?');
		if (d == true){
			var nip = $(this).closest('tr').find('td.nip').text();
			$.ajax({
				type: "POST",
				url: 'http://localhost/database_pegawai/view/hapus_pegawai/'+nip,
				success: function (data) {
					
				}					
			})
		}else{
			return false;
		}
		$(this).closest('tr').fadeOut(function () {
			$(this).remove();
		})
	});

	$('#tabelpelamar').on('click', 'tr td a.hapus_pelamar', function (e) {
		e.preventDefault();
		var d = confirm('Hapus Data?');
		if (d == true){
			var np = $(this).closest('tr').find('td.np').text();
			$.ajax({
				type: "POST",
				url: 'http://localhost/database_pegawai/view/hapus_pelamar/'+np,
				success: function (data) {
					
				}						
			})
		}else{
			return false;
		}
		$(this).closest('tr').fadeOut(function () {
			$(this).remove();
		})
	});
});