<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class dashboard_model extends CI_Model{
    function __construct()
    {
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }

    function get_data_kelamin()
    {
    	$sql = "SELECT COUNT(jns_kelamin) as count FROM `pegawai` WHERE jns_kelamin = 'Laki-laki'";
    	$result['pegawai_laki'] = $this->db->query($sql);
    	$sql = "SELECT COUNT(jns_kelamin) as count FROM `pegawai` WHERE jns_kelamin = 'Perempuan'";
    	$result['pegawai_perempuan'] = $this->db->query($sql);

    	if($result){
    		return $result;
    	}else{
    		return false;
    	}
    }

    public function get_data_pendidikan(){
        $sql = "SELECT distinct(riwayat_pendidikan.nip), pegawai.jns_kelamin from riwayat_pendidikan inner join pegawai on riwayat_pendidikan.nip = pegawai.nip order by riwayat_pendidikan.nip";
        $result = $this->db->query($sql);
        $result = $result->result_array();
        $array = Array("SD_L" => 0, "SD_P" => 0, "SMP_L" => 0, "SMP_P" => 0, "SMA_L" => 0, "SMA_P" => 0, "Strata1_L" => 0, "Strata1_P" => 0);
        foreach ($result as $key => $value) {
            $sql = "SELECT max(pendidikan+0) as pendidikan from riwayat_pendidikan where nip = ".$value['nip'];
            $res = $this->db->query($sql);
            $res = $res->result_array()[0];
            if($res['pendidikan'] == 1 && $value['jns_kelamin'] == "Laki-laki"){
                $array['SD_L'] += 1;
            } else if($res['pendidikan'] == 1 && $value['jns_kelamin'] == "Perempuan"){
                $array['SD_P'] += 1;
            } else if($res['pendidikan'] == 2 && $value['jns_kelamin'] == "Laki-laki"){
                $array['SMP_L'] += 1;
            } else if($res['pendidikan'] == 2 && $value['jns_kelamin'] == "Perempuan"){
                $array['SMP_P'] += 1;
            } else if ($res['pendidikan'] == 3 && $value['jns_kelamin'] == "Laki-laki"){
                $array['SMA_L'] += 1;
            } else if ($res['pendidikan'] == 3 && $value['jns_kelamin'] == "Perempuan"){
                $array['SMA_P'] += 1;
            } else if ($res['pendidikan'] == 4 && $value['jns_kelamin'] == "Laki-laki"){
                $array['Strata1_L'] += 1;
            } else {
                $array['Strata1_P'] += 1;
            }
        }
        if ($result) {
            return $array;
        }else{
            return false;
        }   
    }

    public function get_data_golongan(){
        $sql = "SELECT nip, golongan, jns_kelamin from pegawai";
        $result = $this->db->query($sql);
        $result = $result->result_array();
        $array = Array();
        foreach ($result as $key => $value) {
            if($value['jns_kelamin'] == "Laki-laki"){
                if(array_key_exists($value['golongan']."_L", $array))
                    $array[$value['golongan']."_L"] += 1;
                else
                    $array[$value['golongan']."_L"] = 1;
            }
            else{
                if(array_key_exists($value['golongan']."_P", $array))
                    $array[$value['golongan']."_P"] += 1;
                else
                    $array[$value['golongan']."_P"] = 1;
            }
        }
        $sql = "SELECT distinct(golongan) from pegawai";
        $golongan = $this->db->query($sql);
        $golongan = $golongan->result_array();
        $all['golongan'] = $golongan;
        $all['jumlah'] = $array;

        if ($golongan) {
            return $all;
        }else{
            return false;
        }   
    }

    public function get_data_jabatan(){
        $sql = "SELECT nip, jabatan, jns_kelamin from pegawai";
        $result = $this->db->query($sql);
        $result = $result->result_array();
        $array = Array();
        foreach ($result as $key => $value) {
            if($value['jns_kelamin'] == "Laki-laki"){
                if(array_key_exists($value['jabatan']."_L", $array))
                    $array[$value['jabatan']."_L"] += 1;
                else
                    $array[$value['jabatan']."_L"] = 1;
            }
            else{
                if(array_key_exists($value['jabatan']."_P", $array))
                    $array[$value['jabatan']."_P"] += 1;
                else
                    $array[$value['jabatan']."_P"] = 1;
            }
        }
        $sql = "SELECT distinct(jabatan) from pegawai";
        $jabatan = $this->db->query($sql);
        $jabatan = $jabatan->result_array();
        $all['jabatan'] = $jabatan;
        $all['jumlah'] = $array;

        if ($jabatan) {
            return $all;
        }else{
            return false;
        }   
    }
} ?>
