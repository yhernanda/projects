<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class update_model extends CI_Model{

    function __construct()
    {
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }

    public function convert_pelamar($array=""){
        if($array['np'] != ""){
            $sql = "INSERT INTO `pegawai` (`nama`,`tgl_lahir`,`jns_kelamin`,`alamat_domisili`,`alamat_asal`,`telp_rmh`,`telp_hp`,`agama`,`status_perkawinan`,`tgl_nikah`,`no_ktp`,`tinggi_bdn`,`berat_bdn`,`gol_darah`,`npwp`,`no_jamsostek`,`hobby`,`buku_bacaan`,`anak_no`) SELECT `nama`,`tgl_lahir`,`jns_kelamin`,`alamat_domisili`,`alamat_asal`,`telp_rmh`,`telp_hp`,`agama`,`status_perkawinan`,`tgl_nikah`,`no_ktp`,`tinggi_bdn`,`berat_bdn`,`gol_darah`,`npwp`,`no_jamsostek`,`hobby`,`buku_bacaan`,`anak_no` FROM `pelamar` WHERE `pelamar`.`np`='".$array['np']."'";
            $result = $this->db->query($sql);
            if($result){
                $sql = "UPDATE `pegawai` SET `nip`='".$array['nip']."',`tgl_masuk`='".$array['tgl_masuk']."',`golongan`='".$array['golongan']."',`jabatan`='".$array['jabatan']."',`status_pegawai`='".$array['status_pegawai']."',`pujian_rekomendasi`='".$array['pujian_rekomendasi']."',`insiden_kritis`='".$array['insiden_kritis']."' WHERE `pegawai`.`nip` = ''";
                $result = $this->db->query($sql);
            } else {
                return FALSE;
            }
            
            $darurat = $this->db->query("SELECT * FROM `data_darurat_pelamar`  WHERE `np`='".$array['np']."'")->result_array();            
            $keluarga = $this->db->query("SELECT * FROM `keluarga_pelamar`  WHERE `np`='".$array['np']."'")->result_array();
            $k_managerial = $this->db->query("SELECT * FROM `kompetensi_managerial_pelamar` WHERE `np`='".$array['np']."'")->result_array();
            $k_teknis = $this->db->query("SELECT * FROM `kompetensi_teknis_pelamar` WHERE `np`='".$array['np']."'")->result_array();
            $ortu = $this->db->query("SELECT * FROM `orang_tua_pelamar` WHERE `np`='".$array['np']."'")->result_array();
            $pendidikan = $this->db->query("SELECT * FROM `riwayat_pendidikan_pelamar` WHERE `np`='".$array['np']."'")->result_array();

            if(isset($darurat)){
                foreach($darurat as $arr){
                    $arr['nip'] = $array['nip'];
                    unset($arr['np']);
                    unset($arr['id_data_darurat_pelamar']);
                    $result = $this->db->insert('data_darurat', $arr);
                }
            }
            if(isset($keluarga)){
                foreach($keluarga as $arr){
                    $arr['nip'] = $array['nip'];
                    unset($arr['np']);
                    unset($arr['id_keluarga_pelamar']);
                    $result = $this->db->insert('keluarga', $arr);
                }
            }
            if(isset($k_managerial)){
                foreach($k_managerial as $arr){
                    $arr['nip'] = $array['nip'];
                    unset($arr['np']);
                    unset($arr['id_managerial_pelamar']);
                    $result = $this->db->insert('kompetensi_managerial', $arr);
                }
            }
            if(isset($k_teknis)){
                foreach($k_teknis as $arr){
                    $arr['nip'] = $array['nip'];
                    unset($arr['np']);
                    unset($arr['id_teknis_pelamar']);
                    $result = $this->db->insert('kompetensi_teknis', $arr);
                }
            }
            if(isset($ortu)){
                foreach($ortu as $arr){
                    $arr['nip'] = $array['nip'];
                    unset($arr['np']);
                    unset($arr['id_orang_tua_pelamar']);
                    $result = $this->db->insert('orang_tua', $arr);
                }
            }
            if(isset($pendidikan)){
                foreach($pendidikan as $arr){
                    $arr['nip'] = $array['nip'];
                    unset($arr['np']);
                    unset($arr['id_pendidikan_pelamar']);
                    $result = $this->db->insert('riwayat_pendidikan', $arr);
                }
            }

            if ($result) {
                return true;
            }else{
                return false;
            }
            /*$array['pujian_rekomendasi'];
            $result[0]['insiden_kritis'] = $array['insiden_kritis'];
            $result = $result[0];
            $result2 = $this->db->insert('pegawai', $result);

            $sql = "SELECT foto from pelamar where np = '$np'";
            $res = $this->db->query($sql);
            $foto = $res->result_array();
            $foto = $foto[0]['foto'];
            $sql = "DELETE from pelamar where np = '$np'";
            $res = $this->db->query($sql);
            if ($res) {
                unlink($foto);
                return true;

            }else{
                return false;
            }
            if ($result) {
                return true;
            }else{
                return false;
            }*/
        }
    }

    public function update_gaji($array="", $oldnip=""){
        $sql = "UPDATE `gaji` SET `gaji_pokok`=".$array['gaji_pokok'].",`tunj_jabatan`=".$array['tunj_jabatan'].",`tunj_kehadiran`=".$array['tunj_kehadiran'].",`tunj_kemahalan`=".$array['tunj_kemahalan']." WHERE nip = ".$oldnip;
        $result = $this->db->query($sql, $array);
        if ($result) {
            return true;
        }else{
            return false;
        }   
    }

    public function update_kompetensi_teknis($array="", $value=""){
        if($value != ""){
            $sql = "UPDATE `kompetensi_teknis` SET `nama_kompetensi`='".$array['nama_kompetensi']."',`penguasaan`='".$array['penguasaan']."' WHERE id_teknis = ".$value;
            $result = $this->db->query($sql, $array);
        }else{
            $result = $this->db->insert('kompetensi_teknis', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_kompetensi_teknis_pelamar($array="", $value=""){
        if($value != ""){
            $sql = "UPDATE `kompetensi_teknis_pelamar` SET `nama_kompetensi`='".$array['nama_kompetensi']."',`penguasaan`='".$array['penguasaan']."' WHERE id_teknis_pelamar = ".$value;
            $result = $this->db->query($sql, $array);
        }else{
            $result = $this->db->insert('kompetensi_teknis_pelamar', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_kompetensi_managerial($array="", $value=""){
        if($value != ""){
            $sql = "UPDATE `kompetensi_managerial` SET `nama_kompetensi`='".$array['nama_kompetensi']."',`penguasaan`='".$array['penguasaan']."' WHERE id_managerial = ".$value;
            $result = $this->db->query($sql, $array);
        }else{
            $result = $this->db->insert('kompetensi_managerial', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_kompetensi_managerial_pelamar($array="", $value=""){
        if($value != ""){
            $sql = "UPDATE `kompetensi_managerial_pelamar` SET `nama_kompetensi`='".$array['nama_kompetensi']."',`penguasaan`='".$array['penguasaan']."' WHERE id_managerial_pelamar = ".$value;
            $result = $this->db->query($sql, $array);
        }else{
            $result = $this->db->insert('kompetensi_managerial_pelamar', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_pegawai($array, $imgdata, $oldnip){
        if($imgdata != "empty"){
            //echo "masuk"; die;

            $imgdata = $imgdata['full_path'];
            $array['foto'] = substr($imgdata, 33);
        }else{
            $array['foto'] = NULL;
        }

        $tgl_nikah = '`tgl_nikah`=NULL';
        $tgl_berhenti = '`tgl_berhenti`=NULL';

        if($array['tgl_nikah'] != null && $array['tgl_nikah'] != "")
            $tgl_nikah = "`tgl_nikah`='".$array['tgl_nikah']."'";
        if($array['tgl_berhenti'] != null && $array['tgl_berhenti'] != "")
            $tgl_berhenti = "`tgl_berhenti`='".$array['tgl_berhenti']."'";

        $sql = "UPDATE `pegawai` SET `nip`='".$array['nip']."',`nama`='".$array['nama']."',`tgl_lahir`='".$array['tgl_lahir']."',`jns_kelamin`='".$array['jns_kelamin']."',`alamat_domisili`='".$array['alamat_domisili']."',`alamat_asal`='".$array['alamat_asal']."',`telp_rmh`='".$array['telp_rmh']."',`telp_hp`='".$array['telp_hp']."',`agama`='".$array['agama']."',`status_perkawinan`='".$array['status_perkawinan']."',".$tgl_nikah.",`no_ktp`='".$array['no_ktp']."',`tinggi_bdn`=".$array['tinggi_bdn'].",`berat_bdn`=".$array['berat_bdn'].",`gol_darah`='".$array['gol_darah']."',`npwp`='".$array['npwp']."',`no_jamsostek`='".$array['no_jamsostek']."',`tgl_masuk`='".$array['tgl_masuk']."',".$tgl_berhenti.",`hobby`='".$array['hobby']."',`buku_bacaan`='".$array['buku_bacaan']."',`golongan`='".$array['golongan']."',`jabatan`='".$array['jabatan']."',`status_pegawai`='".$array['status_pegawai']."',`pujian_rekomendasi`='".$array['pujian_rekomendasi']."',`insiden_kritis`='".$array['insiden_kritis']."',`foto`='".$array['foto']."' WHERE nip = '".$oldnip."'";
       // echo $sql; die;

        $result = $this->db->query($sql, $array);
        if ($result) {
            return true;
        }else{
            return false;
        }   
    }

    public function update_pelamar($array, $imgdata, $oldnp){
        if($imgdata != "empty"){
            //echo "masuk"; die;
            $imgdata = $imgdata['full_path'];
            $array['foto'] = substr($imgdata, 33);
        }else{
            $array['foto'] = NULL;
        }
        
        $tgl_nikah = '`tgl_nikah`=NULL';

        if($array['tgl_nikah'] != null && $array['tgl_nikah'] != "")
            $tgl_nikah = "`tgl_nikah`='".$array['tgl_nikah']."'";

        $sql = "UPDATE `pelamar` SET `np`='".$array['np']."',`nama`='".$array['nama']."',`tgl_lahir`='".$array['tgl_lahir']."',`jns_kelamin`='".$array['jns_kelamin']."',`alamat_domisili`='".$array['alamat_domisili']."',`alamat_asal`='".$array['alamat_asal']."',`telp_rmh`='".$array['telp_rmh']."',`telp_hp`='".$array['telp_hp']."',`agama`='".$array['agama']."',`status_perkawinan`='".$array['status_perkawinan']."',".$tgl_nikah.",`no_ktp`='".$array['no_ktp']."',`tinggi_bdn`=".$array['tinggi_bdn'].",`berat_bdn`=".$array['berat_bdn'].",`gol_darah`='".$array['gol_darah']."',`npwp`='".$array['npwp']."',`no_jamsostek`='".$array['no_jamsostek']."',`tgl_daftar`='".$array['tgl_daftar']."',`hobby`='".$array['hobby']."',`buku_bacaan`='".$array['buku_bacaan']."',`foto`='".$array['foto']."' WHERE np = '".$oldnp."'";
        $result = $this->db->query($sql, $array);
        if ($result) {
            return true;
        }else{
            return false;
        }   
    }

    public function update_riwayat_pendidikan($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `riwayat_pendidikan` SET `pendidikan`='".$array['pendidikan']."',`nama_sekolah`='".$array['nama_sekolah']."',`jurusan`='".$array['jurusan']."',`kota`='".$array['kota']."',`thn_lulus`=".$array['thn_lulus']." WHERE id_pendidikan = ".$id;
            $result = $this->db->query($sql, $array);
        }else{
            $result = $this->db->insert('riwayat_pendidikan', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_riwayat_pendidikan_pelamar($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `riwayat_pendidikan_pelamar` SET `pendidikan`='".$array['pendidikan']."',`nama_sekolah`='".$array['nama_sekolah']."',`jurusan`='".$array['jurusan']."',`kota`='".$array['kota']."',`thn_lulus`=".$array['thn_lulus']." WHERE id_pendidikan_pelamar = ".$id;
            $result = $this->db->query($sql, $array);
        }else{
            $result = $this->db->insert('riwayat_pendidikan_pelamar', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_nomor_anak_pegawai($nip, $value){
        $sql = "UPDATE `pegawai` SET `anak_no`= ".$value." WHERE nip = ".$nip;
        $result = $this->db->query($sql);
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_nomor_anak_pelamar($np, $value){
        $sql = "UPDATE `pelamar` SET `anak_no`= ".$value." WHERE np = ".$np;
        $result = $this->db->query($sql);
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_ortu_pegawai($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `orang_tua` SET `status`='".$array['status']."',`orang_tua`='".$array['orang_tua']."',`nama`='".$array['nama']."',`tgl_lahir`='".$array['tgl_lahir']."',`pendidikan`='".$array['pendidikan']."',`ket_pekerjaan`='".$array['ket_pekerjaan']."',`alamat`='".$array['alamat']."',`no_telp`='".$array['no_telp']."' WHERE id_orang_tua = ".$id;
            $result = $this->db->query($sql, $array);    
        }else{
            $result = $this->db->insert('orang_tua', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_ortu_pelamar($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `orang_tua_pelamar` SET `status`='".$array['status']."',`orang_tua`='".$array['orang_tua']."',`nama`='".$array['nama']."',`tgl_lahir`='".$array['tgl_lahir']."',`pendidikan`='".$array['pendidikan']."',`ket_pekerjaan`='".$array['ket_pekerjaan']."',`alamat`='".$array['alamat']."',`no_telp`='".$array['no_telp']."' WHERE id_orang_tua_pelamar = ".$id;
            $result = $this->db->query($sql, $array);    
        }else{
            $result = $this->db->insert('orang_tua_pelamar', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_kel_pegawai($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `keluarga` SET `status`='".$array['status']."',`nama`='".$array['nama']."',`tgl_lahir`='".$array['tgl_lahir']."',`pendidikan`='".$array['pendidikan']."',`ket_pekerjaan`='".$array['ket_pekerjaan']."' WHERE id_keluarga = ".$id;
            $result = $this->db->query($sql, $array);    
        }else{
            $result = $this->db->insert("keluarga", $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_kel_pelamar($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `keluarga_pelamar` SET `status`='".$array['status']."',`nama`='".$array['nama']."',`tgl_lahir`='".$array['tgl_lahir']."',`pendidikan`='".$array['pendidikan']."',`ket_pekerjaan`='".$array['ket_pekerjaan']."' WHERE id_keluarga_pelamar = ".$id;
            $result = $this->db->query($sql, $array);    
        }else{
            $result = $this->db->insert("keluarga_pelamar", $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_darurat_pegawai($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `data_darurat` SET `nama`='".$array['nama']."',`alamat`='".$array['alamat']."',`no_telp`='".$array['no_telp']."' WHERE id_data_darurat = ".$id;
            $result = $this->db->query($sql, $array);    
        }else{
            $result = $this->db->insert('data_darurat', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function update_darurat_pelamar($array="", $id=""){
        if($id != ""){
            $sql = "UPDATE `data_darurat_pelamar` SET `nama`='".$array['nama']."',`alamat`='".$array['alamat']."',`no_telp`='".$array['no_telp']."' WHERE id_data_darurat_pelamar = ".$id;
            $result = $this->db->query($sql, $array);    
        }else{
            $result = $this->db->insert('data_darurat_pelamar', $array);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_riwayat_pendidikan($id = ""){
        $sql = "DELETE from riwayat_pendidikan where id_pendidikan = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_riwayat_pendidikan_pelamar($id = ""){
        $sql = "DELETE from riwayat_pendidikan_pelamar where id_pendidikan_pelamar = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_kompetensi_teknis($id = ""){
        $sql = "DELETE from kompetensi_teknis where id_teknis = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_kompetensi_teknis_pelamar($id = ""){
        $sql = "DELETE from kompetensi_teknis_pelamar where id_teknis_pelamar = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_kompetensi_managerial($id = ""){
        $sql = "DELETE from kompetensi_managerial where id_managerial = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_kompetensi_managerial_pelamar($id = ""){
        $sql = "DELETE from kompetensi_managerial_pelamar where id_managerial_pelamar = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_keluarga($id = ""){
        $sql = "DELETE from keluarga where id_keluarga = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }   
    }

    public function hapus_keluarga_pelamar($id = ""){
        $sql = "DELETE from keluarga_pelamar where id_keluarga_pelamar = '$id'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }   
    }
}
?>