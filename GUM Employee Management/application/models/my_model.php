<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class My_model extends CI_Model{

    function __construct()
    {
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }

    public function search_pegawai($value='')
    {
        $sql = "SELECT * from pegawai where nip LIKE '%$value%' OR nama LIKE '%$value%'";
        $result = $this->db->query($sql);
        
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function search_pelamar($value='')
    {
        $sql = "SELECT * from pelamar where np LIKE '%$value%' OR nama LIKE '%$value%'";
        $result = $this->db->query($sql);
        
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }
    }
}
?>