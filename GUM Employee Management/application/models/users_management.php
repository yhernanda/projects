<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class users_management extends CI_Model{

    function __construct()
    {
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }

    public function login_validation($value)
    {
        $sql = "SELECT * from user where username = ? and password = ?";
        $res = $this->db->query($sql, $value);
        if ($res->num_rows() > 0) {
        	$sql = "UPDATE `user` SET `login_terakhir`=CURRENT_TIMESTAMP WHERE username = ?";
        	$this->db->query($sql, $value);
            return $res->row_array();
        }else{
            return false;
        }
    }

    public function get_all_users()
    {
        $sql = "SELECT * from user";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->result_array();
        }else{
            return false;
        }
    }

    public function get_user($name = "", $check = ""){
        $sql = "SELECT * from user where username = '".$name."'";
        $res = $this->db->query($sql);
        $rtr = Array("value" => "", "id" => "");
        if ($res->num_rows() > 0) {
        	$rtr['value'] = "exist";
        	if($check == "edit"){
                //Ambil user ID Dari user yg didapat
        		$res = $res->result_array();
            	$rtr['value'] = $res[0]['user_id'];
            }
            return $rtr;
        }else{
            $rtr['value'] = "no";
            if($check == "new"){
                $sql = "SHOW TABLE STATUS LIKE 'user'";
                $res = $this->db->query($sql);
                $id = $res->result_array();
                $rtr['id'] = $id[0]['Auto_increment'];    
            }
            return $rtr;
        }   
    }

    public function get_user_detail($id = ""){
        $sql = "SELECT password, role from user where user_id = ".$id;
        $res =  $this->db->query($sql);
        $res = $res->result_array();
        return $res;
    }

    public function regisuser($value='', $imgdata="")
    {
        if($imgdata != "empty"){
            $imgdata = $imgdata['full_path'];// get the path of the image
            $value["foto"] = substr($imgdata, 33);
        }else{
            $value['foto'] = NULL;
        } 
        $result = $this->db->insert('user', $value);
        if ($result) {
            $id = $this->db->insert_id();
            $d = "SELECT * from user where user_id = '$id'";
            $res = $this->db->query($d);
            if ($res) {
                return $res->result_array();
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function edituser($value='', $imgdata="")
    {
        if($imgdata != "empty"){
            $imgdata = $imgdata['full_path'];// get the path of the image
            $result = $this->db->query("SELECT foto from user where user_id = ".$value['user_id']);
            $result = $result->result_array();
            $result = $result[0]['foto'];
            if($result != "")
                unlink($result);

            if($value['password'] != "") 
	        	$result = $this->db->query('UPDATE `user` SET `username`="'.$value['username'].'",`full_name`="'.$value['full_name'].'",`password`="'.$value['password'].'",`role`="'.$value['role'].'",`foto`="'.substr($imgdata, 33).'" WHERE `user_id`='.$value['user_id']);
	        else
	        	$result = $this->db->query('UPDATE `user` SET `username`="'.$value['username'].'",`full_name`="'.$value['full_name'].'",`role`="'.$value['role'].'",`foto`="'.substr($imgdata, 33).'" WHERE `user_id`='.$value['user_id']);
        }else{
            if($value['password'] != "") 
	        	$result = $this->db->query('UPDATE `user` SET `username`="'.$value['username'].'",`full_name`="'.$value['full_name'].'",`password`="'.$value['password'].'",`role`="'.$value['role'].'" WHERE `user_id`='.$value['user_id']);
	        else
	        	$result = $this->db->query('UPDATE `user` SET `username`="'.$value['username'].'",`full_name`="'.$value['full_name'].'",`role`="'.$value['role'].'" WHERE `user_id`='.$value['user_id']);
        }
        if ($result) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_user($value='')
    {
        $sql = "SELECT foto from user where user_id = $value";
        $res = $this->db->query($sql);
        $foto = $res->result_array();
        $foto = $foto[0]['foto'];
        $sql = "DELETE from user where user_id = '$value'";
        $res = $this->db->query($sql);
        if ($res) {
            if($foto != "")
                unlink($foto);
            return true;
        }else{
            return false;
        }
    }

} ?>