<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class payroll_management extends CI_Model{

    function __construct()
    {
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }

    function get_data_pegawai(){
    	$result = $this->db->query("SELECT pegawai.nip, pegawai.nama, pegawai.golongan, pegawai.foto, gaji.gaji_pokok, gaji.tunj_jabatan, gaji.tunj_kehadiran, gaji.tunj_kemahalan, gaji.total_hari, gaji.jamsostek_dplk, gaji.pph_ps, gaji.askes, gaji.pinjaman_bunga, gaji.bpjs, gaji.jamsostek_dplk_pengurangan, gaji.jamsostek, gaji.dplk  from pegawai inner join gaji where pegawai.nip = gaji.nip");
    	$result = $result->result_array();
    	return $result;
    }

    function get_export_pegawai(){
        $result = $this->db->query("SELECT pegawai.nip, pegawai.nama, pegawai.golongan, gaji.gaji_pokok, gaji.tunj_jabatan, gaji.tunj_kehadiran, gaji.tunj_kemahalan, gaji.total_hari, gaji.jamsostek_dplk, gaji.pph_ps, gaji.askes, gaji.pinjaman_bunga, gaji.bpjs, gaji.jamsostek_dplk_pengurangan, gaji.jamsostek, gaji.dplk from pegawai inner join gaji where pegawai.nip = gaji.nip");
        $result = $result->result_array();
        return $result;
    }

    function set_gaji_pokok($nilai, $nip){
    	$result = $this->db->query("UPDATE `gaji` SET `gaji_pokok`=".$nilai." WHERE `nip`=".$nip);
    	return $result;
    }

    function set_tunj_jabatan($nilai, $nip){
    	$result = $this->db->query("UPDATE `gaji` SET `tunj_jabatan`=".$nilai." WHERE `nip`=".$nip);
    	return $result;
    }

    function set_tunj_kemahalan($nilai, $nip){
    	$result = $this->db->query("UPDATE `gaji` SET `tunj_kemahalan`=".$nilai." WHERE `nip`=".$nip);
    	echo $nilai; die;
    	return $result;
    }

    function set_tunj_kehadiran($nilai, $nip){
    	$result = $this->db->query("UPDATE `gaji` SET `tunj_kehadiran`=".$nilai." WHERE `nip`=".$nip);
    	return $result;
    }
    
    function set_detail_gaji($detail, $nilai, $nip){
        $result = $this->db->query("UPDATE `gaji` SET `".$detail."`=".$nilai." WHERE `nip`=".$nip);
        return $result;
    }
} ?>