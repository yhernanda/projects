<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class input_model extends CI_Model{

    function __construct()
    {
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }   

	public function insert_pegawai($array, $imgdata){
        if($imgdata != "empty"){
            $imgdata = $imgdata['full_path'];// get the path of the image
            $array["foto"] = substr($imgdata, 33);//path default from controller
        }else{
            $array['foto'] = NULL;
        }
        $res = $this->db->insert('pegawai', $array);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_pelamar($array, $imgdata){
        if($imgdata != "empty"){
            $imgdata = $imgdata['full_path'];// get the path of the image
            $array["foto"] = substr($imgdata, 33);
        }else{
            $array['foto'] = NULL;
        }
        $res = $this->db->insert('pelamar', $array);
        if ($res) {
            return true;
        }else{
            return false;
        }   
    }

    public function insert_gaji($array=""){
        $result = $this->db->insert('gaji', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_kompetensi_teknis($array){
        $result = $this->db->insert('kompetensi_teknis', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_kompetensi_teknis_pelamar($array){
        $result = $this->db->insert('kompetensi_teknis_pelamar', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_kompetensi_managerial($array){
        $result = $this->db->insert('kompetensi_managerial', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_kompetensi_managerial_pelamar($array){
        $result = $this->db->insert('kompetensi_managerial_pelamar', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_riwayat_pendidikan($array){
        $result = $this->db->insert('riwayat_pendidikan', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_riwayat_pendidikan_pelamar($array){
        $result = $this->db->insert('riwayat_pendidikan_pelamar', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_ortu_pegawai($array){
        $result = $this->db->insert('orang_tua', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_ortu_pelamar($array){
        $result = $this->db->insert('orang_tua_pelamar', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_kel_pegawai($array){
        $result = $this->db->insert('keluarga', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_kel_pelamar($array){
        $result = $this->db->insert('keluarga_pelamar', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_darurat_pegawai($array){
        $result = $this->db->insert('data_darurat', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }

    public function insert_darurat_pelamar($array){
        $result = $this->db->insert('data_darurat_pelamar', $array);
        if($result){
            return true;
        }else{
            return false;
        }
    }
}
?>