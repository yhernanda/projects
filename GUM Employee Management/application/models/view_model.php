<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class view_model extends CI_Model{
	function __construct()
    {
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }

    public function get_pegawai_pelamar($jenis = ""){
        $sql = "SELECT * from ".$jenis;
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_detail_pegawai($nip=''){
        $sql = "SELECT * from pegawai WHERE nip = '$nip'";
        $result = $this->db->query($sql);
        $sql = "SELECT `agama`+0 as `agama` from pegawai WHERE nip = '$nip'";
        $result2 = $this->db->query($sql);
        if ($result) {
            $result = $result->result_array();
            $result2 = $result2->result_array();
            $result[0]['agama'] = $result2[0]['agama'];
            return $result;
        }else{
            return false;
        }   
    }



    //EDIT & DETAIL PEGAWAI & PELAMAR
    public function get_detail_pelamar($np=''){
        $sql = "SELECT * from pelamar WHERE np = '$np'";
        $result = $this->db->query($sql);
        $sql = "SELECT `agama`+0 as `agama` from pelamar WHERE np = '$np'";
        $result2 = $this->db->query($sql);
        if ($result) {
            $result = $result->result_array();
            $result2 = $result2->result_array();
            $result[0]['agama'] = $result2[0]['agama'];
            return $result;
        }else{
            return false;
        }   
    }

    public function get_detail_teknis($nip=''){
        $sql = "SELECT * from kompetensi_teknis WHERE nip = '$nip'";
        $result = $this->db->query($sql);

        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_detail_teknis_pelamar($np=''){
        $sql = "SELECT * from kompetensi_teknis_pelamar WHERE np = '$np'";
        $result = $this->db->query($sql);

        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_detail_managerial($nip=''){
        $sql = "SELECT * from kompetensi_managerial WHERE nip = '$nip'";
        $result = $this->db->query($sql);

        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_detail_managerial_pelamar($np=''){
        $sql = "SELECT * from kompetensi_managerial_pelamar WHERE np = '$np'";
        $result = $this->db->query($sql);

        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_riwayat_pendidikan($nip=''){
        $sql = "SELECT * from riwayat_pendidikan WHERE nip = '$nip'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_riwayat_pendidikan_pelamar($np=''){
        $sql = "SELECT * from riwayat_pendidikan_pelamar WHERE np = '$np'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_orang_tua($nip=''){
        $sql = "SELECT * from orang_tua WHERE nip = '$nip'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_orang_tua_pelamar($np=''){
        $sql = "SELECT * from orang_tua_pelamar WHERE np = '$np'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_keluarga($nip=''){
        $sql = "SELECT * from keluarga WHERE nip = '$nip'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_keluarga_pelamar($np=''){
        $sql = "SELECT * from keluarga_pelamar WHERE np = '$np'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_darurat($nip=''){
        $sql = "SELECT * from data_darurat WHERE nip = '$nip'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_darurat_pelamar($np=''){
        $sql = "SELECT * from data_darurat_pelamar WHERE np = '$np'";
        $result = $this->db->query($sql);

        if($result){
            return $result->result_array();
        }else{
            return false;
        }   
    }




    //EXPORT ALL FROM VIEW
    public function get_data_darurat($jenis = ""){
        if($jenis == "pegawai")
            $sql = "SELECT * from data_darurat";
        else
            $sql = "SELECT * from data_darurat_pelamar";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_kel($jenis = ""){
        if($jenis == "pegawai")
            $sql = "SELECT * from keluarga";
        else
            $sql = "SELECT * from keluarga_pelamar";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_managerial($jenis = ""){
        if($jenis == "pegawai")
            $sql = "SELECT * from kompetensi_managerial";
        else
            $sql = "SELECT * from kompetensi_managerial_pelamar";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_teknis($jenis = ""){
        if($jenis == "pegawai")
            $sql = "SELECT * from kompetensi_teknis";
        else
            $sql = "SELECT * from kompetensi_teknis_pelamar";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_ortu($jenis = ""){
        if($jenis == "pegawai")
            $sql = "SELECT * from orang_tua";
        else
            $sql = "SELECT * from orang_tua_pelamar";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_pendidikan($jenis = ""){
        if($jenis == "pegawai")
            $sql = "SELECT * from riwayat_pendidikan";
        else
            $sql = "SELECT * from riwayat_pendidikan_pelamar";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_gaji(){
        $sql = "SELECT * from gaji";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }

    public function get_detail_gaji($nip=''){
        $sql = "SELECT * from gaji WHERE nip = '$nip'";
        $result = $this->db->query($sql);

        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }   
    }




    //HAPUS PEGAWAI & PELAMAR
    public function hapus_pegawai($nip='')
    {
        $sql = "SELECT foto from pegawai where nip = '$nip'";
        $res = $this->db->query($sql);
        $foto = $res->result_array();
        $foto = $foto[0]['foto'];
        $sql = "DELETE from pegawai where nip = '$nip'";
        $res = $this->db->query($sql);
        if ($res) {
            if($foto != "")
                unlink($foto);
            return true;
        }else{
            return false;
        }
    }

    public function hapus_pelamar($np='')
    {
        $sql = "SELECT foto from pelamar where np = '$np'";
        $res = $this->db->query($sql);
        $foto = $res->result_array();
        $foto = $foto[0]['foto'];
        $sql = "DELETE from pelamar where np = '$np'";
        $res = $this->db->query($sql);
        if ($res) {
            if($foto != "")
                unlink($foto);
            return true;
        }else{
            return false;
        }
    }
}
?>