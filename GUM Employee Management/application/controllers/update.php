<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class update extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('update_model');
		$this->load->model('view_model');
		//$this->load->view('javascript');		
	}

	public function convert_pelamar(){
		if($this->session->userdata('user_input') == ""){
			redirect('welcome');
		}

		$array = array(	
				'np' => $this->input->post('np'),			
				'tgl_masuk' => date('Y-m-d', strtotime($this->input->post('tgl_masuk'))),
				'nip' => $this->input->post('nip'),
				'golongan' => $this->input->post('golongan'),
				'jabatan' => $this->input->post('jabatan'),
				'status_pegawai' => $this->input->post('status_pegawai'),
				'pujian_rekomendasi' => $this->input->post('pujian_rekomendasi'),
				'insiden_kritis' => $this->input->post('insiden_kritis')
		);

		$query = $this->update_model->convert_pelamar($array);
		if($query){
			$this->view_model->hapus_pelamar($this->input->post('np'));
			$data['pegawai'] = $this->view_model->get_pegawai_pelamar("pegawai");
			$this->load->view('viewdata', $data);
		}else{
			return FALSE; 
		}
	}

	public function update_pegawai(){
		if($this->session->userdata('user_input') == ""){
			redirect('welcome');
		}

		//print_r($_POST); die;

		//Proses GAJI//
		/*
		$array = array(
				'nip' => $this->input->post('nip'),
				'gaji_pokok' => $this->input->post('gaji_pokok'),
				'tunj_jabatan' => $this->input->post('tunj_jabatan'),
				'tunj_kehadiran' => $this->input->post('tunj_kehadiran'),
				'tunj_kemahalan' => $this->input->post('tunj_kemahalan')
		);

		if(!($this->update_model->update_gaji($array, $this->input->post('nip-old')))){
			die; echo "error updating data!";
		}*/

		//Proses Kompetensi//
		if(isset($_POST['teknis'])){
			foreach($_POST['teknis'] as $kompetensi){
				$id = "";
				if(isset($kompetensi['id']))
					$id = $kompetensi['id'];
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'nip' => $this->input->post('nip'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->update_model->update_kompetensi_teknis($array, $id))){
					die; echo "error updating data!";
				}
			}
		}

		if(isset($_POST['managerial'])){
			foreach($_POST['managerial'] as $kompetensi){
				$id = "";
				if(isset($kompetensi['id']))
					$id = $kompetensi['id'];
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'nip' => $this->input->post('nip'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->update_model->update_kompetensi_managerial($array, $id))){
					die; echo "error updating data!";
				}
			}
		}
		

		//Proses PEGAWAI//
        $tgl_berhenti = null;
		$tgl_nikah = null;

		if($this->input->post('tgl_berhenti') != "" || $this->input->post('tgl_berhenti') != null)
			$tgl_berhenti = date('Y-m-d', strtotime($this->input->post('tgl_berhenti')));
		if($this->input->post('tgl_nikah') != "" || $this->input->post('tgl_nikah') != null)
			$tgl_nikah = date('Y-m-d', strtotime($this->input->post('tgl_nikah')));

		//echo $this->input->post('tgl_nikah')." aaaaa ".$tgl_nikah; die;
        $array = array(
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'tgl_lahir' => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
				'jns_kelamin' => $this->input->post('jns_kelamin'),
				'alamat_domisili' => $this->input->post('alamat_domisili'),
				'alamat_asal' => $this->input->post('alamat_asal'),
				'telp_rmh' => $this->input->post('telp_rmh'),
				'telp_hp' => $this->input->post('telp_hp'),
				'agama' => $this->input->post('agama'),
				'status_perkawinan' => $this->input->post('status_perkawinan'),
				'tgl_nikah' => $tgl_nikah,
				'no_ktp' => $this->input->post('no_ktp'),
				'tinggi_bdn' => $this->input->post('tinggi_bdn'),
				'berat_bdn' => $this->input->post('berat_bdn'),
				'gol_darah' => $this->input->post('gol_darah'),
				'npwp' => $this->input->post('npwp'),
				'no_jamsostek' => $this->input->post('no_jamsostek'),
				'tgl_masuk' => date('Y-m-d', strtotime($this->input->post('tgl_masuk'))),
				'tgl_berhenti' => $tgl_berhenti,
				'hobby' => $this->input->post('hobby'),
				'buku_bacaan' => $this->input->post('buku_bacaan'),
				'golongan' => $this->input->post('golongan'),
				'jabatan' => $this->input->post('jabatan'),
				'status_pegawai' => $this->input->post('status_pegawai'),
				'pujian_rekomendasi' => $this->input->post('pujian_rekomendasi'),
				'insiden_kritis' => $this->input->post('insiden_kritis')
		);

		if($_FILES['picture']['name'] != ""){
			$config['upload_path'] = 'C:\xampp\htdocs\database_pegawai\assets\imageassets\uploads\pegawai';
	        $config['allowed_types'] = 'gif|jpg|png';
	        $config['file_name'] = $this->input->post('nip');
	        $config['max_size']  = '1000';
	        $config['max_width'] = '3000';
	        $config['max_height'] = '4000';
	        $config['overwrite'] = TRUE;
	        $this->load->library('upload', $config);
	       	if (! $this->upload->do_upload('picture'))
			{
				echo "Image too Large!"; die;
			}

			$query = $this->update_model->update_pegawai($array, $this->upload->data(), $this->input->post('nip-old'));
		}else{
			$query = $this->update_model->update_pegawai($array, "empty", $this->input->post('nip-old'));
		}

		if ($query) {

		}else{
			echo "Error Inserting Data!"; die;
		}
	}

	public function update_pelamar(){
		if($this->session->userdata('user_input') == ""){
			redirect('welcome');
		}

		//print_r($_POST); die;

		//Proses Kompetensi//
		if(isset($_POST['teknis'])){
			foreach($_POST['teknis'] as $kompetensi){
				$id = "";
				if(isset($kompetensi['id']))
					$id = $kompetensi['id'];
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'np' => $this->input->post('np'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->update_model->update_kompetensi_teknis_pelamar($array, $id))){
					die; echo "error updating data!";
				}
			}
		}

		if(isset($_POST['managerial'])){
			foreach($_POST['managerial'] as $kompetensi){
				$id = "";
				if(isset($kompetensi['id']))
					$id = $kompetensi['id'];
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'np' => $this->input->post('np'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->update_model->update_kompetensi_managerial_pelamar($array, $id))){
					die; echo "error updating data!";
				}
			}
		}
		

		//Proses PELAMAR//
		$tgl_nikah = null;
		if($this->input->post('tgl_nikah') != "")
			$tgl_nikah = date('Y-m-d', strtotime($this->input->post('tgl_nikah')));
        $array = array(
				'np' => $this->input->post('np'),
				'nama' => $this->input->post('nama'),
				'tgl_lahir' => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
				'jns_kelamin' => $this->input->post('jns_kelamin'),
				'alamat_domisili' => $this->input->post('alamat_domisili'),
				'alamat_asal' => $this->input->post('alamat_asal'),
				'telp_rmh' => $this->input->post('telp_rmh'),
				'telp_hp' => $this->input->post('telp_hp'),
				'agama' => $this->input->post('agama'),
				'status_perkawinan' => $this->input->post('status_perkawinan'),
				'tgl_nikah' => $tgl_nikah,
				'no_ktp' => $this->input->post('no_ktp'),
				'tinggi_bdn' => $this->input->post('tinggi_bdn'),
				'berat_bdn' => $this->input->post('berat_bdn'),
				'gol_darah' => $this->input->post('gol_darah'),
				'npwp' => $this->input->post('npwp'),
				'no_jamsostek' => $this->input->post('no_jamsostek'),
				'tgl_daftar' => date('Y-m-d', strtotime($this->input->post('tgl_daftar'))),
				'hobby' => $this->input->post('hobby'),
				'buku_bacaan' => $this->input->post('buku_bacaan')
		);

		if($_FILES['picture']['name'] != ""){
			$config['upload_path'] = 'C:\xampp\htdocs\database_pegawai\assets\imageassets\uploads\pelamar';
	        $config['file_name'] = $this->input->post('np');
	        $config['allowed_types'] = 'gif|jpg|png';
	        $config['max_size']  = '1000';
	        $config['max_width'] = '3000';
	        $config['max_height'] = '4000';
	        $config['overwrite'] = TRUE;
	        $this->load->library('upload', $config);
	       	if (! $this->upload->do_upload('picture'))
			{
				echo "Image too Large!";	die;
			}

			$query = $this->update_model->update_pelamar($array, $this->upload->data(), $this->input->post('np-old'));
		}else{
			$query = $this->update_model->update_pelamar($array, "empty", $this->input->post('np-old'));
		}

		if ($query) {
			$this->edit_pelamar($this->input->post('np'));	
		}else{
			echo "Error Inserting Data!"; die;
		}
	}

	public function exportdata_detail_pegawai($nip = ""){
		$query['data'] = $this->view_model->get_detail_pegawai($nip);
		$query['gaji'] = $this->view_model->get_detail_gaji($nip);
		$query['teknis'] = $this->view_model->get_detail_teknis($nip);
		$query['managerial'] = $this->view_model->get_detail_managerial($nip);
		$query['pendidikan'] = $this->view_model->get_riwayat_pendidikan($nip);
		$query['ortu'] = $this->view_model->get_orang_tua($nip);
		$query['keluarga'] = $this->view_model->get_keluarga($nip);
		$query['darurat'] = $this->view_model->get_darurat($nip);
		//print_r($query);
		$this->load->view('excel_detail_pegawai',$query);	
	}

	public function exportdata_detail_pelamar($np = ""){
		$query['data'] = $this->view_model->get_detail_pelamar($np);
		$query['teknis'] = $this->view_model->get_detail_teknis_pelamar($np);
		$query['managerial'] = $this->view_model->get_detail_managerial_pelamar($np);
		$query['pendidikan'] = $this->view_model->get_riwayat_pendidikan_pelamar($np);
		$query['ortu'] = $this->view_model->get_orang_tua_pelamar($np);
		$query['keluarga'] = $this->view_model->get_keluarga_pelamar($np);
		$query['darurat'] = $this->view_model->get_darurat_pelamar($np);
		$this->load->view('excel_detail_pelamar',$query);	
	}

	public function update_keluarga_pegawai(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		//print_r($_POST); die;

		/*PENDIDIKAN*/
		if(isset($_POST['pendidikan'])){
			foreach($_POST['pendidikan'] as $pend){
				$id = "";
				if(isset($pend['id_pendidikan']))
					$id = $pend['id_pendidikan'];
				$pendidikan = $pend['pendidikan'];
				$nama = $pend['nama'];
				$jurusan = $pend['jurusan'];
				$kota = $pend['kota'];
				$lulus = $pend['lulus'];
				$array = array(
					'nip' => $this->input->post('nip'),
					'pendidikan' => $pendidikan,
					'nama_sekolah' => $nama,
					'jurusan' => $jurusan,
					'kota' => $kota,
					'thn_lulus' => $lulus
				);

				if(!($this->update_model->update_riwayat_pendidikan($array, $id))){
					die; echo "error updating data!";
				}
			}
		}

		if(!($this->update_model->insert_nomor_anak_pegawai($this->input->post('nip'),$this->input->post('anak_nomor')))){
			die; echo "error updating data!";
		}

		if(isset($_POST['ortu'])){
			$count = 0;
			foreach ($_POST['ortu'] as $ortu) {
				if($count == 1){
					$alamat1 = $ortu['alamat'];
					$no_telp1 = $ortu['no_telp'];
				}elseif ($count == 3) {
					$alamat2 = $ortu['alamat'];
					$no_telp2 = $ortu['no_telp'];
				}
				$count+=1;
			} $count = 0;
			foreach($_POST['ortu'] as $ortu){
				$id = "";
				if(isset($ortu['id_orang_tua']))
					$id = $ortu['id_orang_tua'];
				$status = $ortu['status'];
				$orang_tua = $ortu['orang_tua'];
				$nama = $ortu['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($ortu['tgl_lahir']));
				$pendidikan = $ortu['pendidikan'];
				$ket_pekerjaan = $ortu['ket_pekerjaan'];
				
				$array = array(
					'nip' => $this->input->post('nip'),
					'status' => $status,
					'orang_tua' => $orang_tua,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if($count == 0 || $count == 1){
					$array['alamat'] = $alamat1;
					$array['no_telp'] = $no_telp1;
				}else{
					$array['alamat'] = $alamat2;
					$array['no_telp'] = $no_telp2;
				}
				if(!($this->update_model->update_ortu_pegawai($array, $id))){
					die; echo "error updating data!";
				}
				$count+=1;
			}
		}

		if(isset($_POST['kel'])){
			foreach($_POST['kel'] as $kel){
				$id = "";
				if(isset($kel['id_keluarga']))
					$id = $kel['id_keluarga'];
				$status = $kel['status'];
				$nama = $kel['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($kel['tgl_lahir']));
				$pendidikan = $kel['pendidikan'];
				$ket_pekerjaan = $kel['ket_pekerjaan'];
				
				$array = array(
					'nip' => $this->input->post('nip'),
					'status' => $status,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if(!($this->update_model->update_kel_pegawai($array, $id))){
					die; echo "error updating data!";
				}
			}
		}

		if(isset($_POST['darurat'])){
			foreach($_POST['darurat'] as $drt){
				$id = "";
				if(isset($drt['id_data_darurat']))
					$id = $drt['id_data_darurat'];
				$nama = $drt['nama'];
				$alamat = $drt['alamat'];
				$no_telp = $drt['no_telp'];
				
				$array = array(
					'nip' => $this->input->post('nip'),
					'nama' => $nama,
					'alamat' => $alamat,
					'no_telp' => $no_telp
				);

				if(!($this->update_model->update_darurat_pegawai($array, $id))){
					die; echo "error updating data!";
				}
			}
		}
		
		redirect('view/viewdata_pegawai');
	}

	public function update_keluarga_pelamar(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		//print_r($_POST); die;

		/*PENDIDIKAN*/
		if(isset($_POST['pendidikan'])){
			foreach($_POST['pendidikan'] as $pend){
				$id = "";
				if(isset($pend['id_pendidikan_pelamar']))
					$id = $pend['id_pendidikan_pelamar'];
				$pendidikan = $pend['pendidikan'];
				$nama = $pend['nama'];
				$jurusan = $pend['jurusan'];
				$kota = $pend['kota'];
				$lulus = $pend['lulus'];
				$array = array(
					'np' => $this->input->post('np'),
					'pendidikan' => $pendidikan,
					'nama_sekolah' => $nama,
					'jurusan' => $jurusan,
					'kota' => $kota,
					'thn_lulus' => $lulus
				);

				if(!($this->update_model->update_riwayat_pendidikan_pelamar($array, $id))){
					die; echo "error updating data!";
				}
			}
		}

		if(!($this->update_model->insert_nomor_anak_pelamar($this->input->post('np'),$this->input->post('anak_nomor')))){
					die; echo "error updating data!";
		}

		if(isset($_POST['ortu'])){
			$count = 0;
			foreach ($_POST['ortu'] as $ortu) {
				if($count == 1){
					$alamat1 = $ortu['alamat'];
					$no_telp1 = $ortu['no_telp'];
				}elseif ($count == 3) {
					$alamat2 = $ortu['alamat'];
					$no_telp2 = $ortu['no_telp'];
				}
				$count+=1;
			} $count = 0;
			foreach($_POST['ortu'] as $ortu){
				$id = "";
				if(isset($ortu['id_orang_tua_pelamar']))
					$id = $ortu['id_orang_tua_pelamar'];
				$status = $ortu['status'];
				$orang_tua = $ortu['orang_tua'];
				$nama = $ortu['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($ortu['tgl_lahir']));
				$pendidikan = $ortu['pendidikan'];
				$ket_pekerjaan = $ortu['ket_pekerjaan'];
				
				$array = array(
					'np' => $this->input->post('np'),
					'status' => $status,
					'orang_tua' => $orang_tua,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if($count == 0 || $count == 1){
					$array['alamat'] = $alamat1;
					$array['no_telp'] = $no_telp1;
				}else{
					$array['alamat'] = $alamat2;
					$array['no_telp'] = $no_telp2;
				}
				if(!($this->update_model->update_ortu_pelamar($array, $id))){
					die; echo "error updating data!";
				}
				$count+=1;
			}
		}

		if(isset($_POST['kel'])){
			foreach($_POST['kel'] as $kel){
				$id = "";
				if(isset($kel['id_keluarga_pelamar']))
					$id = $kel['id_keluarga_pelamar'];
				$status = $kel['status'];
				$nama = $kel['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($kel['tgl_lahir']));
				$pendidikan = $kel['pendidikan'];
				$ket_pekerjaan = $kel['ket_pekerjaan'];
				
				$array = array(
					'np' => $this->input->post('np'),
					'status' => $status,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if(!($this->update_model->update_kel_pelamar($array, $id))){
					die; echo "error updating data!";
				}
			}
		}

		if(isset($_POST['darurat'])){
			foreach($_POST['darurat'] as $drt){
				$id = "";
				if(isset($drt['id_data_darurat_pelamar']))
					$id = $drt['id_data_darurat_pelamar'];
				$nama = $drt['nama'];
				$alamat = $drt['alamat'];
				$no_telp = $drt['no_telp'];
				
				$array = array(
					'np' => $this->input->post('np'),
					'nama' => $nama,
					'alamat' => $alamat,
					'no_telp' => $no_telp
				);

				if(($this->update_model->update_darurat_pelamar($array, $id))){
							
				}else{
					die; echo "error updating data!";
				}
			}
		}

		redirect('view/viewdata_pelamar');
	}

	public function hapus_pendidikan($id){
		$result = $this->update_model->hapus_riwayat_pendidikan($id);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function hapus_pendidikan_pelamar($id){
		$result = $this->update_model->hapus_riwayat_pendidikan_pelamar($id);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function hapus_kompetensi_teknis($id){
		$result = $this->update_model->hapus_kompetensi_teknis($id);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function hapus_kompetensi_teknis_pelamar($id){
		$result = $this->update_model->hapus_kompetensi_teknis_pelamar($id);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function hapus_kompetensi_managerial($id){
		$result = $this->update_model->hapus_kompetensi_managerial($id);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function hapus_kompetensi_managerial_pelamar($id){
		$result = $this->update_model->hapus_kompetensi_managerial_pelamar($id);

		header('Content-Type: application/json');
		echo json_encode($result);	
	}

	public function hapus_keluarga($id = ""){
		$result = $this->update_model->hapus_keluarga($id);

		header('Content-Type: application/json');
		echo json_encode($result);		
	}

	public function hapus_keluarga_pelamar($id = ""){
		$result = $this->update_model->hapus_keluarga_pelamar($id);

		header('Content-Type: application/json');
		echo json_encode($result);		
	}
}

?>