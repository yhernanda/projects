<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class input extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('input_model');
		$this->load->model('update_model');
		//$this->load->view('javascript');		
	}

	public function inputdata_pegawai()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		
		$this->load->view('inputdata');	
	}

	public function inputdata_pelamar()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		
		$this->load->view('inputdata_pelamar');	
	}

	public function insert_pegawai()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		//print_r($_POST); die;

		//Proses PEGAWAI//
		$tgl_berhenti = null;
		$tgl_nikah = null;
		if($this->input->post('tgl_berhenti') != "")
			$tgl_berhenti = date('Y-m-d', strtotime($this->input->post('tgl_berhenti')));
		if($this->input->post('tgl_nikah') != "")
			$tgl_nikah = date('Y-m-d', strtotime($this->input->post('tgl_nikah')));
        $array = array(
				'nip' => $this->input->post('nip'),
				'nama' => $this->input->post('nama'),
				'tgl_lahir' => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
				'jns_kelamin' => $this->input->post('jns_kelamin'),
				'alamat_domisili' => $this->input->post('alamat_domisili'),
				'alamat_asal' => $this->input->post('alamat_asal'),
				'telp_rmh' => $this->input->post('telp_rmh'),
				'telp_hp' => $this->input->post('telp_hp'),
				'agama' => $this->input->post('agama'),
				'status_perkawinan' => $this->input->post('status_perkawinan'),
				'tgl_nikah' => $tgl_nikah,
				'no_ktp' => $this->input->post('no_ktp'),
				'tinggi_bdn' => $this->input->post('tinggi_bdn'),
				'berat_bdn' => $this->input->post('berat_bdn'),
				'gol_darah' => $this->input->post('gol_darah'),
				'npwp' => $this->input->post('npwp'),
				'no_jamsostek' => $this->input->post('no_jamsostek'),
				'tgl_masuk' => date('Y-m-d', strtotime($this->input->post('tgl_masuk'))),
				'tgl_berhenti' => $tgl_berhenti,
				'hobby' => $this->input->post('hobby'),
				'buku_bacaan' => $this->input->post('buku_bacaan'),
				'golongan' => $this->input->post('golongan'),
				'jabatan' => $this->input->post('jabatan'),
				'status_pegawai' => $this->input->post('status_pegawai'),
				'pujian_rekomendasi' => $this->input->post('pujian_rekomendasi'),
				'insiden_kritis' => $this->input->post('insiden_kritis')
		);

		if($_FILES['picture']['name'] != ""){
			$config['upload_path'] = 'C:\xampp\htdocs\database_pegawai\assets\imageassets\uploads\pegawai';
	        $config['allowed_types'] = 'gif|jpg|png';
	        $config['file_name'] = $this->input->post('nip');
	        $config['max_size']  = '1000';
	        $config['max_width'] = '3000';
	        $config['max_height'] = '4000';
	        $config['overwrite'] = TRUE;
	        $this->load->library('upload', $config);
	       	if (! $this->upload->do_upload('picture'))
			{
				echo "Image too Large!";	die;
			}

			$query = $this->input_model->insert_pegawai($array, $this->upload->data());
		}else{
			$query = $this->input_model->insert_pegawai($array, "empty");
		}

		//Proses GAJI//
		$array = array(
				'nip' => $this->input->post('nip'),
				'gaji_pokok' => $this->input->post('gaji_pokok'),
				'tunj_jabatan' => $this->input->post('tunj_jabatan'),
				'tunj_kehadiran' => $this->input->post('tunj_kehadiran'),
				'tunj_kemahalan' => $this->input->post('tunj_kemahalan')
		);

		if(!($this->input_model->insert_gaji($array))){
			die; echo "error inserting data!";
		}

		//Proses Kompetensi//
		if(isset($_POST['teknis'])){
			foreach($_POST['teknis'] as $kompetensi){
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'nip' => $this->input->post('nip'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->input_model->insert_kompetensi_teknis($array))){
					die; echo "error inserting data!";
				}
			}
		}

		if(isset($_POST['managerial'])){
			foreach($_POST['managerial'] as $kompetensi){
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'nip' => $this->input->post('nip'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->input_model->insert_kompetensi_managerial($array))){
					die; echo "error inserting data!";
				}
			}
		}
		
		if ($query) {
			 	$this->load->view('viewdata');
		}else{
			echo "Error Inserting Data!"; die;
		}
	}

	public function insert_pelamar(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		//print_r($_POST); die;

		$array = array(
				'np' => $this->input->post('np'),
				'nama' => $this->input->post('nama'),
				'tgl_lahir' => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
				'jns_kelamin' => $this->input->post('jns_kelamin'),
				'alamat_domisili' => $this->input->post('alamat_domisili'),
				'alamat_asal' => $this->input->post('alamat_asal'),
				'telp_rmh' => $this->input->post('telp_rmh'),
				'telp_hp' => $this->input->post('telp_hp'),
				'agama' => $this->input->post('agama'),
				'status_perkawinan' => $this->input->post('status_perkawinan'),
				'tgl_nikah' => date('Y-m-d', strtotime($this->input->post('tgl_nikah'))),
				'no_ktp' => $this->input->post('no_ktp'),
				'tinggi_bdn' => $this->input->post('tinggi_bdn'),
				'berat_bdn' => $this->input->post('berat_bdn'),
				'gol_darah' => $this->input->post('gol_darah'),
				'npwp' => $this->input->post('npwp'),
				'no_jamsostek' => $this->input->post('no_jamsostek'),
				'tgl_daftar' => date('Y-m-d', strtotime($this->input->post('tgl_daftar'))),
				'hobby' => $this->input->post('hobby'),
				'buku_bacaan' => $this->input->post('buku_bacaan')
		);

		if($_FILES['picture']['name'] != ""){
			$config['upload_path'] = 'C:\xampp\htdocs\database_pegawai\assets\imageassets\uploads\pelamar';
			$config['file_name'] = $this->input->post('np');
	        $config['allowed_types'] = 'gif|jpg|png';
	        $config['max_size']  = '1000';
	        $config['max_width'] = '3000';
	        $config['max_height'] = '4000';
	        $config['overwrite'] = TRUE;
	        $this->load->library('upload', $config);
	       	if (! $this->upload->do_upload('picture'))
			{
				echo "Image too Large!";	die;
			}

			$query = $this->input_model->insert_pelamar($array, $this->upload->data());
		}else{
			$query = $this->input_model->insert_pelamar($array, "empty");
		}

		//Proses Kompetensi//
		if(isset($_POST['teknis'])){
			foreach($_POST['teknis'] as $kompetensi){
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'np' => $this->input->post('np'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->input_model->insert_kompetensi_teknis_pelamar($array))){
					die; echo "error inserting data!";
				}
			}
		}

		if(isset($_POST['managerial'])){
			foreach($_POST['managerial'] as $kompetensi){
				$nama = $kompetensi['name'];
				$val = $kompetensi['value'];
				$array = array(
					'np' => $this->input->post('np'),
					'nama_kompetensi' => $nama,
					'penguasaan' => $val
				);

				if(!($this->input_model->insert_kompetensi_managerial_pelamar($array))){
					die; echo "error inserting data!";
				}
			}
		}
		
		if ($query) {
			 	$this->load->view('inputdata_pelamar');
		}else{
			echo "Error Inserting Data!"; die;
		}
	}

	public function insert_keluarga_pegawai(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		//print_r($_POST); die;

		if(isset($_POST['pendidikan'])){
			foreach($_POST['pendidikan'] as $pend){
				$pendidikan = $pend['pendidikan'];
				$nama = $pend['nama'];
				$jurusan = $pend['jurusan'];
				$kota = $pend['kota'];
				$lulus = $pend['lulus'];
				$array = array(
					'nip' => $this->input->post('nip'),
					'pendidikan' => $pendidikan,
					'nama_sekolah' => $nama,
					'jurusan' => $jurusan,
					'kota' => $kota,
					'thn_lulus' => $lulus
				);

				if(!($this->input_model->insert_riwayat_pendidikan($array))){
					die; echo "error inserting data!";
				}
			}
		}

		if(!($this->update_model->insert_nomor_anak_pegawai($this->input->post('nip'),$this->input->post('anak_nomor')))){
					die; echo "error inserting data!";
		}

		if(isset($_POST['ortu'])){
			$count = 0;
			foreach ($_POST['ortu'] as $ortu) {
				if($count == 1){
					$alamat1 = $ortu['alamat'];
					$no_telp1 = $ortu['no_telp'];
				}elseif ($count == 3) {
					$alamat2 = $ortu['alamat'];
					$no_telp2 = $ortu['no_telp'];
				}
				$count+=1;
			} $count = 0;
			foreach($_POST['ortu'] as $ortu){
				$status = $ortu['status'];
				$orang_tua = $ortu['orang_tua'];
				$nama = $ortu['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($ortu['tgl_lahir']));
				$pendidikan = $ortu['pendidikan'];
				$ket_pekerjaan = $ortu['ket_pekerjaan'];
				
				$array = array(
					'nip' => $this->input->post('nip'),
					'status' => $status,
					'orang_tua' => $orang_tua,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if($count == 0 || $count == 1){
					$array['alamat'] = $alamat1;
					$array['no_telp'] = $no_telp1;
				}else{
					$array['alamat'] = $alamat2;
					$array['no_telp'] = $no_telp2;
				}
				if(!($this->input_model->insert_ortu_pegawai($array))){
					die; echo "error inserting data!";
				}
				$count+=1;
			}
		}

		if(isset($_POST['kel'])){
			foreach($_POST['kel'] as $kel){
				$status = $kel['status'];
				$nama = $kel['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($kel['tgl_lahir']));
				$pendidikan = $kel['pendidikan'];
				$ket_pekerjaan = $kel['ket_pekerjaan'];
				
				$array = array(
					'nip' => $this->input->post('nip'),
					'status' => $status,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if(!($this->input_model->insert_kel_pegawai($array))){
					die; echo "error inserting data!";
				}
			}
		}

		if(isset($_POST['darurat'])){
			foreach($_POST['darurat'] as $drt){
				$nama = $drt['nama'];
				$alamat = $drt['alamat'];
				$no_telp = $drt['no_telp'];
				
				$array = array(
					'nip' => $this->input->post('nip'),
					'nama' => $nama,
					'alamat' => $alamat,
					'no_telp' => $no_telp
				);

				if(!($this->input_model->insert_darurat_pegawai($array))){
					die; echo "error inserting data!";
				}
			}
		}
		
		redirect('view/viewdata_pegawai');		
	}

	public function insert_keluarga_pelamar(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		//print_r($_POST); die;

		if(isset($_POST['pendidikan'])){
			foreach($_POST['pendidikan'] as $pend){
				$pendidikan = $pend['pendidikan'];
				$nama = $pend['nama'];
				$jurusan = $pend['jurusan'];
				$kota = $pend['kota'];
				$lulus = $pend['lulus'];
				$array = array(
					'np' => $this->input->post('np'),
					'pendidikan' => $pendidikan,
					'nama_sekolah' => $nama,
					'jurusan' => $jurusan,
					'kota' => $kota,
					'thn_lulus' => $lulus
				);

				if(!($this->input_model->insert_riwayat_pendidikan_pelamar($array))){
					die; echo "error inserting data!";
				}
			}
		}

		if(!($this->update_model->insert_nomor_anak_pelamar($this->input->post('np'),$this->input->post('anak_nomor')))){
			die; echo "error inserting data!";
		}

		if(isset($_POST['ortu'])){
			$count = 0;
			foreach ($_POST['ortu'] as $ortu) {
				if($count == 1){
					$alamat1 = $ortu['alamat'];
					$no_telp1 = $ortu['no_telp'];
				}elseif ($count == 3) {
					$alamat2 = $ortu['alamat'];
					$no_telp2 = $ortu['no_telp'];
				}
				$count+=1;
			} $count = 0;
			foreach($_POST['ortu'] as $ortu){
				$status = $ortu['status'];
				$orang_tua = $ortu['orang_tua'];
				$nama = $ortu['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($ortu['tgl_lahir']));
				$pendidikan = $ortu['pendidikan'];
				$ket_pekerjaan = $ortu['ket_pekerjaan'];
				
				$array = array(
					'np' => $this->input->post('np'),
					'status' => $status,
					'orang_tua' => $orang_tua,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if($count == 0 || $count == 1){
					$array['alamat'] = $alamat1;
					$array['no_telp'] = $no_telp1;
				}else{
					$array['alamat'] = $alamat2;
					$array['no_telp'] = $no_telp2;
				}
				if(!($this->input_model->insert_ortu_pelamar($array))){
					die; echo "error inserting data!";
				}
				$count+=1;
			}
		}

		if(isset($_POST['kel'])){
			foreach($_POST['kel'] as $kel){
				$status = $kel['status'];
				$nama = $kel['nama'];
				$tgl_lahir = date('Y-m-d', strtotime($kel['tgl_lahir']));
				$pendidikan = $kel['pendidikan'];
				$ket_pekerjaan = $kel['ket_pekerjaan'];
				
				$array = array(
					'np' => $this->input->post('np'),
					'status' => $status,
					'nama' => $nama,
					'tgl_lahir' => $tgl_lahir,
					'pendidikan' => $pendidikan,
					'ket_pekerjaan' => $ket_pekerjaan
				);

				if(!($this->input_model->insert_kel_pelamar($array))){
					die; echo "error inserting data!";
				}
			}
		}

		if(isset($_POST['darurat'])){
			foreach($_POST['darurat'] as $drt){
				$nama = $drt['nama'];
				$alamat = $drt['alamat'];
				$no_telp = $drt['no_telp'];
				
				$array = array(
					'np' => $this->input->post('np'),
					'nama' => $nama,
					'alamat' => $alamat,
					'no_telp' => $no_telp
				);

				if(!($this->input_model->insert_darurat_pelamar($array))){
					die; echo "error inserting data!";
				}
			}
		}
		
		redirect('view/viewdata_pelamar');
	}

}
?>