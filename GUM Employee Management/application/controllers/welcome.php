<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('my_model');
		$this->load->model('dashboard_model');
		//$this->load->view('javascript');		
	}

	public function index()
	{
		if ($this->session->userdata('user_input')['username'] != "") {
			redirect('welcome/dashboard_view');
		}
		
		$this->load->view('home');
	}

	public function dashboard_view(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		
		//$this->data_golongan();
		$this->load->view('dashboard');
	}

	public function data_kelamin(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		
		$result['pegawai_laki'] = $this->dashboard_model->get_data_kelamin()['pegawai_laki']->result_array()[0];
		$result['pegawai_perempuan'] = $this->dashboard_model->get_data_kelamin()['pegawai_perempuan']->result_array()[0];
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function data_pendidikan(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		$result = $this->dashboard_model->get_data_pendidikan();
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function data_golongan(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		$result = $this->dashboard_model->get_data_golongan();
		$final = '[[';
		$count = 1;
		foreach ($result['golongan'] as $key => $value) {
			if($count != 1) $final .= ",";
			$final .= '{"golongan":"'.$value['golongan'].'","laki":';
				if(isset($result['jumlah'][$value['golongan']."_L"]))
					$final .= $result['jumlah'][$value['golongan']."_L"];
				else
					$final .= '0';
			$final .= ',"perempuan":';
				if(isset($result['jumlah'][$value['golongan']."_P"]))
					$final .= $result['jumlah'][$value['golongan']."_P"];
				else
					$final .= '0';
			$final .= '}';
			$count += 1;
		}
		$final .= "]]";
		
		$final = json_decode($final, true);
		$new_final = array();
		foreach ($final as $value) {
			foreach ($value as $sub_value) {
				$new_final[] = $sub_value;
			}
		}

		//$final['data'] = $final;
		header('Content-Type: application/json');
		echo json_encode($new_final);	
	}

	public function data_jabatan(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		$result = $this->dashboard_model->get_data_jabatan();
		$final = '[[';
		$count = 1;
		foreach ($result['jabatan'] as $key => $value) {
			if($count != 1) $final .= ",";
			$final .= '{"jabatan":"'.$value['jabatan'].'","laki":';
				if(isset($result['jumlah'][$value['jabatan']."_L"]))
					$final .= $result['jumlah'][$value['jabatan']."_L"];
				else
					$final .= '0';
			$final .= ',"perempuan":';
				if(isset($result['jumlah'][$value['jabatan']."_P"]))
					$final .= $result['jumlah'][$value['jabatan']."_P"];
				else
					$final .= '0';
			$final .= '}';
			$count += 1;
		}
		$final .= "]]";
		
		$final = json_decode($final, true);
		$new_final = array();
		foreach ($final as $value) {
			foreach ($value as $sub_value) {
				$new_final[] = $sub_value;
			}
		}

		//$final['data'] = $final;
		header('Content-Type: application/json');
		echo json_encode($new_final);	
	}

    public function upload_image($value = ""){
        $this->my_model->do_upload(); //execute the upload function
    }

	
    //SEARCH PEGAWAI & PELAMAR
	public function search_pegawai($value='')
	{
		$result = $this->my_model->search_pegawai($value);
		//$result = 'true';
		header('Content-Type: application/json');
		echo json_encode($result);
		//die();
	}

	public function search_pelamar($value='')
	{
		$result = $this->my_model->search_pelamar($value);
		//$result = 'true';
		header('Content-Type: application/json');
		echo json_encode($result);
		//die();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */