<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class payroll extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model("payroll_management");
    }

    function view_payroll(){
    	if (!isset($this->session->userdata('user_input')['username'])) {
			redirect('welcome');
		}

		$this->load->view("payroll");
	}

	function get_data_pegawai(){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		$result = $this->payroll_management->get_data_pegawai();
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	function set_detail_gaji($detail, $nilai, $nip){
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		$result = $this->payroll_management->set_detail_gaji($detail, $nilai, $nip);
		return($result);
	}

	function export_gaji_pegawai(){
		$data['gaji'] = $this->payroll_management->get_export_pegawai();
		$this->load->view("excel_gaji_pegawai", $data);
	}
} ?>