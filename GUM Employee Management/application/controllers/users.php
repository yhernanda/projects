<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class users extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->model("users_management");
    }

    public function process_login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('userpass', 'Password', 'required|trim|xss_clean|max_length[50]');

		if ($this->form_validation->run() == TRUE) {
			// if validation run
			$array = array('username' => $this->input->post('username'), 'password' => sha1($this->input->post('userpass')));
			if ($query = $this->users_management->login_validation($array)) {
				//jika berhasil, redirect juga
				
				$this->session->set_userdata('user_input', $query);
				$this->session->set_flashdata('message', 'berhasil');
				redirect('welcome/dashboard_view');
			} else {
				$this->session->set_flashdata('message', '* Akun anda tidak dapat ditemukan');
			}
		} else {
			$this->session->set_flashdata('message', validation_errors());
		}
		redirect('welcome');
	}

	public function logout()
	{
		$this->session->unset_userdata('user_input');
		redirect('welcome');
	}

	public function getuserdata()
	{
		header('Content-Type: application/json');
		$data['id'] = $this->session->userdata('user_input')['user_id'];
		$data['role'] = $this->session->userdata('user_input')['role'];
		echo json_encode($data);
	}

	public function daftar_user()
	{
		if ((isset($this->session->userdata('user_input')['username']) && $this->session->userdata('user_input')['role'] == "HRD") || !isset($this->session->userdata('user_input')['username'])) {
			redirect('welcome');
		}

		$data['users'] = $this->users_management->get_all_users();
		$this->load->view('userlist', $data);
	}

	public function regisuser()
	{
		if ((isset($this->session->userdata('user_input')['username']) && $this->session->userdata('user_input')['role'] == "HRD") || !isset($this->session->userdata('user_input')['username'])) {
			redirect('welcome');
		}

		$res = $this->users_management->get_user($this->input->post("username"), "new");
		$check = $res['value'];
		$id_foto = $res['id'];
	
		if ($check == "exist"){
			$result = Array(
				'error' => "exist"
			);
			header('Content-Type: application/json');
			echo json_encode($result);		
		}
		else {
			$insert = Array(
				"username" => $this->input->post("username"),
				"full_name" => $this->input->post("full_name"),
				"password" => sha1($this->input->post("password")),
				"role" => $this->input->post("role")
			);

			if($_FILES['picture']['name'] != ""){
				$config['upload_path'] = 'C:\xampp\htdocs\database_pegawai\assets\imageassets\uploads\user';
		        $config['allowed_types'] = 'gif|jpg|png';
		        $config['file_name'] = $id_foto;
		        $config['max_size']  = '1000';
		        $config['max_width'] = '3000';
		        $config['max_height'] = '4000';
		        $config['overwrite'] = TRUE;
		        $this->load->library('upload', $config);
		       	if (!$this->upload->do_upload('picture'))
				{
					echo "Image too Large!";	die;
				}

				$result = $this->users_management->regisuser($insert, $this->upload->data());
			}else{
				$result = $this->users_management->regisuser($insert, "empty");
			}
			if($result){
				header('Content-Type: application/json');
				echo json_encode($result);		
			}
		}
	}

	public function edituser()
	{
		if ((isset($this->session->userdata('user_input')['username']) && $this->session->userdata('user_input')['role'] == "HRD") || !isset($this->session->userdata('user_input')['username'])) {
			redirect('welcome');
		}

		$res = $this->users_management->get_user($_POST["username"], "edit");
		$check = $res['value']; //the User_id from database
		$id_foto = $_POST["user_id"]; //the User_id from current user that was submitted to change
		//echo $check." ".$_FILES['picture']['name']; die;
	
		//If there is a different iD from database with same username as the current submited new username, the access denied.
		//There can't be two user with same username
		if($check = "no" || $check == $id_foto){
			$password = "";
			$result = "";
			if($_POST["password"] != ""){
				//Check Current password for change
				$detail = $this->users_management->get_user_detail($_POST["user_id"]);
				if($detail[0]['role'] == $this->session->userdata('user_input')['role']
					&& $detail[0]['password'] == sha1($_POST["current_password"]))
					$password = sha1($_POST["password"]);
				else{
					$result = Array(
						'error' => "wrong"
					);
					header('Content-Type: application/json');
					echo json_encode($result);
				}

			}

			if($result['error'] != "wrong"){
				$insert = Array(
					"user_id" => $id_foto,
					"username" => $_POST["username"],
					"full_name" => $_POST["full_name"],
					"password" => $password,
					"role" => $_POST["role"]
				);

				if($_FILES['picture']['name'] != ""){
					$config['upload_path'] = 'C:\xampp\htdocs\database_pegawai\assets\imageassets\uploads\user';
			        $config['allowed_types'] = 'gif|jpg|png|bmp';
			        $config['file_name'] = $id_foto;
			        $config['max_size']  = '1000';
			        $config['max_width'] = '3000';
			        $config['max_height'] = '4000';
			        $config['overwrite'] = TRUE;
			        $this->load->library('upload', $config);
			       	if (!$this->upload->do_upload('picture'))
					{
						echo "Image too Large!"; die;
					}

					$result = $this->users_management->edituser($insert, $this->upload->data());
				}else{
					$result = $this->users_management->edituser($insert, "empty");
				}

				$result = Array(
					'error' => "none"
				);
				header('Content-Type: application/json');
				echo json_encode($result);	
			}
		}
		else if ($check != $id_foto){
			$result = Array(
				'error' => "exist"
			);
			header('Content-Type: application/json');
			echo json_encode($result);		
		}
	}

	public function hapus_user($user_id)
	{
		if ((isset($this->session->userdata('user_input')['username']) && $this->session->userdata('user_input')['role'] == "HRD") || !isset($this->session->userdata('user_input')['username'])) {
			redirect('welcome');
		}

		$result = $this->users_management->hapus_user($user_id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
} ?>