<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('view_model');
		//$this->load->view('javascript');		
	}

	public function viewdata_pegawai()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		
		$data['pegawai'] = $this->view_model->get_pegawai_pelamar("pegawai");
		$this->load->view('viewdata', $data);	
	}

	public function viewdata_pelamar()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		
		$data['pelamar'] = $this->view_model->get_pegawai_pelamar("pelamar");
		$this->load->view('viewdata_pelamar', $data);	
	}

	//Klik dari view untuk edit detail pegawai
	public function edit_pegawai($value = "")
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		$data['pegawai'] = $this->view_model->get_detail_pegawai($value);
		$data['teknis'] = $this->view_model->get_detail_teknis($value);
		$data['managerial'] = $this->view_model->get_detail_managerial($value);
		$data['pendidikan'] = $this->view_model->get_riwayat_pendidikan($value);
		$data['orang_tua'] = $this->view_model->get_orang_tua($value);
		$data['keluarga'] = $this->view_model->get_keluarga($value);
		$data['darurat'] = $this->view_model->get_darurat($value);
		//print_r($data['gaji']); die;
		/*header('Content-Type: application/json');
		echo json_encode($data);*/
		$this->load->view('editdata', $data);
	}

	//Klik dari view untuk edit detail pelamar
	public function edit_pelamar($value = "")
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}

		$data['pelamar'] = $this->view_model->get_detail_pelamar($value);
		$data['teknis'] = $this->view_model->get_detail_teknis_pelamar($value);
		$data['managerial'] = $this->view_model->get_detail_managerial_pelamar($value);
		$data['pendidikan'] = $this->view_model->get_riwayat_pendidikan_pelamar($value);
		$data['orang_tua'] = $this->view_model->get_orang_tua_pelamar($value);
		$data['keluarga'] = $this->view_model->get_keluarga_pelamar($value);
		$data['darurat'] = $this->view_model->get_darurat_pelamar($value);
		//print_r($data['gaji']); die;
		/*header('Content-Type: application/json');
		echo json_encode($data);*/
		//print_r($data); die;
		$this->load->view('editdata_pelamar', $data);
	}

	public function exportdata_pegawai_pelamar($jenis = ""){
		$query['data'] = $this->view_model->get_pegawai_pelamar($jenis);
		$query['darurat'] = $this->view_model->get_data_darurat($jenis);
		$query['keluarga'] = $this->view_model->get_kel($jenis);
		$query['managerial'] = $this->view_model->get_managerial($jenis);
		$query['teknis'] = $this->view_model->get_teknis($jenis);
		$query['ortu'] = $this->view_model->get_ortu($jenis);
		$query['pendidikan'] = $this->view_model->get_pendidikan($jenis);

		if($jenis == "pegawai"){
			$query['gaji'] = $this->view_model->get_gaji();
			$this->load->view('excel_pegawai',$query);
		} else {
			$this->load->view('excel_pelamar',$query);
		}
		
	}

	public function hapus_pegawai($nip)
	{
		$result = $this->view_model->hapus_pegawai($nip);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function hapus_pelamar($np)
	{
		$result = $this->view_model->hapus_pelamar($np);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
}