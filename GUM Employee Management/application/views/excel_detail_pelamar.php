<?php 
header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename=exceldetailpelamar.xls');
header('Pragma: no-cache');
header('Expires: 0');
?>
<table border="1" width='70%'>
	<tr>
		<th colspan='19'>DATA PELAMAR</th>
	</tr>
	<tr>
		<th>No. Pelamar</th>
		<th>Nama Lengkap</th>
		<th>Tgl. Lahir</th>
		<th>Jns. Kelamin</th>
		<th>Alamat Domisili</th>
		<th>Alamat Asal</th>
		<th>No. Telp. Rumah</th>
		<th>No. Telp. HP</th>
		<th>Agama</th>
		<th>Status Nikah</th>
		<th>Tanggal Nikah</th>
		<th>No. KTP</th>
		<th>Tinggi Bdn</th>
		<th>Berat Bdn</th>
		<th>Gol. Darah</th>
		<th>NPWP</th>
		<th>No. Jamsostek</th>
		<th>Tgl. Daftar</th>
		<th>Anak No.</th>
	</tr>
	<?php
		$align = 'style="vertical-align:middle;" align="center"';
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td '.$align.'>="'.$value["np"].'"</td>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["tgl_lahir"].'</td>
					<td '.$align.'>'.$value["jns_kelamin"].'</td>
					<td '.$align.'>'.$value["alamat_domisili"].'</td>
					<td '.$align.'>'.$value["alamat_asal"].'</td>
					<td '.$align.'>="'.$value["telp_rmh"].'"</td>
					<td '.$align.'>="'.$value["telp_hp"].'"</td>
					<td '.$align.'>'.$value["agama"].'</td>
					<td '.$align.'>'.$value["status_perkawinan"].'</td>
					<td '.$align.'>'.$value["tgl_nikah"].'</td>
					<td '.$align.'>="'.$value["no_ktp"].'"</td>
					<td '.$align.'>'.$value["tinggi_bdn"].'</td>
					<td '.$align.'>'.$value["berat_bdn"].'</td>
					<td '.$align.'>'.$value["gol_darah"].'</td>
					<td '.$align.'>="'.$value["npwp"].'"</td>
					<td '.$align.'>="'.$value["no_jamsostek"].'"</td>
					<td '.$align.'>'.$value["tgl_daftar"].'</td>
					<td '.$align.'>'.$value["anak_no"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='2'>KOMPETENSI TEKNIS</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($teknis)){
			foreach ($teknis as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["nama_kompetensi"].'</td>
					<td '.$align.'>'.$value["penguasaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='2'>KOMPETENSI MANAGERIAL</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($managerial)){
			foreach ($managerial as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["nama_kompetensi"].'</td>
					<td '.$align.'>'.$value["penguasaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th>HOBBY PELAMAR</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td align="left" style="vertical-align:middle;">'.$value["hobby"].'</td>
				</tr>';
			}
		}
	?>
	<tr>
		<th>BUKU BACAAN</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td align="left" style="vertical-align:middle;">'.$value["buku_bacaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='5'>PENDIDIKAN PELAMAR</th>
	</tr>
	<tr>
		<th>Pendidikan</th>
		<th>Nama Sekolah</th>
		<th>Jurusan</th>
		<th>Kota</th>
		<th>Thn. Lulus</th>
	</tr>
	<?php
		if(!empty($pendidikan)){
			foreach ($pendidikan as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["pendidikan"].'</td>
					<td '.$align.'>'.$value["nama_sekolah"].'</td>
					<td '.$align.'>'.$value["jurusan"].'</td>
					<td '.$align.'>'.$value["kota"].'</td>
					<td '.$align.'>'.$value["thn_lulus"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='6'>ORANG TUA</th>
	</tr>
	<tr>
		<th>Status</th>
		<th>Nama</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket/Pekerjaan</th>
		<th>No. Telpon</th>
	</tr>
	<?php
		if(!empty($ortu)){
			$count = 1;
			$size = count($ortu);
			foreach ($ortu as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["orang_tua"].' '.$value["status"].'</td>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["tgl_lahir"].'</td>
					<td '.$align.'>'.$value["pendidikan"].'</td>
					<td '.$align.'>'.$value["ket_pekerjaan"].'</td>';
					if($count == 1)
						echo '<td '.$align.' rowspan="'.$size.'">="'.$value["no_telp"].'"</td>';
				echo '</tr>';
				$count++;
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='5'>KELUARGA</th>
	</tr>
	<tr>
		<th>Status</th>
		<th>Nama</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket/Pekerjaan</th>
	</tr>
	<?php
		if(!empty($keluarga)){
			foreach ($keluarga as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["status"].'</td>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["tgl_lahir"].'</td>
					<td '.$align.'>'.$value["pendidikan"].'</td>
					<td '.$align.'>'.$value["ket_pekerjaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='3'>NOMOR DARURAT</th>
	</tr>
	<tr>
		<th>Nama</th>
		<th>Alamat</th>
		<th>No. Telp</th>
	</tr>
	<?php
		if(!empty($darurat)){
			foreach ($darurat as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["alamat"].'</td>
					<td '.$align.'>="'.$value["no_telp"].'"</td>
				</tr>';
			}
		}
	?>
</table>