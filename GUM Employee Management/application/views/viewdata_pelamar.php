<!DOCTYPE html>
<html lang="em">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>PT. GUM DB Pegawai</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Junction.otf">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/timeline.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sb-admin-2.css">

    <!-- DataTables CSS & JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>

	<!-- JQuery & JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/js.js"></script>
    <!--<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slidebars.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/slidebars.js"></script>-->

</head>
<body>
        <?php include "navbar.php"; ?>
        <script>
			$('#view_pelamar').removeClass('hidden');
		</script>

		<div id="page-wrapper">
			<div style="height:30px;">
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
                    <h4><i class="glyphicon glyphicon-user">&nbsp</i>Daftar Pelamar</h4>
                </div>
                <div class="panel-body">
					<table id="tabelpelamar" class="table table-striped table-responsive table-hover" style="overflow-x:auto; display:block;">
						<thead>
							<tr class="bg-primary" align="center" style="vertical-align:middle;">
								<th>Aksi</th>
								<th>NP</th>
								<th>Nama Lengkap</th>
								<th>Tgl. Lahir</th>
								<th>Jns. Kelamin</th>
								<th>Alamat Domisili</th>
								<th>No. HP</th>
								<th>Agama</th>
								<th>Status Nikah</th>
								<th>Gol. Darah</th>
								<th>Tgl. Daftar</th>								
							</tr>
						</thead>
						<tbody>
							<?php
								if(!empty($pelamar)){
									foreach ($pelamar as $key => $value) {
										echo "<tr>
											<td style='vertical-align:middle' align='center'>
												<a href='view/edit_pelamar/".$value['np']."' class='edit_pelamar' id='tool' data-toggle='tooltip' data-placement='top' title='Detail/Edit'><i class='glyphicon glyphicon-edit'></i></a>
												<a style='cursor:pointer;' class='hapus_pelamar' id='tool' data-toggle='tooltip' data-placement='top' title='Delete'><i class='glyphicon glyphicon-trash'></i></a>
											</td>
											<td class='np' style='vertical-align:middle' align='center'>".$value['np']."</td>
											<td style='vertical-align:middle'>".$value['nama']."</td>
											<td style='vertical-align:middle'>".date('d-m-Y', strtotime($value['tgl_lahir']))."</td>
											<td style='vertical-align:middle'>".$value['jns_kelamin']."</td>
											<td style='vertical-align:middle'>".$value['alamat_domisili']."</td>
											<td style='vertical-align:middle'>".$value['telp_hp']."</td>
											<td style='vertical-align:middle'>".$value['agama']."</td>
											<td style='vertical-align:middle'>".$value['status_perkawinan']."</td>
											<td style='vertical-align:middle'>".$value['gol_darah']."</td>
											<td style='vertical-align:middle'>".date('d-m-Y', strtotime($value['tgl_daftar']))."</td>											
										</tr>";
									}
								}
							?>
						</tbody>
						<tfoot>
							<tr class="bg-default" align="center" style="vertical-align:middle;">
								<th>Aksi</th>
								<th>NP</th>
								<th>Nama Lengkap</th>
								<th>Tgl. Lahir</th>
								<th>Jns. Kelamin</th>
								<th>Alamat Domisili</th>
								<th>No. HP</th>
								<th>Agama</th>
								<th>Status Nikah</th>
								<th>Gol. Darah</th>
								<th>Tgl. Daftar</th>								
							</tr>
						</tfoot>
					</table>
                </div>
			</div>
		</div>
	</div>

		<!-- MODALS 
		<div class="modal fade" id="detail_data" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        <h3 class="modal-title" id="myModalLabel">Data Keluarga</h3>
				    </div>
				    <div class="modal-body" >
						<div class="form-group">
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelDetailData">
									<thead>
										<tr class="warning">
											<td>Nama</td>
											<td>Keterangan	</td>
										</tr>
									</thead>
									<tbody id="t_bodydatamodal">
										
									</tbody>
								</table>												
							</div>
						</div>
				    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Pilih</button>
					</div>
				</div>
			</div>
		</div> -->
</body>
</html>