<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function (event){
		var pria;
		var wanita;
		
		function dashboard_data(){
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>welcome/data_kelamin",
				success: function (data) {
					Morris.Donut({
						element: 'jns_kelamin_chart',
						data: [{
							label: "Pegawai Laki-laki",
							value: data["pegawai_laki"]["count"]
						},{
							label: "Pegawai Perempuan",
							value: data["pegawai_perempuan"]["count"]
						}],
						resize: true
					});
				}
			})

			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>welcome/data_pendidikan",
				success: function (data) {
					Morris.Bar({
				        element: 'data_pendidikan_chart',
				        data: [{
				            y: 'SD',
				            a: data['SD_L'],
				            b: data['SD_P']
				        }, {
				            y: 'SMP',
				            a: data['SMP_L'],
				            b: data['SMP_P']
				        }, {
				            y: 'SMA',
				            a: data['SMA_L'],
				            b: data['SMA_P']
				        }, {
				            y: 'Strata 1',
				            a: data['Strata1_L'],
				            b: data['Strata1_P']
				        }],
				        xkey: 'y',
				        ykeys: ['a', 'b'],
				        labels: ['Laki-laki', 'Perempuan'],
				        hideHover: 'auto',
				        resize: true
				    });
				},
				error: function (data) {
					console.log(data);
					alert('gagal');
				}
			})

			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>welcome/data_golongan",
				success: function (data) {
					console.log(data);
					Morris.Bar({
				        element: 'data_golongan_chart',
				        data: data,
				        xkey: 'golongan',
				        ykeys: ['laki', 'perempuan'],
				        labels: ['Laki-laki', 'Perempuan'],
				        hideHover: 'auto',
				        resize: true
				    });
				},
				error: function (data) {
					console.log(data);
					alert('gagal');
				}
			})

			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>welcome/data_jabatan",
				success: function (data) {
					console.log(data);
					Morris.Bar({
				        element: 'data_jabatan_chart',
				        data: data,
				        xkey: 'jabatan',
				        ykeys: ['laki', 'perempuan'],
				        labels: ['Laki-laki', 'Perempuan'],
				        hideHover: 'auto',
				        resize: true
				    });
				},
				error: function (data) {
					console.log(data);
					alert('gagal');
				}
			})

		}
		
		dashboard_data();
	});
</script>