<!DOCTYPE html>
<html lang="em">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>PT. GUM DB Pegawai</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Junction.otf">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/timeline.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sb-admin-2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">

    <!-- DataTables CSS & JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>

	<!-- JQuery & JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/js.js"></script>

</head>
<body>
	<div id="wrapper">
		<?php include "navbar.php"; ?>
		<script>
			$('#view_pegawai').removeClass('hidden');
		</script>
		
		<div id="page-wrapper">
			<div style="height:30px;">
				
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
                    <h4><i class="glyphicon glyphicon-briefcase">&nbsp</i>Daftar Pegawai</h4>
                </div>
                <div class="panel-body">
					<table id="tabelpegawai" class="table table-striped table-responsive table-hover">
						<thead>
							<tr class="bg-primary" align="center">
								<th>Aksi</th>
								<th>NIP</th>
								<th>Nama Lengkap</th>
								<th>Status</th>
								<th>Golongan</th>
								<th>Tgl. Lahir</th>
								<th>Jns. Kelamin</th>
								<th>Alamat Domisili</th>
								<th>No. HP</th>
								<th>Agama</th>
								<th>Status Nikah</th>
								<th>Gol. Darah</th>
								<th>Tgl. Masuk</th>
								<th>Tgl. Berhenti</th>
								<th>Masa Kerja</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if(!empty($pegawai)){
									foreach ($pegawai as $key => $value) {
										echo "<tr>
											<td style='vertical-align:middle' align='center'>
												<a href='view/edit_pegawai/".$value['nip']."' class='edit_pegawai' id='tool' data-toggle='tooltip' data-placement='top' title='Detail/Edit'><i class='glyphicon glyphicon-edit'></i></a>
												<a style='cursor:pointer;' class='hapus_pegawai' id='tool' data-toggle='tooltip' data-placement='top' title='Delete'><i class='glyphicon glyphicon-trash'></i></a>
											</td>
											<td class='nip' style='vertical-align:middle' align='center'>".$value['nip']."</td>
											<td style='vertical-align:middle'>".$value['nama']."</td>
											<td style='vertical-align:middle'>".$value['status_pegawai']."</td>
											<td style='vertical-align:middle'>".$value['golongan']."</td>
											<td style='vertical-align:middle'>".date('d-m-Y', strtotime($value['tgl_lahir']))."</td>
											<td style='vertical-align:middle'>".$value['jns_kelamin']."</td>
											<td style='vertical-align:middle'>".$value['alamat_domisili']."</td>
											<td style='vertical-align:middle'>".$value['telp_hp']."</td>
											<td style='vertical-align:middle'>".$value['agama']."</td>
											<td style='vertical-align:middle'>".$value['status_perkawinan']."</td>
											<td style='vertical-align:middle'>".$value['gol_darah']."</td>
											<td style='vertical-align:middle'>".date('d-m-Y', strtotime($value['tgl_masuk']))."</td>
											<td style='vertical-align:middle'>";
												$date1 = date_create(date('Y-m-d'));
												if($value["tgl_berhenti"] != ''){
													$date1 = date_create($value["tgl_berhenti"]);
												}
												$date2 = date_create($value["tgl_masuk"]);
												$diff=date_diff($date1,$date2);
											echo '<td style="vertical-align:middle">'.$diff->format("%y tahun, %m bulan, %d hari").'</td>';
										echo "</tr>";
									}
								}
							?>
						</tbody>
						<tfoot>
							<tr class="bg-default" align="center" style="vertical-align:middle;">
								<th>Aksi</th>
								<th>NIP</th>
								<th>Nama Lengkap</th>
								<th>Status</th>
								<th>Golongan</th>
								<th>Tgl. Lahir</th>
								<th>Jns. Kelamin</th>
								<th>Alamat Domisili</th>
								<th>No. HP</th>
								<th>Agama</th>
								<th>Status Nikah</th>
								<th>Gol. Darah</th>
								<th>Tgl. Masuk</th>
								<th>Tgl. Berhenti</th>
								<th>Masa Kerja</th>															
							</tr>
						</tfoot>
					</table>
                </div>
			</div>
		</div>
	</div>
</body>
</html>