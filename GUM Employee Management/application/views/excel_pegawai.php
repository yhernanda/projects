<?php 
header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename=excel_data_pegawai.xls');
header('Pragma: no-cache');
header('Expires: 0');
?>
<table border="1" width='70%'>
	<tr>
		<?php
			//Kalau login as hrd, gaji ilang trus golongan masuk tabel Data Pribadi
			if($this->session->userdata("user_input")["role"] == 'Admin' || $this->session->userdata("user_input")["role"] == 'Direktur'){
				$colspanData = 63;
				$colspanPribadi = 22;
			} else {
				$colspanData = 47;
				$colspanPribadi = 25;
			}
		?>
			<th colspan='<?php echo $colspanData; ?>'>DATA PEGAWAI</th>
	</tr>
	<tr>
		<th colspan='<?php echo $colspanPribadi; ?>'>Data Pribadi</th>
		<th colspan='2'>Kompetensi Teknis</th>
		<th colspan='2'>Kompetensi Managerial</th>
		<?php
			if($this->session->userdata("user_input")["role"] == 'Admin' || $this->session->userdata("user_input")["role"] == 'Direktur'){
		?>
			<th colspan='12'>Gaji Pokok & Tunjangan</th>
			<th colspan='4'>Pengurangan</th>
			<th colspan='3'>Gaji Diterima</th>
		<?php } ?>
		<th colspan='5'>Riwayat Pendidikan</th>
		<th colspan='8'>Data Keluarga</th>
		<th colspan='3'>Data Darurat</th>
		<th colspan='2'>Catatan Perusahaan</th>
	</tr>
	<tr>
		<th>NIP</th>
		<?php
			if($this->session->userdata("user_input")["role"] == 'HRD'){
		?>
			<th>Golongan</th>
			<th>Jabatan</th>
			<th>Status</th>
		<?php } ?>
		<th>Nama Lengkap</th>
		<th>Tgl. Lahir</th>
		<th>Jns. Kelamin</th>
		<th>Alamat Domisili</th>
		<th>Alamat Asal</th>
		<th>No. Telp. Rumah</th>
		<th>No. Telp. HP</th>
		<th>Agama</th>
		<th>Status Nikah</th>
		<th>Tanggal Nikah</th>
		<th>No. KTP</th>
		<th>Tinggi Bdn</th>
		<th>Berat Bdn</th>
		<th>Gol. Darah</th>
		<th>NPWP</th>
		<th>No. Jamsostek</th>
		<th>Tgl. Masuk</th>
		<th>Tgl. Berhenti</th>
		<th>Masa Kerja</th>
		<th>Hobby</th>
		<th>Buku Bacaan</th>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
		<?php
			if($this->session->userdata("user_input")["role"] == 'Admin' || $this->session->userdata("user_input")["role"] == 'Direktur'){
		?>
			<th>Golongan</th>
			<th>Jabatan</th>
			<th>Status</th>
			<th>Gaji Pokok</th>
			<th>Tnj. Jabatan</th>
			<th>Tnj. Kehadiran</th>
			<th>Tnj. Kemahalan</th>
			<th>Gaji</th>
			<th>JAMSOSTEK & DPLK</th>
			<th>PPH PS 21 (PAJAK)/BULAN</th>
			<th>ASKES/BULAN</th>
			<th>Total Gaji</th>
			<th>Pinjaman Bunga</th>
			<th>BPJS</th>
			<th>JAMSOSTEK/DPLK</th>
			<th>Total Pengurangan</th>
			<th>Total Gaji Diterima</th>
			<th>JAMSOSTEK</th>
			<th>DPLK</th>
		<?php } ?>
		<th>Pendidikan</th>
		<th>Nama Sekolah</th>
		<th>Jurusan</th>
		<th>Kota</th>
		<th>Th. Lulus</th>
		<th>Anak Nomor</th>
		<th>Status</th>
		<th>Nama Keluarga</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket./Pekerjaan</th>
		<th>Alamat</th>
		<th>No. Telp</th>
		<th>Nama Keluarga</th>
		<th>Alamat</th>
		<th>No. Telp</th>
		<th>Pujian & Rekomendasi</th>
		<th>Insiden Kritis</th>
	</tr>
	<?php 
		$align = 'style="vertical-align:middle;"';
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td align="center" '.$align.'>="'.$value["nip"].'"</td>';
					if($this->session->userdata("user_input")["role"] == 'HRD'){
						echo '<td align="center" '.$align.'>'.$value["golongan"].'</td>
							<td align="center" '.$align.'>'.$value["jabatan"].'</td>
							<td align="center" '.$align.'>'.$value["status_pegawai"].'</td>';
					}
					echo '<td align="center" '.$align.'>'.$value["nama"].'</td>
					<td align="center" '.$align.'>'.$value["tgl_lahir"].'</td>
					<td align="center" '.$align.'>'.$value["jns_kelamin"].'</td>
					<td align="center" '.$align.'>'.$value["alamat_domisili"].'</td>
					<td align="center" '.$align.'>'.$value["alamat_asal"].'</td>
					<td align="center" '.$align.'>="'.$value["telp_rmh"].'"</td>
					<td align="center" '.$align.'>="'.$value["telp_hp"].'"</td>
					<td align="center" '.$align.'>'.$value["agama"].'</td>
					<td align="center" '.$align.'>'.$value["status_perkawinan"].'</td>
					<td align="center" '.$align.'>'.$value["tgl_nikah"].'</td>
					<td align="center" '.$align.'>="'.$value["no_ktp"].'"</td>
					<td align="center" '.$align.'>'.$value["tinggi_bdn"].'</td>
					<td align="center" '.$align.'>'.$value["berat_bdn"].'</td>
					<td align="center" '.$align.'>'.$value["gol_darah"].'</td>
					<td align="center" '.$align.'>="'.$value["npwp"].'"</td>
					<td align="center" '.$align.'>="'.$value["no_jamsostek"].'"</td>
					<td align="center" '.$align.'>'.$value["tgl_masuk"].'</td>
					<td align="center" '.$align.'>'.$value["tgl_berhenti"].'</td>';
					$date1 = date_create(date('Y-m-d'));
					if($value["tgl_berhenti"] != ''){
						$date1 = date_create($value["tgl_berhenti"]);
					}
					$date2 = date_create($value["tgl_masuk"]);
					$diff=date_diff($date1,$date2);
					echo '<td align="center" '.$align.'>'.$diff->format("%y tahun, %m bulan, %d hari").'</td>';
					echo '<td align="center" '.$align.'>'.$value["hobby"].'</td>
					<td align="center" '.$align.'>'.$value["buku_bacaan"].'</td>
					<td align="center" '.$align.'>';
				foreach ($teknis as $key => $tkns) {
					if($tkns["nip"] == $value["nip"])
					echo $tkns["nama_kompetensi"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($teknis as $key => $tkns) {
					if($tkns["nip"] == $value["nip"])
					echo $tkns["penguasaan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($managerial as $key => $mngr) {
					if($mngr["nip"] == $value["nip"])
					echo $mngr["nama_kompetensi"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($managerial as $key => $mngr) {
					if($mngr["nip"] == $value["nip"])
					echo $mngr["penguasaan"].'<br>';
				} echo '</td>';
				if(!empty($gaji) && ($this->session->userdata("user_input")["role"] == 'Admin' || $this->session->userdata("user_input")["role"] == 'Direktur')){
					echo '<td align="center" '.$align.'>'.$value["golongan"].'</td>
						<td align="center" '.$align.'>'.$value["jabatan"].'</td>
						<td align="center" '.$align.'>'.$value["status_pegawai"].'</td>';
					foreach ($gaji as $key => $gj) {
						if($gj["nip"] == $value["nip"])
						echo '<td align="center" '.$align.'>'.$gj['gaji_pokok'].'</td>
						<td align="center" '.$align.'>'.$gj["tunj_jabatan"].'</td>
						<td align="center" '.$align.'>'.($gj["total_hari"] * $gj["tunj_kehadiran"]).'</td>
						<td align="center" '.$align.'>'.$gj["tunj_kemahalan"].'</td>
						<td align="center" '.$align.'>'.($gj['gaji_pokok'] + $gj["tunj_jabatan"] + ($gj["total_hari"] * $gj["tunj_kehadiran"]) + $gj["tunj_kemahalan"]).'</td>
						<td align="center" '.$align.'>'.$gj["jamsostek_dplk"].'</td>
						<td align="center" '.$align.'>'.$gj["pph_ps"].'</td>
						<td align="center" '.$align.'>'.$gj["askes"].'</td>
						<td align="center" '.$align.'>'.($gj['jamsostek_dplk'] + $gj["pph_ps"] + $gj["askes"]).'</td>
						<td align="center" '.$align.'>'.$gj["pinjaman_bunga"].'</td>
						<td align="center" '.$align.'>'.$gj["bpjs"].'</td>
						<td align="center" '.$align.'>'.$gj["jamsostek_dplk_pengurangan"].'</td>
						<td align="center" '.$align.'>'.($gj['pinjaman_bunga'] + $gj["bpjs"] + $gj["jamsostek_dplk_pengurangan"]).'</td>
						<td align="center" '.$align.'></td>
						<td align="center" '.$align.'>'.$gj["jamsostek"].'</td>
						<td align="center" '.$align.'>'.$gj["dplk"].'</td>';
					}
				}
				echo '<td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["nip"] == $value["nip"])
					echo $pend["pendidikan"].'<br>';	
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["nip"] == $value["nip"])
					echo $pend["nama_sekolah"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["nip"] == $value["nip"])
					echo $pend["jurusan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["nip"] == $value["nip"])
					echo $pend["kota"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["nip"] == $value["nip"])
					echo $pend["thn_lulus"].'<br>';
				} echo '</td>';
				echo '<td align="center" '.$align.'>'.$value["anak_no"].'</td>
				<td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["nip"] == $value["nip"])
					echo $ot["orang_tua"].' '.$ot["status"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["nip"] == $value["nip"])
					echo $kel["status"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["nip"] == $value["nip"])
					echo $ot["nama"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["nip"] == $value["nip"])
					echo $kel["nama"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["nip"] == $value["nip"])
					echo $ot["tgl_lahir"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["nip"] == $value["nip"])
					echo $kel["tgl_lahir"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["nip"] == $value["nip"])
					echo $ot["pendidikan"].'<br>';
				}
				foreach ($keluarga as $key => $kel) {
					if($kel["nip"] == $value["nip"])
					echo $kel["pendidikan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["nip"] == $value["nip"])
					echo $ot["ket_pekerjaan"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["nip"] == $value["nip"])
					echo $kel["ket_pekerjaan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["nip"] == $value["nip"])
					echo $ot["alamat"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["nip"] == $value["nip"])
					echo '="'.$ot["no_telp"].'"<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($darurat as $key => $drt) {
					if($drt["nip"] == $value["nip"])
					echo $drt["nama"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($darurat as $key => $drt) {
					if($drt["nip"] == $value["nip"])
					echo $drt["alamat"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($darurat as $key => $drt) {
					if($drt["nip"] == $value["nip"])
					echo '="'.$drt["no_telp"].'"<br>';
				} echo '</td>';
				echo '<td align="center" '.$align.'>'.$value["pujian_rekomendasi"].'</td>
					<td align="center" '.$align.'>'.$value["insiden_kritis"].'</td>';
			}
		}
	?>
</table>