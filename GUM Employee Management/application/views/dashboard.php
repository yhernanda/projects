<!DOCTYPE html>
<html lang="em">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>PT. GUM DB Pegawai</title>
    <?php include "dashboard-js.php"; ?>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Junction.otf">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/timeline.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sb-admin-2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/morris.css">

    <!-- DataTables CSS & JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>

	<!-- JQuery & JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/js.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/raphael-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/morris.min.js"></script>

</head>
<body>
    <div id="wrapper">
        <?php include "navbar.php"; ?>

        <div id="page-wrapper">
            <div style="padding-top:40px;">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-stats"></i> Chart Jenis Kelamin Pegawai
                        </div>
                        <div class="panel-body">
                            <div id="jns_kelamin_chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>    
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-stats"></i> Chart Pendidikan Terakhir Pegawai
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="data_pendidikan_chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-stats"></i> Chart Golongan Pegawai
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="data_golongan_chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-stats"></i> Chart Jabatan Pegawai
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="data_jabatan_chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>