<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PT. GUM DB Pegawai</title>
	<?php include "javascript.php"; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/inputdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.vertical-tabs.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/timeline.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sb-admin-2.css">

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>
</head>
<body>
	<div id="wrapper">
		<?php include "navbar.php"; ?>
		<script>
			$('.edit_pelamar').removeClass('hidden');
			$('.edit_pelamar').removeClass('hidden');
		</script>

	    <div id="page-wrapper" style="font-size:12px;">
			<div class="col-md-10">
				<div class="data-container tab-content" >
					<div class="tab-pane active" id="a">
						<div class="col-md-12 customtitle">
							<h1>Update Data Pribadi Pelamar</h1>
						</div>
						<form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" id="updateDataPribadiPelamar">
							<div class="col-md-12">
								<div class="form-group" style="float:left; margin-left:20px; margin-right:10px;">
									<div class="col-md-3">
										<?php if($pelamar[0]['foto'] != ""){ ?>
											<img src="<?php echo base_url().$pelamar[0]['foto']; ?>" id="picture" data-toggle='tooltip' data-placement='bottom' title='Ganti Gambar'>
										<?php }else{ ?>
											<img src="<?php echo base_url(); ?>assets/img/default_user.jpg" id="picture" data-toggle='tooltip' data-placement='bottom' title='Ganti Gambar'>
										<?php } ?>
										<input id="btn-upload" type="file" name="picture" style="display:none;" onchange="preview(this)">
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">			
								    	<label class="control-label col-md-3" >Nomor Urut Pelamar </label>
										<div class="col-md-9">
											<input required style="display:none;" type="text" class="form-control" id="np-old" name="np-old" placeholder="Nomor Urut Pelamar" value="<?php echo $pelamar[0]['np']; ?>" />
											<input required type="text" class="form-control" id="np" name="np" placeholder="Nomor Urut Pelamar" value="<?php echo $pelamar[0]['np']; ?>" />
										</div>
									</div>
									<div class="form-group">			
								    	<label class="control-label col-md-3" >Nama Lengkap </label>
										<div class="col-md-9">
											<input required type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap Pelamar" value="<?php echo $pelamar[0]['nama']; ?>" />
										</div>
									</div>
									<div class="form-group">			
							            <label class="control-label col-md-3" >Jenis Kelamin </label>
										<div class="col-md-9">
											<div class="radio-list">
												<input required type="radio" style="margin-left: 0px" name="jns_kelamin" id="newJenisKelamin" value="1" data-title="Pria" <?php if($pelamar[0]['jns_kelamin'] == "Laki-laki"){ ?> checked="checked" <?php } ?>/><span style="margin-left:5px">Laki-laki</span> 
												<input required type="radio" style="margin-left: 10px" name="jns_kelamin" id="newJenisKelamin2" value="2" data-title="Wanita"<?php if($pelamar[0]['jns_kelamin'] == "Perempuan"){ ?> checked="checked" <?php } ?>/><span style="margin-left:5px">Perempuan</span>
											</div>
										</div>
									</div>
									<div class="form-group">			
						            	<label class="control-label col-md-3" >Tanggal Lahir </label>
										<div class="col-md-9">
											<div class="input-group">
											  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
											  	<input required type="text" class="form-control" data-provide ="datepicker" name="tgl_lahir" date-date-format="dd/mm/yyyy" placeholder="Tanggal Lahir" value="<?php echo date('d-m-Y', strtotime($pelamar[0]['tgl_lahir'])); ?>">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Alamat Domisili</label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="alamat_domisili" name="alamat_domisili" placeholder="Alamat Domisili" value="<?php echo $pelamar[0]['alamat_domisili']; ?>" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Alamat Asal</label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="alamat_asal" name="alamat_asal" placeholder="Alamat Asal" value="<?php echo $pelamar[0]['alamat_asal']; ?>" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >No. Telp. Rumah</label>
								<div class="col-md-4">
									<input type="text" class="form-control num" id="telp_rumah" name="telp_rmh" placeholder="Nomor Telepon Rumah" value="<?php echo $pelamar[0]['telp_rmh']; ?>" />
								</div>
								<label class="control-label col-md-1" >No. HP</label>
								<div class="col-md-4">
									<input required type="text" class="form-control num" id="telp_hp" name="telp_hp" placeholder="Nomor Telepon Seluler" value="<?php echo $pelamar[0]['telp_hp']; ?>" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Agama </label>
								<div class="col-md-9">
									<select required class="form-control select" name="agama" id="agama">
										<option value="1" <?php if ($pelamar[0]['agama'] == "1") { ?> selected="selected" <?php } ?>>Kristen</option>
										<option value="2" <?php if ($pelamar[0]['agama'] == "2") { ?> selected="selected" <?php } ?>>Katolik</option>
										<option value="3" <?php if ($pelamar[0]['agama'] == "3") { ?> selected="selected" <?php } ?>>Islam</option>
										<option value="4" <?php if ($pelamar[0]['agama'] == "4") { ?> selected="selected" <?php } ?>>Hindu</option>
										<option value="5" <?php if ($pelamar[0]['agama'] == "5") { ?> selected="selected" <?php } ?>>Budha</option>
										<option value="6" <?php if ($pelamar[0]['agama'] == "6") { ?> selected="selected" <?php } ?>>Konghucu</option>
									</select>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Status Nikah </label>
								<div class="col-md-9">
									<select required class="form-control select isian" name="status_perkawinan" id="status_perkawinan">
										<option value="1" <?php if ($pelamar[0]['status_perkawinan'] == "Menikah") { ?> selected="selected" <?php } ?>>Menikah</option>
										<option value="2" <?php if ($pelamar[0]['status_perkawinan'] == "Belum Menikah") { ?> selected="selected" <?php } ?>>Belum Menikah</option>
									</select>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Tanggal Nikah </label>
								<div class="col-md-9">
									<div class="input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
									  	<input required type="text" class="form-control" data-provide ="datepicker" name="tgl_nikah" id="tgl_nikah" date-date-format="dd/mm/yyyy" placeholder="Tanggal Pernikahan" <?php if ($pelamar[0]['status_perkawinan'] == "Belum Menikah") { ?> disabled="disabled" <?php } else { ?> value="<?php echo date('d-m-Y', strtotime($pelamar[0]['tgl_nikah'])); ?>" <?php } ?>>
									</div>
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Nomor KTP</label>
								<div class="col-md-9">
									<input required type="text" class="form-control num" id="no_ktp" name="no_ktp" placeholder="Nomor Kartu Tanda Penduduk" value="<?php echo $pelamar[0]['no_ktp']; ?>" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Tinggi Badan</label>
								<div class="col-md-4">
									<input required type="text" class="form-control num" id="tinggi_badan" name="tinggi_bdn" placeholder="Tinggi Badan"  value="<?php echo $pelamar[0]['tinggi_bdn']; ?>" />
								</div>
								<label class="control-label col-md-1" >Berat</label>
								<div class="col-md-4">
									<input required type="text" class="form-control num" id="berat_badan" name="berat_bdn" placeholder="Berat Badan"  value="<?php echo $pelamar[0]['berat_bdn']; ?>" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Golongan Darah </label>
								<div class="col-md-9">
									<select required class="form-control select" name="gol_darah" id="gol_darah">
										<option value="1" <?php if ($pelamar[0]['gol_darah'] == "A") { ?> selected="selected" <?php } ?>>A</option>
										<option value="2" <?php if ($pelamar[0]['gol_darah'] == "B") { ?> selected="selected" <?php } ?>>B</option>
										<option value="3" <?php if ($pelamar[0]['gol_darah'] == "AB") { ?> selected="selected" <?php } ?>>AB</option>
										<option value="4" <?php if ($pelamar[0]['gol_darah'] == "O") { ?> selected="selected" <?php } ?>>O</option>
									</select>
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >NPWP </label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="npwp" name="npwp" placeholder="Nomor Pokok Wajib Pajak"  value="<?php echo $pelamar[0]['npwp']; ?>" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >No. Jamsostek </label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="no_jamsostek" name="no_jamsostek" placeholder="Nomor Jaminan Sosial Tenaga Kerja" value="<?php echo $pelamar[0]['no_jamsostek']; ?>" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Tanggal Daftar </label>
								<div class="col-md-9">
									<div class="input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
									  	<input required type="text" class="form-control" data-provide ="datepicker" name="tgl_daftar" date-date-format="dd/mm/yyyy" placeholder="Tanggal Masuk" value="<?php echo date('d-m-Y', strtotime($pelamar[0]['tgl_daftar'])); ?>">
									</div>
								</div>
							</div>


							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Hobby </label>
								<div class="col-md-9">
									<div class="btn-toolbar" data-role="hobbyEditor-toolbar" data-target="#hobbyEditor">
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle='dropdown' id="tool" data-placement='top' title='Font' data-original-title="Font"><i class="glyphicon glyphicon-font"></i><b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Font Size' data-original-title="Font Size"><i class="glyphicon glyphicon-text-height"></i>&nbsp;<b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
										  <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
										  <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
										  </ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="bold" id="tool" data-placement='top' title='Bold' data-original-title="Bold (Ctrl/Cmd+B)"><i class="glyphicon glyphicon-bold"></i></a>
										<a class="btn btn-primary" data-edit="italic" id="tool" data-placement='top' title='Italic' data-original-title="Italic (Ctrl/Cmd+I)"><i class="glyphicon glyphicon-italic"></i></a>
										<a class="btn btn-primary" data-edit="underline" id="tool" data-placement='top' title='Underline' data-original-title="Underline (Ctrl/Cmd+U)"><i class="glyphicon glyphicon-text-width"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="insertunorderedlist" id="tool" data-placement='top' title='Bullet List' data-original-title="Bullet list"><i class="glyphicon glyphicon-list"></i></a>
										<a class="btn btn-primary" data-edit="insertorderedlist" id="tool" data-placement='top' title='Number List' data-original-title="Number list"><i class="glyphicon glyphicon-list-alt"></i></a>
										<a class="btn btn-primary" data-edit="outdent" id="tool" data-placement='top' title='Outdent' data-original-title="Reduce indent (Shift+Tab)"><i class="glyphicon glyphicon-indent-left"></i></a>
										<a class="btn btn-primary" data-edit="indent" id="tool" data-placement='top' title='Indent' data-original-title="Indent (Tab)"><i class="glyphicon glyphicon-indent-right"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="justifyleft" id="tool" data-placement='top' title='Align Left' data-original-title="Align Left (Ctrl/Cmd+L)"><i class="glyphicon glyphicon-align-left"></i></a>
										<a class="btn btn-primary" data-edit="justifycenter" id="tool" data-placement='top' title='Center' data-original-title="Center (Ctrl/Cmd+E)"><i class="glyphicon glyphicon-align-center"></i></a>
										<a class="btn btn-primary" data-edit="justifyright" id="tool" data-placement='top' title='Align Right' data-original-title="Align Right (Ctrl/Cmd+R)"><i class="glyphicon glyphicon-align-right"></i></a>
										<a class="btn btn-primary" data-edit="justifyfull" id="tool" data-placement='top' title='Justify' data-original-title="Justify (Ctrl/Cmd+J)"><i class="glyphicon glyphicon-align-justify"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Hyperlink' data-original-title="Hyperlink"><i class="glyphicon glyphicon-link"></i></a>
										<div class="dropdown-menu input-append">
										  <input class="span2" placeholder="URL" type="text" data-edit="createLink">
										  <button class="btn" type="button">Add</button>
										</div>
										<a class="btn btn-primary" data-edit="unlink" id="tool" data-placement='top' title='Remove Hyperlink' data-original-title="Remove Hyperlink"><i class="glyphicon glyphicon-remove"></i></a>

										</div>

										<div class="btn-group">
										<a class="btn btn-primary tool" data-placement='top' title='Insert Picture (or just drag and drop)' id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="glyphicon glyphicon-picture"></i></a>
										<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="undo" id="tool" data-placement='top' title='Undo' data-original-title="Undo (Ctrl/Cmd+Z)"><i class="glyphicon glyphicon-backward"></i></a>
										<a class="btn btn-primary" data-edit="redo" id="tool" data-placement='top' title='Redo' data-original-title="Redo (Ctrl/Cmd+Y)"><i class="glyphicon glyphicon-forward"></i></a>
										</div>
									</div>
									
									<input id="hobbyInput" type="text" style="display:none;" name="hobby">
									<div id="hobbyEditor" name="hobby" type="text" class="form-control Editor">
										<?php echo $pelamar[0]["hobby"]; ?>
									</div>
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Buku dll. yang anda baca </label>
								<div class="col-md-9">
									<div class="btn-toolbar" data-role="bukuEditor-toolbar" data-target="#bukuEditor">
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle='dropdown' id="tool" data-placement='top' title='Font' data-original-title="Font"><i class="glyphicon glyphicon-font"></i><b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Font Size' data-original-title="Font Size"><i class="glyphicon glyphicon-text-height"></i>&nbsp;<b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
										  <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
										  <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
										  </ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="bold" id="tool" data-placement='top' title='Bold' data-original-title="Bold (Ctrl/Cmd+B)"><i class="glyphicon glyphicon-bold"></i></a>
										<a class="btn btn-primary" data-edit="italic" id="tool" data-placement='top' title='Italic' data-original-title="Italic (Ctrl/Cmd+I)"><i class="glyphicon glyphicon-italic"></i></a>
										<a class="btn btn-primary" data-edit="underline" id="tool" data-placement='top' title='Underline' data-original-title="Underline (Ctrl/Cmd+U)"><i class="glyphicon glyphicon-text-width"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="insertunorderedlist" id="tool" data-placement='top' title='Bullet List' data-original-title="Bullet list"><i class="glyphicon glyphicon-list"></i></a>
										<a class="btn btn-primary" data-edit="insertorderedlist" id="tool" data-placement='top' title='Number List' data-original-title="Number list"><i class="glyphicon glyphicon-list-alt"></i></a>
										<a class="btn btn-primary" data-edit="outdent" id="tool" data-placement='top' title='Outdent' data-original-title="Reduce indent (Shift+Tab)"><i class="glyphicon glyphicon-indent-left"></i></a>
										<a class="btn btn-primary" data-edit="indent" id="tool" data-placement='top' title='Indent' data-original-title="Indent (Tab)"><i class="glyphicon glyphicon-indent-right"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="justifyleft" id="tool" data-placement='top' title='Align Left' data-original-title="Align Left (Ctrl/Cmd+L)"><i class="glyphicon glyphicon-align-left"></i></a>
										<a class="btn btn-primary" data-edit="justifycenter" id="tool" data-placement='top' title='Center' data-original-title="Center (Ctrl/Cmd+E)"><i class="glyphicon glyphicon-align-center"></i></a>
										<a class="btn btn-primary" data-edit="justifyright" id="tool" data-placement='top' title='Align Right' data-original-title="Align Right (Ctrl/Cmd+R)"><i class="glyphicon glyphicon-align-right"></i></a>
										<a class="btn btn-primary" data-edit="justifyfull" id="tool" data-placement='top' title='Justify' data-original-title="Justify (Ctrl/Cmd+J)"><i class="glyphicon glyphicon-align-justify"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Hyperlink' data-original-title="Hyperlink"><i class="glyphicon glyphicon-link"></i></a>
										<div class="dropdown-menu input-append">
										  <input class="span2" placeholder="URL" type="text" data-edit="createLink">
										  <button class="btn" type="button">Add</button>
										</div>
										<a class="btn btn-primary" data-edit="unlink" id="tool" data-placement='top' title='Remove Hyperlink' data-original-title="Remove Hyperlink"><i class="glyphicon glyphicon-remove"></i></a>

										</div>

										<div class="btn-group">
										<a class="btn btn-primary tool" data-placement='top' title='Insert Picture (or just drag and drop)' id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="glyphicon glyphicon-picture"></i></a>
										<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="undo" id="tool" data-placement='top' title='Undo' data-original-title="Undo (Ctrl/Cmd+Z)"><i class="glyphicon glyphicon-backward"></i></a>
										<a class="btn btn-primary" data-edit="redo" id="tool" data-placement='top' title='Redo' data-original-title="Redo (Ctrl/Cmd+Y)"><i class="glyphicon glyphicon-forward"></i></a>
										</div>
									</div>
									
									<input id="bukuInput" type="text" style="display:none;" name="buku_bacaan">
									<div id="bukuEditor" name="buku_bacaan" type="text" class="form-control Editor">
										<?php echo $pelamar[0]['buku_bacaan']; ?>
									</div>
								</div>
							</div>

							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Kompetensi Teknis </label>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="tabelTeknis">
										<thead>
											<tr class="bg-primary">
												<td width="5%" align="center" rowspan="2"><br>Aksi</td>
												<td align="center" rowspan="2"><br>Kompetensi Teknis</td>
												<td align="center" colspan="4" width="40%">Tingkat Penguasaan</td>
											</tr>
											<tr class="bg-primary">
												<td width="10%" align="center">Tidak</td>
												<td width="10%" align="center">Cukup</td>
												<td width="10%" align="center">Menguasai</td>
												<td width="10%" align="center">Mahir</td>
											</tr>
										</thead>
										<tbody id="teknis" style="background-color:#f9f9f9;">
											<?php
											If(isset($teknis)){
											$count = 0;
											foreach ($teknis as $key => $value) {
												$dynamic='<tr>
													<td class="hidden"><input type="text" class="hidden id_kompetensi" name="teknis['.$count.'][id]" value="'.$value["id_teknis_pelamar"].'"></td>
													<td align="center"><a style="cursor:pointer;" class="hapusTeknisPelamarEdit" id="tool" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a></td>
													<td>'.$value["nama_kompetensi"].'<input type="text" class="hidden" name="teknis['.$count.'][name]" value="'.$value["nama_kompetensi"].'"></td>
													<td align="center"><input required type="radio" name="teknis['.$count.'][value]" value="1" ';
												if($value["penguasaan"] == "Tidak/Kurang") {$dynamic.='checked="checked"';}
												$dynamic.='></td>
													<td align="center"><input required type="radio" name="teknis['.$count.'][value]" value="2" ';
												if($value["penguasaan"] == "Cukup") {$dynamic.='checked="checked"';}
												$dynamic.='></td>
													<td align="center"><input required type="radio" name="teknis['.$count.'][value]" value="3" ';
												if($value["penguasaan"] == "Menguasai") {$dynamic.='checked="checked"';}
												$dynamic.='></td>
													<td align="center"><input required type="radio" name="teknis['.$count.'][value]" value="4" ';
												if($value["penguasaan"] == "Mahir") {$dynamic.='checked="checked"';}
												$dynamic.='></td></tr>';
												echo $dynamic;
												$count += 1;
											}} ?>
										</tbody>
									</table>
									<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#modalTeknis"><i class="glyphicon glyphicon-plus"></i> Tambah Kompetensi</button>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Kompetensi Managerial </label>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="tabelManagerial">
										<thead>
											<tr class="bg-primary">
												<td width="5%" align="center" rowspan="2"><br>Aksi</td>
												<td align="center" rowspan="2"><br>Kompetensi Managerial</td>
												<td align="center" colspan="4" width="40%">Tingkat Penguasaan</td>
											</tr>
											<tr class="bg-primary">
												<td width="10%" align="center">Tidak</td>
												<td width="10%" align="center">Cukup</td>
												<td width="10%" align="center">Menguasai</td>
												<td width="10%" align="center">Mahir</td>
											</tr>
										</thead>
										<tbody id="managerial" style="background-color:#f9f9f9;">
											<?php
											if(isset($managerial)){
											$count = 0;
											foreach ($managerial as $key => $value) {
												$dynamic='<tr>
													<td class="hidden"><input type="text" class="hidden id_kompetensi" name="managerial['.$count.'][id]" value="'.$value["id_managerial_pelamar"].'"></td>
													<td align="center"><a style="cursor:pointer;" class="hapusManagerialPelamarEdit" id="tool" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a></td>
													<td>'.$value["nama_kompetensi"].'<input type="text" class="hidden" name="managerial['.$count.'][name]" value="'.$value["nama_kompetensi"].'"></td>
													<td align="center"><input required type="radio" name="managerial['.$count.'][value]" value="1" ';
												if($value["penguasaan"] == "Tidak/Kurang") $dynamic.='checked="checked"';
												$dynamic.='></td>
													<td align="center"><input required type="radio" name="managerial['.$count.'][value]" value="2" ';
												if($value["penguasaan"] == "Cukup") $dynamic.='checked="checked"';
												$dynamic.='></td>
													<td align="center"><input required type="radio" name="managerial['.$count.'][value]" value="3" ';
												if($value["penguasaan"] == "Menguasai") $dynamic.='checked="checked"';
												$dynamic.='></td>
													<td align="center"><input required type="radio" name="managerial['.$count.'][value]" value="4" ';
												if($value["penguasaan"] == "Mahir") $dynamic.='checked="checked"';
												$dynamic.='></td></tr>';
												echo $dynamic;
												$count += 1;
											}} ?>
										</tbody>
									</table>
									<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#modalManagerial"><i class="glyphicon glyphicon-plus"></i> Tambah Kompetensi</button>
								</div>
							</div>
							

							<div class="form-group">			
				            	<div class="col-md-12">
				            		<div class="col-md-5 pull-right custombutton">
				            			<a href="<?php echo base_url(); ?>update/exportdata_detail_pelamar/<?php echo $pelamar[0]['np']; ?>" class="btn btn-primary" id="tool" data-toggle="tooltip" data-placement="top" title="Export Data"><i class="glyphicon glyphicon-download-alt"></i> Export to Excel</a>					            		
					            		<button class="btn btn-success submitPribadi" id="tool" data-toggle="tooltip" data-placement="top" title="Simpan Data Pelamar" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
					            	</div>
			            		</div>
							</div>
						</form>
					</div> <!-- Tab Pribadi -->




					<!-- RIWAYAT PENDIDIKAN & KELUARGA PEGAWAI -->
					<div class="tab-pane" id="b">
						<div class="col-md-12 customtitle">
							<h1>Riwayat Pendidikan & Keluarga Pelamar</h1>
						</div>
						<form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" id="dataKeluargaPegawai" action="<?php echo base_url(); ?>update/update_keluarga_pelamar">
							<div id="bottom" class="form-group hidden">			
						    	<label class="control-label col-md-2" >Nomor Urut Pelamar </label>
								<div class="col-md-9">
									<input required type="text" required class="form-control" id="npKeluarga" data-toggle="modal" data-target="#npPegawai" name="np" placeholder="Nomor Induk Pegawai" value="<?php echo $pelamar[0]["np"]; ?>"/>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Riwayat Pendidikan </label>
								<div class="col-md-9">
									<table class="table table-hover" id="tabelPendidikan">
										<thead>
											<tr class="bg-primary">
												<td align="center">Aksi</td>
												<td align="center" width="17%">Pendidikan</td>
												<td align="center">Nama Sekolah</td>
												<td align="center" width="15%">Jurusan</td>
												<td align="center" width="20%">Kota</td>
												<td align="center" width="15%">Th. Lulus</td>
											</tr>
										</thead>
										<tbody id="pendidikan" style="background-color:#f9f9f9;">
											<?php
											if(isset($pendidikan)){
												$count = 0;
												foreach ($pendidikan as $value) {
													$ec = '<tr>
														<td class="hidden"><input required type="text" class="hidden id_pendidikan" name="pendidikan['.$count.'][id_pendidikan_pelamar]" value="'.$value["id_pendidikan_pelamar"].'"></td>
														<td style="vertical-align:middle;" align="center"><a style="cursor:pointer;" class="hapusPendidikanPelamarEdit" id="tool" data-placement="top" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a></td>
														<td class="hidden id_pendidikan">'.$value['id_pendidikan_pelamar'].'</td>
														<td>
															<select required class="form-control select" name="pendidikan['.$count.'][pendidikan]">
																<option value="1" ';
																if($value['pendidikan'] == "SD") $ec.='selected="selected"';
																$ec.='>SD</option>
																<option value="2" ';
																if($value['pendidikan'] == "SMP/Sederajat") $ec.='selected="selected"';
																$ec.='>SMP</option>
																<option value="3" ';
																if($value['pendidikan'] == "SMA/Sederajat") $ec.='selected="selected"';
																$ec.='>SMA</option>
																<option value="4" ';
																if($value['pendidikan'] == "Diploma 1") $ec.='selected="selected"';
																$ec.='>Diploma 1</option>
																<option value="5" ';
																if($value['pendidikan'] == "Diploma 2") $ec.='selected="selected"';
																$ec.='>Diploma 2</option>
																<option value="6" ';
																if($value['pendidikan'] == "Diploma 3") $ec.='selected="selected"';
																$ec.='>Diploma 3</option>
																<option value="7" ';
																if($value['pendidikan'] == "Strata 1") $ec.='selected="selected"';
																$ec.='>Strata 1</option>
																<option value="8" ';
																if($value['pendidikan'] == "Strata 2") $ec.='selected="selected"';
																$ec.='>Strata 2</option>
																<option value="9" ';
																if($value['pendidikan'] == "Strata 3") $ec.='selected="selected"';
																$ec.='>Strata 3</option>
																<option value="10" ';
																if($value['pendidikan'] == "Akademis") $ec.='selected="selected"';
																$ec.='>Akademis</option>
															</select>
														</td>
														<td><input required type="text" class="form-control" name="pendidikan['.$count.'][nama]" placeholder="Nama Sekolah" value="'.$value["nama_sekolah"].'"></td>
														<td><input type="text" class="form-control" name="pendidikan['.$count.'][jurusan]" placeholder="Jurusan" value="'.$value["jurusan"].'"/></td>
														<td><input required type="text" class="form-control" name="pendidikan['.$count.'][kota]" placeholder="Kota Sekolah" value="'.$value["kota"].'"/></td>
														<td><input required type="text" class="form-control num" name="pendidikan['.$count.'][lulus]" placeholder="Tahun Lulus" value="'.$value["thn_lulus"].'"/></td>
													</tr>';
													echo $ec;
													$count+=1;
												}
											}
											?>
										</tbody>
									</table>
									<button type="button" id="tambahPendidikan" class="btn btn-info pull-right"><i class="glyphicon glyphicon-plus"></i> Tambah Pendidikan</button>
								</div>
							</div>
							<div id="bottom" class="form-group">
								<label class="control-label col-md-2">Anak Nomor</label>
								<div class="col-md-9">
									<input required type="text" class="form-control num" name="anak_nomor" placeholder="Urutan anda dalam silsilah anak Keluarga" value="<?php echo $pelamar[0]["anak_no"]; ?>">
								</div>
							</div>
							<div id="bottom" class="form-group">
								<label class="control-label col-md-2">Susunan Keluarga</label>
								<div class="col-md-9">
									<table class="table table-hover" id="tabelOrtu">
										<thead>
											<tr class="bg-primary">
												<td colspan="6" align="center">Orang Tua</td>
											</tr>
											<tr class="bg-primary">
												<td align="center">Status</td>
												<td align="center">Nama</td>
												<td align="center">Tgl. Lahir</td>
												<td align="center">Pendidikan</td>
												<td align="center">Ket/Pekerjaan</td>
											</tr>
										</thead>
										<tbody id="rowOrtu" style="background-color:#f9f9f9;">
											<?php
											$i = 0;
											foreach ($orang_tua as $value) {
												$status = 1;
												$ortu = "Ayah";
												$statusOrtu = "Kandung";
												if($i == 1 || $i == 3) $ortu = "Ibu"; 
												if($i > 1){ $status += 1; $statusOrtu = "Mertua"; }
												$print = '<tr>
													<td class="hidden"><input required type="text" id="ortu'.$statusOrtu.'" class="hidden id_orang_tua" name="ortu['.$i.'][id_orang_tua_pelamar]" value="'.$value["id_orang_tua_pelamar"].'"></td>';
													$print .= '<td class="hidden"><input required type="text" name="ortu['.$i.'][status]" value="'.$status.'"></td>
													<td style="vertical-align:middle;">'.$ortu.' '.$statusOrtu.'<input required type="text" class="hidden" name="ortu['.$i.'][orang_tua]" value="'.$ortu.'"></td>
													<td><input required type="text" name="ortu['.$i.'][nama]" class="form-control" placeholder="Nama '.$ortu.'" value="'.$value["nama"].'"></td>
													<td><input type="text" class="form-control" data-provide ="datepicker" name="ortu['.$i.'][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir '.$ortu.'" value="'.date('d-m-Y', strtotime($value['tgl_lahir'])).'"></td>
													<td>
														<select required name="ortu['.$i.'][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir '.$ortu.'">
															<option value="1" '; if($value["pendidikan"] == "SD") { $print .= 'selected="selected"'; } $print .= '>SD</option>
															<option value="2" '; if($value["pendidikan"] == "SMP/Sederajat") { $print .= 'selected="selected"'; } $print .= '>SMP</option>
															<option value="3" '; if($value["pendidikan"] == "SMA/Sederajat") { $print .= 'selected="selected"'; } $print .= '>SMA</option>
															<option value="4" '; if($value["pendidikan"] == "Diploma 1") { $print .= 'selected="selected"'; } $print .= '>Diploma 1</option>
															<option value="5" '; if($value["pendidikan"] == "Diploma 2") { $print .= 'selected="selected"'; } $print .= '>Diploma 2</option>
															<option value="6" '; if($value["pendidikan"] == "Diploma 3") { $print .= 'selected="selected"'; } $print .= '>Diploma 3</option>
															<option value="7" '; if($value["pendidikan"] == "Strata 1") { $print .= 'selected="selected"'; } $print .= '>Strata 1</option>
															<option value="8" '; if($value["pendidikan"] == "Strata 2") { $print .= 'selected="selected"'; } $print .= '>Strata 2</option>
															<option value="9" '; if($value["pendidikan"] == "Strata 3") { $print .= 'selected="selected"'; } $print .= '>Strata 3</option>
															<option value="10" '; if($value["pendidikan"] == "Akademis") { $print .= 'selected="selected"'; } $print .= '>Akademis</option>
														</select>	
													</td>
													<td><input required type="text" name="ortu['.$i.'][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan '.$ortu.'" value="'.$value["ket_pekerjaan"].'"></td>
												</tr>
												';
												if($i == 1 || $i == 3){
													$print .= '<tr>
														<td style="vertical-align:middle;">Alamat</td>
														<td colspan="4"><input required type="text" name="ortu['.$i.'][alamat]" class="form-control" value="'.$value["alamat"].'"></td>
													</tr>
													<tr>
														<td style="vertical-align:middle;">Nomor Telepon</td>
														<td colspan="4"><input type="text" name="ortu['.$i.'][no_telp]" class="form-control num" value="'.$value["no_telp"].'"></td>
													</tr>';	
												}
												echo $print;
												$i += 1;
											}
											?>
										</tbody>
									</table>
									<div class="pull-right">
										<button type="button" id="tambahOrtu" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Tambah Data Orang Tua</button>
										<button type="button" id="tambahMertua" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Tambah Data Mertua</button>
									</div>
									<br><br><br>
									<table class="table table-hover" id="tabelLainnya">
										<thead>
											<tr class="bg-primary">
												<td colspan="6" align="center">Lainnya</td>
											</tr>
											<tr class="bg-primary">
												<td align="center" width="15%">Status</td>
												<td align="center">Nama</td>
												<td align="center">Tgl. Lahir</td>
												<td align="center">Pendidikan</td>
												<td align="center">Ket/Pekerjaan</td>
												<td align="center" width="10%">Aksi</td>
											</tr>
										</thead>
										<tbody id="rowSaudara" style="background-color:#f9f9f9;">
											<?php
											$count = 0; 
											foreach ($keluarga as $kel) {
												if($kel['status'] == "Kakak" || $kel['status'] == "Adik"){
													?>
													<tr>
														<td class="hidden"><input required type="text" class="hidden id_keluarga" name="kel[<?php echo $count; ?>][id_keluarga_pelamar]" value="<?php echo $kel["id_keluarga_pelamar"]; ?>"></td>
														<td>
															<select required class="form-control select" name="kel[<?php echo $count; ?>][status]">
																<option value="1" <?php if($kel['status'] == "Kakak") { ?> selected="selected" <?php } ?>>Kakak</option>
																<option value="2" <?php if($kel['status'] == "Adik") { ?> selected="selected" <?php } ?>>Adik</option>
															</select>
														</td>
														<td><input required type="text" name="kel[<?php echo $count; ?>][nama]" class="form-control" placeholder="Nama Keluarga" value="<?php echo $kel['nama']; ?>"></td>
														<td><input type="text" class="form-control" data-provide ="datepicker" name="kel[<?php echo $count; ?>][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Keluarga" value="<?php echo date('d-m-Y', strtotime($kel['tgl_lahir'])); ?>"></td>
														<td>
															<select required name="kel[<?php echo $count; ?>][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Keluarga">
																<option value="1" <?php if($kel['pendidikan'] == "SD") { ?> selected="selected" <?php } ?>>SD</option>
																<option value="2" <?php if($kel['pendidikan'] == "SMP/Sederajat") { ?> selected="selected" <?php } ?>>SMP</option>
																<option value="3" <?php if($kel['pendidikan'] == "SMA/Sederajat") { ?> selected="selected" <?php } ?>>SMA</option>
																<option value="4" <?php if($kel['pendidikan'] == "Diploma 1") { ?> selected="selected" <?php } ?>>Diploma 1</option>
																<option value="5" <?php if($kel['pendidikan'] == "Diploma 2") { ?> selected="selected" <?php } ?>>Diploma 2</option>
																<option value="6" <?php if($kel['pendidikan'] == "Diploma 3") { ?> selected="selected" <?php } ?>>Diploma 3</option>
																<option value="7" <?php if($kel['pendidikan'] == "Strata 1") { ?> selected="selected" <?php } ?>>Strata 1</option>
																<option value="8" <?php if($kel['pendidikan'] == "Strata 2") { ?> selected="selected" <?php } ?>>Strata 2</option>
																<option value="9" <?php if($kel['pendidikan'] == "Strata 3") { ?> selected="selected" <?php } ?>>Strata 3</option>
																<option value="10" <?php if($kel['pendidikan'] == "Akademis") { ?> selected="selected" <?php } ?>>Akademis</option>
															</select>	
														</td>
														<td><input required type="text" name="kel[<?php echo $count; ?>][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Keluarga" value="<?php echo $kel["ket_pekerjaan"]; ?>"></td>
														<td style="vertical-align:middle" align="center">
															<a style='cursor:pointer;' class="hapusKeluargaPelamarEdit" data-placement="top" title="Hapus Keluarga" id="tool"><i class="glyphicon glyphicon-trash"></i></a>
														</td>
													</tr>
													<?php
													$count += 1;
												}
											} ?> 
										</tbody>
										<tbody id="rowPasangan" style="background-color:#f9f9f9;">
											<?php
											foreach ($keluarga as $kel) { 
												if ($kel['status'] == "Suami" || $kel['status'] == "Isteri") { ?>
													<tr>
														<td class="hidden"><input required type="text" class="hidden id_keluarga" name="kel[<?php echo $count; ?>][id_keluarga_pelamar]" value="<?php echo $kel["id_keluarga_pelamar"]; ?>"></td>
														<td>
															<select required class="form-control select" name="kel[<?php echo $count; ?>][status]">
																<option value="4" <?php if($kel['status'] == "Suami") { ?> selected="selected" <?php } ?>>Suami</option>
																<option value="5" <?php if($kel['status'] == "Isteri") { ?> selected="selected" <?php } ?>>Isteri</option>
															</select>
														</td>
														<td><input required type="text" name="kel[<?php echo $count; ?>][nama]" class="form-control" placeholder="Nama Pasangan" value="<?php echo $kel['nama']; ?>"></td>
														<td><input required type="text" class="form-control" data-provide ="datepicker" name="kel[<?php echo $count; ?>][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Pasangan" value="<?php echo date('d-m-Y', strtotime($kel['tgl_lahir'])); ?>"></td>
														<td>
															<select required name="kel[<?php echo $count; ?>][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Pasangan">
																<option value="1" <?php if($kel['pendidikan'] == "SD") { ?> selected="selected" <?php } ?>>SD</option>
																<option value="2" <?php if($kel['pendidikan'] == "SMP/Sederajat") { ?> selected="selected" <?php } ?>>SMP</option>
																<option value="3" <?php if($kel['pendidikan'] == "SMA/Sederajat") { ?> selected="selected" <?php } ?>>SMA</option>
																<option value="4" <?php if($kel['pendidikan'] == "Diploma 1") { ?> selected="selected" <?php } ?>>Diploma 1</option>
																<option value="5" <?php if($kel['pendidikan'] == "Diploma 2") { ?> selected="selected" <?php } ?>>Diploma 2</option>
																<option value="6" <?php if($kel['pendidikan'] == "Diploma 3") { ?> selected="selected" <?php } ?>>Diploma 3</option>
																<option value="7" <?php if($kel['pendidikan'] == "Strata 1") { ?> selected="selected" <?php } ?>>Strata 1</option>
																<option value="8" <?php if($kel['pendidikan'] == "Strata 2") { ?> selected="selected" <?php } ?>>Strata 2</option>
																<option value="9" <?php if($kel['pendidikan'] == "Strata 3") { ?> selected="selected" <?php } ?>>Strata 3</option>
																<option value="10" <?php if($kel['pendidikan'] == "Akademis") { ?> selected="selected" <?php } ?>>Akademis</option>
															</select>	
														</td>
														<td><input required type="text" name="kel[<?php echo $count; ?>][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Pasangan" value="<?php echo $kel['ket_pekerjaan']; ?>"></td>
														<td style="vertical-align:middle" align="center">
															<a style='cursor:pointer;' class="hapusKeluargaPelamarEdit" data-placement="top" title="Hapus Pasangan" id="tool"><i class="glyphicon glyphicon-trash"></i></a>
														</td>
													</tr>		
												<?php 
												$count += 1; } 
											} ?>
										</tbody>
										<tbody id="rowAnak" style="background-color:#f9f9f9;">
											<?php
											foreach ($keluarga as $kel) { 
												if ($kel['status'] == "Anak") { ?>
													<tr>
														<td class="hidden"><input required type="text" class="hidden id_keluarga" name="kel[<?php echo $count; ?>][id_keluarga_pelamar]" value="<?php echo $kel["id_keluarga_pelamar"]; ?>"></td>
														<td style="vertical-align:middle;" align="center">
															Anak<input required type="text" class="hidden" name="kel[<?php echo $count; ?>][status]" value="3">
														</td>
														<td><input required type="text" name="kel[<?php echo $count; ?>][nama]" class="form-control" placeholder="Nama Anak" value="<?php echo $kel['nama']; ?>"></td>
														<td><input required type="text" class="form-control" data-provide ="datepicker" name="kel[<?php echo $count; ?>][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Anak" value="<?php echo date('d-m-Y', strtotime($kel['tgl_lahir'])); ?>"></td>
														<td>
															<select required name="kel[<?php echo $count; ?>][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Anak">
																<option value="1" <?php if($kel['pendidikan'] == "SD") { ?> selected="selected" <?php } ?>>SD</option>
																<option value="2" <?php if($kel['pendidikan'] == "SMP/Sederajat") { ?> selected="selected" <?php } ?>>SMP</option>
																<option value="3" <?php if($kel['pendidikan'] == "SMA/Sederajat") { ?> selected="selected" <?php } ?>>SMA</option>
																<option value="4" <?php if($kel['pendidikan'] == "Diploma 1") { ?> selected="selected" <?php } ?>>Diploma 1</option>
																<option value="5" <?php if($kel['pendidikan'] == "Diploma 2") { ?> selected="selected" <?php } ?>>Diploma 2</option>
																<option value="6" <?php if($kel['pendidikan'] == "Diploma 3") { ?> selected="selected" <?php } ?>>Diploma 3</option>
																<option value="7" <?php if($kel['pendidikan'] == "Strata 1") { ?> selected="selected" <?php } ?>>Strata 1</option>
																<option value="8" <?php if($kel['pendidikan'] == "Strata 2") { ?> selected="selected" <?php } ?>>Strata 2</option>
																<option value="9" <?php if($kel['pendidikan'] == "Strata 3") { ?> selected="selected" <?php } ?>>Strata 3</option>
																<option value="10" <?php if($kel['pendidikan'] == "Akademis") { ?> selected="selected" <?php } ?>>Akademis</option>
															</select>	
														</td>
														<td><input required type="text" name="kel[<?php echo $count; ?>][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Anak" value="<?php echo $kel['ket_pekerjaan']; ?>"></td>
														<td style="vertical-align:middle" align="center">
															<a style='cursor:pointer;' class="hapusKeluargaPelamarEdit" data-placement="top" title="Hapus Anak" id="tool"><i class="glyphicon glyphicon-trash"></i></a>
														</td>
													</tr>
												<?php 
												$count += 1; } 
											} ?>
										</tbody>
									</table>
									<div class="pull-right">
										<button type="button" id="tambahSaudara" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Tambah Data Saudara</button>
										<button type="button" id="tambahPasangan" class="btn btn-info pasangan"><i class="glyphicon glyphicon-plus"></i> Tambah Data Pasangan</button>
										<button type="button" id="tambahAnak" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Tambah Data Anak</button>
									</div>
									<br><br><br>
									<table class="table table-hover">
										<thead>
											<tr class="bg-primary">
												<td colspan="3" align="center">Keluarga yang Dapat Dihubungi Dalam Keadaan Darurat</td>
											</tr>
											<tr class="bg-primary">
												<td align="center">Nama</td>
												<td align="center">Alamat</td>
												<td align="center">No. Telepon</td>
											</tr>
										</thead>
										<tbody style="background-color:#f9f9f9;">
												<tr>
												<?php if(isset($darurat[0])) echo '<td class="hidden"><input required type="text" class="hidden id_data_darurat_pelamar" name="darurat[0][id_data_darurat_pelamar]" value="'.$darurat[0]['id_data_darurat_pelamar'].'"></td>'; ?>
												<td><input type="text" required class="form-control" name="darurat[0][nama]" placeholder="Nama Keluarga" <?php if(isset($darurat[0])) echo "value='".$darurat[0]['nama']."'" ?>></td>
												<td><input type="text" required class="form-control" name="darurat[0][alamat]" placeholder="Alamat Keluarga" <?php if(isset($darurat[0])) echo "value='".$darurat[0]['alamat']."'" ?>></td>
												<td><input type="text" required class="form-control num" name="darurat[0][no_telp]" placeholder="Nomor Keluarga" <?php if(isset($darurat[0])) echo "value='".$darurat[0]['no_telp']."'" ?>></td>
											</tr>
											<tr>
												<?php if(isset($darurat[0])) echo '<td class="hidden"><input type="text" class="hidden id_data_darurat_pelamar" name="darurat[1][id_data_darurat_pelamar]" value="'.$darurat[1]['id_data_darurat_pelamar'].'"></td>'; ?>
												<td><input type="text" class="form-control" name="darurat[1][nama]" placeholder="Nama Keluarga" <?php if(isset($darurat[1])) echo "value='".$darurat[1]['nama']."'" ?>></td>
												<td><input type="text" class="form-control" name="darurat[1][alamat]" placeholder="Alamat Keluarga" <?php if(isset($darurat[1])) echo "value='".$darurat[1]['alamat']."'" ?>></td>
												<td><input type="text" class="form-control num" name="darurat[1][no_telp]" placeholder="Nomor Keluarga" <?php if(isset($darurat[1])) echo "value='".$darurat[1]['no_telp']."'" ?>></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<!-- submit -->
							<div class="form-group">			
				            	<div class="col-md-12">
				            		<div class="col-md-3 pull-right custombutton">					            		
					            		<button class="btn btn-success" id="tool" data-toggle="tooltip" data-placement="top" title="Simpan Data Pelamar" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
					            	</div>
			            		</div>
							</div>
						</form>
					</div> <!-- Tab Pendidikan -->	

					<!-- MODAL  -->
					<!-- CONVERT PELAMAR -->
					<div class="modal fade" id="modalAngkatPelamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
							        <h3 class="modal-title" id="myModalLabel">Convert Pelamar</h3>
							    </div>
							    <form enctype="multipart/form-data" role="form" method="POST" class="form-horizontal" id="convertPelamar" action="<?php echo base_url(); ?>update/convert_pelamar/<?php echo $pelamar[0]['np']; ?>">
								    <div class="modal-body" >
										<div id="bottom" class="form-group">
											<label class="control-label col-md-2" style="padding-right: 0px;">Tgl. Masuk</label>
											<div class="input-group col-md-9">
											  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
											  	<input required type="text" class="form-control" data-provide ="datepicker" name="tgl_masuk" date-date-format="dd/mm/yyyy" placeholder="Tanggal Masuk">
											</div>
										</div>
										<div id="bottom" class="form-group">			
									    	<label class="control-label col-md-2" >NIP </label>
											<div class="col-md-9">
												<input required style="display:none;" type="text" class="form-control" id="np" name="np" placeholder="Nomor Urut Pelamar" value="<?php echo $pelamar[0]['np']; ?>" />
												<input required type="text" class="form-control" id="nip" name="nip" placeholder="Nomor Induk Pegawai"/>
											</div>
										</div>
										<div id="bottom" class="form-group">
											<label class="control-label col-md-2">Golongan</label>
											<div class="col-md-9">
												<input required type="text" class="form-control" name="golongan" placeholder="Golongan Pegawai"/>
											</div>
										</div>
										<div id="bottom" class="form-group">
											<label class="control-label col-md-2">Jabatan</label>
											<div class="col-md-9">
												<input required type="text" class="form-control" name="jabatan" placeholder="Jabatan Pegawai"/>
											</div>
										</div>
										<div id="bottom" class="form-group">
											<label class="control-label col-md-2" style="padding-right: 0px;">Status Pegawai </label>
											<div class="col-md-9">
												<select class="form-control select isian" name="status_pegawai" id="status_pegawai">
													<option value="1">Pegawai Tetap</option>
													<option value="2">Pegawai Kontrak</option>
													<option value="3">Masa Training</option>
												</select>
											</div>
										</div>
										<div id="bottom" class="form-group">			
									    	<label class="control-label col-md-2" >Pujian dan Rekomendasi </label>
											<div class="col-md-9">
												<div class="btn-toolbar" data-role="rekomendasiEditor-toolbar" data-target="#rekomendasiEditor">
													<div class="btn-group">
													<a class="btn btn-primary dropdown-toggle" data-toggle='dropdown' id="tool" data-placement='top' title='Font' data-original-title="Font"><i class="glyphicon glyphicon-font"></i><b class="caret"></b></a>
													  <ul class="dropdown-menu">
													  <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Font Size' data-original-title="Font Size"><i class="glyphicon glyphicon-text-height"></i>&nbsp;<b class="caret"></b></a>
													  <ul class="dropdown-menu">
													  <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
													  <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
													  <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
													  </ul>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="bold" id="tool" data-placement='top' title='Bold' data-original-title="Bold (Ctrl/Cmd+B)"><i class="glyphicon glyphicon-bold"></i></a>
													<a class="btn btn-primary" data-edit="italic" id="tool" data-placement='top' title='Italic' data-original-title="Italic (Ctrl/Cmd+I)"><i class="glyphicon glyphicon-italic"></i></a>
													<a class="btn btn-primary" data-edit="underline" id="tool" data-placement='top' title='Underline' data-original-title="Underline (Ctrl/Cmd+U)"><i class="glyphicon glyphicon-text-width"></i></a>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="insertunorderedlist" id="tool" data-placement='top' title='Bullet List' data-original-title="Bullet list"><i class="glyphicon glyphicon-list"></i></a>
													<a class="btn btn-primary" data-edit="insertorderedlist" id="tool" data-placement='top' title='Number List' data-original-title="Number list"><i class="glyphicon glyphicon-list-alt"></i></a>
													<a class="btn btn-primary" data-edit="outdent" id="tool" data-placement='top' title='Outdent' data-original-title="Reduce indent (Shift+Tab)"><i class="glyphicon glyphicon-indent-left"></i></a>
													<a class="btn btn-primary" data-edit="indent" id="tool" data-placement='top' title='Indent' data-original-title="Indent (Tab)"><i class="glyphicon glyphicon-indent-right"></i></a>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="justifyleft" id="tool" data-placement='top' title='Align Left' data-original-title="Align Left (Ctrl/Cmd+L)"><i class="glyphicon glyphicon-align-left"></i></a>
													<a class="btn btn-primary" data-edit="justifycenter" id="tool" data-placement='top' title='Center' data-original-title="Center (Ctrl/Cmd+E)"><i class="glyphicon glyphicon-align-center"></i></a>
													<a class="btn btn-primary" data-edit="justifyright" id="tool" data-placement='top' title='Align Right' data-original-title="Align Right (Ctrl/Cmd+R)"><i class="glyphicon glyphicon-align-right"></i></a>
													<a class="btn btn-primary" data-edit="justifyfull" id="tool" data-placement='top' title='Justify' data-original-title="Justify (Ctrl/Cmd+J)"><i class="glyphicon glyphicon-align-justify"></i></a>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Hyperlink' data-original-title="Hyperlink"><i class="glyphicon glyphicon-link"></i></a>
													<div class="dropdown-menu input-append">
													  <input class="span2" placeholder="URL" type="text" data-edit="createLink">
													  <button class="btn" type="button">Add</button>
													</div>
													<a class="btn btn-primary" data-edit="unlink" id="tool" data-placement='top' title='Remove Hyperlink' data-original-title="Remove Hyperlink"><i class="glyphicon glyphicon-remove"></i></a>

													</div>

													<div class="btn-group">
													<a class="btn btn-primary tool" data-placement='top' title='Insert Picture (or just drag and drop)' id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="glyphicon glyphicon-picture"></i></a>
													<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="undo" id="tool" data-placement='top' title='Undo' data-original-title="Undo (Ctrl/Cmd+Z)"><i class="glyphicon glyphicon-backward"></i></a>
													<a class="btn btn-primary" data-edit="redo" id="tool" data-placement='top' title='Redo' data-original-title="Redo (Ctrl/Cmd+Y)"><i class="glyphicon glyphicon-forward"></i></a>
													</div>
												</div>
												
												<input id="rekomendasiInput" type="text" style="display:none;" name="pujian_rekomendasi">
												<div id="rekomendasiEditor" name="pujian_rekomendasi" type="text" class="form-control Editor">
												</div>
											</div>
										</div>
										<div id="bottom" class="form-group">			
									    	<label class="control-label col-md-2" >Insiden Kritis </label>
											<div class="col-md-9">
												<div class="btn-toolbar" data-role="insidenEditor-toolbar" data-target="#insidenEditor">
													<div class="btn-group">
													<a class="btn btn-primary dropdown-toggle" data-toggle='dropdown' id="tool" data-placement='top' title='Font' data-original-title="Font"><i class="glyphicon glyphicon-font"></i><b class="caret"></b></a>
													  <ul class="dropdown-menu">
													  <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Font Size' data-original-title="Font Size"><i class="glyphicon glyphicon-text-height"></i>&nbsp;<b class="caret"></b></a>
													  <ul class="dropdown-menu">
													  <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
													  <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
													  <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
													  </ul>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="bold" id="tool" data-placement='top' title='Bold' data-original-title="Bold (Ctrl/Cmd+B)"><i class="glyphicon glyphicon-bold"></i></a>
													<a class="btn btn-primary" data-edit="italic" id="tool" data-placement='top' title='Italic' data-original-title="Italic (Ctrl/Cmd+I)"><i class="glyphicon glyphicon-italic"></i></a>
													<a class="btn btn-primary" data-edit="underline" id="tool" data-placement='top' title='Underline' data-original-title="Underline (Ctrl/Cmd+U)"><i class="glyphicon glyphicon-text-width"></i></a>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="insertunorderedlist" id="tool" data-placement='top' title='Bullet List' data-original-title="Bullet list"><i class="glyphicon glyphicon-list"></i></a>
													<a class="btn btn-primary" data-edit="insertorderedlist" id="tool" data-placement='top' title='Number List' data-original-title="Number list"><i class="glyphicon glyphicon-list-alt"></i></a>
													<a class="btn btn-primary" data-edit="outdent" id="tool" data-placement='top' title='Outdent' data-original-title="Reduce indent (Shift+Tab)"><i class="glyphicon glyphicon-indent-left"></i></a>
													<a class="btn btn-primary" data-edit="indent" id="tool" data-placement='top' title='Indent' data-original-title="Indent (Tab)"><i class="glyphicon glyphicon-indent-right"></i></a>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="justifyleft" id="tool" data-placement='top' title='Align Left' data-original-title="Align Left (Ctrl/Cmd+L)"><i class="glyphicon glyphicon-align-left"></i></a>
													<a class="btn btn-primary" data-edit="justifycenter" id="tool" data-placement='top' title='Center' data-original-title="Center (Ctrl/Cmd+E)"><i class="glyphicon glyphicon-align-center"></i></a>
													<a class="btn btn-primary" data-edit="justifyright" id="tool" data-placement='top' title='Align Right' data-original-title="Align Right (Ctrl/Cmd+R)"><i class="glyphicon glyphicon-align-right"></i></a>
													<a class="btn btn-primary" data-edit="justifyfull" id="tool" data-placement='top' title='Justify' data-original-title="Justify (Ctrl/Cmd+J)"><i class="glyphicon glyphicon-align-justify"></i></a>
													</div>
													<div class="btn-group">
													<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Hyperlink' data-original-title="Hyperlink"><i class="glyphicon glyphicon-link"></i></a>
													<div class="dropdown-menu input-append">
													  <input class="span2" placeholder="URL" type="text" data-edit="createLink">
													  <button class="btn" type="button">Add</button>
													</div>
													<a class="btn btn-primary" data-edit="unlink" id="tool" data-placement='top' title='Remove Hyperlink' data-original-title="Remove Hyperlink"><i class="glyphicon glyphicon-remove"></i></a>

													</div>

													<div class="btn-group">
													<a class="btn btn-primary tool" data-placement='top' title='Insert Picture (or just drag and drop)' id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="glyphicon glyphicon-picture"></i></a>
													<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
													</div>
													<div class="btn-group">
													<a class="btn btn-primary" data-edit="undo" id="tool" data-placement='top' title='Undo' data-original-title="Undo (Ctrl/Cmd+Z)"><i class="glyphicon glyphicon-backward"></i></a>
													<a class="btn btn-primary" data-edit="redo" id="tool" data-placement='top' title='Redo' data-original-title="Redo (Ctrl/Cmd+Y)"><i class="glyphicon glyphicon-forward"></i></a>
													</div>
												</div>
												
												<input id="insidenInput" type="text" style="display:none;" name="insiden_kritis">
												<div id="insidenEditor" type="text" class="form-control Editor" onchange="ubahInsiden()">											
												</div>
											</div>
										</div>
								    </div>
									<div class="modal-footer">
										<button type="button" class="btn btn-warning tutupKompetensi" data-dismiss="modal">Tutup</button>
										<button type="submit" class="btn btn-success">Tambah</button>
									</div>
								</form>
							</div>
						</div>
					</div>

					<!-- MODAL TAMBAH TEKNIS -->
					<div class="modal fade" id="modalTeknis" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
							        <h3 class="modal-title" id="myModalLabel">Tambah Kompetensi Teknis</h3>
							    </div>
							    <form enctype="multipart/form-data" role="form" method="POST" class="form-horizontal" id="tambahTeknis">
								    <div class="modal-body" >
										<div class="form-group">
											<div id="bottom" class="form-group">	
												<label class="control-label col-md-2" style="margin-top:8px; margin-right:10px;">Kompetensi</label>
												<div class="col-md-9">
													<input required type="text" class="form-control" name="namaKompetensi" id="namaTeknis" placeholder="Nama Kompetensi"/>
												</div>
												<br><br>	
											</div>		
										</div>
								    </div>
									<div class="modal-footer">
										<button type="button" class="btn btn-warning tutupKompetensi" data-dismiss="modal">Tutup</button>
										<button type="submit" class="btn btn-success">Tambah</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- MODAL TAMBAH MANAGERIAL -->
					<div class="modal fade" id="modalManagerial" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
							        <h3 class="modal-title" id="myModalLabel">Tambah Kompetensi Managerial</h3>
							    </div>
							    <form enctype="multipart/form-data" role="form" method="POST" class="form-horizontal" id="tambahManagerial">
								    <div class="modal-body" >
										<div class="form-group">
											<div id="bottom" class="form-group">	
												<label class="control-label col-md-2" style="margin-top:8px; margin-right:10px;">Kompetensi</label>
												<div class="col-md-9">
													<input required type="text" class="form-control" name="namaKompetensi" id="namaManagerial" placeholder="Nama Kompetensi"/>
												</div>
												<br><br>	
											</div>		
										</div>
								    </div>
									<div class="modal-footer">
										<button type="button" class="btn btn-warning tutupKompetensi" data-dismiss="modal">Tutup</button>
										<button type="submit" class="btn btn-success">Tambah</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					
					<div class="modal fade" id="npPegawai" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							        <h3 class="modal-title" id="myModalLabel">No. Pelamar</h3>
							    </div>
							    <div class="modal-body" >
									<div class="form-group">
										<div class="form-group">	
											<div class="col-md-6">
												<input type="text" class="form-control" name="katakunci" id="katakunci_pelamar" placeholder="cari..."/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>
											<br><br>	
										</div>		
										<div style="margin-left:20px; margin-right:20px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 10px 0px 10px">
											<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchKK">
												<thead>
													<tr class="warning">
														<td>No. Pelamar</td>
														<td>Nama Lengkap</td>
														<td width="10%">Pilih</td>
													</tr>
												</thead>
												<tbody id="t_body_pelamar">
													
												</tbody>
											</table>												
										</div>
									</div>
							    </div>
								<div class="modal-footer">
									<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- Tab Content -->
			</div> <!-- Col MD 10 -->
			<div class="col-md-2">	
				<ul class="nav nav-tabs tabs-right">
					<li class="active"><a href="#a" data-toggle="tab" id="tabA"><span class="glyphicon glyphicon-user"></span><br>Data Pribadi</a></li>
					<li><a href="#b" data-toggle="tab" id="tabB"><span class="glyphicon glyphicon-education"></span>&nbsp&nbsp<span class="glyphicon glyphicon-home"></span><br>Riwayat Pendidikan<br>& Susunan Keluarga</a></li>
				</ul>
			</div> <!-- Col MD 2 -->
		</div>
	</div>
</body>
</html>