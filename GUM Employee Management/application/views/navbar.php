<html>
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0; background-color: rgb(34,45,50);">
        <div class="navbar-header" style="padding:0px; margin-top:2px;">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="margin-top:-10px; margin-left:-5px; color:#fff;" class="navbar-brand" href="<?php echo base_url(); ?>welcome/dashboard_view"><h4>&nbsp<i class="glyphicon glyphicon-home">&nbsp</i>PT Gloria Usaha Mulia</h4></a>
        </div>
        <!-- /.navbar-header -->
        <ul class="nav navbar-top-links navbar-right">
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="glyphicon glyphicon-user glyphicon-fw"></i> <?php echo $this->session->userdata('user_input')['username']; ?> <i class="caret"></i>
                </a>
                <ul class="dropdown-menu dropdown-task">
                    <?php if($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur"){?>
                        <li>
                            <a class="user" href="<?php echo base_url(); ?>users/daftar_user">
                                <i class="glyphicon glyphicon-cog glyphicon-fw"></i>&nbsp Manajemen User
                            </a>
                        </li>
                        <li class="divider"></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url(); ?>users/logout"><i class="glyphicon glyphicon-log-out glyphicon-fw"></i>&nbsp Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->
        <div style="height:5px;">
        </div>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li style="height:160px">
                        <div style="height:120px; width:80px; margin:auto; padding-top:20px;">
                            <?php if($this->session->userdata('user_input')['foto'] != ""){?>
                            	<img src="<?php echo base_url().$this->session->userdata('user_input')['foto'] ?>" class="img-thumbnail" style="height:98px; width:80px; overflow:auto;">
                            <?php } else { ?>
                            	<img src="<?php echo base_url(); ?>assets/img/default_user.jpg" class="img-thumbnail">
                            <?php } ?>                               
                            <h5 style="text-align:center;"><?php echo $this->session->userdata('user_input')['username']; ?></h5>
                        </div>
                    </li>
                    <li>
                        <a class="menu" href="<?php echo base_url(); ?>welcome/dashboard_view"><i class="glyphicon glyphicon-dashboard">&nbsp</i>Grafik</a>
                    </li>
                    <li>
                        <a href="#"><i class="glyphicon glyphicon-list-alt">&nbsp</i>View Data<span class="pull-right glyphicon glyphicon-chevron-left"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="menu" href="<?php echo base_url(); ?>view/viewdata_pegawai"><i class="glyphicon glyphicon-briefcase">&nbsp</i>Pegawai</a></li>
                            <li><a class="menu" href="<?php echo base_url(); ?>view/viewdata_pelamar"><i class="glyphicon glyphicon-user">&nbsp</i>Pelamar</a></li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="glyphicon glyphicon-edit">&nbsp</i>Input Data<span class="pull-right glyphicon glyphicon-chevron-left"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a class="menu" href="<?php echo base_url(); ?>input/inputdata_pegawai"><i class="glyphicon glyphicon-briefcase">&nbsp</i>Pegawai</a></li>
                            <li><a class="menu" href="<?php echo base_url(); ?>input/inputdata_pelamar"><i class="glyphicon glyphicon-user">&nbsp</i>Pelamar</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="menu" href="<?php echo base_url(); ?>payroll/view_payroll"><i class="glyphicon glyphicon-usd">&nbsp</i>Payroll</a>
                    </li>
                    <li id="view_pegawai" class="hidden">
                        <a href="<?php echo base_url(); ?>view/exportdata_pegawai_pelamar/pegawai" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="glyphicon glyphicon-download-alt">&nbsp</i>Export Data
                        </a>
                    </li>
                    <li id="view_pelamar" class="hidden">
                        <a href="<?php echo base_url(); ?>view/exportdata_pegawai_pelamar/pelamar" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="glyphicon glyphicon-download-alt">&nbsp</i>Export Data
                        </a>
                    </li>
                    <?php
                        if(isset($pegawai[0])){
                    ?>
                        <li id="edit_pegawai" class="hidden">
                            <a href="<?php echo base_url(); ?>update/exportdata_detail_pegawai/<?php echo $pegawai[0]['nip']; ?>"><i class="glyphicon glyphicon-download-alt">&nbsp</i>Export Data</a>
                        </li>
                    <?php
                        } elseif (isset($pelamar[0])) {
                    ?>
                        <li class="edit_pelamar hidden">
                            <a href="#" data-toggle="modal" data-target="#modalAngkatPelamar"><i class="glyphicon glyphicon-briefcase">&nbsp</i>Convert Pelamar</a>
                        </li>
                        <li class="edit_pelamar hidden">
                            <a href="<?php echo base_url(); ?>update/exportdata_detail_pelamar/<?php echo $pelamar[0]['np']; ?>"><i class="glyphicon glyphicon-download-alt">&nbsp</i>Export Data</a>
                        </li>
                    <?php
                        }
                    ?>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
</html>