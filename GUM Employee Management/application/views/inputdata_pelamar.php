<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>PT. GUM DB Pegawai</title>
	<?php include "javascript.php"; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/inputdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.vertical-tabs.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/timeline.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sb-admin-2.css">
  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/prettify.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>
</head>
<body>
	<div id="wrapper">
		<?php include "navbar.php"; ?>

		<div id="page-wrapper" style="font-size:12px;">
			<div style="height:30px;">
				
			</div>
			<div class="col-md-10">
				<div class="data-container tab-content" >
					<div class="tab-pane active" id="a">
						<div class="col-md-12 customtitle">
							<h1>Data Pribadi Pelamar</h1>
						</div>
						<form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" id="dataPribadiPelamar" action="<?php echo base_url() ?>input/insert_pelamar/">
							<div class="col-md-12">
								<div class="form-group" style="float:left; margin-left:20px; margin-right:10px;">
									<div class="col-md-3">
										<img src="<?php echo base_url(); ?>assets/img/default_user.jpg" id="picture" data-toggle='tooltip' data-placement='bottom' title='Ganti Gambar'>
										<input id="btn-upload" type="file" name="picture" style="display:none;" onchange="preview(this)">
									</div>
								</div>
								<div class="col-md-9">
									<div class="form-group">			
								    	<label class="control-label col-md-3" >Nomor Urut Pelamar </label>
										<div class="col-md-9">
											<input required type="text" class="form-control" id="np" name="np" placeholder="Nomor Urut Pelamar"/>
										</div>
									</div>
									<div class="form-group">			
								    	<label class="control-label col-md-3" >Nama Lengkap </label>
										<div class="col-md-9">
											<input required type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap Pelamar"/>
										</div>
									</div>
									<div class="form-group">			
							            <label class="control-label col-md-3" >Jenis Kelamin </label>
										<div class="col-md-9">
											<div class="radio-list">
												<input required type="radio" style="margin-left: 0px" name="jns_kelamin" id="newJenisKelamin" value="1" data-title="Pria"/><span style="margin-left:5px">Laki-laki</span> 
												<input required type="radio" style="margin-left: 10px" name="jns_kelamin" id="newJenisKelamin2" value="2" data-title="Wanita"/><span style="margin-left:5px">Perempuan</span> 
											</div>
										</div>
									</div>
									<div class="form-group">			
						            	<label class="control-label col-md-3" >Tanggal Lahir </label>
										<div class="col-md-9">
											<div class="input-group">
											  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
											  	<input required type="text" class="form-control" data-provide ="datepicker" name="tgl_lahir" date-date-format="dd/mm/yyyy" placeholder="Tanggal Lahir">
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Alamat Domisili</label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="alamat_domisili" name="alamat_domisili" placeholder="Alamat Domisili" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Alamat Asal</label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="alamat_asal" name="alamat_asal" placeholder="Alamat Asal" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >No. Telp. Rumah</label>
								<div class="col-md-4">
									<input type="text" class="form-control num" id="telp_rumah" name="telp_rmh" placeholder="Nomor Telepon Rumah" />
								</div>
								<label class="control-label col-md-1" >No. HP</label>
								<div class="col-md-4">
									<input required type="text" class="form-control num" id="telp_hp" name="telp_hp" placeholder="Nomor Telepon Seluler" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Agama </label>
								<div class="col-md-9">
									<select required class="form-control select" name="agama" id="agama">
										<option value="1">Kristen</option>
										<option value="2">Katolik</option>
										<option value="3">Islam</option>
										<option value="4">Hindu</option>
										<option value="5">Budha</option>
										<option value="6">Konghucu</option>
									</select>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Status Nikah </label>
								<div class="col-md-9">
									<select required class="form-control select isian" name="status_perkawinan" id="status_perkawinan">
										<option value="1" selected>Menikah</option>
										<option value="2">Belum Menikah</option>
									</select>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Tanggal Nikah </label>
								<div class="col-md-9">
									<div class="input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
									  	<input required type="text" class="form-control" data-provide ="datepicker" name="tgl_nikah" id="tgl_nikah" date-date-format="dd/mm/yyyy" placeholder="Tanggal Pernikahan">
									</div>
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Nomor KTP</label>
								<div class="col-md-9">
									<input required type="text" class="form-control num" id="no_ktp" name="no_ktp" placeholder="Nomor Kartu Tanda Penduduk" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Tinggi Badan</label>
								<div class="col-md-4">
									<input required type="text" class="form-control num" id="tinggi_badan" name="tinggi_bdn" placeholder="Tinggi Badan" />
								</div>
								<label class="control-label col-md-1" >Berat</label>
								<div class="col-md-4">
									<input required type="text" class="form-control num" id="berat_badan" name="berat_bdn" placeholder="Berat Badan" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Golongan Darah </label>
								<div class="col-md-9">
									<select required class="form-control select" name="gol_darah" id="gol_darah">
										<option value="1">A</option>
										<option value="2">B</option>
										<option value="3">AB</option>
										<option value="4">O</option>
									</select>
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >NPWP </label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="npwp" name="npwp" placeholder="Nomor Pokok Wajib Pajak" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >No. Jamsostek </label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="no_jamsostek" name="no_jamsostek" placeholder="Nomor Jaminan Sosial Tenaga Kerja" />
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Tanggal Daftar </label>
								<div class="col-md-9">
									<div class="input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
									  	<input required type="text" class="form-control" data-provide ="datepicker" name="tgl_daftar" date-date-format="dd/mm/yyyy" placeholder="Tanggal Masuk">
									</div>
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Hobby </label>
								<div class="col-md-9">
									<div class="btn-toolbar" data-role="hobbyEditor-toolbar" data-target="#hobbyEditor">
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle='dropdown' id="tool" data-placement='top' title='Font' data-original-title="Font"><i class="glyphicon glyphicon-font"></i><b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Font Size' data-original-title="Font Size"><i class="glyphicon glyphicon-text-height"></i>&nbsp;<b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
										  <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
										  <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
										  </ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="bold" id="tool" data-placement='top' title='Bold' data-original-title="Bold (Ctrl/Cmd+B)"><i class="glyphicon glyphicon-bold"></i></a>
										<a class="btn btn-primary" data-edit="italic" id="tool" data-placement='top' title='Italic' data-original-title="Italic (Ctrl/Cmd+I)"><i class="glyphicon glyphicon-italic"></i></a>
										<a class="btn btn-primary" data-edit="underline" id="tool" data-placement='top' title='Underline' data-original-title="Underline (Ctrl/Cmd+U)"><i class="glyphicon glyphicon-text-width"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="insertunorderedlist" id="tool" data-placement='top' title='Bullet List' data-original-title="Bullet list"><i class="glyphicon glyphicon-list"></i></a>
										<a class="btn btn-primary" data-edit="insertorderedlist" id="tool" data-placement='top' title='Number List' data-original-title="Number list"><i class="glyphicon glyphicon-list-alt"></i></a>
										<a class="btn btn-primary" data-edit="outdent" id="tool" data-placement='top' title='Outdent' data-original-title="Reduce indent (Shift+Tab)"><i class="glyphicon glyphicon-indent-left"></i></a>
										<a class="btn btn-primary" data-edit="indent" id="tool" data-placement='top' title='Indent' data-original-title="Indent (Tab)"><i class="glyphicon glyphicon-indent-right"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="justifyleft" id="tool" data-placement='top' title='Align Left' data-original-title="Align Left (Ctrl/Cmd+L)"><i class="glyphicon glyphicon-align-left"></i></a>
										<a class="btn btn-primary" data-edit="justifycenter" id="tool" data-placement='top' title='Center' data-original-title="Center (Ctrl/Cmd+E)"><i class="glyphicon glyphicon-align-center"></i></a>
										<a class="btn btn-primary" data-edit="justifyright" id="tool" data-placement='top' title='Align Right' data-original-title="Align Right (Ctrl/Cmd+R)"><i class="glyphicon glyphicon-align-right"></i></a>
										<a class="btn btn-primary" data-edit="justifyfull" id="tool" data-placement='top' title='Justify' data-original-title="Justify (Ctrl/Cmd+J)"><i class="glyphicon glyphicon-align-justify"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Hyperlink' data-original-title="Hyperlink"><i class="glyphicon glyphicon-link"></i></a>
										<div class="dropdown-menu input-append">
										  <input class="span2" placeholder="URL" type="text" data-edit="createLink">
										  <button class="btn" type="button">Add</button>
										</div>
										<a class="btn btn-primary" data-edit="unlink" id="tool" data-placement='top' title='Remove Hyperlink' data-original-title="Remove Hyperlink"><i class="glyphicon glyphicon-remove"></i></a>

										</div>

										<div class="btn-group">
										<a class="btn btn-primary tool" data-placement='top' title='Insert Picture (or just drag and drop)' id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="glyphicon glyphicon-picture"></i></a>
										<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="undo" id="tool" data-placement='top' title='Undo' data-original-title="Undo (Ctrl/Cmd+Z)"><i class="glyphicon glyphicon-backward"></i></a>
										<a class="btn btn-primary" data-edit="redo" id="tool" data-placement='top' title='Redo' data-original-title="Redo (Ctrl/Cmd+Y)"><i class="glyphicon glyphicon-forward"></i></a>
										</div>
									</div>
									
									<input id="hobbyInput" type="text" style="display:none;" name="hobby">
									<div id="hobbyEditor" name="hobby" type="text" class="form-control Editor">
									</div>
								</div>
							</div>
							<div id="bottom" class="form-group">			
						    	<label class="control-label col-md-2" >Buku dll. yang anda baca </label>
								<div class="col-md-9">
									<div class="btn-toolbar" data-role="bukuEditor-toolbar" data-target="#bukuEditor">
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle='dropdown' id="tool" data-placement='top' title='Font' data-original-title="Font"><i class="glyphicon glyphicon-font"></i><b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Font Size' data-original-title="Font Size"><i class="glyphicon glyphicon-text-height"></i>&nbsp;<b class="caret"></b></a>
										  <ul class="dropdown-menu">
										  <li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
										  <li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
										  <li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
										  </ul>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="bold" id="tool" data-placement='top' title='Bold' data-original-title="Bold (Ctrl/Cmd+B)"><i class="glyphicon glyphicon-bold"></i></a>
										<a class="btn btn-primary" data-edit="italic" id="tool" data-placement='top' title='Italic' data-original-title="Italic (Ctrl/Cmd+I)"><i class="glyphicon glyphicon-italic"></i></a>
										<a class="btn btn-primary" data-edit="underline" id="tool" data-placement='top' title='Underline' data-original-title="Underline (Ctrl/Cmd+U)"><i class="glyphicon glyphicon-text-width"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="insertunorderedlist" id="tool" data-placement='top' title='Bullet List' data-original-title="Bullet list"><i class="glyphicon glyphicon-list"></i></a>
										<a class="btn btn-primary" data-edit="insertorderedlist" id="tool" data-placement='top' title='Number List' data-original-title="Number list"><i class="glyphicon glyphicon-list-alt"></i></a>
										<a class="btn btn-primary" data-edit="outdent" id="tool" data-placement='top' title='Outdent' data-original-title="Reduce indent (Shift+Tab)"><i class="glyphicon glyphicon-indent-left"></i></a>
										<a class="btn btn-primary" data-edit="indent" id="tool" data-placement='top' title='Indent' data-original-title="Indent (Tab)"><i class="glyphicon glyphicon-indent-right"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="justifyleft" id="tool" data-placement='top' title='Align Left' data-original-title="Align Left (Ctrl/Cmd+L)"><i class="glyphicon glyphicon-align-left"></i></a>
										<a class="btn btn-primary" data-edit="justifycenter" id="tool" data-placement='top' title='Center' data-original-title="Center (Ctrl/Cmd+E)"><i class="glyphicon glyphicon-align-center"></i></a>
										<a class="btn btn-primary" data-edit="justifyright" id="tool" data-placement='top' title='Align Right' data-original-title="Align Right (Ctrl/Cmd+R)"><i class="glyphicon glyphicon-align-right"></i></a>
										<a class="btn btn-primary" data-edit="justifyfull" id="tool" data-placement='top' title='Justify' data-original-title="Justify (Ctrl/Cmd+J)"><i class="glyphicon glyphicon-align-justify"></i></a>
										</div>
										<div class="btn-group">
										<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" id="tool" data-placement='top' title='Hyperlink' data-original-title="Hyperlink"><i class="glyphicon glyphicon-link"></i></a>
										<div class="dropdown-menu input-append">
										  <input class="span2" placeholder="URL" type="text" data-edit="createLink">
										  <button class="btn" type="button">Add</button>
										</div>
										<a class="btn btn-primary" data-edit="unlink" id="tool" data-placement='top' title='Remove Hyperlink' data-original-title="Remove Hyperlink"><i class="glyphicon glyphicon-remove"></i></a>

										</div>

										<div class="btn-group">
										<a class="btn btn-primary tool" data-placement='top' title='Insert Picture (or just drag and drop)' id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="glyphicon glyphicon-picture"></i></a>
										<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 37px; height: 30px;">
										</div>
										<div class="btn-group">
										<a class="btn btn-primary" data-edit="undo" id="tool" data-placement='top' title='Undo' data-original-title="Undo (Ctrl/Cmd+Z)"><i class="glyphicon glyphicon-backward"></i></a>
										<a class="btn btn-primary" data-edit="redo" id="tool" data-placement='top' title='Redo' data-original-title="Redo (Ctrl/Cmd+Y)"><i class="glyphicon glyphicon-forward"></i></a>
										</div>
									</div>
									
									<input id="bukuInput" type="text" style="display:none;" name="buku_bacaan">
									<div id="bukuEditor" name="buku_bacaan" type="text" class="form-control Editor">
									</div>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Kompetensi Teknis </label>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="tabelTeknis">
										<thead>
											<tr class="bg-primary">
												<td width="5%" align="center" rowspan="2"><br>Aksi</td>
												<td align="center" rowspan="2"><br>Kompetensi Teknis</td>
												<td align="center" colspan="4" width="40%">Tingkat Penguasaan</td>
											</tr>
											<tr class="bg-primary">
												<td width="10%" align="center">Tidak</td>
												<td width="10%" align="center">Cukup</td>
												<td width="10%" align="center">Menguasai</td>
												<td width="10%" align="center">Mahir</td>
											</tr>
										</thead>
										<tbody id="teknis">
											<tr>
												<td align="center"><a style='cursor:pointer;' class='hapusKompetensi' id='tool' data-toggle='tooltip' data-placement='top' title='Hapus'><i class='glyphicon glyphicon-trash'></i></a></td>
												<td>Microsoft Excel<input required type="text" class="hidden" name="teknis[0][name]" value="Microsoft Excel"></td>
												<td align="center"><input required type="radio" name="teknis[0][value]" value="1"></td>
												<td align="center"><input required type="radio" name="teknis[0][value]" value="2"></td>
												<td align="center"><input required type="radio" name="teknis[0][value]" value="3"></td>
												<td align="center"><input required type="radio" name="teknis[0][value]" value="4"></td>
											</tr>
										</tbody>
									</table>
									<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#modalTeknis"><i class="glyphicon glyphicon-plus"></i> Tambah Kompetensi</button>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Kompetensi Managerial </label>
								<div class="col-md-9">
									<table class="table table-striped table-hover" id="tabelManagerial">
										<thead>
											<tr class="bg-primary">
												<td width="5%" align="center" rowspan="2"><br>Aksi</td>
												<td align="center" rowspan="2"><br>Kompetensi Managerial</td>
												<td align="center" colspan="4" width="40%">Tingkat Penguasaan</td>
											</tr>
											<tr class="bg-primary">
												<td width="10%" align="center">Tidak</td>
												<td width="10%" align="center">Cukup</td>
												<td width="10%" align="center">Menguasai</td>
												<td width="10%" align="center">Mahir</td>
											</tr>
										</thead>
										<tbody id="managerial">
											<tr>
												<td align="center"><a style='cursor:pointer;' class='hapusKompetensi' id='tool' data-toggle='tooltip' data-placement='top' title='Hapus'><i class='glyphicon glyphicon-trash'></i></a></td>
												<td>Leadership<input required type="text" class="hidden" name="managerial[0][name]" value="Leadership"></td>
												<td align="center"><input required type="radio" name="managerial[0][value]" value="1"></td>
												<td align="center"><input required type="radio" name="managerial[0][value]" value="2"></td>
												<td align="center"><input required type="radio" name="managerial[0][value]" value="3"></td>
												<td align="center"><input required type="radio" name="managerial[0][value]" value="4"></td>
											</tr>
										</tbody>
									</table>
									<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#modalManagerial"><i class="glyphicon glyphicon-plus"></i> Tambah Kompetensi</button>
								</div>
							</div>
							
							<!-- submit -->
							<div class="form-group">			
				            	<div class="col-md-12">
				            		<div class="col-md-3 pull-right custombutton">					            		
					            		<button class="btn btn-success submitPribadiPelamar" id="tool" data-toggle="tooltip" data-placement="top" title="Simpan Data Pelamar" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
					            	</div>
			            		</div>
							</div>
						</form>
					</div> <!-- Tab Pribadi -->


					<!-- RIWAYAT PENDIDIKAN & KELUARGA PELAMAR -->
					<div class="tab-pane" id="b">
						<div class="col-md-12 customtitle">
							<h1>Riwayat Pendidikan & Keluarga Pelamar</h1>
						</div>
						<form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" id="dataKeluargaPelamar" action="<?php echo base_url(); ?>input/insert_keluarga_pelamar">
							<div id="bottom" class="form-group hidden">			
						    	<label class="control-label col-md-2" >Nomor Urut Pelamar </label>
								<div class="col-md-9">
									<input required type="text" class="form-control" id="npKeluarga" data-toggle="modal" data-target="#npPelamar" name="np" placeholder="Nomor Urut Pelamar"/>
								</div>
							</div>
							<div id="bottom" class="form-group">			
				            	<label class="control-label col-md-2" >Riwayat Pendidikan </label>
								<div class="col-md-9">
									<table id="tabelPendidikan" class="table table-hover">
										<thead>
											<tr class="bg-primary">
												<td align="center">Aksi</td>
												<td align="center" width="17%">Pendidikan</td>
												<td align="center">Nama Sekolah</td>
												<td align="center" width="15%">Jurusan</td>
												<td align="center" width="20%">Kota</td>
												<td align="center" width="15%">Th. Lulus</td>
											</tr>
										</thead>
										<tbody id="pendidikan" style="background-color:#f9f9f9;">
											<tr>
												<td style="vertical-align:middle;" align="center"><a style='cursor:pointer;' class='hapusPendidikan' id='tool' data-placement='top' title='Hapus'><i class='glyphicon glyphicon-trash'></i></a></td>
												<td>
													<select required class="form-control select" name="pendidikan[0][pendidikan]">
														<option value="1">SD</option>
														<option value="2">SMP/Sederajat</option>
														<option value="3">SMA/Sederajat</option>
														<option value="4">Diploma 1</option>
														<option value="5">Diploma 2</option>
														<option value="6">Diploma 3</option>
														<option value="7">Strata 1</option>
														<option value="8">Strata 2</option>
														<option value="9">Strata 3</option>
														<option value="10">Akademis</option>
													</select>
												</td>
												<td><input required type="text" class="form-control" name="pendidikan[0][nama]" placeholder="Nama Sekolah"/></td>
												<td><input type="text" class="form-control" name="pendidikan[0][jurusan]" placeholder="Jurusan"/></td>
												<td><input required type="text" class="form-control" name="pendidikan[0][kota]" placeholder="Kota Sekolah"/></td>
												<td><input required type="text" class="form-control num" name="pendidikan[0][lulus]" placeholder="Tahun Lulus"/></td>
											</tr>
										</tbody>
									</table>
									<button type="button" id="tambahPendidikan" class="btn btn-info pull-right"><i class="glyphicon glyphicon-plus"></i> Tambah Pendidikan</button>
								</div>
							</div>
							<div id="bottom" class="form-group">
								<label class="control-label col-md-2">Anak Nomor</label>
								<div class="col-md-9">
									<input required type="text" class="form-control num" name="anak_nomor" placeholder="Urutan anda dalam silsilah anak Keluarga">
								</div>
							</div>
							<div id="bottom" class="form-group">
								<label class="control-label col-md-2">Susunan Keluarga</label>
								<div class="col-md-9">
									<table class="table table-hover" id="tabelKeluarga">
										<thead>
											<tr class="bg-primary">
												<td colspan="6" align="center">Orang Tua</td>
											</tr>
											<tr class="bg-primary">
												<td align="center">Status</td>
												<td align="center">Nama</td>
												<td align="center">Tgl. Lahir</td>
												<td align="center">Pendidikan</td>
												<td align="center">Ket/Pekerjaan</td>
											</tr>
										</thead>
										<tbody style="background-color:#f9f9f9;">
											<?php
											for ($i=0; $i < 4; $i++) {
												$status = 1;
												$ortu = "Ayah";
												$statusOrtu = "Kandung";
												$class = ""; $tr = "";
												if($i == 1 || $i == 3) $ortu = "Ibu"; 
												if($i > 1){ $status += 1; $statusOrtu = "Mertua"; $class = " pasangan"; $tr = 'class="rowPasangan"';}
												$print = '<tr '.$tr.'>';
													$print .= '<td class="hidden"><input required class="'.$class.'" type="text" name="ortu['.$i.'][status]" value="'.$statusOrtu.'"></td>
													<td style="vertical-align:middle;">'.$ortu.' '.$statusOrtu.'<input required type="text" class="hidden'.$class.'" name="ortu['.$i.'][orang_tua]" value="'.$ortu.'"></td>
													<td><input required type="text" name="ortu['.$i.'][nama]" class="form-control'.$class.'" placeholder="Nama '.$ortu.'"></td>
													<td><input type="text" class="form-control'.$class.'" data-provide ="datepicker" name="ortu['.$i.'][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir '.$ortu.'"></td>
													<td>
														<select required name="ortu['.$i.'][pendidikan]" class="form-control select'.$class.'" placeholder="Pendidikan Terakhir '.$ortu.'">
															<option value="1">SD</option>
															<option value="2">SMP/Sederajat</option>
															<option value="3">SMA/Sederajat</option>
															<option value="4">Diploma 1</option>
															<option value="5">Diploma 2</option>
															<option value="6">Diploma 3</option>
															<option value="7">Strata 1</option>
															<option value="8">Strata 2</option>
															<option value="9">Strata 3</option>
															<option value="10">Akademis</option>
														</select>	
													</td>
													<td><input required type="text" name="ortu['.$i.'][ket_pekerjaan]" class="form-control'.$class.'" placeholder="Ket/Pekerjaan '.$ortu.'"></td>
												</tr>
												';
												if($i == 1 || $i == 3){
													$print .= '<tr '.$tr.'>
														<td style="vertical-align:middle;">Alamat</td>
														<td colspan="6"><input required type="text" name="ortu['.$i.'][alamat]" class="form-control'.$class.'"></td>
													</tr>
													<tr '.$tr.'>
														<td style="vertical-align:middle;">Nomor Telepon</td>
														<td colspan="6"><input type="text" name="ortu['.$i.'][no_telp]" class="form-control num'.$class.'"></td>
													</tr>';	
												}
												echo $print;
											}
											?>
										</tbody>
									</table>
									<table class="table table-hover" id="tabelLainnya">
										<thead>
											<tr class="bg-primary">
												<td colspan="6" align="center">Lainnya</td>
											</tr>
											<tr class="bg-primary">
												<td align="center" width="15%">Status</td>
												<td align="center">Nama</td>
												<td align="center">Tgl. Lahir</td>
												<td align="center">Pendidikan</td>
												<td align="center">Ket/Pekerjaan</td>
												<td align="center" width="10%">Aksi</td>
											</tr>
										</thead>
										<tbody id="rowSaudara" style="background-color:#f9f9f9;">
											<tr>
												<td>
													<select required class="form-control select" name="kel[0][status]">
														<option value="1" selected="selected">Kakak</option>
														<option value="2">Adik</option>
													</select>
												</td>
												<td><input required type="text" name="kel[0][nama]" class="form-control" placeholder="Nama Keluarga"></td>
												<td><input type="text" class="form-control" data-provide ="datepicker" name="kel[0][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Keluarga"></td>
												<td>
													<select required name="kel[0][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Keluarga">
														<option value="1">SD</option>
														<option value="2">SMP/Sederajat</option>
														<option value="3">SMA/Sederajat</option>
														<option value="4">Diploma 1</option>
														<option value="5">Diploma 2</option>
														<option value="6">Diploma 3</option>
														<option value="7">Strata 1</option>
														<option value="8">Strata 2</option>
														<option value="9">Strata 3</option>
														<option value="10">Akademis</option>
													</select>	
												</td>
												<td><input required type="text" name="kel[0][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Keluarga"></td>
												<td style="vertical-align:middle" align="center">
													<a style='cursor:pointer;' class="hapusKakak" data-placement="top" title="Hapus Keluarga" id="tool"><i class="glyphicon glyphicon-trash"></i></a>
												</td>
											</tr>
										</tbody>
										<tbody class="rowPasangan" id="rowPasangan" style="background-color:#f9f9f9;">
											<tr>
												<td>
													<select required class="form-control select pasangan" name="kel[1][status]">
														<option value="4" selected="selected">Suami</option>
														<option value="5">Isteri</option>
													</select>
												</td>
												<td><input required type="text" name="kel[1][nama]" class="form-control pasangan" placeholder="Nama Pasangan"></td>
												<td><input required type="text" class="form-control pasangan" data-provide ="datepicker" name="kel[1][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Pasangan"></td>
												<td>
													<select required name="kel[1][pendidikan]" class="form-control select pasangan" placeholder="Pendidikan Terakhir Pasangan">
														<option value="1">SD</option>
														<option value="2">SMP/Sederajat</option>
														<option value="3">SMA/Sederajat</option>
														<option value="4">Diploma 1</option>
														<option value="5">Diploma 2</option>
														<option value="6">Diploma 3</option>
														<option value="7">Strata 1</option>
														<option value="8">Strata 2</option>
														<option value="9">Strata 3</option>
														<option value="10">Akademis</option>
													</select>	
												</td>
												<td><input required type="text" name="kel[1][ket_pekerjaan]" class="form-control pasangan" placeholder="Ket/Pekerjaan Pasangan"></td>
												<td style="vertical-align:middle" align="center">
													<a style='cursor:pointer;' class="hapusPasangan pasangan" data-placement="top" title="Hapus Pasangan" id="tool"><i class="glyphicon glyphicon-trash"></i></a>
												</td>
											</tr>
										</tbody>
										<tbody id="rowAnak" style="background-color:#f9f9f9;">
											<tr>
												<td style="vertical-align:middle;" align="center">
													Anak<input required type="text" class="hidden" name="kel[2][status]" value="3">
												</td>
												<td><input required type="text" name="kel[2][nama]" class="form-control" placeholder="Nama Anak"></td>
												<td><input required type="text" class="form-control" data-provide ="datepicker" name="kel[2][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Anak"></td>
												<td>
													<select required name="kel[2][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Anak">
														<option value="1">SD</option>
														<option value="2">SMP/Sederajat</option>
														<option value="3">SMA/Sederajat</option>
														<option value="4">Diploma 1</option>
														<option value="5">Diploma 2</option>
														<option value="6">Diploma 3</option>
														<option value="7">Strata 1</option>
														<option value="8">Strata 2</option>
														<option value="9">Strata 3</option>
														<option value="10">Akademis</option>
													</select>	
												</td>
												<td><input required type="text" name="kel[2][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Anak"></td>
												<td style="vertical-align:middle" align="center">
													<a style='cursor:pointer;' class="hapusAnak" data-placement="top" title="Hapus Anak" id="tool"><i class="glyphicon glyphicon-trash"></i></a>
												</td>
											</tr>
										</tbody>
									</table>
									<div class="pull-right">
										<button type="button" id="tambahSaudara" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Tambah Data Saudara</button>
										<button type="button" id="tambahAnak" class="btn btn-info"><i class="glyphicon glyphicon-plus"></i> Tambah Data Anak</button>	
									</div>
								</div>
							</div>
							<div id="bottom" class="form-group">
								<label class="control-label col-md-2"></label>
								<div class="col-md-9">
									<table class="table table-hover">
										<thead>
											<tr class="bg-primary">
												<td colspan="3" align="center">Keluarga yang Dapat Dihubungi Dalam Keadaan Darurat</td>
											</tr>
											<tr class="bg-primary">
												<td align="center">Nama</td>
												<td align="center">Alamat</td>
												<td align="center">No. Telepon</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input type="text" required class="form-control" name="darurat[0][nama]" placeholder="Nama Keluarga"></td>
												<td><input type="text" required class="form-control" name="darurat[0][alamat]" placeholder="Alamat Keluarga"></td>
												<td><input type="text" required class="form-control num" name="darurat[0][no_telp]" placeholder="Nomor Keluarga"></td>
											</tr>
											<tr>
												<td><input type="text" class="form-control" name="darurat[1][nama]" placeholder="Nama Keluarga"></td>
												<td><input type="text" class="form-control" name="darurat[1][alamat]" placeholder="Alamat Keluarga"></td>
												<td><input type="text" class="form-control num" name="darurat[1][no_telp]" placeholder="Nomor Keluarga"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<!-- submit -->
							<div class="form-group">			
				            	<div class="col-md-12">
				            		<div class="col-md-3 pull-right custombutton">					            		
					            		<button class="btn btn-success" id="tool" data-toggle="tooltip" data-placement="top" title="Simpan Data Pelamar" type="submit"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
					            	</div>
			            		</div>
							</div>
						</form>
					</div> <!-- Tab Pendidikan -->	
				
					<div class="modal fade" id="modalTeknis" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
							        <h3 class="modal-title" id="myModalLabel">Tambah Kompetensi Teknis</h3>
							    </div>
							    <form enctype="multipart/form-data" role="form" method="POST" class="form-horizontal" id="tambahTeknis">
								    <div class="modal-body" >
										<div class="form-group">
											<div id="bottom" class="form-group">	
												<label class="control-label col-md-2" style="margin-top:8px; margin-right:10px;">Kompetensi</label>
												<div class="col-md-9">
													<input required type="text" class="form-control" name="namaKompetensi" id="namaTeknis" placeholder="Nama Kompetensi"/>
												</div>
												<br><br>	
											</div>		
										</div>
								    </div>
									<div class="modal-footer">
										<button type="button" class="btn btn-warning tutupKompetensi" data-dismiss="modal">Tutup</button>
										<button type="submit" class="btn btn-success">Tambah</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal fade" id="modalManagerial" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
							        <h3 class="modal-title" id="myModalLabel">Tambah Kompetensi Managerial</h3>
							    </div>
							    <form enctype="multipart/form-data" role="form" method="POST" class="form-horizontal" id="tambahManagerial">
								    <div class="modal-body" >
										<div class="form-group">
											<div id="bottom" class="form-group">	
												<label class="control-label col-md-2" style="margin-top:8px; margin-right:10px;">Kompetensi</label>
												<div class="col-md-9">
													<input required type="text" class="form-control" name="namaKompetensi" id="namaManagerial" placeholder="Nama Kompetensi"/>
												</div>
												<br><br>	
											</div>		
										</div>
								    </div>
									<div class="modal-footer">
										<button type="button" class="btn btn-warning tutupKompetensi" data-dismiss="modal">Tutup</button>
										<button type="submit" class="btn btn-success">Tambah</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="modal fade" id="nipPegawai" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							        <h3 class="modal-title" id="myModalLabel">NIP Pegawai</h3>
							    </div>
							    <div class="modal-body" >
									<div class="form-group">
										<div class="form-group">	
											<div class="col-md-6">
												<input type="text" class="form-control" name="katakunci" id="katakunci_pegawai" placeholder="cari..."/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>
											<br><br>	
										</div>		
										<div style="margin-left:20px; margin-right:20px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 10px 0px 10px">
											<table class="table table-striped table-bordered table-hover tabelinformasi">
												<thead>
													<tr class="warning">
														<td>NIP</td>
														<td>Nama Lengkap</td>
														<td width="10%">Pilih</td>
													</tr>
												</thead>
												<tbody id="t_body_pegawai">
													
												</tbody>
											</table>												
										</div>
									</div>
							    </div>
								<div class="modal-footer">
									<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal fade" id="npPelamar" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							        <h3 class="modal-title" id="myModalLabel">No. Pelamar</h3>
							    </div>
							    <div class="modal-body" >
									<div class="form-group">
										<div class="form-group">	
											<div class="col-md-6">
												<input type="text" class="form-control" name="katakunci" id="katakunci_pelamar" placeholder="cari..."/>
											</div>
											<div class="col-md-2">
												<button type="button" class="btn btn-info">Cari</button>
											</div>
											<br><br>	
										</div>		
										<div style="margin-left:20px; margin-right:20px;"><hr></div>
										<div class="portlet-body" style="margin: 0px 10px 0px 10px">
											<table class="table table-striped table-bordered table-hover tabelinformasi">
												<thead>
													<tr class="warning">
														<td>NIP</td>
														<td>Nama Lengkap</td>
														<td width="10%">Pilih</td>
													</tr>
												</thead>
												<tbody id="t_body_pelamar">
													
												</tbody>
											</table>												
										</div>
									</div>
							    </div>
								<div class="modal-footer">
									<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
				</div> <!-- Tab Content -->
			</div> <!-- Col MD 10 -->
			<div class="col-md-2">	
				<ul class="nav nav-tabs tabs-right">
					<li class="active"><a href="#a" data-toggle="tab" id="tabA"><span class="glyphicon glyphicon-user"></span><br>Data Pribadi</a></li>
					<li><a href="#b" data-toggle="tab" id="tabB"><span class="glyphicon glyphicon-education"></span>&nbsp&nbsp<span class="glyphicon glyphicon-home"></span><br>Riwayat Pendidikan<br>& Susunan Keluarga</a></li>
				</ul>
			</div> <!-- Col MD 2 -->
		</div>
	</div>
</body>
</html>