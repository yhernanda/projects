<?php 
header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename=exceldetailpegawai.xls');
header('Pragma: no-cache');
header('Expires: 0');
?>
<table border="1" width='70%'>
	<tr>
		<?php if($this->session->userdata("user_input")["role"] == 'HRD'){ ?>
			<th colspan='23'>DATA PEGAWAI</th>
		<?php }elseif ($this->session->userdata("user_input")["role"] == 'Admin' || $this->session->userdata("user_input")["role"] == 'Direktur') { ?>
			<th colspan='21'>DATA PEGAWAI</th>
		<?php } ?>
	</tr>
	<tr>
		<th>NIP</th>
		<th>Nama Lengkap</th>
		<th>Tgl. Lahir</th>
		<th>Jns. Kelamin</th>
		<th>Alamat Domisili</th>
		<th>Alamat Asal</th>
		<th>No. Telp. Rumah</th>
		<th>No. Telp. HP</th>
		<th>Agama</th>
		<th>Status Nikah</th>
		<th>Tanggal Nikah</th>
		<th>No. KTP</th>
		<th>Tinggi Bdn</th>
		<th>Berat Bdn</th>
		<th>Gol. Darah</th>
		<th>NPWP</th>
		<th>No. Jamsostek</th>
		<?php if($this->session->userdata("user_input")["role"] == 'HRD'){ ?>
			<th>Golongan</th>
			<th>Jabatan</th>
		<?php } ?>
		<th>Tgl. Masuk</th>
		<th>Tgl. Berhenti</th>
		<th>Masa Kerja</th>
		<th>Anak No.</th>
	</tr>
	<?php
		$align = 'style="vertical-align:middle;" align="center"';
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td '.$align.'>="'.$value["nip"].'"</td>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["tgl_lahir"].'</td>
					<td '.$align.'>'.$value["jns_kelamin"].'</td>
					<td '.$align.'>'.$value["alamat_domisili"].'</td>
					<td '.$align.'>'.$value["alamat_asal"].'</td>
					<td '.$align.'>="'.$value["telp_rmh"].'"</td>
					<td '.$align.'>="'.$value["telp_hp"].'"</td>
					<td '.$align.'>'.$value["agama"].'</td>
					<td '.$align.'>'.$value["status_perkawinan"].'</td>
					<td '.$align.'>'.$value["tgl_nikah"].'</td>
					<td '.$align.'>="'.$value["no_ktp"].'"</td>
					<td '.$align.'>'.$value["tinggi_bdn"].'</td>
					<td '.$align.'>'.$value["berat_bdn"].'</td>
					<td '.$align.'>'.$value["gol_darah"].'</td>
					<td '.$align.'>="'.$value["npwp"].'"</td>
					<td '.$align.'>="'.$value["no_jamsostek"].'"</td>';
					if($this->session->userdata("user_input")["role"] == 'HRD'){
						echo '<td '.$align.'>'.$value["golongan"].'</td>
						<td '.$align.'>'.$value["jabatan"].'</td>';
					}	
					echo '<td '.$align.'>'.$value["tgl_masuk"].'</td>
					<td '.$align.'>'.$value["tgl_berhenti"].'</td>';
					$date1 = date_create(date('Y-m-d'));
					if($value["tgl_berhenti"] != ''){
						$date1 = date_create($value["tgl_berhenti"]);
					}
					$date2 = date_create($value["tgl_masuk"]);
					$diff=date_diff($date1,$date2);
					echo '<td '.$align.'>'.$diff->format("%y tahun, %m bulan, %d hari").'</td>
					<td '.$align.'>'.$value["anak_no"].'</td>
				</tr>';
			}
		}
	?>
</table>
<?php if(!empty($gaji) && ($this->session->userdata("user_input")["role"] == 'Admin' || $this->session->userdata("user_input")["role"] == 'Direktur')){ ?>
	<br>
	<br>
	<br>
	<table border="1" width='70%'>
		<tr>
			<th colspan='7'>GAJI PEGAWAI</th>
		</tr>
		<tr>
			<th>Golongan</th>
			<th>Jabatan</th>
			<th>Gaji Pokok</th>
			<th>Tnj. Jabatan</th>
			<th>Jml. Hadir</th>
			<th>Bsr. Tunjangan</th>
			<th>Tnj. Kemahalan</th>
			<th>Total</th>
		</tr>
		<?php 
			if(!empty($gaji)){
				foreach ($gaji as $key => $gaji) {
					echo '<tr>
						<td '.$align.'>'.$value["golongan"].'</td>
						<td '.$align.'>'.$value["jabatan"].'</td>
						<td '.$align.'>'.$gaji["gaji_pokok"].'</td>
						<td '.$align.'>'.$gaji["tunj_jabatan"].'</td>
						<td '.$align.'>'.$gaji["total_hari"].'</td>
						<td '.$align.'>'.$gaji["tunj_kehadiran"].'</td>
						<td '.$align.'>'.$gaji["tunj_kemahalan"].'</td>
						<td '.$align.'>'.($gaji["gaji_pokok"]+$gaji["tunj_jabatan"]+($gaji["total_hari"]*$gaji["tunj_kehadiran"])+$gaji["tunj_kemahalan"]).'</td>
					</tr>';
				}
			}
		?>
	</table>
<?php } ?>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='2'>KOMPETENSI TEKNIS</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($teknis)){
			foreach ($teknis as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["nama_kompetensi"].'</td>
					<td '.$align.'>'.$value["penguasaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='2'>KOMPETENSI MANAGERIAL</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($managerial)){
			foreach ($managerial as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["nama_kompetensi"].'</td>
					<td '.$align.'>'.$value["penguasaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th>HOBBY PEGAWAI</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td align="left" style="vertical-align:middle;">'.$value["hobby"].'</td>
				</tr>';
			}
		}
	?>
	<tr>
		<th>BUKU BACAAN</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td align="left" style="vertical-align:middle;">'.$value["buku_bacaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='5'>PENDIDIKAN PEGAWAI</th>
	</tr>
	<tr>
		<th>Pendidikan</th>
		<th>Nama Sekolah</th>
		<th>Jurusan</th>
		<th>Kota</th>
		<th>Thn. Lulus</th>
	</tr>
	<?php
		if(!empty($pendidikan)){
			foreach ($pendidikan as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["pendidikan"].'</td>
					<td '.$align.'>'.$value["nama_sekolah"].'</td>
					<td '.$align.'>'.$value["jurusan"].'</td>
					<td '.$align.'>'.$value["kota"].'</td>
					<td '.$align.'>'.$value["thn_lulus"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='6'>ORANG TUA</th>
	</tr>
	<tr>
		<th>Status</th>
		<th>Nama</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket/Pekerjaan</th>
		<th>No. Telpon</th>
	</tr>
	<?php
		if(!empty($ortu)){
			$count = 1;
			$size = count($ortu);
			foreach ($ortu as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["orang_tua"].' '.$value["status"].'</td>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["tgl_lahir"].'</td>
					<td '.$align.'>'.$value["pendidikan"].'</td>
					<td '.$align.'>'.$value["ket_pekerjaan"].'</td>';
					if($count == 1)
						echo '<td '.$align.' rowspan="'.$size.'">="'.$value["no_telp"].'"</td>';
				echo '</tr>';
				$count++;
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='5'>KELUARGA</th>
	</tr>
	<tr>
		<th>Status</th>
		<th>Nama</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket/Pekerjaan</th>
	</tr>
	<?php
		if(!empty($keluarga)){
			foreach ($keluarga as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["status"].'</td>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["tgl_lahir"].'</td>
					<td '.$align.'>'.$value["pendidikan"].'</td>
					<td '.$align.'>'.$value["ket_pekerjaan"].'</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='3'>NOMOR DARURAT</th>
	</tr>
	<tr>
		<th>Nama</th>
		<th>Alamat</th>
		<th>No. Telp</th>
	</tr>
	<?php
		if(!empty($darurat)){
			foreach ($darurat as $key => $value) {
				echo '<tr>
					<td '.$align.'>'.$value["nama"].'</td>
					<td '.$align.'>'.$value["alamat"].'</td>
					<td '.$align.'>="'.$value["no_telp"].'"</td>
				</tr>';
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border="1" width='70%'>
	<tr>
		<th colspan='2'>CATATAN PERUSAHAAN</th>
	</tr>
	<tr>
		<th>Pujian & Rekomendasi</th>
		<th>Insiden Kritis</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td align="left" style="vertical-align:middle;">'.$value["pujian_rekomendasi"].'</td>
					<td align="left" style="vertical-align:middle;">'.$value["insiden_kritis"].'</td>
				</tr>';
			}
		}
	?>
</table>