<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>PT. GUM DB Pegawai</title>
	<?php include "userlist-js.php"; ?>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Junction.otf">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/timeline.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sb-admin-2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">

    <!-- DataTables CSS & JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>

	<!-- JQuery & JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
</head>
<body>
	<div id="wrapper">
		<?php include "navbar.php"; ?>

		<div id="page-wrapper">			
			<div class="col-md-12" style="margin-top:40px;">
				<div class="panel panel-default">
					<div class="panel-heading">
                        <h4><i class="glyphicon glyphicon-user">&nbsp</i>Daftar User</h4>
                    </div>
                    <div class="panel-body">
						<table class="table table-striped table-responsive table-bordered table-hover" id="tabel_user">
							<thead>
								<tr class="bg-primary">
									<td align="center">Foto</td>
									<td align="center">Username</td>
									<td align="center">Nama Lengkap</td>
									<td align="center">Login Terakhir</td>
									<td align="center">Role</td>									
									<td align="center">Aksi</td>
								</tr>
							</thead>
							<tbody id="t_body_users" style="background-color: #fff;">
								<?php  
									if (!empty($users)) {
										$align = "vertical-align:middle";
										foreach ($users as $value) {
											echo '<tr>'.
													'<td style="display:none" class="user_id">'.$value['user_id'].'</td>'.
													'<td align="center" style="'.$align.'"><img src="'.base_url();
													if($value['foto'] != "")
														echo $value['foto'];
													else
														echo "assets/img/default_user.jpg";
													echo '" class="img-thumbnail" style="cursor:pointer; height:98px; overflow:auto;"></td>'.
													'<td align="center" class="username" style="'.$align.'">'.$value['username'].'</td>'.
													'<td align="center" class="full_name" style="'.$align.'">'.$value['full_name'].'</td>'.
													'<td align="center" style="'.$align.'">'.$value['login_terakhir'].'</td>'.
													'<td align="center" class="role" style="'.$align.'">'.$value['role'].'</td>'.													
													'<td align="center" style="'.$align.'">';
														if(($value['role'] != "Admin" && $this->session->userdata('user_input')['role'] == "Direktur")||$this->session->userdata('user_input')['role'] == "Admin")
															echo '<a href="#" class="edituser"><i id="tool" data-toggle="modal" data-target="#edituser" data-placement="top" title="Edit User" class="glyphicon glyphicon-edit" style="cursor:pointer"></i></a>';
														if($value['user_id'] != $this->session->userdata('user_input')['user_id'] && (($this->session->userdata('user_input')['role'] == "Admin")
															|| ($this->session->userdata('user_input')['role'] == "Direktur" && $value['role'] != "Admin")))
															echo '&nbsp<a href="#" class="hapususer"><i id="tool" data-toggle="tooltip" data-placement="top" title="Hapus User" class="glyphicon glyphicon-trash" style="cursor:pointer"></i></a>';
													echo '</td>'.
												'</tr>';
										}
									}
								?>
							</tbody>
						</table>
						<div class="pull-right"><a href="#" class="btn btn-success" data-toggle="modal" data-target="#regisuser">Tambah User Baru</a></div>
                    </div>
				</div>

				<div class="modal fade" id="regisuser" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<form method="POST" enctype="multipart/form-data" class="form-horizontal" role="form" id="formregistrasi">
							<div class="modal-content" style="width:450px; margin:auto;">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							        <h3 class="modal-title" id="myModalLabel">Registrasi User Baru</h3>
							    </div>
							    <div class="modal-body" style="font-size:12px;">
									<div class="form-group">
										<div style="height:120px; width:80px; margin:auto;">
			                                <img src="<?php echo base_url(); ?>assets/img/default_user.jpg" class="img-thumbnail"  id="picture" data-toggle='tooltip' data-placement='bottom' title='Ganti Gambar' style="cursor:pointer; height:98px; overflow:auto;">
			                                <input id="btn-upload" type="file" name="picture" style="display:none;" onchange="preview(this)">
			                            </div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Username <span style="color:red">*</span></label>
										<div class="col-md-9">
											<input required type="text" class="form-control" id="username" name="username" placeholder="Username"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Full Name</label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Password <span style="color:red">*</span></label>
										<div class="col-md-9">
											<input required type="password" class="form-control" id="password" name="password" placeholder="Password"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Confirm Password <span style="color:red">*</span></label>
										<div class="col-md-9">
											<input required type="password" class="form-control" id="confirm_password" placeholder="Confirm Password"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Role <span style="color:red">*</span></label>
										<div class="col-md-9">
											<select class="form-control select" name="role" id="role">												
												<!--<?php 
													/*if($this->session->userdata('user_input')['role'] == "Admin")
														echo "<option value='1'>Admin</option>"; */
												?>-->									
												<option value="2">Direktur</option>
												<option value="3">HRD</option>
											</select>
										</div>
									</div>
							    </div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-warning">REGISTRASI</button>
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="modal fade" id="edituser" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<form method="POST" enctype="multipart/form-data" class="form-horizontal" role="form" id="formedit">
							<div class="modal-content" style="width:450px; margin:auto;">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
							        <h3 class="modal-title" id="myModalLabel">Edit User</h3>
							    </div>
							    <div class="modal-body" style="font-size:12px;">
									<div class="form-group">
										<div style="height:120px; width:80px; margin:auto;">
			                                <img src="<?php echo base_url(); ?>assets/img/default_user.jpg" class="img-thumbnail"  id="edit_picture" data-toggle='tooltip' data-placement='bottom' title='Ganti Gambar' style="cursor:pointer; height:98px; overflow:auto;">
			                                <input id="edit_btn-upload" type="file" name="picture" style="display:none;" onchange="preview(this)">
			                            </div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Username <span style="color:red">*</span></label>
										<div class="col-md-9">
											<input type="text" class="form-control hidden" id="edit_id" name="user_id"/>
											<input required type="text" class="form-control" id="edit_username" name="username" placeholder="Username"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Full Name</label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="edit_full_name" name="full_name" placeholder="Full Name"/>
										</div>
									</div>			
									<div class="form-group current_password">
										<label class="control-label col-md-3">Current Password <span style="color:red">*</span></label>
										<div class="col-md-9">
											<input type="password" class="form-control" id="current_password" placeholder="Current Password" name="current_password" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">New Password <span style="color:red">*</span></label>
										<div class="col-md-9">
											<input type="password" class="form-control" id="new_password" placeholder="New Password" name="password" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Confirm Password <span style="color:red">*</span></label>
										<div class="col-md-9">
											<input type="password" class="form-control" id="edit_confirm_password" placeholder="Confirm Password"/>
										</div>
									</div>									
									<div class="form-group">
										<label class="control-label col-md-3">Role <span style="color:red">*</span></label>
										<div class="col-md-9">
											<select class="form-control select" name="role" id="edit_role">
												<?php 
													if($this->session->userdata('user_input')['role'] == "Admin")
														echo "<option value='Admin'>Admin</option>";
												?>
												<option value="Direktur">Direktur</option>
												<option value="HRD">HRD</option>
											</select>
										</div>
									</div>
							    </div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-warning">REGISTRASI</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>		
		</div>
	</div>
</body>
</html>