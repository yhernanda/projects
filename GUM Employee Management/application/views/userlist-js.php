<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

<script type="text/javascript">
		$(document).ready(function () {
			var dataAjax;
			var currentPic;
			$('[id="tool"]').tooltip();
			$('#picture').tooltip();
			$('#edit_picture').tooltip();

			$('#picture').click(function(){
				$('#btn-upload').click();
			})

			$('#edit_picture').click(function(){
				$('#edit_btn-upload').click();
			})

			$(document).on('change', '#btn-upload', function (event){
				if (event.target.files && event.target.files[0]) {
			        var reader = new FileReader();
			        reader.onload = function (e) {
			            $('#picture').attr('src', e.target.result);
			        }

			        reader.readAsDataURL(event.target.files[0]);
			    }
			})

			$(document).on('change', '#edit_btn-upload', function (event){
				if (event.target.files && event.target.files[0]) {
			        var reader = new FileReader();
			        reader.onload = function (e) {
			            $('#edit_picture').attr('src', e.target.result);
			        }

			        reader.readAsDataURL(event.target.files[0]);
			    }
			})

			$('#tabel_user').on('click', 'tr td a.edituser', function (e) {
				currentPic = $(this).closest('tr').find('img.img-thumbnail');
				$('#edit_picture').attr("src", currentPic.attr("src"));
	        	$('#edit_id').val($(this).closest('tr').find('td.user_id').text());
	        	$('#edit_username').val($(this).closest('tr').find('td.username').text());
	        	$('#edit_full_name').val($(this).closest('tr').find('td.full_name').text())
				$('#new_password').val('');
				$('#edit_confirm_password').val('');
				$("select#edit_role").val($(this).closest('tr').find('td.role').text());
				$.ajax({
					type: "POST",
					url: '<?php echo base_url() ?>users/getuserdata/',
					async: false,
					success: function(data){
						
						if($('#edit_id').val() == data['id'])
							$('#edit_role').attr('disabled', 'disabled');
						else
							$('#edit_role').removeAttr("disabled");

						if($('#edit_role').val() == data['role'])
							$('.current_password').removeClass("hidden");
						else
							$('.current_password').addClass("hidden");
					}
				})
			})

			$('#tabel_user').on('click', 'tr td a.hapususer', function (e) {
				e.preventDefault();
				
				var d = confirm('Hapus user?');
				if (d == true){
					var user_id = $(this).closest('tr').find('td.user_id').text();
					$.ajax({
						type: "POST",
						url: '<?php echo base_url() ?>users/hapus_user/'+user_id,
						success: function (data) {
						},
						error: function  (data) {
							alert('terjadi kesalahan, gagal');
							return false;
						}						
					})
				}else{
					return false;
				}
				$(this).closest('tr').fadeOut(function () {
					$(this).remove();
				})
			})

			$(document).on('submit', '#formedit', function(e){
				if($('#new_password').val() != $('#edit_confirm_password').val()){
					e.preventDefault();
					alert("Cek password anda kembali!");
					$('#new_password').focus();
				} else {
					$('#edit_role').removeAttr("disabled");
					$('#edituser').modal('hide');
					var formData = new FormData($(this)[0]);
					$.ajax({
				        url: "<?php echo base_url() ?>users/edituser/",
				        type: 'POST',
				        data: formData,
				        async: false,				    						       
				        success: function (data) {
				        alert("!!!!!");			        	
				        	if(data['error'] == "none"){
				        		alert("Data berhasil diubah!");
				        	}
				        	else if(data['error'] == "exist"){
								alert("Username tidak tersedia!");
							} else if(data['error'] == "wrong") {
								alert("Password awal salah!");
							} 
													
				        	window.location.replace("<?php echo base_url() ?>users/daftar_user#")
				    		
				    		/*$('#edit_btn-upload').val('');				  
				        	$('#edit_id').val('');
				        	$('#edit_username').val('');
				        	$('#edit_full_name').val('');
							$('#new_password').val('');
							$('#edit_confirm_password').val('');
							$("select#edit_role").val("Admin");*/													
				        },

				        cache: false,
				        contentType: false,
				        processData: false
				    });
				}
			})

			$(document).on('submit', '#formregistrasi', function(e){
				if($('#password').val() != $('#confirm_password').val()){
					e.preventDefault();
					alert("Cek password anda kembali!");
					$('#password').focus();
				} else {
					$('#regisuser').modal('hide');
					var formData = new FormData($(this)[0]);
					$.ajax({
				        url: "<?php echo base_url() ?>users/regisuser/",
				        type: 'POST',
				        data: formData,
				        async: false,
				        success: function (data) {
				        	console.log(data);		    
				    		$('#btn-upload').val('');
				        	$('#picture').attr("src", "<?php echo base_url(); ?>assets/img/default_user.jpg");
				        	$('#username').val('');
				        	$('#full_name').val('');
							$('#password').val('');
							$('#confirm_password').val('');
							$("#role option[value='"+1+"']").attr("selected", "selected");

							if(data['error'] != "exist"){
								alert('User baru berhasil ditambahkan!');
								var img = "assets/img/default_user.jpg";
								$('#tabel_user tbody:first').append(
									'<tr>'+
										'<td style="display:none" class="my_user_id">'+data[1]['user_id']+'</td>'+
										'<td id="img" align="center"></td>'+
										'<td align="center">'+data[1]['username']+'</td>'+
										'<td align="center">'+data[1]['password']+'</td>'+
										'<td align="center">'+data[1]['role']+'</td>'+
										'<td align="center"><a href="#" class="hapususer"><i id="tool" data-toggle="tooltip" data-placement="top" title="hapus user" data- class="glyphicon glyphicon-trash" style="cursor:pointer"></i></a></td>'+
									'</tr>'
								)
							} else {
								alert("Username tidak tersedia!");
							}	
				        },
				        error: function  (data) {
							alert('terjadi kesalahan, gagal');
							return false;
						},	
				        cache: false,
				        contentType: false,
				        processData: false
				    });
					
		    		return false;
				}
			})

			/*$('#formregistrasi').submit(function (e) {
				e.preventDefault();
				var item = {};
			    item[1] = {};
				item[1]['username'] = $('#username').val();
				item[1]['password'] = $('#password').val();
				item[1]['role'] = $('#role').find('option:selected').val();
				//item[1]['foto'] = $('#foto').val();

				if (item[1]['username'] == "" || item[1]['password'] == "") {
					alert('isi data dengan benar');
					$('#username').focus();
				}else{
					$.ajax({
						type: "POST",
						data: item,
						url: '<?php echo base_url() ?>users/regisuser',
						success: function (data) {
							$('#username').val('');
							$('#password').val('');
							$("#role option[value='"+1+"']").attr("selected", "selected");
							//$('#foto').val('');

							if(data['error'] != "exist"){
								alert('User baru berhasil ditambahkan!');

								$('#tabel_user tbody:first').append(
									'<tr>'+
										'<td style="display:none" class="my_user_id">'+data['user_id']+'</td>'+
										'<td></td>'+
										'<td align="center">'+data['username']+'</td>'+
										'<td align="center">'+data['password']+'</td>'+
										'<td align="center">'+data['role']+'</td>'+
										'<td align="center"><a href="#" class="hapususer"><i id="tool" data-toggle="tooltip" data-placement="top" title="hapus user" data- class="glyphicon glyphicon-trash" style="cursor:pointer"></i></a></td>'+
									'</tr>'
								)
							} else {
								alert("Username tidak tersedia!");
							}
						},
						error: function (data) {
							alert('terjadi kesalahan, gagal');
							return false;
						}						
					})

					$('#regisuser').modal('hide');
				}
			})*/
		})
	</script>