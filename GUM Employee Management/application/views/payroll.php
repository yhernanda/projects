<!DOCTYPE html>
<html lang="em">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>PT. GUM DB Pegawai</title>
    <?php include "payroll-js.php"; ?>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Junction.otf">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/metisMenu.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/timeline.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sb-admin-2.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/morris.css">

    <!-- DataTables CSS & JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>

	<!-- JQuery & JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/js.js"></script>    

</head>
<body>
    <div id="wrapper">
        <?php include "navbar.php"; ?>
        
        <div id="page-wrapper" style="padding-top:40px;">
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-user">&nbsp</i>Data Pegawai
                        </div>
                        <div class="panel-body">
                            <form class="form form-horizontal">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url(); ?>assets/img/default_user.jpg" class="img-thumbnail" id="foto">
                                </div>                                                     
                                <div class="form-group">                                    
                                    <label class="control-label col-md-2">NIP</label>
                                    <div class="col-md-6" style="margin-bottom:5px;">
                                        <input disabled type="text" class="form-control" id="nip" name="nip"/>
                                    </div>
                                    <label class="control-label col-md-2">Nama</label>
                                    <div class="col-md-6" style="margin-bottom:5px;">
                                        <input disabled type="text" class="form-control" id="nama" name="nama"/>
                                    </div>
                                    <label class="control-label col-md-2">Golongan</label>
                                    <div class="col-md-6">
                                        <input disabled type="text" class="form-control" id="golongan" name="golongan"/>
                                    </div>
                                </div>                                                
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div> <!-- Col-md-6 -->

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-calendar">&nbsp</i>Bulan & Hari Kerja
                        </div>
                        <div class="panel-body" style="padding-left:30px;">
                            <form class="form form-horizontal">                                                
                                <div class="form-group">
                                    <label class="control-label col-md-4">Bulan</label>
                                    <div class="col-md-5">
                                        <input disabled type="text" class="form-control" value="<?php echo date('M Y'); ?>">
                                    </div>                                
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Total Hari Kerja</label>
                                    <div class="col-md-5" style="margin-bottom:20px;">
                                        <input type="text" class="form-control hari" id="hari_kerja" placeholder="Jumlah hari">
                                    </div>
                                </div>                            
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div> <!-- Col-md-6 -->
            </div>            
                        
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="glyphicon glyphicon-briefcase">&nbsp</i>Absensi Pegawai
                    </div>
                    <div class="panel-body" style="padding-left:30px;">
                        <form class="form form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-4">Cuti</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control absen" placeholder="Jumlah hari">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Mangkir</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control absen" placeholder="Jumlah hari">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Ijin</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control absen" placeholder="Jumlah hari">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Sakit</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control absen" placeholder="Jumlah hari">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label col-md-5">Total Jumlah Absen</label>
                                <div class="col-md-4">
                                    <input disabled type="text" id="total_absen" class="form-control absen">
                                    <input disabled type="text" class="form-control number hidden" name="total_hari" id="total_hari">
                                </div>
                            </div>
                        </form>                            
                    </div>
                </div>
            </div>
            
            <?php if ($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur") { ?>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="glyphicon glyphicon-usd">&nbsp</i>Tunjangan
                        </div>
                        <div class="panel-body" style="padding-left:30px">
                            <form class="form form-horizontal" role="form" method="post">
                                <?php
                                    $status = "disabled";                                
                                    if ($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur") {
                                        $status = "";
                                    }
                                ?>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Gaji Pokok</label>
                                    <div class="col-md-6">
                                        <input <?php echo $status; ?> type="text" class="form-control number" name="gaji_pokok" id="gaji_pokok" placeholder="Gaji Pokok">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tunjangan Jabatan</label>
                                    <div class="col-md-6">
                                        <input <?php echo $status; ?> type="text" class="form-control number" name="tunj_jabatan" id="tunj_jabatan" placeholder="Tunjangan Jabatan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tunjangan Kemahalan</label>
                                    <div class="col-md-6">
                                        <input <?php echo $status; ?> type="text" class="form-control number" name="tunj_kemahalan" id="tunj_kemahalan" placeholder="Tunjangan Kemahalan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Tunjangan Kehadiran</label>                                
                                    <div class="col-md-9">
                                        <div class="col-md-5" style="padding-left:0px;">
                                            <input disabled type="text" class="form-control" name="nominal_kehadiran" id="nominal_kehadiran" placeholder="Tunjangan Kehadiran">
                                        </div>                                    
                                        <label class="control-label col-md-1">Hari</label>
                                        <div class="col-md-2">
                                            <input disabled type="text" class="form-control number" name="total_hari" id="total_hari">                                                                        
                                        </div>                                    
                                        <label class="control-label col-md-1">x</label>
                                        <div class="col-md-3">
                                            <input <?php echo $status; ?> type="text" class="form-control number" name="tunj_kehadiran" placeholder="Nominal" id="tunj_kehadiran">
                                        </div>                                    
                                    </div>                                                       
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Gaji</label>
                                    <div class="col-md-6">
                                        <input disabled type="text" class="form-control number" name="gaji" id="gaji">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-usd">&nbsp</i>Tunjangan
                            </div>
                            <div class="panel-body" style="padding-left:30px">
                                <form class="form form-horizontal" role="form" method="post">
                                    <?php
                                        $status = "disabled";                                
                                        if ($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur") {
                                            $status = "";
                                        }
                                    ?>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">JAMSOSTEK & DPLK</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number tunjangan" name="jamsostek_dplk" id="jamsostek_dplk" placeholder="Jamsostek & DPLK">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">PPH PS 21 (PAJAK)/BULAN</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number tunjangan" name="pph_ps" id="pph_ps" placeholder="PPH PS">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">ASKES/BULAN</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number tunjangan" name="askes" id="askes" placeholder="Askes/Bulan">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Total Gaji</label>
                                        <div class="col-md-6">
                                            <input disabled type="text" class="form-control number" name="total_gaji" id="total_gaji">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-usd">&nbsp</i>Pengurangan
                            </div>
                            <div class="panel-body" style="padding-left:30px">
                                <form class="form form-horizontal" role="form" method="post">
                                    <?php
                                        $status = "disabled";                                
                                        if ($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur") {
                                            $status = "";
                                        }
                                    ?>

                                    <div class="form-group">
                                        <label class="control-label col-md-3">Pinjaman & Bunga</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number kurang" name="pinjaman_bunga" id="pinjaman_bunga" placeholder="Pinjaman & Bunga">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">BPJS</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number kurang" name="bpjs" id="bpjs" placeholder="BPJS">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">JAMSOSTEK/DPLK</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number kurang" name="jamsostek_dplk_pengurangan" id="jamsostek_dplk_pengurangan" placeholder="Askes/Bulan">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Total Pengurangan</label>
                                        <div class="col-md-6">
                                            <input disabled type="text" class="form-control number" name="total_kurang" id="total_kurang">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="glyphicon glyphicon-usd">&nbsp</i>Gaji Diterima
                            </div>
                            <div class="panel-body" style="padding-left:30px">
                                <form class="form form-horizontal" role="form" method="post">
                                    <?php
                                        $status = "disabled";                                
                                        if ($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur") {
                                            $status = "";
                                        }
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Total Gaji Diterima</label>
                                        <div class="col-md-6">
                                            <input disabled type="text" class="form-control number" name="thp" id="thp">
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">JAMSOSTEK</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number" name="jamsostek" id="jamsostek" placeholder="JAMSOSTEK">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">DPLK</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number" name="dplk" id="dplk" placeholder="DPLK">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">BPJS</label>
                                        <div class="col-md-6">
                                            <input <?php echo $status; ?> type="text" class="form-control number" name="bpjs" id="bpjs" placeholder="BPJS">
                                        </div>
                                    </div>                        
                                </form>
                            </div>
                        </div>
                    </div>
                </div> <!-- Col-md-6 -->   
            <?php } 
                if ($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur"){ $name = "role"; }
                else $name = "";
            ?>
            <a id="next" name="<?php echo $name; ?>" class="btn btn-success pull-right">Next <i class="glyphicon glyphicon-chevron-right"></i></a>
            <a disabled="disabled" id="back" name="<?php echo $name; ?>" class="btn btn-success pull-right"><i class="glyphicon glyphicon-chevron-left"> Back</i></a>            
        </div>
    </div>
</body>
</html>