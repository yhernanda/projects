<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.js"></script>

<script type="text/javascript">

	$(document).ready(function (event) {
		var teknis = $('#tabelTeknis tbody tr').length;
		var manager = $('#tabelManagerial tbody tr').length;
		var keluarga = $('#rowSaudara tr').length + $('#rowPasangan tr').length + $('#rowAnak tr').length;
		var gajiSebelum = 0;
		var hadirSebelum = 0;
		var pendidikan = $('#tabelPendidikan tbody tr').length;
		var numOrtu = 0;
		//alert($('#rowSaudara tr').length + " " + $('#rowPasangan tr').length + " " + $('#rowAnak tr').length);


		$('#picture').click(function(){
			$('#btn-upload').click();
		})

		$(document).on('change', '#btn-upload', function (event){
			if (event.target.files && event.target.files[0]) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $('#picture').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(event.target.files[0]);
		    }
		})

		/*$(document).on('blur', '#insidenEditor', function (){
			alert($(this).html());
		});*/
		$('#picture').tooltip();
		$('[id="tool"]').tooltip();

		$(document).on('change', '#nip', function(event){
			if($(event.target).val().match(/^\d+$/)){
				$(event.target).val($(event.target).val().substr(0, 4));
			} else{
				alert("Nomor Induk harus berupa karakter numerik!");
				$(event.target).val("");				
			}
		})

		$(document).on('change', '#np', function(event){
			if($(event.target).val().match(/^\d+$/)){
				$(event.target).val($(event.target).val().substr(0, 4));
			} else{
				alert("Nomor Pelamar harus berupa karakter numerik!");
				$(event.target).val("");				
			}
		})

		$(document).on('change', '.num', function(event){
			if(!$(event.target).val().match(/^\d+$/)){
				alert("Input harus berupa karakter numerik!");
				$(event.target).val("");				
			}
		})

		if($('#ortuKandung').length){
			$('#tambahOrtu').attr("disabled", "disabled");
			numOrtu = 2;
		}
		if($('#ortuMertua').length){
			$('#tambahMertua').attr("disabled", "disabled");
			numOrtu = 2;
		}
		if($('#tabelLainnya > #rowPasangan > tr').length > 0){
			$('#tambahPasangan').attr("disabled", "disabled");
		}

		$(document).on('click', '.submitPribadi', function (){
			$('#hobbyInput').val($('#hobbyEditor').html());
			$('#bukuInput').val($('#bukuEditor').html());
			$('#rekomendasiInput').val($('#rekomendasiEditor').html());
			$('#insidenInput').val($('#insidenEditor').html());
			$('#nip-keluarga').val($('#nip').val())
		})

		$('#katakunci_pegawai').keyup(function (e) {
			e.preventDefault();
			var keyword = $('#katakunci_pegawai').val();
			if(keyword != ""){
				$.ajax({
					type: "POST",
					url: "<?php echo base_url() ?>welcome/search_pegawai/"+keyword,
					success: function (data) {
						$('#t_body_pegawai').empty();
						for (var i = data.length - 1; i >= 0; i--) {
							$('#t_body_pegawai').append(
								'<tr>'+
									'<td>'+data[i]["nip"]+'</td>'+
									'<td>'+data[i]["nama"]+'</td>'+
									'<td><input type="radio" class="select_pegawai" id="'+data[i]['nip']+'" name="select_pegawai" value="'+data[i]['nip']+'"/></td>'+
								'</tr>'
							);

						};
					},
					error: function (data) {
						console.log(data);
						alert('gagal');
					}
				})
			}
			
		});

		$('#katakunci_pelamar').keyup(function (e) {
			e.preventDefault();
			var keyword = $('#katakunci_pelamar').val();
			if(keyword != ""){
				$.ajax({
					type: "POST",
					url: "<?php echo base_url() ?>welcome/search_pelamar/"+keyword,
					success: function (data) {
						$('#t_body_pelamar').empty();
						for (var i = data.length - 1; i >= 0; i--) {
							$('#t_body_pelamar').append(
								'<tr>'+
									'<td>'+data[i]["np"]+'</td>'+
									'<td>'+data[i]["nama"]+'</td>'+
									'<td><input type="radio" class="select_pelamar" id="'+data[i]['np']+'" name="select_pelamar" value="'+data[i]['np']+'"/></td>'+
								'</tr>'
							);

						};
					},
					error: function (data) {
						console.log(data);
						alert('gagal');
					}
				})	
			}
		});

		$(document).on('click', '.select_pegawai', function (event){
			var nip = event.target.id;
			var text_nip = document.getElementById('nipKeluarga');
			text_nip.value = nip;
		});

		$(document).on('click', '.select_pelamar', function (event){
			var np = event.target.id;
			var text_np = document.getElementById('npKeluarga');
			text_np.value = np;
		});

		$('#hobbyEditor').wysiwyg({ toolbarSelector: '[data-role=hobbyEditor-toolbar]'});

		$('#bukuEditor').wysiwyg({ toolbarSelector: '[data-role=bukuEditor-toolbar]'});

		$('#rekomendasiEditor').wysiwyg({ toolbarSelector: '[data-role=rekomendasiEditor-toolbar]'});

		$('#insidenEditor').wysiwyg({ toolbarSelector: '[data-role=insidenEditor-toolbar]'});

		$(document).on('click', '.tutupKompetensi', function (){
			$('#namaKompetensi').val("");
		});

		$(document).on('change', '#status_perkawinan', function(e){
			e.preventDefault();
			if($(this).val() == 2){
				$('#tgl_nikah').attr("disabled", "disabled");
				$('.pasangan').attr("disabled", "disabled");
				$('.rowPasangan').addClass("hidden");
			} else {
				$('#tgl_nikah').removeAttr("disabled");
				$('.pasangan').removeAttr("disabled");
				$('.rowPasangan').removeClass("hidden");			}
		});

		$(document).on('submit', '#dataPribadiPegawai', function(){
			if($('#tgl_nikah').val() == ""){
				$('#tgl_nikah').val(null);
			}
			if($('#tgl_berhenti').val() == ""){
				$('#tgl_berhenti').val(null);	
			}
			var formData = new FormData($(this)[0]);

			$.ajax({
		        url: "<?php echo base_url() ?>input/insert_pegawai/",
		        type: 'POST',
		        data: formData,
		        async: false,
		        success: function (data) {
		        	$('#nipKeluarga').val($('#nip').val());
		            $('#tabB').click();
					$('#tabA').attr("disabled", "disabled");
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });

    		return false;
		})

		$(document).on('submit', '#dataPribadiPelamar', function(){
			if($('#tgl_nikah').val() == ""){
				$('#tgl_nikah').val(null);
			}
			var formData = new FormData($(this)[0]);

			$.ajax({
		        url: "<?php echo base_url() ?>input/insert_pelamar/",
		        type: 'POST',
		        data: formData,
		        async: false,
		        success: function (data) {
		        	$('#npKeluarga').val($('#np').val());
		            $('#tabB').click();
					$('#tabA').attr("disabled", "disabled");
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });

    		return false;
		})

		$(document).on('submit', '#updateDataPribadiPegawai', function(){
			if($('#tgl_nikah').val() == ""){
				$('#tgl_nikah').val(null);
			}
			if($('#tgl_berhenti').val() == ""){
				$('#tgl_berhenti').val(null);	
			}
			var formData = new FormData($(this)[0]);
			console.log(formData);
			$.ajax({
		        url: "<?php echo base_url() ?>update/update_pegawai/",
		        type: 'POST',
		        data: formData,
		        async: false,
		        success: function (data) {
		            $('#nipKeluarga').val($('#nip').val());
		            $('#tabB').click();
					$('#tabA').attr("disabled", "disabled");
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });

    		return false;
		})

		$(document).on('submit',"form#updateDataPribadiPelamar", function(){
			if($('#tgl_nikah').val() == ""){
				$('#tgl_nikah').val(null);
			}
			var formData = new FormData($(this)[0]);

			$.ajax({
		        url: "<?php echo base_url() ?>update/update_pelamar/",
		        type: 'POST',
		        data: formData,
		        async: false,
		        success: function (data) {
		        	$('#npKeluarga').val($('#np').val());
		            $('#tabB').click();
					$('#tabA').attr("disabled", "disabled");
		        },
		        cache: false,
		        contentType: false,
		        processData: false
		    });

    		return false;
		})

		$(document).on('submit', '#tambahTeknis', function (e) {
			e.preventDefault();
			var keyword = $('#namaTeknis').val();
			$('#teknis').append(
					'<tr>'+
						'<td align="center"><a style="cursor:pointer;" class="hapusKompetensi" id="tool" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a></td>'+
						'<td>' + keyword + '<input type="text" class="hidden" name="teknis['+teknis+'][name]" value="' + keyword + '"></td>'+
						'<td align="center">' + '<input required type="radio" name="teknis['+teknis+'][value]" value="1"></td>'+
						'<td align="center">' + '<input required type="radio" name="teknis['+teknis+'][value]" value="2"></td>'+
						'<td align="center">' + '<input required type="radio" name="teknis['+teknis+'][value]" value="3"></td>'+
						'<td align="center">' + '<input required type="radio" name="teknis['+teknis+'][value]" value="4"></td>'+
					'</tr>'
			);
			$('#modalTeknis').modal('toggle');	
			$('#namaTeknis').val("");
			teknis += 1;
		});

		$(document).on('submit', '#tambahManagerial', function (e) {
			e.preventDefault();
			var keyword = $('#namaManagerial').val();
			$('#managerial').append(
					'<tr>'+
						'<td align="center"><a style="cursor:pointer;" class="hapusKompetensi" id="tool" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a></td>'+
						'<td>' + keyword + '<input type="text" class="hidden" name="managerial['+manager+'][name]" value="' + keyword + '"></td>'+
						'<td align="center">' + '<input required type="radio" name="managerial['+manager+'][value]" value="1"></td>'+
						'<td align="center">' + '<input required type="radio" name="managerial['+manager+'][value]" value="2"></td>'+
						'<td align="center">' + '<input required type="radio" name="managerial['+manager+'][value]" value="3"></td>'+
						'<td align="center">' + '<input required type="radio" name="managerial['+manager+'][value]" value="4"></td>'+
					'</tr>'
			);
			$('#modalManagerial').modal('toggle');	
			$('#namaManagerial').val("");
			manager += 1;
		});

		$(document).on('click', '#tambahPendidikan', function (e) {
			e.preventDefault();
			$('#pendidikan').append(
				'<tr>'+
					'<td style="vertical-align:middle;" align="center"><a style="cursor:pointer;" class="hapusPendidikan" id="tool" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="glyphicon glyphicon-trash"></i></a></td>'+
					'<td>'+
						'<select required class="form-control select" name="pendidikan['+pendidikan+'][pendidikan]">'+
							'<option value="1">SD</option>'+
							'<option value="2">SMP/Sederajat</option>'+
							'<option value="3">SMA/Sederajat</option>'+
							'<option value="4">Diploma 1</option>'+
							'<option value="5">Diploma 2</option>'+
							'<option value="6">Diploma 3</option>'+
							'<option value="7">Strata 1</option>'+
							'<option value="8">Strata 2</option>'+
							'<option value="9">Strata 3</option>'+
							'<option value="10">Akademis</option>'+
						'</select>'+
					'</td>'+
					'<td><input required type="text" class="form-control" name="pendidikan['+pendidikan+'][nama]" placeholder="Nama Sekolah"></td>'+
					'<td><input type="text" class="form-control" name="pendidikan['+pendidikan+'][jurusan]" placeholder="Jurusan"></td>'+
					'<td><input required type="text" class="form-control" name="pendidikan['+pendidikan+'][kota]" placeholder="Kota Sekolah"></td>'+
					'<td><input required type="text" class="form-control" name="pendidikan['+pendidikan+'][lulus]" placeholder="Tahun Lulus"></td>'+
				'</tr>'
			);
			pendidikan += 1;
		});

		function tambahOrtu(status){
			var ortu = "Ayah";
			var tambahan = "";
			for (var i = numOrtu; i < numOrtu+2; i++) {
				if(i == numOrtu+1)
				{
					ortu = "Ibu";
					tambahan = '<tr>'+
						'<td style="vertical-align:middle;">Alamat</td>'+
						'<td colspan="5"><input required type="text" name="ortu['+i+'][alamat]" class="form-control"></td>'+
					'</tr>'+
					'<tr>'+
						'<td style="vertical-align:middle;">Nomor Telepon</td>'+
						'<td colspan="5"><input type="text" name="ortu['+i+'][no_telp]" class="form-control"></td>'+
					'</tr>';
				}
				$('#rowOrtu').append('<tr>'+
					'<td class="hidden"><input required type="text" name="ortu['+i+'][status]" value="'+ status +'"></td>'+
					'<td style="vertical-align:middle;">'+ ortu +' '+ status +'<input required type="text" class="hidden" name="ortu['+i+'][orang_tua]" value="'+ ortu +'"></td>'+
					'<td><input required type="text" name="ortu['+i+'][nama]" class="form-control" placeholder="Nama '+ortu+'"></td>'+
					'<td><input type="text" class="form-control" data-provide ="datepicker" name="ortu['+i+'][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir '+ ortu +'"></td>'+
					'<td>'+
						'<select required name="ortu['+i+'][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir '+ ortu +'">'+
							'<option value="1">SD</option>'+
							'<option value="2">SMP/Sederajat</option>'+
							'<option value="3">SMA/Sederajat</option>'+
							'<option value="4">Diploma 1</option>'+
							'<option value="5">Diploma 2</option>'+
							'<option value="6">Diploma 3</option>'+
							'<option value="7">Strata 1</option>'+
							'<option value="8">Strata 2</option>'+
							'<option value="9">Strata 3</option>'+
							'<option value="10">Akademis</option>'+
						'</select>'+
					'</td>'+
						'<td><input required type="text" name="ortu['+i+'][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan '+ ortu +'"></td>'+
					'</tr>'+ tambahan);	
			};
			numOrtu+=2;
		}

		$(document).on('click', '#tambahOrtu', function(e){
			tambahOrtu("Kandung");
			$(this).attr("disabled", "disabled");
		})

		$(document).on('click', '#tambahMertua', function(e){
			tambahOrtu("Mertua");
			$(this).attr("disabled", "disabled");
		})

		$(document).on('click', '#tambahSaudara', function(e){
			e.preventDefault();
			$('#rowSaudara').append('<tr>'+
				'<td>'+
					'<select required class="form-control select" name="kel['+keluarga+'][status]">'+
						'<option value="1">Kakak</option>'+
						'<option value="2">Adik</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<input required type="text" name="kel['+keluarga+'][nama]" class="form-control" placeholder="Nama Keluarga">'+
				'</td>'+
				'<td>'+
					'<input type="text" class="form-control" data-provide ="datepicker" name="kel['+keluarga+'][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Keluarga">'+
				'</td>'+
				'<td>'+
					'<select required name="kel['+keluarga+'][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Keluarga">'+
						'<option value="1">SD</option>'+
							'<option value="2">SMP/Sederajat</option>'+
							'<option value="3">SMA/Sederajat</option>'+
							'<option value="4">Diploma 1</option>'+
							'<option value="5">Diploma 2</option>'+
							'<option value="6">Diploma 3</option>'+
							'<option value="7">Strata 1</option>'+
							'<option value="8">Strata 2</option>'+
							'<option value="9">Strata 3</option>'+
							'<option value="10">Akademis</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<input required type="text" name="kel['+keluarga+'][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Keluarga">'+
				'</td>'+
				'<td style="vertical-align:middle" align="center">'+
					'<a style="cursor:pointer;" class="hapuskeluarga" data-placement="top" title="Hapus Keluarga" id="tool"><i class="glyphicon glyphicon-trash"></i></a></td></tr>'
			);
			keluarga+=1;
		});

		$(document).on('click', '#tambahPasangan', function(e){
			e.preventDefault();
			$('#rowPasangan').append('<tr>'+
				'<td>'+
					'<select required class="form-control select" name="kel['+keluarga+'][status]">'+
						'<option value="4">Suami</option>'+
						'<option value="5">Isteri</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<input required type="text" name="kel['+keluarga+'][nama]" class="form-control" placeholder="Nama Pasangan">'+
				'</td>'+
				'<td>'+
					'<input required type="text" class="form-control" data-provide ="datepicker" name="kel['+keluarga+'][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Suami/Isteri">'+
				'</td>'+
				'<td>'+
					'<select required name="kel['+keluarga+'][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Suami/Isteri">'+
						'<option value="1">SD</option>'+
						'<option value="2">SMP/Sederajat</option>'+
						'<option value="3">SMA/Sederajat</option>'+
						'<option value="4">Diploma 1</option>'+
						'<option value="5">Diploma 2</option>'+
						'<option value="6">Diploma 3</option>'+
						'<option value="7">Strata 1</option>'+
						'<option value="8">Strata 2</option>'+
						'<option value="9">Strata 3</option>'+
						'<option value="10">Akademis</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<input required type="text" name="kel['+keluarga+'][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Suami/Isteri">'+
				'</td>'+
				'<td style="vertical-align:middle" align="center">'+
					'<a style="cursor:pointer;" class="hapusPasangan" data-placement="top" title="Hapus Suami/Isteri" id="tool"><i class="glyphicon glyphicon-trash"></i></a></td></tr>'
			);
			keluarga+=1;
			$(this).attr("disabled", "disabled")
		});

		$(document).on('click', '#tambahAnak', function(e){			
			e.preventDefault();
			$('#rowAnak').append('<tr>'+
				'<td style="vertical-align:middle;" align="center">'+
					'Anak<input required type="text" class="hidden" name="kel['+keluarga+'][status]" value="3">'+
				'</td>'+
				'<td><input required type="text" name="kel['+keluarga+'][nama]" class="form-control" placeholder="Nama Anak"></td>'+
				'<td><input required type="text" class="form-control" data-provide ="datepicker" name="kel['+keluarga+'][tgl_lahir]" date-date-format="dd/mm/yyyy" placeholder="Tgl. Lahir Anak"></td>'+
				'<td>'+
					'<select required name="kel['+keluarga+'][pendidikan]" class="form-control select" placeholder="Pendidikan Terakhir Anak">'+
						'<option value="1">SD</option>'+
						'<option value="2">SMP/Sederajat</option>'+
						'<option value="3">SMA/Sederajat</option>'+
						'<option value="4">Diploma 1</option>'+
						'<option value="5">Diploma 2</option>'+
						'<option value="6">Diploma 3</option>'+
						'<option value="7">Strata 1</option>'+
						'<option value="8">Strata 2</option>'+
						'<option value="9">Strata 3</option>'+
						'<option value="10">Akademis</option>'+
					'</select>'+	
				'</td>'+
				'<td><input required type="text" name="kel['+keluarga+'][ket_pekerjaan]" class="form-control" placeholder="Ket/Pekerjaan Anak"></td>'+
				'<td style="vertical-align:middle" align="center">'+
					'<a style="cursor:pointer;" class="hapusAnak" data-placement="top" title="Hapus Anak" id="tool"><i class="glyphicon glyphicon-trash"></i></a>'+
				'</td>'+
			'</tr>');
			keluarga+=1;
		})

		$(document).on('click', '.hapusPendidikan', function(e){
			var d = confirm('Hapus Riwayat?');
			if(d == true){
				$(this).closest('tr').fadeOut(function () {
					$(this).remove();
				})
			} else {
				return false;
			}
		});

		function hapusPendidikan (url, e){
			var d = confirm('Hapus Riwayat?');
			if(d == true){
				$.ajax({
					type: "POST",
					url: url,
					success: function (data) {
							
					},
					error: function  (data) {
						alert('terjadi kesalahan, gagal');
						return false;
					}						
				});
				$(e.target).closest('tr').fadeOut(function () {
					$(e.target).remove();
				})
			} else {
				return false;
			}
		}

		$(document).on('click', '.hapusPendidikanEdit', function(e){
			var id = $(this).closest('tr').find('td.id_pendidikan').text();
			var url = 'http://localhost/database_pegawai/update/hapus_pendidikan/'+id;
			hapusPendidikan(url, e);
		});

		$(document).on('click', '.hapusPendidikanPelamarEdit', function(e){
			var id = $(this).closest('tr').find('td.id_pendidikan').text();
			var url = 'http://localhost/database_pegawai/update/hapus_pendidikan_pelamar/'+id;
			hapusPendidikan(url, e);
		});

		$(document).on('click', '.hapusKompetensi', function(e){
			var d = confirm('Hapus Kompetensi?');
			if(d == true){
				$(this).closest('tr').fadeOut(function () {
					$(this) .remove();
				})
			} else {
				return false;
			}
		});

		function hapusTeknis(e, url){
			var d = confirm('Hapus Kompetensi?');
			if(d == true){
				$.ajax({
					type: "POST",
					url: url,
					success: function (data) {
						
					},
					error: function  (data) {
						alert('terjadi kesalahan, gagal');
						return false;
					}						
				});
				$(e.target).closest('tr').fadeOut(function () {
					$(e.target).remove();
				})
			} else {
				return false;
			}
			return rtr;
		}

		$(document).on('click', '.hapusTeknisEdit', function(e){
			var id = $(this).closest('tr').find('input.id_kompetensi').val();
			var url = 'http://localhost/database_pegawai/update/hapus_kompetensi_teknis/'+id;
			hapusTeknis(e, url);
		});

		$(document).on('click', '.hapusTeknisPelamarEdit', function(e){
			var id = $(this).closest('tr').find('input.id_kompetensi').val();
			var url = 'http://localhost/database_pegawai/update/hapus_kompetensi_teknis_pelamar/'+id;
			hapusTeknis(e, url);
		});

		function hapusManagerial(url, e){
			var d = confirm('Hapus Kompetensi?');
			if(d == true){
				$.ajax({
					type: "POST",
					url: url,
					success: function (data) {
							
					},
					error: function  (data) {
						alert('terjadi kesalahan, gagal');
						return false;
					}						
				});
				$(e.target).closest('tr').fadeOut(function () {
					$(e.target).remove();
				})
			} else {
				return false;
			}
		}

		$(document).on('click', '.hapusManagerialEdit', function(e){
			var id = $(this).closest('tr').find('input.id_kompetensi').val();
			var url = 'http://localhost/database_pegawai/update/hapus_kompetensi_managerial/'+id;
			hapusManagerial(url, e);
		});

		$(document).on('click', '.hapusManagerialPelamarEdit', function(e){
			var id = $(this).closest('tr').find('input.id_kompetensi').val();
			var url = 'http://localhost/database_pegawai/update/hapus_kompetensi_managerial_pelamar/'+id;
			hapusManagerial(url, e);
		});

		function hapusKeluarga(url, e){
			var d = confirm('Hapus Keluarga?');
			if(d == true){
				$.ajax({
					type: "POST",
					url: url,
					success: function (data) {
						
					},
					error: function  (data) {
						alert('terjadi kesalahan, gagal');
						return false;
					}						
				});
				$(e.target).closest('tr').fadeOut(function () {
					$(e.target).remove();
				})
			} else {
				return false;
			}
		}

		$(document).on('click', '.hapusKeluargaEdit', function(e){
			var id = $(this).closest('tr').find('input.id_keluarga').val();
			var url = 'http://localhost/database_pegawai/update/hapus_keluarga/'+id;
			hapusKeluarga(url, e);
		});

		$(document).on('click', '.hapusKeluargaPelamarEdit', function(e){
			var id = $(this).closest('tr').find('input.id_keluarga').val();
			var url = 'http://localhost/database_pegawai/update/hapus_keluarga_pelamar/'+id;
			hapusKeluarga(url, e);
		});

		$(document).on('click', '.hapusSaudara', function(e){
			var d = confirm('Hapus Saudara?');
			if(d == true){
				$(this).closest('tr').fadeOut(function () {
					$(this).remove();
				})
			} else {
				return false;
			}
		});

		$(document).on('click', '.hapusAnak', function(e){
			var d = confirm('Hapus Keluarga?');
			if(d == true){
				$(this).closest('tr').fadeOut(function () {
					$(this).remove();
				})
			} else {
				return false;
			}
		});

		$(document).on('click', '.hapusPasangan', function(e){
			var d = confirm('Hapus Pasangan?');
			if(d == true){
				$(this).closest('tr').fadeOut(function () {
					$(this).remove();
				})
				$('#tambahPasangan').removeAttr("disabled");
			} else {
				return false;
			}
		});

		//All about GAJI//
		$(document).on('click', '#detailGaji', function(e){
			e.preventDefault();
			var target = $(e.target).attr('name');
			target = document.getElementById(target);
			$(target).attr('class', $(target).attr('class').replace(' hidden', ""));
			$(e.target).html("<i class='glyphicon glyphicon-chevron-up'></i> Sembunyikan");
			$(e.target).attr('id', "sembunyikanGaji");
		})

		$(document).on('click', '#sembunyikanGaji', function(e){
			e.preventDefault();
			var target = $(e.target).attr('name');
			target = document.getElementById(target);
			$(target).attr('class', $(target).attr('class')+" hidden");
			$(e.target).html("<i class='glyphicon glyphicon-chevron-down'></i> Detail Gaji");
			$(e.target).attr('id', "detailGaji");
		})

		$(document).on('focus', '#sumGaji', function(event){
			if($(event.target).val() != "" && !$(event.target).hasClass('hadir')){
				gajiSebelum = parseInt($(event.target).val());
			}
		})

		$(document).on('focus', '.jmlHadir', function(event){
			if($(event.target).val() != "" && $(event.target).hasClass('hadir')){
				hadirSebelum = parseInt($(event.target).val()) * parseInt($('.tunjangan').val());
			}else{
				hadirSebelum = parseInt($(event.target).val()) * parseInt($('.hadir').val());
			}
		})

		$(document).on('change', '#sumGaji', function(event){
			if($(event.target).val().match(/^\d+$/)){
				if($(event.target).hasClass('jmlHadir')){
					var multiple = ""
					if($(event.target).hasClass('hadir'))
						multiple = ".tunjangan";
					else
						multiple = ".hadir";

					if(hadirSebelum != 0){
						var sum = (parseInt($(event.target).val()) * parseInt($(multiple).val())) - hadirSebelum;
						hadirSebelum = 0;
					}else{
						var sum = parseInt($(event.target).val()) * parseInt($(multiple).val());
					}
				}else{
					if(gajiSebelum != 0){
						var sum = parseInt($(event.target).val()) - gajiSebelum;
						gajiSebelum = 0;
					}else{
						var sum = parseInt($(event.target).val());
					}
				}
				if($('#totalGaji').val() != ""){
					var totGaji = parseInt($('#totalGaji').val());
					totGaji += sum;
					$('#totalGaji').val(totGaji);
				}else{
					$('#totalGaji').val(sum);
				}
			}else{
				alert("Tidak dapat menyimpan karakter non numerik!");
				$(event.target).val("");
			}
			if($(event.target).val() ==""){
				var totGaji = parseInt($('#totalGaji').val());
				if(!$(event.target).hasClass('jmlHadir')){
					totGaji -= gajiSebelum;
					gajiSebelum = 0;
				}else{
					totGaji -= hadirSebelum;
					hadirSebelum = 0;
				}
				$('#totalGaji').val(totGaji);
				$(event.target).val(0);
			}
		})

		/*
		for (var i = 1; i < 5; i++) {
							'<td align="center">' + 'input type="checkbox" name="kompetensi[]" value="' + keyword.replace(" ", "_") + '_' + i + '">' + '</td>'+
						};

		$('.toggle').click(function(event){
			event.preventDefault();
			var target = $(event.target).attr('href');
			if ($(target).attr('class') == $(target).attr('id') + " hidden") {
				$(target).attr("class", $(target).attr('id'));
				$(this).attr("class", "toggle glyphicon glyphicon-arrow-up");
				$(this).text(" Hide");
			} else {
				$(target).attr("class", $(target).attr('id') + " hidden");
				$(this).attr("class", "toggle glyphicon glyphicon-arrow-down");
				$(this).text(" Expand");
			}
		});*/

	});
</script>