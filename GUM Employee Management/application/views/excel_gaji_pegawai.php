<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=excel_gaji_pegawai.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

<?php
for ($i=0, $count=count($gaji); $i<$count; $i+=2) {
	$hsl_gaji =  $gaji[$i]['gaji_pokok'] + $gaji[$i]['tunj_jabatan'] + ($gaji[$i]['tunj_kehadiran']*$gaji[$i]['total_hari']);
	$hsl_gaji2 = $gaji[$i+1]['gaji_pokok'] + $gaji[$i+1]['tunj_jabatan'] + ($gaji[$i+1]['tunj_kehadiran']*$gaji[$i+1]['total_hari']);
	$total_gaji = $hsl_gaji + $gaji[$i]['jamsostek_dplk'] + $gaji[$i]['pph_ps'] + $gaji[$i]['askes'];
	$total_gaji2 = $hsl_gaji2 + $gaji[$i+1]['jamsostek_dplk'] + $gaji[$i+1]['pph_ps'] + $gaji[$i+1]['askes'];
	$total_pengurangan = $gaji[$i]['pinjaman_bunga'] + $gaji[$i]['bpjs'] + $gaji[$i]['jamsostek_dplk_pengurangan'];
	$total_pengurangan2 = $gaji[$i+1]['pinjaman_bunga'] + $gaji[$i+1]['bpjs'] + $gaji[$i+1]['jamsostek_dplk_pengurangan'];
	$total_diterima = $hsl_gaji - $total_pengurangan;
	$total_diterima2 = $hsl_gaji2 - $total_pengurangan2;
?>
	
<div class="col-md-6">
	<table class='table table-striped' border='1'>
		<thead>
			<tr>
				<td colspan='3'>
					<h4>SLIP GAJI KARYAWAN</h4>
					NIP: <?php echo $gaji[$i]['nip']; ?><br>
					NAMA: <?php echo $gaji[$i]['nama']; ?><br>
					GUM YOGYA<br>
					<?php echo date('F Y'); ?>
				</td>
				<td width="5%"></td>
				<td colspan='3'>
					<h4>SLIP GAJI KARYAWAN</h4>
					NIP: <?php echo $gaji[$i+1]['nip']; ?><br>
					NAMA: <?php echo $gaji[$i+1]['nama']; ?><br>
					GUM YOGYA<br>
					<?php echo date('F Y'); ?>
				</td>
			</tr>
			<tr>
				<td class='bg-primary' colspan='3' style="text-align: center; font-weight: bolder;">
					KETERANGAN	
				</td>
				<td width="5%"></td>
				<td class='bg-primary' colspan='3' style="text-align: center; font-weight: bolder;">
					KETERANGAN	
				</td>
			</tr>
		</thead>
		<tbody>
				<tr>
					<td colspan='3' style="font-weight:bolder">TUNJANGAN:</td>
					<td width="5%"></td>
					<td colspan='3' style="font-weight:bolder">TUNJANGAN:</td>		
				</tr>
				<tr>
					<td width="3%" align="center">1</td>
					<td>Gaji Pokok & Tunjangan Kemahalan</td>
					<td><?php echo $gaji[$i]['gaji_pokok'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">1</td>
					<td>Gaji Pokok & Tunjangan Kemahalan</td>
					<td><?php echo $gaji[$i+1]['gaji_pokok'] ?></td>
				</tr>
				<tr>
					<td width="3%" align="center">2</td>
					<td>Tunjangan Jabatan/Spesialis/Fungsional</td>
					<td><?php echo $gaji[$i]['tunj_jabatan'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">2</td>
					<td>Tunjangan Jabatan/Spesialis/Fungsional</td>
					<td><?php echo $gaji[$i+1]['tunj_jabatan'] ?></td>
				</tr>
				<tr>
					<td width="3%" align="center">3</td>
					<td>Tunjangan Kehadiran (<?php echo $gaji[$i]['tunj_kehadiran'] ?> x <?php echo $gaji[$i]['total_hari'] ?> hr)</td>
					<td><?php echo $gaji[$i]['tunj_kehadiran']*$gaji[$i]['total_hari'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">3</td>
					<td>Tunjangan Kehadiran (<?php echo $gaji[$i+1]['tunj_kehadiran'] ?> x <?php echo $gaji[$i+1]['total_hari'] ?> hr)</td>
					<td><?php echo $gaji[$i+1]['tunj_kehadiran']*$gaji[$i+1]['total_hari'] ?></td>
				</tr>
				<tr>
					<td colspan='2' align='right' style="font-weight:bolder">GAJI</td>
					<td><?php echo $hsl_gaji ?></td>
					<td width="5%"></td>
					<td colspan='2' align='right' style="font-weight:bolder">GAJI</td>
					<td><?php echo $hsl_gaji2 ?></td>
				</tr>
				<tr>
					<td colspan='3'></td>
					<td width="5%"></td>
					<td colspan='3'></td>
				</tr>
				<tr>
					<td width="3%" align="center">4</td>
					<td>JAMSOSTEK & DPLK</td>
					<td><?php echo $gaji[$i]['jamsostek_dplk'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">4</td>
					<td>JAMSOSTEK & DPLK</td>
					<td><?php echo $gaji[$i+1]['jamsostek_dplk'] ?></td>
				</tr>
				<tr>
					<td width="3%" align="center">5</td>
					<td>PPH PS 21(PAJAK)/BLN</td>
					<td><?php echo $gaji[$i]['pph_ps'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">5</td>
					<td>PPH PS 21(PAJAK)/BLN</td>
					<td><?php echo $gaji[$i+1]['pph_ps'] ?></td>
				</tr>
				<tr>
					<td width="3%" align="center">6</td>
					<td>ASKES/BLN</td>
					<td><?php echo $gaji[$i]['askes'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">6</td>
					<td>ASKES/BLN</td>
					<td><?php echo $gaji[$i+1]['askes'] ?></td>
				</tr>
				<tr>
					<td colspan='2' align='right' style="font-weight:bolder">TOTAL GAJI</td>
					<td><?php echo $total_gaji ?></td>
					<td width="5%"></td>
					<td colspan='2' align='right' style="font-weight:bolder">TOTAL GAJI</td>
					<td><?php echo $total_gaji2 ?></td>
				</tr>
				<tr>
					<td colspan='3'></td>
					<td width="5%"></td>
					<td colspan='3'></td>
				</tr>
				<tr>
					<td colspan='3' style="font-weight:bolder">PENGURANGAN:</td>
					<td width="5%"></td>
					<td colspan='3' style="font-weight:bolder">PENGURANGAN:</td>
				</tr>
				<tr>
					<td width="3%" align="center">A</td>
					<td>PINJAMAN & BUNGA</td>
					<td><?php echo $gaji[$i]['pinjaman_bunga'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">A</td>
					<td>PINJAMAN & BUNGA</td>
					<td><?php echo $gaji[$i+1]['pinjaman_bunga'] ?></td>
				</tr>
				<tr>
					<td width="3%" align="center">B</td>
					<td>BPJS</td>
					<td><?php echo $gaji[$i]['bpjs'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">B</td>
					<td>BPJS</td>
					<td><?php echo $gaji[$i+1]['bpjs'] ?></td>
				</tr>
				<tr>
					<td width="3%" align="center">C</td>
					<td>JAMSOSTEK & DPLK</td>
					<td><?php echo $gaji[$i]['jamsostek_dplk_pengurangan'] ?></td>
					<td width="5%"></td>
					<td width="3%" align="center">C</td>
					<td>JAMSOSTEK & DPLK</td>
					<td><?php echo $gaji[$i+1]['jamsostek_dplk_pengurangan'] ?></td>
				</tr>
				<tr>
					<td colspan='2' align='right' style="font-weight:bolder">TOTAL PENGURANGAN</td>
					<td><?php echo $total_pengurangan ?></td>
					<td width="5%"></td>
					<td colspan='2' align='right' style="font-weight:bolder">TOTAL PENGURANGAN</td>
					<td><?php echo $total_pengurangan2 ?></td>
				</tr>
				<tr>
					<td colspan='3'></td>
					<td width="5%"></td>
					<td colspan='3'></td>
				</tr>
				<tr>
					<td colspan='2' align='left' style="font-weight:bolder">TOTAL GAJI DITERIMA</td>
					<td><?php echo $total_diterima ?></td>
					<td width="5%"></td>
					<td colspan='2' align='left' style="font-weight:bolder">TOTAL GAJI DITERIMA</td>
					<td><?php echo $total_diterima2 ?></td>
				</tr>
				<tr>
					<td colspan='3'></td>
					<td width="5%"></td>
					<td colspan='3'></td>
				</tr>
				<tr>
					<td colspan="2" align="left">JAMSOSTEK</td>
					<td><?php echo $gaji[$i]['jamsostek'] ?></td>
					<td width="5%"></td>
					<td colspan="2" align="left">JAMSOSTEK</td>
					<td><?php echo $gaji[$i+1]['jamsostek'] ?></td>
				</tr>
				<tr>
					<td colspan="2" align="left">DPLK</td>
					<td><?php echo $gaji[$i]['dplk'] ?></td>
					<td width="5%"></td>
					<td colspan="2" align="left">DPLK</td>
					<td><?php echo $gaji[$i+1]['dplk'] ?></td>
				</tr>
				<tr>
					<td colspan="2" align="left">BPJS</td>
					<td><?php echo $gaji[$i]['jamsostek_dplk'] + $gaji[$i]['jamsostek_dplk_pengurangan'] ?></td>
					<td width="5%"></td>
					<td colspan="2" align="left">BPJS</td>
					<td><?php echo $gaji[$i+1]['jamsostek_dplk'] + $gaji[$i+1]['jamsostek_dplk_pengurangan'] ?></td>
				</tr>
				<tr>
					<td colspan='3'></td>
					<td width="5%"></td>
					<td colspan='3'></td>
				</tr>
		</tbody>
	</table>

	<table>
		<thead>
			<tr>
				<td align="left">Bag. Keuangan</td>
				<td></td>
				<td align="right">Menyetujui</td>
				<td width="5%"></td>
				<td align="left">Bag. Keuangan</td>
				<td></td>
				<td align="right">Menyetujui</td>	
			</tr>
			<tr></tr>
		</thead>
	</table>
</div>
<br>
<?php } ?>

<!---
<table class="table table-striped" border='1'>
	<thead>
		<tr>
			<th class="bg-primary" colspan="10">Detail Gaji Pegawai <?php echo date('M Y'); ?></th>
		</tr>
		<tr>
			<th class="bg-primary" rowspan="2">NIP</th>
			<th class="bg-primary" rowspan="2">Nama</th>
			<th class="bg-primary" rowspan="2">Golongan</th>
			<th class="bg-primary" rowspan="2">Gaji Pokok</th>
			<th class="bg-primary" rowspan="2">Tunjangan Jabatan</th>
			<th class="bg-primary" rowspan="2">Tunjangan Kemahalan</th>
			<th class="bg-primary" colspan="3">Tunjangan Kehadiran</th>
			<th class="bg-primary" rowspan="2">Take Home Pay</th>
		</tr>
		<tr>
			<th class="bg-primary">Hari Efektif</th>
			<th class="bg-primary">Nominal Perhari</th>
			<th class="bg-primary">Total Tunjangan</th>
		</tr>
	</thead>
	<tbody>
		<?php
			/*$kehadiran = $gaji[$i]['tunj_kehadiran']*$value['total_hari'];
			$thp = $value['gaji_pokok']+$value['tunj_jabatan']+$value['tunj_kemahalan']+$kehadiran;
			echo "<tr>
				<td style='color:black;'><?php $value['nip'] ?></td>
				<td style='color:black;'>".$value['nama']."</td>
				<td style='color:black;'>".$value['golongan']."</td>
				<td style='color:black;' align='right'>Rp.".$value['gaji_pokok'].",-</td>
				<td style='color:black;' align='right'>Rp.".$value['tunj_jabatan'].",-</td>
				<td style='color:black;' align='right'>Rp.".$value['tunj_kemahalan'].",-</td>
				<td style='color:black;' align='center'>".$value['total_hari']."</td>
				<td style='color:black;' align='right'>Rp.".$value['tunj_kehadiran'].",-</td>
				<td style='color:black;' align='right'>Rp.".$kehadiran.",-</td>
				<td style='color:black;' align='right'>Rp.".$thp.",-</td>
			</tr>";*/
		?>
	</tbody>
</table>
-->