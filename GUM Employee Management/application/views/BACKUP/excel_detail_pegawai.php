<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldetailpegawai.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border='1' width="70%">
	<tr>
		<?php
			if($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "Direktur"){
				$colspanData = 62;
				$colspanPribadi = 22;
			} else {
				$colspanData = 46;
				$colspanPribadi = 24;
			}
		?>
		<th colspan="<?php echo $colspanData; ?>">DATA PEGAWAI</th>
	</tr>
	<tr>
		<th>NIP</th>
		<?php
			if($this->session->userdata('user_input')['role'] == "HRD"){
		?>
			<th>Golongan</th>
			<th>Status</th>
		<?php } ?>
		<th>Nama Lengkap</th>
		<th>Tgl. Lahir</th>
		<th>Jns. Kelamin</th>
		<th>Alamat Domisili</th>
		<th>Alamat Asal</th>
		<th>No. Telp. Rumah</th>
		<th>No. Telp. HP</th>
		<th>Agama</th>
		<th>Status Nikah</th>
		<th>Tanggal Nikah</th>
		<th>No. KTP</th>
		<th>Tinggi Bdn</th>
		<th>Berat Bdn</th>
		<th>Gol. Darah</th>
		<th>NPWP</th>
		<th>No. Jamsostek</th>
		<th>Tgl. Masuk</th>
		<th>Tgl. Berhenti</th>
		<th>Masa Kerja</th>
		<th>Anak No.</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['nip']."</td>";
					if($this->session->userdata('user_input')['role'] == "HRD") { ?>
						<th>Golongan</th>
						<th>Status</th>
					<?php } ?>
					echo "<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['tgl_lahir']."</td>
					<td align='center'>".$value['jns_kelamin']."</td>
					<td align='center'>".$value['alamat_domisili']."</td>
					<td align='center'>".$value['alamat_asal']."</td>
					<td align='center'>".$value['telp_rmh']."</td>
					<td align='center'>".$value['telp_hp']."</td>
					<td align='center'>".$value['agama']."</td>
					<td align='center'>".$value['status_perkawinan']."</td>
					<td align='center'>".$value['tgl_nikah']."</td>
					<td align='center'>".$value['no_ktp']."</td>
					<td align='center'>".$value['tinggi_bdn']."</td>
					<td align='center'>".$value['berat_bdn']."</td>
					<td align='center'>".$value['gol_darah']."</td>
					<td align='center'>".$value['npwp']."</td>
					<td align='center'>".$value['no_jamsostek']."</td>
					<td align='center'>".$value['tgl_masuk']."</td>
					<td align='center'>".$value['tgl_berhenti']."</td>
					<td align='center'>".floor((time() - strtotime($value['tgl_masuk']))/(60*60*24))."</td>
					<td align='center'>".$value['anak_no']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<?php
if(!empty($gaji) && ($this->session->userdata('user_input')['role'] == "Admin" || $this->session->userdata('user_input')['role'] == "	Direktur")){ ?>

	<table border='1' width="70%">
		<tr>
			<th colspan="19">GAJI PEGAWAI</th>
		</tr>
		<tr>
			<th colspan="12">Gaji Pokok & Tunjangan</th>
			<th colspan="4">Pengurangan</th>
			<th colspan="3">Gaji Diterima</th>
		</tr>
		<tr>
			<th>Golongan</th>
			<th>Status</th>
			<th>Gaji Pokok</th>
			<th>Tnj. Jabatan</th>
			<th>Jml. Hadir</th>
			<th>Bsr. Tunjangan</th>
			<th>Tnj. Kemahalan</th>
			<th>TOTAL POKOK</th>
			<th>JAMSOSTEK & DPLK</th>
			<th>PPH PS 21 (PAJAK)/BULAN</th>
			<th>ASKES/BULAN</th>
			<th>TOTAL GAJI</th>
			<th>Pinjaman & Bunga</th>
			<th>BPJS</th>
			<th>JAMSOSTEK/DPLK</th>
			<th>TOTAL PENGURANGAN</th>
			<th>TOTAL GAJI DITERIMA</th>
			<th>JAMSOSTEK</th>
			<th>DPLK</th>
		</tr>
		<?php 
			foreach ($gaji as $key => $gaji) {
				echo "<tr>
					<td align='center'>".$value['golongan']."</td>
					<td align='center'>".$value['status_pegawai']."</td>
					<td align='center'>".$gaji['gaji_pokok']."</td>
					<td align='center'>".$gaji['tunj_jabatan']."</td>
					<td align='center'>".$gaji['total_hari']."</td>
					<td align='center'>".$gaji['tunj_kehadiran']."</td>
					<td align='center'>".$gaji['tunj_kemahalan']."</td>
					<td align='center'>".($gaji['gaji_pokok']+$gaji['tunj_jabatan']+($gaji['total_hari']*$gaji['tunj_kehadiran'])+$gaji['tunj_kemahalan'])."</td>
					<td align='center'>".$gaji['jamsostek_dplk']."</td>
					<td align='center'>".$gaji['pph_ps']."</td>
					<td align='center'>".$gaji['askes']."</td>
					<td align='center'></td>
					<td align='center'>".$gaji['pinjaman_bunga']."</td>
					<td align='center'>".$gaji['bpjs']."</td>
					<td align='center'>".$gaji['jamsostek_dplk_pengurangan']."</td>
					<td align='center'></td>
					<td align='center'></td>
					<td align='center'>".$gaji['jamsostek']."</td>
					<td align='center'>".$gaji['dplk']."</td>
				</tr>";
			}
		?>
	</table>
	<br>
	<br>
	<br>
<?php } ?>
<table border='1' width="70%">
	<tr>
		<th colspan="2">KOMPETENSI TEKNIS</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($teknis)){
			foreach ($teknis as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['nama_kompetensi']."</td>
					<td align='center'>".$value['penguasaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="2">KOMPETENSI MANAGERIAL</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($managerial)){
			foreach ($managerial as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['nama_kompetensi']."</td>
					<td align='center'>".$value['penguasaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th>HOBBY PEGAWAI</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo "<tr>
					<td align='left' style='vertical-align:middle;'>".$value['hobby']."</td>
				</tr>";
			}
		}
	?>
	<tr>
		<th>BUKU BACAAN</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo "<tr>
					<td align='left' style='vertical-align:middle;'>".$value['buku_bacaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="5">PENDIDIKAN PEGAWAI</th>
	</tr>
	<tr>
		<th>Pendidikan</th>
		<th>Nama Sekolah</th>
		<th>Jurusan</th>
		<th>Kota</th>
		<th>Thn. Lulus</th>
	</tr>
	<?php
		if(!empty($pendidikan)){
			foreach ($pendidikan as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['pendidikan']."</td>
					<td align='center'>".$value['nama_sekolah']."</td>
					<td align='center'>".$value['jurusan']."</td>
					<td align='center'>".$value['kota']."</td>
					<td align='center'>".$value['thn_lulus']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="5">KELUARGA</th>
	</tr>
	<tr>
		<th>Status</th>
		<th>Nama</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket/Pekerjaan</th>
	</tr>
	<?php
		if(!empty($ortu)){
			foreach ($ortu as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['orang_tua']." ".$value['status']."</td>
					<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['tgl_lahir']."</td>
					<td align='center'>".$value['pendidikan']."</td>
					<td align='center'>".$value['ket_pekerjaan']."</td>
				</tr>";
			}
		}
	?>
	<?php
		if(!empty($keluarga)){
			foreach ($keluarga as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['status']."</td>
					<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['tgl_lahir']."</td>
					<td align='center'>".$value['pendidikan']."</td>
					<td align='center'>".$value['ket_pekerjaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="3">NOMOR DARURAT</th>
	</tr>
	<tr>
		<th>Nama</th>
		<th>Alamat</th>
		<th>No. Telp</th>
	</tr>
	<?php
		if(!empty($darurat)){
			foreach ($darurat as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['alamat']."</td>
					<td align='center'>".$value['no_telp']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="2">CATATAN PERUSAHAAN</th>
	</tr>
	<tr>
		<th>Pujian & Rekomendasi</th>
		<th>Insiden Kritis</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo "<tr>
					<td align='left' style='vertical-align:middle;'>".$value['pujian_rekomendasi']."</td>
					<td align='left' style='vertical-align:middle;'>".$value['insiden_kritis']."</td>
				</tr>";
			}
		}
	?>
</table>