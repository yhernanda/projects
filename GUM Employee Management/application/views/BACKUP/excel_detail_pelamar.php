<?php 
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=exceldetailpelamar.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border='1' width="70%">
	<tr>
		<th colspan="19">DATA PELAMAR</th>
	</tr>
	<tr>
		<th>No. Pelamar</th>
		<th>Nama Lengkap</th>
		<th>Tgl. Lahir</th>
		<th>Jns. Kelamin</th>
		<th>Alamat Domisili</th>
		<th>Alamat Asal</th>
		<th>No. Telp. Rumah</th>
		<th>No. Telp. HP</th>
		<th>Agama</th>
		<th>Status Nikah</th>
		<th>Tanggal Nikah</th>
		<th>No. KTP</th>
		<th>Tinggi Bdn</th>
		<th>Berat Bdn</th>
		<th>Gol. Darah</th>
		<th>NPWP</th>
		<th>No. Jamsostek</th>
		<th>Tgl. Daftar</th>
		<th>Anak No.</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['np']."</td>
					<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['tgl_lahir']."</td>
					<td align='center'>".$value['jns_kelamin']."</td>
					<td align='center'>".$value['alamat_domisili']."</td>
					<td align='center'>".$value['alamat_asal']."</td>
					<td align='center'>".$value['telp_rmh']."</td>
					<td align='center'>".$value['telp_hp']."</td>
					<td align='center'>".$value['agama']."</td>
					<td align='center'>".$value['status_perkawinan']."</td>
					<td align='center'>".$value['tgl_nikah']."</td>
					<td align='center'>".$value['no_ktp']."</td>
					<td align='center'>".$value['tinggi_bdn']."</td>
					<td align='center'>".$value['berat_bdn']."</td>
					<td align='center'>".$value['gol_darah']."</td>
					<td align='center'>".$value['npwp']."</td>
					<td align='center'>".$value['no_jamsostek']."</td>
					<td align='center'>".$value['tgl_daftar']."</td>
					<td align='center'>".$value['anak_no']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="2">KOMPETENSI TEKNIS</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($teknis)){
			foreach ($teknis as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['nama_kompetensi']."</td>
					<td align='center'>".$value['penguasaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="2">KOMPETENSI MANAGERIAL</th>
	</tr>
	<tr>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
	</tr>
	<?php
		if(!empty($managerial)){
			foreach ($managerial as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['nama_kompetensi']."</td>
					<td align='center'>".$value['penguasaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th>HOBBY PELAMAR</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo "<tr>
					<td align='left' style='vertical-align:middle;'>".$value['hobby']."</td>
				</tr>";
			}
		}
	?>
	<tr>
		<th>BUKU BACAAN</th>
	</tr>
	<?php 
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo "<tr>
					<td align='left' style='vertical-align:middle;'>".$value['buku_bacaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="5">PENDIDIKAN PELAMAR</th>
	</tr>
	<tr>
		<th>Pendidikan</th>
		<th>Nama Sekolah</th>
		<th>Jurusan</th>
		<th>Kota</th>
		<th>Thn. Lulus</th>
	</tr>
	<?php
		if(!empty($pendidikan)){
			foreach ($pendidikan as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['pendidikan']."</td>
					<td align='center'>".$value['nama_sekolah']."</td>
					<td align='center'>".$value['jurusan']."</td>
					<td align='center'>".$value['kota']."</td>
					<td align='center'>".$value['thn_lulus']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="5">KELUARGA</th>
	</tr>
	<tr>
		<th>Status</th>
		<th>Nama</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket/Pekerjaan</th>
	</tr>
	<?php
		if(!empty($ortu)){
			foreach ($ortu as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['orang_tua']." ".$value['status']."</td>
					<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['tgl_lahir']."</td>
					<td align='center'>".$value['pendidikan']."</td>
					<td align='center'>".$value['ket_pekerjaan']."</td>
				</tr>";
			}
		}
	?>
	<?php
		if(!empty($keluarga)){
			foreach ($keluarga as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['status']."</td>
					<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['tgl_lahir']."</td>
					<td align='center'>".$value['pendidikan']."</td>
					<td align='center'>".$value['ket_pekerjaan']."</td>
				</tr>";
			}
		}
	?>
</table>
<br>
<br>
<br>
<table border='1' width="70%">
	<tr>
		<th colspan="3">NOMOR DARURAT</th>
	</tr>
	<tr>
		<th>Nama</th>
		<th>Alamat</th>
		<th>No. Telp</th>
	</tr>
	<?php
		if(!empty($darurat)){
			foreach ($darurat as $key => $value) {
				echo "<tr>
					<td align='center'>".$value['nama']."</td>
					<td align='center'>".$value['alamat']."</td>
					<td align='center'>".$value['no_telp']."</td>
				</tr>";
			}
		}
	?>
</table>