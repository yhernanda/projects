<?php 
header('Content-type: application/octet-stream');
header('Content-Disposition: attachment; filename=excel_data_pelamar.xls');
header('Pragma: no-cache');
header('Expires: 0');
?>
<table border="1" width='70%'>
	<tr>
		<th colspan='40'>DATA PELAMAR</th>
	</tr>
	<tr>
		<th colspan='20'>Data Pribadi</th>
		<th colspan='2'>Kompetensi Teknis</th>
		<th colspan='2'>Kompetensi Managerial</th>
		<th colspan='5'>Riwayat Pendidikan</th>
		<th colspan='8'>Data Keluarga</th>
		<th colspan='3'>Data Darurat</th>
	</tr>
	<tr>
		<th>No. Pelamar</th>
		<th>Nama Lengkap</th>
		<th>Tgl. Lahir</th>
		<th>Jns. Kelamin</th>
		<th>Alamat Domisili</th>
		<th>Alamat Asal</th>
		<th>No. Telp. Rumah</th>
		<th>No. Telp. HP</th>
		<th>Agama</th>
		<th>Status Nikah</th>
		<th>Tanggal Nikah</th>
		<th>No. KTP</th>
		<th>Tinggi Bdn</th>
		<th>Berat Bdn</th>
		<th>Gol. Darah</th>
		<th>NPWP</th>
		<th>No. Jamsostek</th>
		<th>Tgl. Masuk</th>
		<th>Hobby</th>
		<th>Buku Bacaan</th>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
		<th>Kompetensi</th>
		<th>Penguasaan</th>
		<th>Pendidikan</th>
		<th>Nama Sekolah</th>
		<th>Jurusan</th>
		<th>Kota</th>
		<th>Th. Lulus</th>
		<th>Anak Nomor</th>
		<th>Status</th>
		<th>Nama Keluarga</th>
		<th>Tgl. Lahir</th>
		<th>Pendidikan</th>
		<th>Ket./Pekerjaan</th>
		<th>Alamat</th>
		<th>No. Telp</th>
		<th>Nama Keluarga</th>
		<th>Alamat</th>
		<th>No. Telp</th>
	</tr>
	<?php 
		$align = 'style="vertical-align:middle;"';
		if(!empty($data)){
			foreach ($data as $key => $value) {
				echo '<tr>
					<td align="center" '.$align.'>="'.$value["np"].'"</td>
					<td align="center" '.$align.'>'.$value["nama"].'</td>
					<td align="center" '.$align.'>'.$value["tgl_lahir"].'</td>
					<td align="center" '.$align.'>'.$value["jns_kelamin"].'</td>
					<td align="center" '.$align.'>'.$value["alamat_domisili"].'</td>
					<td align="center" '.$align.'>'.$value["alamat_asal"].'</td>
					<td align="center" '.$align.'>="'.$value["telp_rmh"].'"</td>
					<td align="center" '.$align.'>="'.$value["telp_hp"].'"</td>
					<td align="center" '.$align.'>'.$value["agama"].'</td>
					<td align="center" '.$align.'>'.$value["status_perkawinan"].'</td>
					<td align="center" '.$align.'>'.$value["tgl_nikah"].'</td>
					<td align="center" '.$align.'>="'.$value["no_ktp"].'"</td>
					<td align="center" '.$align.'>'.$value["tinggi_bdn"].'</td>
					<td align="center" '.$align.'>'.$value["berat_bdn"].'</td>
					<td align="center" '.$align.'>'.$value["gol_darah"].'</td>
					<td align="center" '.$align.'>="'.$value["npwp"].'"</td>
					<td align="center" '.$align.'>="'.$value["no_jamsostek"].'"</td>
					<td align="center" '.$align.'>'.$value["tgl_daftar"].'</td>
					<td align="center" '.$align.'>'.$value["hobby"].'</td>
					<td align="center" '.$align.'>'.$value["buku_bacaan"].'</td>
					<td align="center" '.$align.'>';
				foreach ($teknis as $key => $tkns) {
					if($tkns["np"] == $value["np"])
					echo $tkns["nama_kompetensi"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($teknis as $key => $tkns) {
					if($tkns["np"] == $value["np"])
					echo $tkns["penguasaan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($managerial as $key => $mngr) {
					if($mngr["np"] == $value["np"])
					echo $mngr["nama_kompetensi"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($managerial as $key => $mngr) {
					if($mngr["np"] == $value["np"])
					echo $mngr["penguasaan"].'<br>';
				} echo '</td>';
				echo '<td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["np"] == $value["np"])
					echo $pend["pendidikan"].'<br>';	
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["np"] == $value["np"])
					echo $pend["nama_sekolah"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["np"] == $value["np"])
					echo $pend["jurusan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["np"] == $value["np"])
					echo $pend["kota"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($pendidikan as $key => $pend) {
					if($pend["np"] == $value["np"])
					echo $pend["thn_lulus"].'<br>';
				} echo '</td>';
				echo '<td align="center" '.$align.'>'.$value["anak_no"].'</td>
				<td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["np"] == $value["np"])
					echo $ot["orang_tua"].' '.$ot["status"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["np"] == $value["np"])
					echo $kel["status"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["np"] == $value["np"])
					echo $ot["nama"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["np"] == $value["np"])
					echo $kel["nama"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["np"] == $value["np"])
					echo $ot["tgl_lahir"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["np"] == $value["np"])
					echo $kel["tgl_lahir"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["np"] == $value["np"])
					echo $ot["pendidikan"].'<br>';
				}
				foreach ($keluarga as $key => $kel) {
					if($kel["np"] == $value["np"])
					echo $kel["pendidikan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["np"] == $value["np"])
					echo $ot["ket_pekerjaan"].'<br>';
				} 
				foreach ($keluarga as $key => $kel) {
					if($kel["np"] == $value["np"])
					echo $kel["ket_pekerjaan"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["np"] == $value["np"])
					echo $ot["alamat"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($ortu as $key => $ot) {
					if($ot["np"] == $value["np"])
					echo '="'.$ot["no_telp"].'"<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($darurat as $key => $drt) {
					if($drt["np"] == $value["np"])
					echo $drt["nama"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($darurat as $key => $drt) {
					if($drt["np"] == $value["np"])
					echo $drt["alamat"].'<br>';
				} echo '</td><td align="center" '.$align.'>';
				foreach ($darurat as $key => $drt) {
					if($drt["np"] == $value["np"])
					echo '="'.$drt["no_telp"].'"<br>';
				} echo '</td>';
			}
		}
	?>
</table>