<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function (){
		$('.number').val(0);
		$('#hari_kerja').removeAttr("disabled");
		var arr = [];
		var idx = 0;
		var totalAbsen = 0;
		var sebelum = 0;

		$.ajax({
			async: false,
			type: 'POST',
			url: '<?php echo base_url() ?>payroll/get_data_pegawai',
			success: function (data){
				arr = data;
				console.log(arr);
			}
		})

		function updateAllGaji(){
			window.location.replace("<?php echo base_url() ?>payroll/export_gaji_pegawai/");
		}

		//Proses Export Gaji
		$(document).on('click', '#export_gaji', function(){
			updateAllGaji();
		})

		$(document).on('click', '#done', function(){
			window.location.replace("<?php echo base_url() ?>welcome/dashboard_view/");
		})

		//Change data gaji pegawai (Next Pegawai)
		function changeData(){
			if(idx != 0){
				$('#hari_kerja').attr("disabled", "disabled");
			}
			
			if(arr[idx]['foto'] != "" && arr[idx]['foto'] != "NULL" && arr[idx]['foto'] != null){
				$('#foto').attr("src", "<?php echo base_url(); ?>"+arr[idx]['foto']);
			} else {
				$('#foto').attr("src", "<?php echo base_url(); ?>assets/img/default_user.jpg");
			}
			$('.number').val(0);
			$('.absen').val(0);
			$('#nip').val(arr[idx]['nip']);
			$('#nama').val(arr[idx]['nama']);
			$('#golongan').val(arr[idx]['golongan']);
			$('#nip').val(arr[idx]['nip']);
			$('#gaji_pokok').val(arr[idx]['gaji_pokok']);
			$('#tunj_jabatan').val(arr[idx]['tunj_jabatan']);
			$('#tunj_kemahalan').val(arr[idx]['tunj_kemahalan']);
			$('#tunj_kehadiran').val(arr[idx]['tunj_kehadiran']);
			$('#jamsostek_dplk').val(arr[idx]['jamsostek_dplk']);
			$('#pph_ps').val(arr[idx]['pph_ps']);
			$('#askes').val(arr[idx]['askes']);
			$('#pph_ps').val(arr[idx]['pph_ps']);
			$('#askes').val(arr[idx]['askes']);
			$('#pinjaman_bunga').val(arr[idx]['pinjaman_bunga']);
			$('#bpjs').val(arr[idx]['bpjs']);
			$('#jamsostek_dplk_pengurangan').val(arr[idx]['jamsostek_dplk_pengurangan']);
			$('#jamsostek').val(arr[idx]['jamsostek']);
			$('#dplk').val(arr[idx]['dplk']);
			//$('#total_hari').val("");
			//$('#thp').val("");
			//$('#tunj_kehadiran').val("");
			updateGaji();
			totalAbsen = 0;
			idx++;
			if(idx == arr.length){
				if($('#next').attr('name') == "role"){
					$('#next').html("<i class='glyphicon glyphicon-download-alt'></i> Export Data");
					$('#next').attr("id", "export_gaji");	
				} else {
					$('#next').html("<i class='glyphicon glyphicon-ok'></i> Done");
					$('#next').attr("id", "done");
				}
			}
		}

		function backData(){
			if(idx > 0){
				idx-=2;
				if(idx == 0){
					$('#back').attr("disabled", "disabled");
				}
			}
			if(idx != 0){
				$('#hari_kerja').attr("disabled", "disabled");
			}
			
			if(arr[idx]['foto'] != "" && arr[idx]['foto'] != "NULL" && arr[idx]['foto'] != null){
				$('#foto').attr("src", "<?php echo base_url(); ?>"+arr[idx]['foto']);
			} else {
				$('#foto').attr("src", "<?php echo base_url(); ?>assets/img/default_user.jpg");
			}
			$('.number').val(0);
			$('.absen').val(0);
			$('#nip').val(arr[idx]['nip']);
			$('#nama').val(arr[idx]['nama']);
			$('#golongan').val(arr[idx]['golongan']);
			$('#nip').val(arr[idx]['nip']);
			$('#gaji_pokok').val(arr[idx]['gaji_pokok']);
			$('#tunj_jabatan').val(arr[idx]['tunj_jabatan']);
			$('#tunj_kemahalan').val(arr[idx]['tunj_kemahalan']);
			$('#tunj_kehadiran').val(arr[idx]['tunj_kehadiran']);
			$('#jamsostek_dplk').val(arr[idx]['jamsostek_dplk']);
			$('#pph_ps').val(arr[idx]['pph_ps']);
			$('#askes').val(arr[idx]['askes']);
			$('#pph_ps').val(arr[idx]['pph_ps']);
			$('#askes').val(arr[idx]['askes']);
			$('#pinjaman_bunga').val(arr[idx]['pinjaman_bunga']);
			$('#bpjs').val(arr[idx]['bpjs']);
			$('#jamsostek_dplk_pengurangan').val(arr[idx]['jamsostek_dplk_pengurangan']);
			$('#jamsostek').val(arr[idx]['jamsostek']);
			$('#dplk').val(arr[idx]['dplk']);
			//$('#total_hari').val("");
			//$('#thp').val("");
			//$('#tunj_kehadiran').val("");
			updateGaji();
			totalAbsen = 0;
			idx++;
		}

		//Update Total Hari Kerja, T.Kehadiran, & THP
		function updateGaji(){
			var total = $('#hari_kerja').val() - $('#total_absen').val();			
			$('#total_hari').val(total);
			$('#total_hari').change();
			$('#nominal_kehadiran').val($('#total_hari').val() * $('#tunj_kehadiran').val())
			$('#gaji').val(parseInt($('#gaji_pokok').val()) + parseInt($('#tunj_jabatan').val()) + parseInt($('#tunj_kemahalan').val()) + parseInt($('#nominal_kehadiran').val()))
			updateTotalGaji();
			updateTotalPengurangan();
		}

		function updateTotalGaji(){
			$('#total_gaji').val(parseInt($('#gaji').val()) + parseInt($('#jamsostek_dplk').val()) + parseInt($('#pph_ps').val()) + parseInt($('#askes').val()))
			updateTotalDiterima();
		}

		function updateTotalPengurangan(){
			$('#total_kurang').val(parseInt($('#pinjaman_bunga').val()) + parseInt($('#bpjs').val()) + parseInt($('#jamsostek_dplk_pengurangan').val()))
			updateTotalDiterima();
		}

		function updateTotalDiterima(){
			var def = parseInt($('#jamsostek_dplk').val()) + parseInt($('#pph_ps').val()) + parseInt($('#askes').val())
			$('#thp').val(parseInt($('#gaji').val()) - parseInt($('#total_kurang').val()))
		}

		$(document).on('change', '.hari', function(){
			updateGaji();
		})

		$(document).on('change', '.tunjangan', function(){
			updateTotalGaji();
		})

		$(document).on('change', '.kurang', function(){
			updateTotalPengurangan();
		})

		$(document).on('click', '#next', function(){
			changeData();
			$('html,body').animate({scrollTop: 0}, 'medium');
			if(idx != 0){
				$("#back").removeAttr("disabled");
			}
		})

		$(document).on('click', '#back', function(){
			backData();
			$('html,body').animate({scrollTop: 0}, 'medium');
		})

		changeData();


		$(document).on('focus', '.absen', function(e){
			sebelum = $(e.target).val();
		})

		$(document).on('change', '.absen', function(e){
			var sesudah = $(e.target).val();
			var sum = sesudah - sebelum;
			totalAbsen += sum;
			$('#total_absen').val(totalAbsen);
			sebelum = sesudah;
			updateGaji();
		})

		//EDIT Gaji Pegawai
		$(document).on('change', '.number', function(e){
			var nama = $(e.target).attr('name');
 			var nilai = $(e.target).val();
			var nip = $('#nip').val();
			$.ajax({
				type: 'POST',
				url: '<?php echo base_url() ?>payroll/set_detail_gaji/'+nama+'/'+nilai+'/'+nip,
				success: function(data){

				}
			})	
		})
	})
</script>