<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function () {
		var activeTab = null;

		$('a[data-toggle="tab"]').on('click', function(event){
			activeTab = event.target.name;
		});

		$('#katakunci').keyup(function (e) {
			e.preventDefault();
			var keyword = $('#katakunci').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>welcome/searchkk/"+keyword,
				success: function (data) {
					$('#t_body_namakk').empty();
					for (var i = data.length - 1; i >= 0; i--) {
						$('#t_body_namakk').append(
								'<tr>'+
									'<td>'+data[i]["nama_kk"]+'</td>'+
									'<td>'+data[i]["alamat"]+'</td>'+
									'<td><input type="radio" class="select_kk" id="'+data[i]['kk_id']+'" name="select_kk" value="'+data[i]['nama_kk']+'"/></td>'+
								'</tr>'
							);

					};
				},
				error: function (data) {
					console.log(data);
					alert('gagal');
				}
			})
		});

		$(document).on('click', '.select_kk', function (event){
			var id_kk = event.target.id;
			var nama_kk = event.target.value;
			if(activeTab == "aset"){
				var text_kk = document.getElementById('nama_kepala_keluarga_aset');	
				$('#kk_id_aset').attr("value", id_kk);
			} else if(activeTab == "keluarga"){
				var text_kk = document.getElementById('nama_kepala_keluarga_data');	
				$('#kk_id_data').attr("value", id_kk);
			} else{
				var text_kk = document.getElementById('nama_kepala_keluarga');	
				$('#kk_id').attr("value", id_kk);
			}
			text_kk.value = nama_kk;
		});

		$('.toggle').click(function(event){
			event.preventDefault();
			var target = $(event.target).attr('href');
			if ($(target).attr('class') == $(target).attr('id') + " hidden") {
				$(target).attr("class", $(target).attr('id'));
				$(this).attr("class", "toggle glyphicon glyphicon-arrow-up");
				$(this).text(" Hide");
			} else {
				$(target).attr("class", $(target).attr('id') + " hidden");
				$(this).attr("class", "toggle glyphicon glyphicon-arrow-down");
				$(this).text(" Expand");
			}
		});

		$('#formdatakeluarga').submit(function (e) {
			//e.preventDefault();
			
		})


	})
</script>