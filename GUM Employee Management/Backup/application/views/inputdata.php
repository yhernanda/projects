<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DDK - Dusun Munggur</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/inputdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <!--<script type="text/javascript" src="<?=base_url()?>public/js/modernizr-2.6.2.min.js"></script>-->
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
	  	<div class="container">
		    	<div class="col-md-12">
			     	<div class="col-md-6">
			     		
			     	</div>
					<div class="col-md-6">
						<h4 class="pull-right"><i class="glyphicon glyphicon-user">&nbsp;</i><?php echo $this->session->userdata('user_input')['username']; ?> &nbsp;&nbsp;
						<a href="<?php echo base_url(); ?>welcome/viewdata" class="btn btn-success">View Data</a>
						<?php if ($this->session->userdata('user_input')['role'] === '0'): ?>
							<a href="<?php echo base_url(); ?>welcome/daftar_user" class="btn btn-success">Daftar Pengguna</a>
						<?php endif ?>
						<a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-success">Logout</a></h4>
					</div>
				</div>
			
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container" >
		<div class="col-md-12 customtitle" style='color:#fff;'>
			<h1>Dashboard Pengisian Data Dasar Keluarga</h1>
		</div>
		<ul class="nav nav-tabs" role="tablist" id="myTab">
			<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Data KK</a></li>
			<li role="presentation"><a href="#keluarga" name="keluarga" aria-controls="keluarga" role="tab" data-toggle="tab">Data Keluarga</a></li>
			<li role="presentation"><a href="#aset" name="aset" aria-controls="aset" role="tab" data-toggle="tab">Aset Keluarga</a></li>
			<li role="presentation"><a href="#profile" name="profile" aria-controls="profile" role="tab" data-toggle="tab">Data Anggota Keluarga</a></li>
		</ul>
		<div class="tab-content">

			<!-- DATA KEPALA KELUARGA -->
			<div role="tabpanel" class="tab-pane active" id="home">
				<div class="col-md-12 inputdatawarga">
					<form class="form-horizontal" role="form" method="post" id="forminputkk" action="<?php echo base_url(); ?>welcome/insert_kk">
						<div class="form-group">			
					    	<label class="control-label col-md-3" >Nama KK </label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="nama_kk" name="nama_kk" placeholder="Nama Kepala Keluarga"/>
							</div>
						</div>
						<div class="form-group">			
					    	<label class="control-label col-md-3" >Alamat </label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="alamat_kk" name="alamat_kk" placeholder="Alamat" />
							</div>
						</div>
						<div class="form-group">			
					        <label class="control-label col-md-3" >RT/RW </label>
							<div class="col-md-2">
								<input type="text" class="form-control" id="rt_kk" name="rt_kk" placeholder="RT" />
							</div>
							<div class="col-md-2">
								<input type="text" class="form-control" id="rw_kk" name="rw_kk" placeholder="RW" />
							</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Dusun </label>
								<div class="col-md-6">
									<!-- <input type="text" class="form-control" id="dusun" name="dusun" placeholder="Dusun" /> -->
									<select class="form-control select" name="dusun" id="dusun">
										<?php  
											if (!empty($dusun)) {
												foreach ($dusun as $value) {
													echo '<option value="'.$value['nama_dusun'].'">'.$value['nama_dusun'].'</option>';
												}
											}
										?>
									</select>
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Desa/Kelurahan </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="kelurahan" name="kelurahan" value="Watusigar" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Kecamatan </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="kecamatan" name="kecamatan" value="Ngawen" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Kabupaten </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="kabupaten" name="kabupaten" value="Gunung Kidul" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Provinsi </label>
								<div class="col-md-6">
									<input type="text" class="form-control" id="provinsi" name="provinsi" value="Yogyakarta" readonly="" />
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Kepemilikan lahan </label>
								<div class="col-md-6">
									<select class="form-control select" name="kepemilikan_lahan" id="kepemilikan_lahan">
										<?php  
											if (!empty($lahan)) {
												foreach ($lahan as $value) {
													echo '<option value="'.$value['lahan_id'].'">'.$value['keterangan'].'</option>';
												}
											}
										?>
									</select>
								</div>
						</div>
						<div class="form-group">			
				            	<label class="control-label col-md-3" >Komoditas </label>
								<div class="col-md-6">
									<input type="text" class="form-control" data-toggle="modal" data-target="#komoditas_modal" id="komoditas" name="komoditas" placeholder="Komoditas" />
								</div>
						</div>

						<!-- submit -->
						<div class="form-group">			
				            	<div class="col-md-9">
				            		<div class="col-md-2 pull-right custombutton">
					            		<button class="btn btn-primary" id="tool" data-toggle="tooltip" data-placement="top" title="simpan data" type="submit">Simpan</button>
					            	</div>
			            		</div>
						</div>
						
					</form>
				</div>
			</div>




			<!-- DATA ANGGOTA KELUARGA -->
			<div role="tabpanel" class="tab-pane " id="profile">
				<div class="col-md-12  inputdatawarga">
					<form class="form-horizontal" role="form" method="post" id="forminputddk" action="<?php echo base_url(); ?>welcome/insert_anggota">
					  		<div class="form-group">			
				            	<label class="control-label col-md-3" >Nama Kepala Keluarga </label>
								<div class="col-md-9">
									<input type="text" class="hidden" name="kk_id" id="kk_id">
									<input type="text" class="form-control" id="nama_kepala_keluarga" data-toggle="modal" data-target="#namakk" name="nama_kepala_keluarga" placeholder="Nama Kepala Keluarga"/>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Nomor Urut </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="no_urut" name="no_urut" placeholder="Nomor Urut  Anggota Keluarga" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Nama Lengkap </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap" placeholder="Nama Lengkap" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Jenis Kelamin </label>
								<div class="col-md-9">
									<div class="radio-list">
										<label>
											<input type="radio" style="margin-left: 0px" name="jenis_kelamin" id="newJenisKelamin" value="L" data-title="Pria"/><span style="margin-left:5px">Pria</span> 
										</label >
										<label style="margin-left: 10px">
											<input type="radio" style="margin-left: 10px" name="jenis_kelamin" id="newJenisKelamin2" value="P" data-title="Wanita"/><span style="margin-left:5px">Wanita</span> 
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Hub. dg Keluarga </label>
								<div class="col-md-9">
									<select class="form-control select isian" name="hubungan" id="hubungan">
										<?php  
											if (!empty($hubungan)) {
												foreach ($hubungan as $value) {
													echo '<option value="'.$value['keterangan'].'">'.$value['keterangan'].'</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Tempat Lahir </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Kelahiran" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Tanggal Lahir </label>
								<div class="col-md-9">
									<div class="input-group">
									  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-calendar"></i></span>
									  	<input type="text" class="form-control" data-provide ="datepicker" name="tgl_lahir" date-date-format="dd/mm/yyyy" placeholder="Tanggal Lahir">
									</div>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Status Nikah </label>
								<div class="col-md-9">
									<select class="form-control select isian" name="status_nikah" id="status_nikah">
										<option value="Kawin" selected>Kawin</option>
										<option value="Belum Kawin">Belum Kawin</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Agama </label>
								<div class="col-md-9">
									<select class="form-control select" name="agama" id="agama">
										<?php  
											if (!empty($agama)) {
												foreach ($agama as $value) {
													echo '<option value="'.$value['nama_agama'].'">'.$value['nama_agama'].'</option>';
												}
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Gol Darah </label>
								<div class="col-m
								d-9">
									<select class="form-control select" name="gol_darah" id="gol_darah">
										<option value="tidak diketahui" selected>tidak diketahui</option>
										<option value="A" >A</option>
										<option value="AB">AB</option>
										<option value="B">B</option>
										<option value="O">O</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Kewarganegaraan </label>
								<div class="col-md-9">
									<select class="form-control select" name="kewarganegaraan" id="kewarganegaraan">
										<option value="WNI" selected>WNI</option>
										<option value="WNA">WNA</option>
										<option value="DwiKewarganegaraan">Dwi Kewarganegaraan</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Pendidikan Terakhir </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="pendidikan" name="pendidikan" list="pendidikan_option" placeholder="Pendidikan Terakhir">
									<datalist class="" id="pendidikan_option">
										<option value="TK"></option>
										<option value="SD"></option>
										<option value="SMP"></option>
										<option value="SMA"></option>
										<option value="Diploma 3"></option>
										<option value="Sarjana Strata 1"></option>
										<option value="Sarjana Strata 2"></option>
										<option value="Sarjana Strata 3"></option>
										<option value="SLB"></option>
									</datalist>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Mata Pencaharian </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="mata_pencaharian" name="mata_pencaharian" list="mata_pencaharian_option" placeholder="Mata Pencaharian" />
									<datalist id="mata_pencaharian_option">
										<option value="Petani"></option>
										<option value="Buruh Tani"></option>
										<option value="Pegawai Negeri Sipil"></option>
										<option value="Karyawan Swasta"></option>
										<option value="Pengrajin"></option>
										<option value="Pedagang Barang Kelontong"></option>
										<option value="Peternak"></option>
										<option value="Nelayan"></option>
										<option value="Dokter Swasta"></option>
										<option value="Bidan Swasta"></option>
										<option value="Perawat Swasta"></option>
										<option value="TNI"></option>
										<option value="POLRI"></option>
										<option value="Pengusaha kecil, menengah, dan besar"></option>
										<option value="Guru Swasta"></option>
										<option value="Pedagang Keliling"></option>
										<option value="Tukang Kayu"></option>
										<option value="Tukang batu"></option>
										<option value="Pembantu Rumah Tangga"></option>
										<option value="Ibu Rumah Tangga"></option>
										<option value="Pelajar/Mahasiswa"></option>
										<option value="Belum Bekerja"></option>
									</datalist>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Nama Bapak </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nama_bapak" name="nama_bapak" placeholder="Nama Bapak/Ibu" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Akta Kelahiran </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="akta_kelahiran" name="akta_kelahiran" placeholder="Akta Kelahiran" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >NIK </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="nik" name="nik" placeholder="Nomor Induk Keluarga" />
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Akesptor KB </label>
								<div class="col-md-9">
									<select class="form-control select" name="akseptor_kb" id="akseptor_kb">
										<option value="suntik" selected>Kontrasepsi Suntik</option>
										<option value="spiral">Kontrasepsi Spiral</option>
										<option value="kondom">Kontrasepsi Kondom</option>
										<option value="vasektomi">Kontrasepsi Vasektomi</option>
										<option value="tubektomi">Kontrasepsi Tubektomi</option>
										<option value="pil">Kontrasepsi pil</option>
										<option value="alamiah">KB Alamiah/kalender</option>
										<option value="tradisional">Obat Tradisional</option>
										<option value="kba">Alat Kontrasepsi KBA</option>
										<option value="tidak KB">tidak KB</option>
									</select>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Cacat Fisik </label>
								<div class="col-md-9">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr class="warning">
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Tuna Rungu</td>
												<td align="center"><input type="checkbox" name="cacat_fisik[]" value="Tuna Rungu" class="ck_cct_fisik"></td>
												<td>Tuna Wicara</td>
												<td align="center"><input type="checkbox" name="cacat_fisik[]" value="Tuna Wicara" class="ck_cct_fisik"></td>
												<td>Tuna Netra</td>
												<td align="center"><input type="checkbox" name="cacat_fisik[]" value="Tuna Netra" class="ck_cct_fisik"></td>
												<td>Lumpuh</td>
												<td align="center"><input type="checkbox" name="cacat_fisik[]" value="Lumpuh" class="ck_cct_fisik"></td>
												<td>Sumbing</td>
												<td align="center"><input type="checkbox" name="cacat_fisik[]" value="Sumbing" class="ck_cct_fisik"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Cacat Mental </label>
								<div class="col-md-9">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr class="warning">
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Idiot</td>
												<td align="center"><input type="checkbox" name="cacat_mental[]" value="Idiot" class="ck_cct_mental"></td>
												<td>Gila</td>
												<td align="center"><input type="checkbox" name="cacat_mental[]" value="Gila" class="ck_cct_mental"></td>
												<td>Stress</td>
												<td align="center"><input type="checkbox" name="cacat_mental[]" value="Stress" class="ck_cct_mental"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Lembaga Pemerintahan </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="lbg_pemerintahan" name="lbg_pemerintahan" list="lbg_pemerintahan_option" placeholder="Kepala Desa, Sekdes, dsb..." />
									<datalist id="lbg_pemerintahan_option">
										<option value="Kepala Desa/Lurah"></option>
										<option value="Sekretaris Desa/Kelurahan"></option>
										<option value="Kepala Urusan"></option>
										<option value="Kepala Dusun/Lingkungan"></option>
										<option value="Staf Desa/Kelurahan"></option>
										<option value="Ketua BPD"></option>
										<option value="Wakil Ketua BPD"></option>
										<option value="Sekretaris BPD"></option>
										<option value="Anggota BPD"></option>
									</datalist>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Lembaga Kemasyarakatan </label>
								<div class="col-md-9">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr class="warning">
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Pengurus RT</td>
												<td align="center"><input type="checkbox" name="lbg_masyarakat[]" value="Pengurus RT" class="ck_lbg_masyarakat"></td>
												<td>Anggota Pengurus RT</td>
												<td align="center"><input type="checkbox" name="lbg_masyarakat[]" value="Anggota Pengurus RT" class="ck_lbg_masyarakat"></td>
												<td>Pengurus RW</td>
												<td align="center"><input type="checkbox" name="lbg_masyarakat[]" value="Pengurus RW" class="ck_lbg_masyarakat"></td>
												<td>Anggota Pengurus RW</td>
												<td align="center"><input type="checkbox" name="lbg_masyarakat[]" value="Anggota Pengurus RW" class="ck_lbg_masyarakat"></td>
											</tr>
											<tr>
												<td>Pengurus LPMD/LPMP</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus LPMD/LPMP" class="ck_pjk_retribusi"></td>
												<td>Anggota L KMD/K/LPM</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota L KMD/K/LPM" class="ck_pjk_retribusi"></td>
												<td>Pengurus PKK</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus PKK" class="ck_pjk_retribusi"></td>
												<td>Anggota PKK</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota PKK" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Pengurus Lembaga Adat</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Lembaga Adat" class="ck_pjk_retribusi"></td>
												<td>Pengurus Karang Taruna</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Karang Taruna" class="ck_pjk_retribusi"></td>
												<td>Anggota Karang Taruna</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Karang Taruna" class="ck_pjk_retribusi"></td>
												<td>Pengurus Hansip/Linmas</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Hansip/Linmas" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Pengurus Poskamling</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Poskamling" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Perempuan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Perempuan" class="ck_pjk_retribusi"></td>
												<td>Anggota Organisasi Perempuan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Perempuan" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Bapak-bapak</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Bapak-bapak" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Anggota Organisasi Bapak-bapak</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Bapak-bapak" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Keagamaan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Keagamaan" class="ck_pjk_retribusi"></td>
												<td>Anggota Organisasi Keagamaan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Keagamaan" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Profesi Wartawan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Profesi Wartawan" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Anggota Organisasi Profesi Wartawan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Profesi Wartawan" class="ck_pjk_retribusi"></td>
												<td>Pengurus Posyandu</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Posyandu" class="ck_pjk_retribusi"></td>
												<td>Pengurus Posyantekdes</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Posyantekdes" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Kelompok Tani/Nelayan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Kelompok Tani/Nelayan" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Anggota Organisasi Kelompok Tani/Nelayan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Kelompok Tani/Nelayan" class="ck_pjk_retribusi"></td>
												<td>Pengurus Lembaga Gotong Royong</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Lembaga Gotong Royong" class="ck_pjk_retribusi"></td>
												<td>Anggota Lembaga Gotong Royong</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Lembaga Gotong Royong" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Profesi Guru</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Profesi Guru" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Anggota Organisasi Profesi Guru</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Profesi Guru" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Profesi Dokter/Tenaga Medis</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Profesi Dokter/Tenaga Medis" class="ck_pjk_retribusi"></td>
												<td>Anggota Organisasi Profesi Dokter/Tenaga Medis</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Profesi Dokter/Tenaga Medis" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Pensiunan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Pensiunan" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Anggota Organisasi Pensiunan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Pensiunan" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Pemirsa/Pendengar</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Pemirsa/Pendengar" class="ck_pjk_retribusi"></td>
												<td>Anggota Organisasi Pemirsa/Pendengar</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Pemirsa/Pendengar" class="ck_pjk_retribusi"></td>
												<td>Pengurus Lembaga Pencinta Alam</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Lembaga Pencinta Alam" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Anggota Organisasi Pencinta Alam</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Pencinta Alam" class="ck_pjk_retribusi"></td>
												<td>Pengurus Organisasi Pengembangan Ilmu Pengetahuan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Organisasi Pengembangan Ilmu Pengetahuan" class="ck_pjk_retribusi"></td>
												<td>Anggota Organisasi Pengembangan Ilmu Pengetahuan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Organisasi Pengembangan Ilmu Pengetahuan" class="ck_pjk_retribusi"></td>
												<td>Pemilik Yayasan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pemilik Yayasan" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Pengurus Yayasan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Yayasan" class="ck_pjk_retribusi"></td>
												<td>Anggota Yayasan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Yayasan" class="ck_pjk_retribusi"></td>
												<td>Pengurus Satgas Kebersihan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Satgas Kebersihan" class="ck_pjk_retribusi"></td>
												<td>Anggota Satgas Kebersihan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Satgas Kebersihan" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Pengurus Satgas Kebakaran</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Satgas Kebakaran" class="ck_pjk_retribusi"></td>
												<td>Anggota Satgas Kebakaran</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Satgas Kebakaran" class="ck_pjk_retribusi"></td>
												<td>Pengurus Posko Penanggulangan Bencana</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Pengurus Posko Penanggulangan Bencana" class="ck_pjk_retribusi"></td>
												<td>Anggota Tim Penanggulangan Bencana</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Anggota Tim Penanggulangan Bencana" class="ck_pjk_retribusi"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Lembaga Politik </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="lbg_politik" name="lbg_politik" list="lbg_politik_option" placeholder="RT, RW, PKK..." />
									<datalist id="lbg_politik_option">
										<option value="Pengurus Partai Golkar"></option>
										<option value="Anggota Partai Golkar"></option>
										<option value="Pengurus Partai PDI Perjuangan"></option>
										<option value="Anggota Pengurus Partai PDI Perjuangan"></option>
										<option value="INPUT PLEASEEEEE"></option>
									</datalist>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Lembaga Ekonomi yang Dimiliki Anggota Keluarga </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="lbg_ekonomi" name="lbg_ekonomi" list="lbg_ekonomi_option" placeholder="Lembaga Ekonomi yang dimiliki" />
									<datalist id="lbg_ekonomi_option">
										<option value="Koperasi"></option>
										<option value="Unit Usaha Simpan Pinjam"></option>
										<option value="Industri Kerajinan Tangan"></option>
										<option value="Industri Pakaian"></option>
										<option value="Industri Usaha Makanan"></option>
										<option value="Industri Alat Rumah Tangga"></option>
										<option value="Industri Usaha Bahan Bangunan"></option>
										<option value="INPUT PLEASEEEE"></option>
									</datalist>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="co+ntrol-label col-md-3" >Jenis Penyakit yang diderita anggota Keluarga  </label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="penyakit_diderita" name="penyakit_diderita" list="penyakit_diderita_option" placeholder="Jenis Penyakit yang diderita anggota keluarga">
									<datalist class="" id="penyakit_diderita_option">
										<option value="Jantung"></option>
										<option value="Liver"></option>
										<option value="Paru-paru"></option>
										<option value="Kanker"></option>
										<option value="Stroke"></option>
										<option value="Diabetes Melitus"></option>
										<option value="Ginjal"></option>
										<option value="Malaria"></option>
										<option value="Lepra/Kusta"></option>
										<option value="HIV/AIDS"></option>
										<option value="Gila/Stress"></option>
										<option value="TBC"></option>
										<option value="Asma"></option>
									</datalist>
								</div>
							</div>
							<div class="form-group">			
				            	<label class="control-label col-md-3" >Kedudukan Anggota Keluarga sebagai Wajib Pajak dan Retribusi  </label>
								<div class="col-md-9">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr class="warning">
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Wajib Pajak Bumi dan Bangunan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Wajib Pajak Bumi dan Bangunan" class="ck_pjk_retribusi"></td>
												<td>Wajib Pajak penghasilan Perorangan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Wajib Pajak penghasilan Perorangan" class="ck_pjk_retribusi"></td>
												<td>Wajib Pajak Badan/Perusahaan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Wajib Pajak Badan/Perusahaan" class="ck_pjk_retribusi"></td>
											</tr>
											<tr>
												<td>Wajib Pajak Kendaraan Bermotor</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Wajib Pajak Kendaraan Bermotor" class="ck_pjk_retribusi"></td>
												<td>Wajib Retribusi Kebersihan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Wajib Retribusi Kebersihan" class="ck_pjk_retribusi"></td>
												<td>Wajib Retribusi Keamanan</td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Wajib Retribusi Keamanan" class="ck_pjk_retribusi"></td>
											</tr>
											<!--<tr>
												<td><input type="text" id="pajak_retribusi_ins" name="pajak_retribusi[]" value="" class="form-control"></td>
												<td align="center"><input type="checkbox" name="pajak_retribusi[]" value="Wajib Pajak Kendaraan Bermotor" class="ck_pjk_retribusi"></td>
											</tr>-->
										</tbody>
									</table>
								</div>
							</div>

							<div class="form-group">			
				            	<div class="col-md-12">
				            		<div class="col-md-2 pull-right custombutton">
					            		<button class="btn btn-primary" id="tool" data-toggle="tooltip" data-placement="top" title="simpan data" type="submit">Simpan</button>
					            	</div>
				            	</div>
							</div>
					</form>
				</div>		
			</div>



			<!-- DATA KELUARGA -->
			<div role="tabpanel" class="tab-pane " id="keluarga">
				<div class="col-md-12 inputdatawarga">
					<form class="form-horizontal" role="form" method="post" id="formdatakeluarga" action="<?php echo base_url(); ?>welcome/insert_data_kel">
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Nama Kepala Keluarga </label>
							<div class="col-md-9 col-aset">
								<input type="text" class="hidden" name="kk_id_data" id="kk_id_data">
								<input type="text" class="form-control" id="nama_kepala_keluarga_data" data-toggle="modal" data-target="#namakk" name="nama_kepala_keluarga_data" placeholder="Nama Kepala Keluarga"/>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Kepemilikan Rumah </label>
							<div class="col-md-9">
								<input type="text" name="kepemilikan_rumah" class="form-control" list="kepemilikan_rumah_option">
								<datalist class="" id="kepemilikan_rumah_option">
									<option value="Milik Sendiri"></option>
									<option value="Milik Orangtua"></option>
									<option value="Milik Keluarga"></option>
									<option value="Sewa/Kontrak/Pinjam Pakai"></option>
								</datalist>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Penghasilan perbulan </label>
							<div class="col-md-9">
								<select class="form-control select" name="penghasilan_perbulan" id="penghasilan_perbulan">
									<?php  
										if (!empty($penghasilan)) {
											foreach ($penghasilan as $value) {
												echo '<option value="'.$value['keterangan'].'">'.$value['keterangan'].'</option>';
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Pengeluaran perbulan </label>
							<div class="col-md-9">
								<select class="form-control select" name="pengeluaran_perbulan" id="pengeluaran_perbulan">
									<?php  
										if (!empty($penghasilan)) {
											foreach ($penghasilan as $value) {
												echo '<option value="'.$value['keterangan'].'">'.$value['keterangan'].'</option>';
											}
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Sumber Air Minum </label>
							<div class="col-md-9">
								<input type="text" class="form-control" id="air_minum" name="air_minum" list="air_minum_option" placeholder="Sumber Air Minum yang Digunakan" />
								<datalist id="air_minum_option">
									<option value="Mata Air"></option>
									<option value="Sumur Gali"></option>
									<option value="Sumur Pompa"></option>
									<option value="Hidran Umum"></option>
									<option value="PAM"></option>
									<option value="Pipa"></option>
									<option value="Sungai"></option>
									<option value="Embung"></option>
								</datalist>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Kualitas Air Minum </label>
							<div class="col-md-9">
								<select class="form-control select" id="kualitas_air" name="kualitas_air" >
									<option value="Baik" selected>Baik</option>
									<option value="Berasa">Berasa</option>
									<option value="Berwarna">Berwarna</option>
									<option value="Berbau">Berbau</option>
								</select>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Pemanfaatan Mata Air oleh Keluarga </label>
							<div class="col-md-9">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="warning">
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Usaha Perikanan</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Usaha Perikanan" class="ck_pemanfaatan_air"></td>
											<td>Air Minum/Air Baku</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Air Minum/Air Baku" class="ck_pemanfaatan_air"></td>
											<td>Cuci dan Mandi</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Cuci dan Mandi" class="ck_pemanfaatan_air"></td>
											<td>Irigasi</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Irigasi" class="ck_pemanfaatan_air"></td>
										</tr>
										<tr>
											<td>Buang Air Besar</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Buang Air Besar" class="ck_pemanfaatan_air"></td>
											<td>Pembangkit Listrik</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Pembangkit Listrik" class="ck_pemanfaatan_air"></td>
											<td>Sumber Air Panas</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Sumber Air Panas" class="ck_pemanfaatan_air"></td>
											<td>Prasarana Transportasi</td>
											<td align="center"><input type="checkbox" name="pemanfaatan_mata_air[]" value="Prasarana Transportasi" class="ck_pemanfaatan_air"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group">			
					        <label class="control-label col-md-3" >Kualitas Ibu Hamil dalam Keluarga </label>
							<div class="col-md-9">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="warning">
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Ibu hamil periksa di Posyandu</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil periksa di Posyandu" class="ck_kualitas_hamil"></td>
											<td>Ibu hamil periksa di Rumah Sakit</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil periksa di Rumah Sakit" class="ck_kualitas_hamil"></td>
											<td>Ibu hamil periksa di Dokter Praktek</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil periksa di Dokter Praktek" class="ck_kualitas_hamil"></td>
											<td>Ibu hamil periksa di Bidan Praktek</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil periksa di Bidan Praktek" class="ck_kualitas_hamil"></td>
											<td>Ibu hamil periksa di Dukun Terlatih</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil periksa di Dukun Terlatih" class="ck_kualitas_hamil"></td>
											<td>Ibu hamil tidak periksa kesehatan</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil tidak periksa kesehatan" class="ck_kualitas_hamil"></td>
										</tr>
										<tr>
											<td>Ibu hamil yang meninggal</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil yang meninggal" class="ck_kualitas_hamil"></td>
											<td>Ibu hamil melahirkan</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu hamil melahirkan" class="ck_kualitas_hamil"></td>
											<td>Ibu nifas sakit</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu nifas sakit" class="ck_kualitas_hamil"></td>
											<td>Kematian ibu nifas</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Kematian ibu nifas" class="ck_kualitas_hamil"></td>
											<td>Ibu nifas sehat</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Ibu nifas sehat" class="ck_kualitas_hamil"></td>
											<td>Kematian ibu saat melahirkan</td>
											<td align="center"><input type="checkbox" name="kualitas_hamil[]" value="Kematian ibu saat melahirkan" class="ck_kualitas_hamil"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Penderita Sakit dan Kelainan dlm Keluarga </label>
							<div class="col-md-9">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="warning">
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Muntaber</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Muntaber" class="ck_sakit_kelainan"></td>
											<td>Demam Berdarah</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Demam Berdarah" class="ck_sakit_kelainan"></td>
											<td>Kolera</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Kolera" class="ck_sakit_kelainan"></td>
											<td>Polio</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Polio" class="ck_sakit_kelainan"></td>
											<td>Cikungunya</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Cikungunya" class="ck_sakit_kelainan"></td>
										</tr>
										<tr>
											<td>Flu Burung</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Flu Burung" class="ck_sakit_kelainan"></td>
											<td>Busung Lapar</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Busung Lapar" class="ck_sakit_kelainan"></td>
											<td>Kelaparan</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Kelaparan" class="ck_sakit_kelainan"></td>
											<td>Kulit Bersisik</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Kulit Bersisik" class="ck_sakit_kelainan"></td>
											<td>Kelainan Fisik/Mental</td>
											<td align="center"><input type="checkbox" name="sakit_kelainan[]" value="Kelainan Fisik/Mental" class="ck_sakit_kelainan"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Status Gizi Balita dalam Keluarga </label>
							<div class="col-md-9">
								<select class="form-control select" id="gizi_balita" name="gizi_balita">
									<option value="Balita bergizi buruk">Balita bergizi buruk</option>
									<option value="Balita bergizi baik">Balita bergizi baik</option>
									<option value="Balita bergizi kurang">Balita bergizi kurang</option>
									<option value="Balita bergizi lebih">Balita bergizi lebih</option>
								</select>
							</div>
						</div>
						<div class="form-group">			
				           	<label class="control-label col-md-3" >Kualitas Bayi dalam Keluarga </label>
							<div class="col-md-9">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="warning">
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Keguguran kandungan</td>
											<td align="center"><input type="checkbox" name="kualitas_bayi[]" value="Keguguran kandungan" class="ck_kualitas_bayi"></td>
											<td>Bayi lahir hidup normal</td>
											<td align="center"><input type="checkbox" name="kualitas_bayi[]" value="Bayi lahir hidup normal" class="ck_kualitas_bayi"></td>
											<td>Bayi lahir hidup cacat</td>
											<td align="center"><input type="checkbox" name="kualitas_bayi[]" value="Bayi lahir hidup cacat" class="ck_kualitas_bayi"></td>
											<td>Bayi lahir mati</td>
											<td align="center"><input type="checkbox" name="kualitas_bayi[]" value="Bayi lahir mati" class="ck_kualitas_bayi"></td>
										</tr>
										<tr>
											<td>Bayi lahir berat kurang dari 2,5kg</td>
											<td align="center"><input type="checkbox" name="kualitas_bayi[]" value="Bayi lahir berat kurang dari 2,5kg" class="ck_kualitas_bayi"></td>
											<td>Bayi lahir berat lebih dari 4kg</td>
											<td align="center"><input type="checkbox" name="kualitas_bayi[]" value="Bayi lahir berat lebih dari 4kg" class="ck_kualitas_bayi"></td>
											<td colspan="2">Bayi 0-5 tahun hidup menderita kelainan organ tubuh, fisik dan mental</td>
											<td colspan="2" align="center"><input type="checkbox" name="kualitas_bayi[]" value="Bayi 0-5 tahun hidup menderita kelainan organ tubuh, fisik dan mental" class="ck_kualitas_bayi"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group">			
				            <label class="control-label col-md-3" >Kualitas Persalinan dalam Keluarga </label>
							<div class="col-md-9">
								<div class="subtitle">Tempat Persalinan :</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="warning">
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Tempat persalinan Rumah Sakit Umum</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Tempat persalinan Rumah Sakit Umum" class="ck_tempat_persalinan"></td>
											<td>Tempat persalinan Rumah Bersalin</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Tempat persalinan Rumah Bersalin" class="ck_tempat_persalinan"></td>
											<td>Tempat persalinan Puskesmas</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Tempat persalinan Puskesmas" class="ck_tempat_persalinan"></td>
											<td>Tempat persalinan Polindes</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Tempat persalinan Polindes" class="ck_tempat_persalinan"></td>
										</tr>
										<tr>
											<td>Tempat persalinan Balai Kes. Ibu Anak</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Tempat persalinan Balai Kes. Ibu Anak" class="ck_tempat_persalinan"></td>
											<td>Tempat persalinan rumah praktek bidan</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Tempat persalinan rumah praktek bidan" class="ck_tempat_persalinan"></td>
											<td>Tempat praktek dokter</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Tempat praktek dokter" class="ck_tempat_persalinan"></td>
											<td>Rumah Sendiri</td>
											<td align="center"><input type="checkbox" name="tempat_persalinan[]" value="Rumah Sendiri" class="ck_tempat_persalinan"></td>
										</tr>
									</tbody>
								</table>
										
								<div class="subtitle">Pertolongan Persalinan :</div>
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="warning">
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Persalinan ditolong dokter</td>
											<td align="center"><input type="checkbox" name="pertolongan_persalinan[]" value="Persalinan ditolong dokter" class="ck_pertolongan_persalinan"></td>
											<td>Persalinan ditolong bidan</td>
											<td align="center"><input type="checkbox" name="pertolongan_persalinan[]" value="Persalinan ditolong bidan" class="ck_pertolongan_persalinan"></td>
											<td>Persalinan ditolong perawat</td>
											<td align="center"><input type="checkbox" name="pertolongan_persalinan[]" value="Persalinan ditolong perawat" class="ck_pertolongan_persalinan"></td>
											<td>Persalinan ditolong dukun bersalin</td>
											<td align="center"><input type="checkbox" name="pertolongan_persalinan[]" value="Persalinan ditolong dukun bersalin" class="ck_pertolongan_persalinan"></td>
											<td>Persalinan ditolong keluarga</td>
											<td align="center"><input type="checkbox" name="pertolongan_persalinan[]" value="Persalinan ditolong keluarga" class="ck_pertolongan_persalinan"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Cakupan Imunisasi </label>
							<div class="col-md-9">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class="warning">
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
											<td align="center">Aset</td>
											<td width="5%" align="center">Pilih</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>DPT-1</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="DPT-1" class="ck_cakupan_imunisasi"></td>
											<td>BCG</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="BCG" class="ck_cakupan_imunisasi"></td>
											<td>Polio-1</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="Polio-1" class="ck_cakupan_imunisasi"></td>
											<td>DPT-2</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="DPT-2" class="ck_cakupan_imunisasi"></td>
											<td>Polio-2</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="Polio-2" class="ck_cakupan_imunisasi"></td>
										</tr>
										<tr>
											<td>Polio-3</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="Polio-3" class="ck_cakupan_imunisasi"></td>
											<td>DPT-3</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="DPT-3" class="ck_cakupan_imunisasi"></td>
											<td>Campak</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="Campak" class="ck_cakupan_imunisasi"></td>
											<td>Cacar</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="Cacar" class="ck_cakupan_imunisasi"></td>
											<td>Sudah Semua</td>
											<td align="center"><input type="checkbox" name="cakupan_imunisasi[]" value="Sudah Semua" class="ck_cakupan_imunisasi"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Perilaku Hidup Bersih dan Sehat dalam Keluarga </label>
							<div class="col-md-9">
								<select class="form-control select" id="hidup_bersih_sehat" name="hidup_bersih_sehat">
									<option value="WC Permanen/Semi-permanen">WC Permanen/Semi-permanen</option>
									<option value="WC darurat/kurang memenuhi standar kesehatan">WC darurat/kurang memenuhi standar kesehatan</option>
									<option value="Biasa buang air besar di sungai/parit/kebun/hutan">Biasa buang air besar di sungai/parit/kebun/hutan</option>
									<option value="Menggunakan fasilitas MCK Umum">Menggunakan fasilitas MCK Umum</option>
								</select>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Pola Makan Keluarga </label>
							<div class="col-md-9">
								<select class="form-control select" id="pola_makan" name="pola_makan">
									<option value="Kebiasaan Makan sehari 1 kali">Kebiasaan Makan sehari 1 kali</option>
									<option value="Kebiasaan Makan sehari 2 kali">Kebiasaan Makan sehari 2 kali</option>
									<option value="Kebiasaan Makan sehari 3 kali">Kebiasaan Makan sehari 3 kali</option>
									<option value="Kebiasaan Makan sehari lebih dari 3 kali">Kebiasaan Makan sehari lebih dari 3 kali</option>
									<option value="Belum tentu sehari makan 1 kali">Belum tentu sehari makan 1 kali</option>
								</select>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Kebiasaan berobat bila sakit dlm keluarga </label>
							<div class="col-md-9">
								<select class="form-control select" id="kebiasaan_berobat" name="kebiasaan_berobat">
									<option value="Dukun Terlatih">Dukun Terlatih</option>
									<option value="Dokter/puskesmas/mantri kesehatan/perawat/bidan/posyandu">Dokter/puskesmas/mantri kesehatan/perawat/bidan/posyandu</option>
									<option value="Obat tradisional dan dukun pengobatan alternatif">Obat tradisional dan dukun pengobatan alternatif</option>
									<option value="Paranormal">Paranormal</option>
									<option value="Obat tradisional dari keluarga sendiri">Obat tradisional dari keluarga sendiri</option>
									<option value="Tidak Diobati">Tidak Diobati</option>
								</select>
							</div>
						</div>
						<div class="form-group">			
			            	<label class="control-label col-md-3" >Komoditas Tanaman & Buah-buahan </label>
							<div class="col-md-6">

								<SCRIPT language="javascript">
							        function addRow(tableID) {
							 
							            var table = document.getElementById(tableID);
							 
							            var rowCount = table.rows.length;
							            var row = table.insertRow(rowCount);
							 
							            var colCount = table.rows[0].cells.length;
							 
							            for(var i=0; i<colCount; i++) {
							 
							                var newcell = row.insertCell(i);
							 
							                newcell.innerHTML = table.rows[0].cells[i].innerHTML;
							                //alert(newcell.childNodes);
							                switch(newcell.childNodes[0].type) {
							                    case "text":
							                            newcell.childNodes[0].value = "";
							                            break;
							                    case "checkbox":
							                            newcell.childNodes[0].checked = false;
							                            break;
							                    case "select-one":
							                            newcell.childNodes[0].selectedIndex = 0;
							                            break;
							                }
							            }
							        }
							 
							        function deleteRow(tableID) {
							            try {
							            var table = document.getElementById(tableID);
							            var rowCount = table.rows.length;
							 
							            for(var i=0; i<rowCount; i++) {
							                var row = table.rows[i];
							                var chkbox = row.cells[0].childNodes[0];
							                if(null != chkbox && true == chkbox.checked) {
							                    if(rowCount <= 1) {
							                        alert("Cannot delete all the rows.");
							                        break;
							                    }
							                    table.deleteRow(i);
							                    rowCount--;
							                    i--;
							                }
							            }
							        }catch(e) {
							            alert(e);
							        }
							    }
							 
							    </SCRIPT>

							    <input type="button" class="btn btn-success" value="Tambah Komuditas" onclick="addRow('dataTable')" style="margin-bottom:10px;" />
   				 				<input type="button" class="btn btn-danger" value="Hapus Komoditas" onclick="deleteRow('dataTable')" style="margin-bottom:10px;" />

   				 				<table id="dataTable">

							        <tr style="margin-top: 50px">
							            <td><input type="checkbox" name="chk"/></td>
							            <td>&nbsp;&nbsp;&nbsp;</td>
							            <td>
							                <input type="text" class="form-control" name="komoditas_tanaman[]" placeholder="Jenis Komoditas" /></td>
							            </td>
							            <td>&nbsp;&nbsp;&nbsp;</td>
							            <td>
							            	<div class="input-group">	
											  	<input type="text" class="form-control" name="hasil_tanaman[]" placeholder="hasil per tahun (kg)">
												<span class="input-group-addon" id="basic-addon1">Kg</span>
											</div>
							            </td>
							        </tr>
							    </table>

							    <!-- <a href="#komoditas_modal" id="komoditas" name="komoditas" data-toggle="modal"><i class="glyphicon glyphicon-plus">&nbsp;Tambah Komoditas</i></a><br><br> -->

								<!-- <table class="table table-striped table-bordered table-hover table-responsive" id="tblInven">
									<thead>
										<tr class="info" >
											<th  style="text-align:left"> Nama Tanaman </th>
											<th  style="text-align:left" width="40%"> Jumlah per tahun (kg) </th>
											
										</tr>
									</thead>
									<tbody id="addinput">
											<tr>
												<td>Jagung</td>
												<td><input type="text" size="10"></td>			
											</tr>
									</tbody>
								</table> -->
								
								<!-- <input type="text" class="form-control" data-toggle="modal" data-target="#komoditas_modal" id="komoditas" name="komoditas" placeholder="Komoditas" /> -->
							</div>
						</div>

						<div class="form-group">			
					        <div class="col-md-12">
					       		<div class="col-md-2 pull-right custombutton">
						       		<button class="btn btn-primary" id="tool" data-toggle="tooltip" data-placement="top" title="simpan data" type="submit">Simpan</button>
					           	</div>
				          	</div>
						</div>
					</form>
				</div>
			</div>				



			<!-- ASET KELUARGA -->
			<div role="tabpanel" class="tab-pane " id="aset">
				<div class="col-md-12 inputdatawarga">
					<form class="form-horizontal" role="form" method="post" id="formasetkeluarga" action="<?php echo base_url(); ?>welcome/insert_aset">
					  		<div class="form-group">			
				            	<label class="control-label col-md-3" >Nama Kepala Keluarga </label>
								<div class="col-md-9 col-aset">
									<input type="text" class="hidden" name="kk_id_aset" id="kk_id_aset">
									<input type="text" class="form-control" id="nama_kepala_keluarga_aset" data-toggle="modal" data-target="#namakk" name="nama_kepala_keluarga_aset" placeholder="Nama Kepala Keluarga"/>
								</div>
							</div>
					  		<div class="form-group">			
				            	<label class="control-label col-md-3" >Penguasaan Aset Tanah oleh Keluarga </label>
								<div class="col-md-9 col-aset">
									<select class="form-control select" id="aset_tanah" name="aset_tanah" >
										<option value="Tidak memiliki tanah" selected>Tidak memiliki tanah</option>
										<option value="Memiliki tanah antara 0.1-0.2 ha">Memiliki tanah antara 0.1-0.2 ha</option>
										<option value="Memiliki tanah antara 0.21-0.3 ha">Memiliki tanah antara 0.21-0.3 ha</option>
										<option value="Memiliki tanah antara 0.31-0.4 ha">Memiliki tanah antara 0.31-0.4 ha</option>
										<option value="Memiliki tanah antara 0.41-0.5 ha">Memiliki tanah antara 0.41-0.5 ha</option>
										<option value="Memiliki tanah antara 0.51-0.6 ha">Memiliki tanah antara 0.51-0.6 ha</option>
										<option value="Memiliki tanah antara 0.61-0.7 ha">Memiliki tanah antara 0.61-0.7 ha</option>
										<option value="Memiliki tanah antara 0.71-0.8 ha">Memiliki tanah antara 0.71-0.8 ha</option>
										<option value="Memiliki tanah antara 0.81-0.9 ha">Memiliki tanah antara 0.81-0.9 ha</option>
										<option value="Memiliki tanah antara 0.91-1.0 ha">Memiliki tanah antara 0.91-1.0 ha</option>
										<option value="Memiliki tanah antara 1.0-5.0 ha">Memiliki tanah antara 1.0-5.0 ha</option>
										<option value="Memiliki tanah lebih dari 5.0 ha">Memiliki tanah lebih dari 5.0 ha</option>
									</select>
								</div>
							</div>
					  		<div class="form-group">			
				            	<label class="control-label col-md-3" >Aset Sarana Transportasi Umum </label>
								<div class="col-md-9 col-aset">
									<table class="table table-striped table-bordered table-hover tabel_aset">
										<thead>
											<tr class="warning">
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Ojek Motor</td>
												<td align="center"><input type="checkbox" name="aset_trans[]" value="Ojek Motor" class="ck_aset_trans"></td>
												<td>Becak</td>
												<td align="center"><input type="checkbox" name="aset_trans[]" value="Becak" class="ck_aset_trans"></td>
												<td>Cidemo/Angkong/Dokar</td>
												<td align="center"><input type="checkbox" name="aset_trans[]" value="Cidemo/Angkong/Dokar" class="ck_aset_trans"></td>
												<td>Bus Penumpang/Angkutan</td>
												<td align="center"><input type="checkbox" name="aset_trans[]" value="Bus Penumpang/Angkutan" class="ck_aset_trans"></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

					  		<div class="form-group">			
				            	<label class="control-label col-md-3" >Aset Sarana Produksi </label>
								<div class="col-md-9 col-aset">
									<table class="table table-striped table-bordered table-hover tabel_aset">
										<thead>
											<tr class="warning">
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Penggilingan Padi</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Penggilingan Padi" class="ck_aset_sarana"></td>
												<td>Traktor</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Traktor" class="ck_aset_sarana"></td>
												<td>Pabrik Pengolahan Hasil Tani</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Pabrik Pengolahan Hasil Tani" class="ck_aset_sarana"></td>
												<td>Alat Pengolahan Hasil Tani</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Alat Pengolahan Hasil Tani" class="ck_aset_sarana"></td>
											</tr>
											<tr id="aset_sarana" class="aset_sarana hidden">
												<td>Alat Pengolahan Hasil Ternak</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Alat Pengolahan Hasil Ternak" class="ck_aset_sarana"></td>
												<td>Alat Pengolahan Hasil Perkebunan</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Alat Pengolahan Hasil Perkebunan" class="ck_aset_sarana"></td>
												<td>Alat Pengolahan Hasil Hutan</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Alat Pengolahan Hasil Hutan" class="ck_aset_sarana"></td>
												<td>Alat Produksi Kerajinan Keluarga</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Alat Produksi Kerajinan Keluarga" class="ck_aset_sarana"></td>
											</tr>
											<tr id="aset_sarana" class="aset_sarana hidden">
												<td>Kapal Penangkap Ikan</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Kapal Penangkap Ikan" class="ck_aset_sarana"></td>
												<td>Alat Produksi Hasil Tambang</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Alat Produksi Hasil Tambang" class="ck_aset_sarana"></td>
												<td>Alat Produksi Bahan Bakar dan Gas</td>
												<td align="center"><input type="checkbox" name="aset_sarana[]" value="Alat Produksi Bahan Bakar dan Gas" class="ck_aset_sarana"></td>
											</tr>
										</tbody>
									</table>
									<a class="btn btn-primary"><i href=".aset_sarana" class='toggle glyphicon glyphicon-arrow-down'> Expand</i></a>
								</div>
							</div>

					  		<div class="form-group">
								<label class="control-label col-md-3" >Aset Perumahan </label>
								<div class="col-md-9">
									<div class="subtitle">Rumah menurut dinding : </div>
									<input class="form-control" type="text" name="dinding_rumah" id="dinding_rumah" list="dinding_rumah_option">
									<datalist id="dinding_rumah_option">
										<option value="Tembok"></option>
										<option value="Kayu"></option>
										<option value="Bambu"></option>
										<option value="Tanah Liat"></option>
										<option value="Pelepah Kelapa/Lontar/Gebang"></option>
										<option value="Dedaunan"></option>
									</datalist>

									<div class="subtitle">Rumah menurut lantai : </div>
									<input class="form-control" type="text" name="lantai_rumah" id="lantai_rumah" list="lantai_rumah_option">
									<datalist id="lantai_rumah_option">
										<option value="Keramik"></option>
										<option value="Semen"></option>
										<option value="Kayu"></option>
										<option value="Tanah"></option>
									</datalist>
									
									<div class="subtitle">Rumah menurut atap : </div>
									<input class="form-control" type="text" name="atap_rumah" id="atap_rumah" list="atap_rumah_option">
									<datalist id="atap_rumah_option">
										<option value="Genteng"></option>
										<option value="Seng"></option>
										<option value="Asbes"></option>
										<option value="Beton"></option>
										<option value="Bambu"></option>
										<option value="Kayu"></option>
										<option value="Daun Lontar/Gebang/Enau"></option>
									</datalist>
								</div>
							</div>

					  		<div class="form-group">			
				            	<label class="control-label col-md-3" >Aset Lainnya dalam Keluarga </label>
								<div class="col-md-9 col-aset">
									<table class="table table-striped table-bordered table-hover tabel_aset">
										<thead>
											<tr class="warning">
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
												<td align="center">Aset</td>
												<td width="5%" align="center">Pilih</td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>TV</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="TV" class="ck_aset_lain"></td>
												<td>Sepeda Motor Pribadi</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Sepeda Motor Pribadi" class="ck_aset_lain"></td>
												<td>Mobil pribadi</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Mobil Pribadi" class="ck_aset_lain"></td>
												<td>Perhiasan Emas/Berlian</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Perhiasan Emas/Berlian" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Buku Tabungan Bank</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Buku Tabungan Bank" class="ck_aset_lain"></td>
												<td>Buku Surat berharga</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Buku Surat berharga" class="ck_aset_lain"></td>
												<td>Sertifikat Tanah</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Sertifikat Tanah" class="ck_aset_lain"></td>
												<td>Sertifikat Bangunan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Sertifikat Bangunan" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Perahu Bermotor</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Perahu Bermotor" class="ck_aset_lain"></td>
												<td>Kapal Barang</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Kapal Barang" class="ck_aset_lain"></td>
												<td>Kapal Penumpang</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Kapal Penumpang" class="ck_aset_lain"></td>
												<td>Kapal Pesiar</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Kapal Pesiar" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Helikopter Pribadi</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Helikopter Pribadi" class="ck_aset_lain"></td>
												<td>Pesawat Terbang Pribadi</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Pesawat Terbang Pribadi" class="ck_aset_lain"></td>
												<td>Ternak Besar</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Ternak Besar" class="ck_aset_lain"></td>
												<td>Ternak Kecil</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Ternak Kecil" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Sertifikat Deposito</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Sertifikat Deposito" class="ck_aset_lain"></td>
												<td>Perusahaan Industri Besar</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Perusahaan Industri Besar" class="ck_aset_lain"></td>
												<td>Perusahaan Industri Menengah</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Perusahaan Industri Menengah" class="ck_aset_lain"></td>
												<td>Perusahaan Industri Kecil</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Perusahaan Industri Kecil" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Usaha Perikanan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha Perikanan" class="ck_aset_lain"></td>
												<td>Usaha Peternakan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha Peternakan" class="ck_aset_lain"></td>
												<td>Usaha Perkebunan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha Perkebunan" class="ck_aset_lain"></td>
												<td>Usaha Pasar Swalayan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha Pasar Swalayan" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Usaha di Pasar Swayalan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha di Pasar Swayalan" class="ck_aset_lain"></td>
												<td>Usaha di Pasar Tradisional</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha di Pasar Tradisional" class="ck_aset_lain"></td>
												<td>Usaha di Pasar Desa</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha di Pasar Desa" class="ck_aset_lain"></td>
												<td>Usaha Transportasi/Pengangkutan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha Transportasi/Pengangkutan" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Saham di Perusahaan</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Saham di Perusahaan" class="ck_aset_lain"></td>
												<td>Pelanggan Telkom</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Pelanggan Telkom" class="ck_aset_lain"></td>
												<td>HP GSM</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="HP GSM" class="ck_aset_lain"></td>
												<td>HP CDMA</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="HP CDMA" class="ck_aset_lain"></td>
											</tr>
											<tr id="aset_lain" class="aset_lain hidden">
												<td>Usaha Wartel</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Usaha Wartel" class="ck_aset_lain"></td>
												<td>Parabola</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Parabola" class="ck_aset_lain"></td>
												<td>Berlanggan Koran/Majalah</td>
												<td align="center"><input type="checkbox" name="aset_lain[]" value="Berlanggan Koran/Majalah" class="ck_aset_lain"></td>
											</tr>
										</tbody>
									</table>
									<a class="btn btn-primary"><i href=".aset_lain" class='toggle glyphicon glyphicon-arrow-down'> Expand</i></a>
								</div>
							</div>
							<div class="form-group">			
				            	<div class="col-md-12">
				            		<div class="col-md-2 pull-right custombutton">
					            		<button class="btn btn-primary" id="tool" data-toggle="tooltip" data-placement="top" title="simpan data" type="submit">Simpan</button>
					            	</div>
				            	</div>
							</div>
					</form>

				</div>
			</div>
		</div>


		<!-- MODALS & POP UPS -->
		<div class="modal fade" id="namakk" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        <h3 class="modal-title" id="myModalLabel">Nama Kepala Keluarga</h3>
				    </div>
				    <div class="modal-body" >
						<div class="form-group">
							<div class="form-group">	
								<div class="col-md-6">
									<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="cari..."/>
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-info">Cari</button>
								</div>
								<br><br>	
							</div>		
							<div style="margin-left:20px; margin-right:20px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchKK">
									<thead>
										<tr class="warning">
											<td>Nama Kepala Keluarga</td>
											<td>Alamat</td>
											<td width="10%">Pilih</td>
										</tr>
									</thead>
									<tbody id="t_body_namakk">
										
									</tbody>
								</table>												
							</div>
						</div>
				    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="komoditas_modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        <h3 class="modal-title" id="myModalLabel">Komoditas</h3>
				    </div>
				    <div class="modal-body" >
						<div class="form-group">
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchKomoditas">
									<thead>
										<tr class="warning">
											<td>Nama</td>
											<td>Jumlah (per tahun)</td>
											<td width="10%">Pilih</td>
										</tr>
									</thead>
									<tbody id="t_body_komoditas">
										<?php 
											if (!empty($komoditas)) {
												foreach ($komoditas as $value) {
													echo '<tr>';
														echo '<td>'.$value['jenis'].'</td>';
														echo '<td><input type="text" class="form-control inp" id="'.$value['jenis'].'" name="'.$value['jenis'].'" placeholder="'.$value['jenis'].'" /></td>';
														echo '<td width="10%"><input type="checkbox" name="'.$value['id'].'" value="'.$value['id'].'"></td>';
													echo '</tr>';
												}
											}
										?>
										
									</tbody>
								</table>												
							</div>
						</div>
				    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Pilih</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php include "javascript.php"; ?>
</body>
</html>