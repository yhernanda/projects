<!DOCTYPE html>
<html lang="em">
<head>
	<meta charset="utf-8">
	<title>DDK - Dusun Munggur</title>
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/viewdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/Junction.otf">

    <!-- DataTables CSS & JS -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>

	<!-- JQuery & JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/js.js"></script>

</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="col-md-12">
				<div class="col-md-8">
				     		
				</div>
				<div class="col-md-4">
					<h4 class="pull-right"><i class="glyphicon glyphicon-user">&nbsp;</i><?php echo $this->session->userdata('user_input')['username']; ?> &nbsp;&nbsp;
						<a href="<?php echo base_url(); ?>welcome/inputdata" class="btn btn-success" id="back_link">Input Data</a>
						<a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-success">Logout</a></h4>
				</div>
			</div>
				
		</div><!-- /.container-fluid -->
	</nav>

	<div class="container">
		<div class="col-md-12 customtitle" style='color:#fff;'>
			<h1 id="title">Daftar Kepala Keluarga</h1>
		</div>

		<div id="table-wrapper-kk" class="col-md-12 tabelkk">
			<table id="tabelkk" class="table table-striped table-bordered table-responsive">
				<thead>
					<tr>
						<th style="display:none;">id</th>
						<th>Nama KK</th>
						<th>Alamat</th>
						<th>RT</th>
						<th>RW</th>
						<th>Dusun</th>
						<th>Desa</th>
						<th>Kecamatan</th>
						<th>Kabupaten</th>
						<th>Provinsi</th>
						<th>Tanggal Entri</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
						if(!empty($kk)){
							foreach ($kk as $key => $value) {
								echo "<tr>
									<td style='display:none;' class='kk_id'>".$value['kk_id']."</td>
									<td>".$value['nama_kk']."</td>
									<td>".$value['alamat']."</td>
									<td>".$value['rt']."</td>
									<td>".$value['rw']."</td>
									<td>".$value['dusun']."</td>
									<td>".$value['desa']."</td>
									<td>".$value['kecamatan']."</td>
									<td>".$value['kabupaten']."</td>
									<td>".$value['provinsi']."</td>
									<td>".$value['tanggal']."</td>
									<td>
										<a href='#' class='detail_kk' id='tool' data-toggle='tooltip' data-placement='top' title='Detail'><i class='glyphicon glyphicon-menu-hamburger'></i></a>
										<a href='#' class='edit_kk' id='tool' data-toggle='tooltip' data-placement='top' title='Edit'><i class='glyphicon glyphicon-edit'></i></a>
										<a href='#' class='hapus_kk' id='tool' data-toggle='tooltip' data-placement='top' title='Delete'><i class='glyphicon glyphicon-trash'></i></a>
									</td>
								</tr>";
							}
						}
					?>
				</tbody>
				<tfoot>
					<tr>
						<th style="display:none;">id</th>
						<th>Nama KK</th>
						<th>Alamat</th>
						<th>RT</th>
						<th>RW</th>
						<th>Dusun</th>
						<th>Desa</th>
						<th>Kecamatan</th>
						<th>Kabupaten</th>
						<th>Provinsi</th>
						<th>Tanggal Entri</th>
						<th>Aksi</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div id="table-wrapper-dkk" class="col-md-12 tabeldetailkk hidden">
			<table id="tabeldetailkk" class="table table-striped table-bordered table-responsive">
				<thead>
					<tr>
						<th style='display:none'>id</th>
						<th>No Urut</th>
						<th>Nama Lengkap</th>
						<th>Jenis Kelamin</th>
						<th>Hubungan</th>
						<th>Tempat Lahir</th>
				 		<th>Tanggal Lahir</th>
						<th>Agama</th>
						<th>Gol. Darah</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody id="t_bodydetailkk">
					<?php
						if(!empty($detail_kk)){
							foreach ($detail_kk as $key => $value) {
								echo "<tr>
									<td style='display:none' class='anggota_id'>".$value['anggota_id']."</td>
									<td>".$value['no_urut']."</td>
									<td>".$value['nama_lengkap']."</td>
									<td>".$value['jenis_kelamin']."</td>
									<td>".$value['hubungan']."</td>
									<td>".$value['tempat_lahir']."</td>
									<td>".$value['tanggal_lahir']."</td>
									<td>".$value['agama']."</td>
									<td>".$value['gol_darah']."</td>
									<td>
										<a href='#' class='detail_anggota' id='tool' data-toggle='tooltip' data-placement='top' title='Detail'><i class='glyphicon glyphicon-menu-hamburger'></i></a>
										<a href='#' class='edit_anggota' id='tool' data-toggle='tooltip' data-placement='top' title='Edit'><i class='glyphicon glyphicon-edit'></i></a>
										<a href='#' class='hapus_anggota' id='tool' data-toggle='tooltip' data-placement='top' title='Delete'><i class='glyphicon glyphicon-trash'></i></a>
									</td>
								</tr>";
							}
						}
					?>
				</tbody>
				<tfoot>
					<tr>
						<th style='display:none'>id</th>
						<th>No Urut</th>
						<th>Nama Lengkap</th>
						<th>Jenis Kelamin</th>
						<th>Hubungan</th>
						<th>Tempat Lahir</th>
						<th>Tanggal lahir</th>
						<th>Agama</th>
						<th>Gol. Darah</th>
						<th>Aksi</th>
					</tr>
				</tfoot>
			</table>
		</div>

		<div id="table-wrapper-kk" class="col-md-12 tabeldatakk">
			<table id="tabeldatakk" class="table table-striped table-bordered table-responsive">
				<thead>
					<tr>
						<td>Kepemilikan Rumah</td>
						<td>Penghasilan Perbulan</td>
						<td>Pengeluaran Perbulan</td>
						<td>Sumber Air</td>
						<td>Kualitas Air</td>
						<td>Gizi Balita</td>
						<td>PHBS</td>
						<td>Pola Makan</td>
						<td>Aksi</td>
					</tr>
				</thead>
				<tbody id="t_bodydatakk">
				</tbody>
				<tfoot>
					<tr>
						<td>Kepemilikan Rumah</td>
						<td>Penghasilan Perbulan</td>
						<td>Pengeluaran Perbulan</td>
						<td>Sumber Air</td>
						<td>Kualitas Air</td>
						<td>Gizi Balita</td>
						<td>PHBS</td>
						<td>Pola Makan</td>
						<td>Aksi</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div>
	</div>


	<!-- MODALS -->
	<div class="modal fade" id="detail_data" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
			        <h3 class="modal-title" id="myModalLabel">Data Keluarga</h3>
			    </div>
			    <div class="modal-body" >
					<div class="form-group">
						<div class="portlet-body" style="margin: 0px 10px 0px 10px">
							<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelDetailData">
								<thead>
									<tr class="warning">
										<td>Nama</td>
										<td>Keterangan	</td>
									</tr>
								</thead>
								<tbody id="t_bodydatamodal">
									
								</tbody>
							</table>												
						</div>
					</div>
			    </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal">Pilih</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>