<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>DDK - Dusun Munggur</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>public/js/modernizr-2.6.2.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="col-md-12 topmenu"></div>

		<div class="col-md-12 homepage">
			<center>
				<h1>Data Dasar Kependudukan</h1>
				<h3>Padukuhan Munggur, Desa Watusigar, Ngawen</h3>
			</center>
		</div>
		<div class="col-md-12">
			<div class="col-md-4"></div>
			<center><div class="col-md-4" id="right-side">
				<h4>Login</h4>
				<p style="color:red;"><?php echo $this->session->flashdata('message'); ?></p>
				<div class="login-form">
					<form method="post" action="<?php echo base_url(); ?>welcome/process_login/">
						<div class="input-group pad">
						  	<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
						  	<input type="text" name="username" class="form-control" placeholder="Masukkan Username" aria-describedby="basic-addon1" autofocus>
						</div>
						<div class="input-group pad">
						  	<span class="input-group-addon" id="basic-addon2"><i class="glyphicon glyphicon-record"></i></span>
						  	<input type="password" name="userpass" class="form-control" placeholder="Masukkan Password" aria-describedby="basic-addon2">
						</div>
						<button class="btn btn-primary pad" type="submit">Log In</button>
					</form>
				</div>
			</div></center>
			<div class="col-md-4"></div>
		</div>
		
	</div>
</body>
</html>