<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DDK - Dusun Munggur</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-glyphicons.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/inputdata.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css">
  
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function () {
			$('[id="tool"]').tooltip();
		});
    </script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
	  	<div class="container">
		    	<div class="col-md-12">
			     	<div class="col-md-6">
			     		
			     	</div>
					<div class="col-md-6">
						<h4 class="pull-right"><i class="glyphicon glyphicon-user">&nbsp;</i><?php echo $this->session->userdata('user_input')['username']; ?> &nbsp;&nbsp;
						<a href="<?php echo base_url(); ?>welcome/viewdata" class="btn btn-success">View Data</a>
						<?php if ($this->session->userdata('user_input')['role'] === '0'): ?>
							<a href="<?php echo base_url(); ?>welcome/daftar_user" class="btn btn-success">Daftar Pengguna</a>
						<?php endif ?>
						<a href="<?php echo base_url(); ?>welcome/logout" class="btn btn-success">Logout</a></h4>
					</div>
				</div>
			
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container">
		<div class="col-md-12">
			<div class="col-md-8"><h4>Daftar User</h4></div>
			<div class="col-md-4"><a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#regisuser">Tambah User Baru</a></div>
		</div>
		<table class="table table-striped table-bordered table-hover" id="tabel_user">
			<thead>
				<tr class="warning">
					<td align="center">Nama</td>
					<td align="center">Pekerjaan</td>
					<td align="center">Jabatan</td>
					<td align="center">Username</td>
					<td align="center">Dusun</td>
					<td align="center">Action</td>
				</tr>
			</thead>
			<tbody>
				<?php  
					if (!empty($users)) {
						foreach ($users as $value) {
							echo '<tr>'.
									'<td style="display:none" class="my_user_id">'.$value['user_id'].'</td>'.
									'<td>'.$value['nama_pengisi'].'</td>'.
									'<td>'.$value['pekerjaan'].'</td>'.
									'<td>'.$value['jabatan'].'</td>'.
									'<td>'.$value['username'].'</td>'.
									'<td>'.$value['dusun'].'</td>'.
									'<td style="text-align:center"><a href="#" class="hapususer"><i id="tool" data-toggle="tooltip" data-placement="top" title="hapus user" data- class="glyphicon glyphicon-remove" style="cursor:pointer"></i></a></td>'.
								'</tr>';
						}
					}
				?>
				
			</tbody>
		</table>

		<div class="modal fade" id="regisuser" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
				        <h3 class="modal-title" id="myModalLabel">Nama Kepala Keluarga</h3>
				    </div>
				    <div class="modal-body" >
						<div class="form-group">
							<div class="form-group">	
								<div class="col-md-6">
									<input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="cari..."/>
								</div>
								<div class="col-md-2">
									<button type="button" class="btn btn-info">Cari</button>
								</div>
								<br><br>	
							</div>		
							<div style="margin-left:20px; margin-right:20px;"><hr></div>
							<div class="portlet-body" style="margin: 0px 10px 0px 10px">
								<table class="table table-striped table-bordered table-hover tabelinformasi" id="tabelSearchKK">
									<thead>
										<tr class="warning">
											<td>Nama Kepala Keluarga</td>
											<td>Alamat</td>
											<td width="10%">Pilih</td>
										</tr>
									</thead>
									<tbody id="t_body_namakk">
										
									</tbody>
								</table>												
							</div>
						</div>
				    </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

	</div>
	
	<script type="text/javascript">
		$(document).ready(function () {
			$('#tabel_user').on('click', 'tr td a.hapususer', function (e) {
				e.preventDefault();
				
				var d = confirm('apakah akan dihapus?');
				if (d == true){
					var user_id = $(this).closest('tr').find('td.my_user_id').text();
					$.ajax({
						type: "POST",
						url: '<?php echo base_url() ?>welcome/hapus_user/'+user_id,
						success: function (data) {
							
						},
						error: function  (data) {
							alert('terjadi kesalahan, gagal');
							return false;
						}						
					})
				}else{
					return false;
				}
				$(this).closest('tr').fadeOut(function () {
					$(this).remove();
				})
			})
		})
	</script>
</body>
</html>