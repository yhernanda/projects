<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class My_model extends CI_Model{

    public function login_validation($value)
    {
        $sql = "SELECT * from user where username = ? AND password = ?";
        $res = $this->db->query($sql, $value);
        if ($res->num_rows() > 0) {
            return $res->row_array();
        }else{
            return false;
        }
    }

    public function get_all_users()
    {
        $sql = "SELECT * from user where role != '0'";
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            return $res->result_array();
        }else{
            return false;
        }
    }

    public function hapus_user($value='')
    {
        $sql = "DELETE from user where user_id = '$value'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function get_hubungan()
    {
        $sql = "SELECT * from hubungan";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_agama()
    {
        $sql = "SELECT * from agama";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_penghasilan()
    {
        $sql = "SELECT * from penghasilan";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_lahan()
    {
        $sql = "SELECT * from kepemilikan_lahan";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_pemasaran()
    {
        $sql = "SELECT * from pemasaran";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_data_kel($value = "")
    {
        $sql = "SELECT * from data_keluarga WHERE kk_id = '$value'";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_aset_kel($value = "")
    {
        $sql = "SELECT * from aset_keluarga WHERE kk_id = '$value'";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function insert_kk($value='')
    {
        $res = $this->db->insert('keluarga', $value);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_data_kel($array)
    {
        $res = $this->db->insert('data_keluarga', $array);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_aset($array='')
    {
        $res = $this->db->insert('aset_keluarga', $array);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_anggota($array = ""){
        $res =  $this->db->insert('detail_anggota', $array);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function insert_komoditas($value_komoditas='')
    {
        $test_komoditas = $this->db->insert('komoditas', $value_komoditas);
        if($test_komoditas) {
            return true;
        }else{
            return false;
        }
    }

    public function hapus_anggota($value='')
    {
        $sql = "DELETE from detail_anggota where anggota_id = '$value'";
        $res = $this->db->query($sql);
        if ($res) {
            return true;
        }else{
            return false;
        }
    }

    public function get_master_komoditas()
    {
        $sql = "SELECT * from master_komoditas";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_master_dusun()
    {
        $sql = "SELECT * from dusun";
        $result = $this->db->query($sql);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    //Ambil detail KK untuk View Data
    public function get_detail_kk($value = ""){
        $sql = "SELECT * from detail_anggota WHERE kk_id = '$value'";
        $result = $this->db->query($sql);
        if($sql){
            return $result->result_array();
        } else{
            return false;
        }
    }

    public function searchkk($value='')
    {
        $sql = "SELECT * from keluarga where nama_kk LIKE '%$value%'";
        $result = $this->db->query($sql);
        
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    public function get_kk_id($nama_kk)
    {
        $sql = "SELECT * from keluarga where nama_kk LIKE ?";
        $result = $this->db->query($sql, $nama_kk);
        if ($sql) {
            return $result->result_array();
        }else{
            return false;
        }
    }

    //Ambil KK untuk input pilih KK
    public function get_kk(){
        $sql = "SELECT * from keluarga";
        $result = $this->db->query($sql);
        if ($result) {
            return $result->result_array();
        }else{
            return false;
        }
    }
}
?>