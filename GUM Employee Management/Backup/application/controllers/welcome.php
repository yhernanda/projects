<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('my_model');
		//$this->load->view('javascript');		
	}

	public function index()
	{
		if ($this->session->userdata('user_input')['username'] != "") {
			redirect('welcome/inputdata');
		}
		$this->load->view('home');
	}

	public function viewdata()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		
		$data['kk'] = $this->my_model->get_kk();
		$this->load->view('viewdata', $data);	
	}

	public function viewdetailkk($value = "")
	{
		$result = $this->my_model->get_detail_kk($value);
		$result1 = $this->my_model->get_data_kel($value);
		$result2 = $this->my_model->get_aset_kel($value);

		$array = array(
			"anggota" => $result,
			"data" => $result1,
			"aset" => $result2
		);

		header('Content-Type: application/json');
		echo json_encode($array);
	}

	public function get_kk($key){
		return $this->my_model->get_kk($key);
	}

	public function inputdata()
	{
		if ($this->session->userdata('user_input') == "") {
			redirect('welcome');
		}
		$data['kk'] = $this->my_model->get_kk();
		$data['hubungan'] = $this->my_model->get_hubungan();
		$data['agama'] = $this->my_model->get_agama();
		$data['penghasilan'] = $this->my_model->get_penghasilan();
		$data['pengeluaran'] = $data['penghasilan'];
		$data['lahan'] = $this->my_model->get_lahan();
		$data['pemasaran'] = $this->my_model->get_pemasaran();
		$data['komoditas'] = $this->my_model->get_master_komoditas();
		$data['dusun'] = $this->my_model->get_master_dusun();
		$this->load->view('inputdata', $data);	
		
	}

	public function process_login()
	{
		$this->load->model('My_model');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean|max_length[100]');
		$this->form_validation->set_rules('userpass', 'Password', 'required|trim|xss_clean|max_length[50]');

		if ($this->form_validation->run() == TRUE) {
			// if validation run
			$array = array('username' => $this->input->post('username'), 'password' => $this->input->post('userpass'));
			if ($query = $this->my_model->login_validation($array)) {
				//jika berhasil, redirect juga
				
				$this->session->set_userdata('user_input', $query);
				$this->session->set_flashdata('message', 'berhasil');
				redirect('welcome/inputdata');
			} else {
				$this->session->set_flashdata('message', '* Akun anda tidak dapat ditemukan');
			}
		} else {
			$this->session->set_flashdata('message', validation_errors());
		}
		redirect('welcome');
	}

	public function logout()
	{
		$this->session->unset_userdata('user_input');
		redirect('welcome');
	}

	public function daftar_user()
	{
		$data['users'] = $this->my_model->get_all_users();
		$this->load->view('userlist', $data);
	}

	public function hapus_user($user_id)
	{
		$result = $this->my_model->hapus_user($user_id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function insert_kk()
	{
		$array = array(
				'nama_kk' => $this->input->post('nama_kk'),
				'alamat' => $this->input->post('alamat_kk'),
				'rt' => $this->input->post('rt_kk'),
				'rw' => $this->input->post('rw_kk'),
				'dusun' => $this->input->post('dusun'),
				'desa' => $this->input->post('kelurahan'),
				'kecamatan' => $this->input->post('kecamatan'),
				'kabupaten' => $this->input->post('kabupaten'),
				'provinsi' => $this->input->post('provinsi'),
				'tanggal' => date('Y-m-d'),
				'pengisi' => $this->session->userdata('user_input')['user_id']
			);
		/*
		penggunaan for untuk mencari name jenis dan produksi, kemudian masukkan ke database berdasarkan kk_id
		deteksi kk_id terlebih dahulu, inputkan di kk_id yang sama
		optional = gunakan 1 row saja dengan text box yang cukup menampung seluruh variabel
		deteksi \n tiap text box untuk ditampilkan setelah input data
		*/
		$query = $this->my_model->insert_kk($array);
		if ($query) {
			redirect('welcome/inputdata');
		}else{

		}

		// $kk_id['kk_id'] = $this->my_model->get_kk_id($array['nama_kk']);
		// $array_komoditas = array(
		// 		'kk_id' => $this->input->post($kk_id['kk_id']),
		// 		'jenis' => $this->input->post('jenis'),
		// 		'produksi' => $this->input->post('produksi')
		// 	);


		// $query_komoditas = $this->my_model->insert_komoditas($array_komoditas);
		// if ($query_komoditas) {
		// 	redirect('welcome/inputdata');
		// }else{
			
		// }
	}

	public function searchkk($value='')
	{
		$result = $this->my_model->searchkk($value);
		//$result = 'true';
		header('Content-Type: application/json');
		echo json_encode($result);
		//die();
	}

	public function insert_anggota()
	{
		$cacat_fisik = "";
		$cacat_mental = "";
		$lbg_masyarakat = "";
		$pajak_retribusi = "";

		if($this->input->post('cacat_fisik')){
			foreach ($this->input->post('cacat_fisik') as $key => $value) {
				if($value != "") $cacat_fisik .= $value.";";
			}
		}
		if($this->input->post('cacat_mental')){
			foreach ($this->input->post('cacat_mental') as $key => $value) {
				if($value != "") $cacat_mental .= $value.";";
			}
		}
		if($this->input->post('lbg_masyarakat')){
			foreach ($this->input->post('lbg_masyarakat') as $key => $value) {
				if($value != "") $lbg_masyarakat .= $value.";";
			}
		}
		if($this->input->post('pajak_retribusi')){
			foreach ($this->input->post('pajak_retribusi') as $key => $value) {
				if($value != "") $pajak_retribusi .= $value.";";
			}
		}

		$array = array(
			"no_urut" => $this->input->post('no_urut'),
			"kk_id" => $this->input->post('kk_id'),
			"nama_lengkap" => $this->input->post('nama_lengkap'),
			"jenis_kelamin" => $this->input->post('jenis_kelamin'),
			"hubungan" => $this->input->post('hubungan'),
			"tempat_lahir" => $this->input->post('tempat_lahir'),
			"tanggal_lahir" => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
			"status_nikah" => $this->input->post('status_nikah'),
			"agama" => $this->input->post('agama'),
			"gol_darah" => $this->input->post('gol_darah'),
			"kewarganegaraan" => $this->input->post('kewarganegaraan'),
			"pendidikan" => $this->input->post('pendidikan'),
			"mata_pencaharian" => $this->input->post('mata_pencaharian'),
			"orangtua" => $this->input->post('nama_bapak'),
			"no_akta" => $this->input->post('akta_kelahiran'),
			"nik" => $this->input->post('nik'),
			"akseptor_kb" => $this->input->post('akseptor_kb'),
			"cacat_fisik" => $cacat_fisik,
			"cacat_mental" => $cacat_mental,
			"lembaga_pemerintahan" => $this->input->post('lbg_pemerintahan'),
			"lembaga_kemasyarakatan" => $lbg_masyarakat,
			"lembaga_politik" => $this->input->post('lbg_politik'),
			"lembaga_ekonomi_pribadi" => $this->input->post('lbg_ekonomi'),
			"penyakit_diderita" => $this->input->post('penyakit_diderita'),
			"pajak_retribusi" => $pajak_retribusi,			 
		);

		$query = $this->my_model->insert_anggota($array);
		if ($query) {
			redirect('welcome/inputdata');
		}else{
			echo "gagal"; die;
		}
	}

	public function insert_data_kel(){
		$p_air = "";
		$k_hamil = "";
		$sakit_kelainan = "";
		$k_bayi = "";
		$t_persalinan = "";
		$p_persalinan = "";
		$c_imunisasi = "";
		
		if($this->input->post('pemanfaatan_mata_air')){
			foreach ($this->input->post('pemanfaatan_mata_air') as $key => $value) {
				if($value != "") $p_air .= $value.";";
			}
		}
		if($this->input->post('kualitas_hamil')){
			foreach ($this->input->post('kualitas_hamil') as $key => $value) {
				if($value != "") $k_hamil .= $value.";";
			}
		}
		if($this->input->post('sakit_kelainan')){
			foreach ($this->input->post('sakit_kelainan') as $key => $value) {
				if($value != "") $sakit_kelainan .= $value.";";
			}
		}
		if($this->input->post('kualitas_bayi')){
			foreach ($this->input->post('kualitas_bayi') as $key => $value) {
				if($value != "") $k_bayi .= $value.";";
			}
		}
		if($this->input->post('tempat_persalinan')){
			foreach ($this->input->post('tempat_persalinan') as $key => $value) {
				if($value != "") $t_persalinan .= $value.";";
			}
		}
		if($this->input->post('pertolongan_persalinan')){
			foreach ($this->input->post('pertolongan_persalinan') as $key => $value) {
				if($value != "") $p_persalinan .= $value.";";
			}
		}
		if($this->input->post('cakupan_imunisasi')){
			foreach ($this->input->post('cakupan_imunisasi') as $key => $value) {
				if($value != "") $c_imunisasi .= $value.";";
			}
		}

		$array = array(
			"kk_id" => $this->input->post('kk_id_data'),
			"kepemilikan_rumah" => $this->input->post('kepemilikan_rumah'),
			"penghasilan" => $this->input->post('penghasilan_perbulan'),
			"pengeluaran" => $this->input->post('pengeluaran_perbulan'),
			"sumber_air" => $this->input->post('air_minum'),
			"kualitas_air" => $this->input->post('kualitas_air'),
			"pemanfaatan_air" => $p_air,
			"kualitas_hamil" => $k_hamil,
			"sakit_dan_kelainan" => $sakit_kelainan,
			"gizi_balita" => $this->input->post('gizi_balita'),
			"kualitas_bayi" => $k_bayi,
			"kualitas_persalinan" => $t_persalinan.";".$p_persalinan,
			"cakupan_imunisasi" => $c_imunisasi,
			"phbs" => $this->input->post('hidup_bersih_sehat'),
			"pola_makan" => $this->input->post('pola_makan'),
			"kebiasaan_berobat" => $this->input->post('kebiasaan_berobat')
		);

		$query = $this->my_model->insert_data_kel($array);                                                                                                                                                                                                                                                                                       
		if ($query) {
			redirect('welcome/inputdata');
		}else{
			echo "gagal"; die();
		}
	}

	public function insert_aset()
	{	
		$lain = "";
		$sarana = "";
		$trans = "";

		if($this->input->post('aset_lain')){
			foreach ($this->input->post('aset_lain') as $key => $value) {
				if($value != "") $lain .= $value.";";
			}
		}
		if($this->input->post('aset_sarana')){
			foreach ($this->input->post('aset_sarana') as $key => $value) {
				if($value != "") $sarana .= $value.";";
			}
		}
		if($this->input->post('aset_trans')){
			foreach ($this->input->post('aset_trans') as $key => $value) {
				if($value != "") $trans .= $value.";";
			}
		}

		$array = array(
			"kk_id" => $this->input->post('kk_id_aset'),
			"aset_tanah" => $this->input->post('aset_tanah'),
			"aset_transportasi" => $trans,
			"aset_sarana_produksi" => $sarana,
			"aset_perumahan" =>  $this->input->post('dinding_rumah').";".$this->input->post('lantai_rumah').";".$this->input->post('atap_rumah'),
			"aset_lainnya" => $lain
		);

		$query = $this->my_model->insert_aset($array);
		if ($query) {
			redirect('welcome/inputdata');
		}else{
			echo "gagal"; die;
		}
	}

	public function hapus_anggota($anggota_id)
	{
		$result = $this->my_model->hapus_anggota($anggota_id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */