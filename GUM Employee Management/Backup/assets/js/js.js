$(document).ready(function () {
	$('[id="tool"]').tooltip();

	$('#tabelkk').DataTable();

	$('#tabeldetailkk').DataTable();

	$('#tabeldatakk').DataTable();

	$('#tabelkk').on('click', 'tr td a.detail_kk', function (e) {
		e.preventDefault();
		var kk_id = $(this).closest('tr').find('td.kk_id').text();
		alert(kk_id);
		$.ajax({
			type: "POST",
			url: "http://localhost/sensusmunggur/welcome/viewdetailkk/"+kk_id,
			success: function (data) {
				console.log(data);
				$('#back_link').text("Back");
				$('#back_link').attr('href', "http://localhost/sensusmunggur/welcome/viewdata");
				$('#title').text("Detail Anggota Keluarga");
				$('#table-wrapper-kk').addClass("hidden");
				$('#t_bodydetailkk').empty();
				alert(data);
					for (var i = data["anggota"].length - 1; i >= 0; i--) {
						$('#t_bodydetailkk').append(
								'<tr>'+
									'<td style="display:none" class="anggota_id">'+data["anggota"][i]["anggota_id"]+'</td>'+
									'<td>'+data["anggota"][i]["no_urut"]+'</td>'+
									'<td>'+data["anggota"][i]["nama_lengkap"]+'</td>'+
									'<td>'+data["anggota"][i]["jenis_kelamin"]+'</td>'+
									'<td>'+data["anggota"][i]["hubungan"]+'</td>'+
									'<td>'+data["anggota"][i]["tempat_lahir"]+'</td>'+
									'<td>'+data["anggota"][i]["tanggal_lahir"]+'</td>'+
									'<td>'+data["anggota"][i]["agama"]+'</td>'+
									'<td>'+data["anggota"][i]["gol_darah"]+'</td>'+
									'<td>'+
										'<a href="#" class="detail_anggota" id="tool" data-toggle="tooltip" data-placement="top" title="Detail"><i class="glyphicon glyphicon-menu-hamburger"></i></a>'+
										'<a href="#" class="edit_anggota" id="tool" data-toggle="tooltip" data-placement="top" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>'+
										'<a href="#" class="hapus_anggota" id="tool" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>'+
									'</td>'+
								'</tr>'
						);

					};

					for (var i = data["data"].length - 1; i >= 0; i--) {
						$('#t_bodydatakk').append(
								'<tr>'+
									'<td>'+data["data"][i]["kepemilikan_rumah"]+'</td>'+
									'<td>'+data["data"][i]["penghasilan"]+'</td>'+
									'<td>'+data["data"][i]["pengeluaran"]+'</td>'+
									'<td>'+data["data"][i]["sumber_air"]+'</td>'+
									'<td>'+data["data"][i]["kualitas_air"]+'</td>'+
									'<td>'+data["data"][i]["gizi_balita"]+'</td>'+
									'<td>'+data["data"][i]["phbs"]+'</td>'+
									'<td>'+data["data"][i]["pola_makan"]+'</td>'+
									'<td>'+
										'<a href="#" class="detail_data" id="tool" data-toggle="tooltip modal" data-target="#detail_data" data-placement="top" title="Detail"><i class="glyphicon glyphicon-menu-hamburger"></i></a>'+
										'<a href="#" class="edit_data" id="tool" data-toggle="tooltip" data-placement="top" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>'+
										'<a href="#" class="hapus_data" id="tool" data-toggle="tooltip" data-placement="top" title="Delete"><i class="glyphicon glyphicon-trash"></i></a>'+
									'</td>'+
								'</tr>'
						);
						var split = data["data"][i]["pemanfaatan_air"].split(";");
						$('#t_bodydatamodal').append(
							'<tr>'+
								'<td>Kepemilikan Rumah</td>'+
								'<td>'+data["data"][i]["kepemilikan_rumah"]+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Penghasilan</td>'+
								'<td>'+data["data"][i]["penghasilan"]+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Pengeluaran</td>'+
								'<td>'+data["data"][i]["pengeluaran"]+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Kepemilikan Rumah</td>'+
								'<td>'+data["data"][i]["kepemilikan_rumah"]+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Sumber Air Minum</td>'+
								'<td>'+data["data"][i]["sumber_air"]+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td>Kualitas Air Minum</td>'+
								'<td>'+data["data"][i]["kualitas_air"]+'</td>'+
							'</tr>'+
							'<tr>'+
								'<td rowspan="'+split.length+'">Pemanfaatan Mata Air Keluarga</td>'
									
								
						);
						var plus = "";
						for (var j = 0; j <= split.length - 1; j++) {
										plus +='<td>'+split[j]+'</td>'+ '</tr>'+ '<tr>';
						}
						$('#t_bodydatamodal').append(
								'<td>Kualitas Ibu Hamil</td>'+
								//'<td>'+data["data"][i]["kualitas_air"]+'</td>'+
							'</tr>'
						);
					};
				$('#table-wrapper-dkk').removeClass("hidden");
			},
			error: function (data) {
				alert('terjadi kesalahan, gagal');
				return false;
			}
		})
	});

	$('#tabeldatakk').on('click', 'tr td a.detail_kk', function (e) {

	});

	$('#tabeldetailkk').on('click', 'tr td a.hapus_anggota', function (e) {
		e.preventDefault();
		var d = confirm('apakah akan dihapus?');
		if (d == true){
			var anggota_id = $(this).closest('tr').find('td.anggota_id').text();
			$.ajax({
				type: "POST",
				url: 'http://localhost/sensusmunggur/welcome/hapus_anggota/'+anggota_id,
				success: function (data) {
					
				},
				error: function  (data) {
					alert('terjadi kesalahan, gagal');
					return false;
				}						
			})
		}else{
			return false;
		}
		$(this).closest('tr').fadeOut(function () {
			$(this).remove();
		})
	});
});