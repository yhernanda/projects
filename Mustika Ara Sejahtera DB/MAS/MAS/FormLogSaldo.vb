﻿Imports MySql.Data.MySqlClient

Public Class FormLogSaldo
    Private Sub FormLogSaldo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RefreshGridSaldo()
    End Sub

    Private Function ThousandSplitter(ByVal nominal As String)
        Dim temp1 As String
        Dim temp2 As String
        Dim index As Integer = nominal.Length
        While (index - 3) >= 1
            index -= 3
            temp1 = nominal.Substring(0, index)
            temp2 = nominal.Substring(index)
            If temp1 <> "-" Then
                temp1 += "."
            End If
            nominal = temp1 + temp2
        End While
        temp1 = "Rp."
        temp2 = ",-"
        nominal = temp1 + nominal + temp2
        Return nominal
    End Function

    Sub RefreshGridSaldo()
        Dim i
        i = 0
        DataGridView1.Rows.Clear()
        Dim sql As String = "select * from logsaldo where tanggal <= '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & " " & DateTimePicker1.Value.Hour & ":" & DateTimePicker1.Value.Minute & ":" & DateTimePicker1.Value.Second & "' group by tanggal desc"
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If
        If myCommand Is Nothing Then
            myCommand = New MySqlCommand(sql, myConn)
        Else
            myCommand.CommandText = sql
        End If
        myDataReader = myCommand.ExecuteReader
        If myDataReader.HasRows Then
            While myDataReader.Read()
                DataGridView1.Rows.Add()
                DataGridView1.Item(0, i).Value = myDataReader("tanggal")
                DataGridView1.Item(1, i).Value = ThousandSplitter(myDataReader("modal"))
                DataGridView1.Item(2, i).Value = ThousandSplitter(myDataReader("laba"))
                DataGridView1.Item(3, i).Value = ThousandSplitter(myDataReader("promosi"))
                DataGridView1.Item(4, i).Value = ThousandSplitter(myDataReader("asuransi"))
                DataGridView1.Item(5, i).Value = ThousandSplitter(myDataReader("perpuluhan"))
                DataGridView1.Item(6, i).Value = myDataReader("id")
                i = i + 1
            End While
        End If
        If myDataReader.IsClosed = False Then
            myDataReader.Close()
        End If
    End Sub

    Private Sub btnLihat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLihat.Click
        RefreshGridSaldo()
    End Sub
End Class