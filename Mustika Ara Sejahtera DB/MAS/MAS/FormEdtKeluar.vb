﻿Imports MySql.Data.MySqlClient

Public Class FormEdtKeluar
    Public rowId As Integer
    Dim nominalAwal As Integer
    Dim nominalAkhir As Integer
    Dim jenisAwal As String
    Dim jenisAkhir As String
    Dim selisihNominal As Integer

    Private Sub FormEdtKeluar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        jenisAwal = ComboJenis.Text
        nominalAwal = NumNominal.Value
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ComboJenis.Text = ""
        NumNominal.Value = 0
        txtKeterangan.Text = ""
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If ComboJenis.Text <> "" And NumNominal.Value <> 0 Then
            Dim sql As String
            jenisAkhir = ComboJenis.Text
            nominalAkhir = NumNominal.Value
            If DateTimePicker1.Value.Year.Equals(Now.Year) And DateTimePicker1.Value.Month.Equals(Now.Month) And DateTimePicker1.Value.Day.Equals(Now.Day) Then
                sql = "UPDATE `pengeluaran` SET `tanggal`=CURRENT_TIMESTAMP,`jenis`='" & ComboJenis.Text & "',`nominal`=" & NumNominal.Value & ",`keterangan`='" & txtKeterangan.Text & "',`id_user`='" & ModuleDB.pengguna & "' WHERE id = " & rowId
            Else
                sql = "UPDATE `pengeluaran` SET `tanggal`='" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & " " & DateTimePicker1.Value.Hour & ":" & DateTimePicker1.Value.Minute & ":" & DateTimePicker1.Value.Second & "',`jenis`='" & ComboJenis.Text & "',`nominal`=" & NumNominal.Value & ",`keterangan`='" & txtKeterangan.Text & "',`id_user`='" & ModuleDB.pengguna & "' WHERE id = " & rowId
            End If
            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridKeluar()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If

            sql = "INSERT INTO `logsaldo`(`tanggal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`) VALUES ((select `tanggal` from total where id = 1), (select `modal` from total where id = 1), (select `laba` from total where id = 1), (select `promosi` from total where id = 1), (select `asuransi` from total where id = 1), (select `perpuluhan` from total where id = 1))"

            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myDataReader = myCommand.ExecuteReader()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If

            If jenisAwal.Equals(jenisAkhir) Then
                selisihNominal = nominalAwal - nominalAkhir
                If jenisAkhir.Equals("Lain-lain") Then
                    jenisAkhir = "laba"
                End If
                sql = "UPDATE `total` SET `" & jenisAkhir.ToLower & "`=(" & jenisAkhir.ToLower & "+" & selisihNominal & ") WHERE id = 1"
            Else
                If jenisAwal = "Lain-lain" Then
                    jenisAwal = "laba"
                End If
                sql = "UPDATE `total` SET `" & jenisAwal.ToLower & "`=(" & jenisAwal.ToLower & "+" & nominalAwal & ") WHERE id = 1"
                If myConn.State = ConnectionState.Closed Then
                    myConn.Open()
                End If
                If myCommand Is Nothing Then
                    myCommand = New MySqlCommand(sql, myConn)
                Else
                    myCommand.CommandText = sql
                End If
                myCommand.ExecuteNonQuery()
                FormDatabase.RefreshGridTotal()
                If myDataReader.IsClosed = False Then
                    myDataReader.Close()
                End If
                If myConn.State = ConnectionState.Open Then
                    myConn.Close()
                End If

                If jenisAkhir = "Lain-lain" Then
                    jenisAkhir = "laba"
                End If
                sql = "UPDATE `total` SET `" & jenisAkhir.ToLower & "`=(" & jenisAkhir.ToLower & "-" & nominalAkhir & ") WHERE id = 1"
            End If

            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridTotal()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If
            Me.Close()
        Else
            MsgBox("Mohon Lengkapi Semua Kolom", MsgBoxStyle.Information, "MAS Error")
        End If
    End Sub
End Class