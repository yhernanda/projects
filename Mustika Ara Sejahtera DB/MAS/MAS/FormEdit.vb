﻿Imports MySql.Data.MySqlClient

Public Class FormEdit
    Public rowId As Integer
    Dim nominalAwal As Integer
    Dim nominalAkhir As Integer

    Dim Sql As String
    Dim penjualan As String

    Private Function HapusAwal()
        If penjualan = "Normal" Then
            Sql = "UPDATE `total` SET `modal`=(modal-" & (nominalAwal * 40 / 100) & "),`laba`=(laba-" & (nominalAwal * 30 / 100) & "),`promosi`=(promosi-" & (nominalAwal * 10 / 100) & "),`asuransi`=(asuransi-" & (nominalAwal * 10 / 100) & "),`perpuluhan`=(perpuluhan-" & (nominalAwal * 10 / 100) & ") WHERE id = 1"
        ElseIf penjualan = "Anggota" Then
            Sql = "UPDATE `total` SET `modal`=(modal-" & (nominalAwal * 60 / 100) & "),`laba`=(laba-" & (nominalAwal * 20 / 100) & "),`promosi`=(promosi-" & (nominalAwal * 5 / 100) & "),`asuransi`=(asuransi-" & (nominalAwal * 5 / 100) & "),`perpuluhan`=(perpuluhan-" & (nominalAwal * 10 / 100) & ") WHERE id = 1"
        End If
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If
        If myCommand Is Nothing Then
            myCommand = New MySqlCommand(Sql, myConn)
        Else
            myCommand.CommandText = Sql
        End If
        myDataReader = myCommand.ExecuteReader()
        If myDataReader.IsClosed = False Then
            myDataReader.Close()
        End If
    End Function

    Private Sub FormEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        nominalAwal = NumNominal.Value
        If comboJual.Text = "" Then
            penjualan = "Normal"
        Else
            penjualan = comboJual.Text
        End If
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If ComboTransaksi.Text <> "" And comboJual.Text <> "" And NumJumlah.Value <> 0 And NumNominal.Value <> 0 Then
            Sql = "INSERT INTO `logsaldo`(`tanggal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`) VALUES ((select `tanggal` from total where id = 1), (select `modal` from total where id = 1), (select `laba` from total where id = 1), (select `promosi` from total where id = 1), (select `asuransi` from total where id = 1), (select `perpuluhan` from total where id = 1))"

            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(Sql, myConn)
            Else
                myCommand.CommandText = Sql
            End If
            myDataReader = myCommand.ExecuteReader()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If

            HapusAwal()
            nominalAkhir = NumNominal.Value

            If comboJual.Text = "Normal" Then
                If DateTimePicker1.Value.Year.Equals(Now.Year) And DateTimePicker1.Value.Month.Equals(Now.Month) And DateTimePicker1.Value.Day.Equals(Now.Day) Then
                    Sql = "UPDATE `pemasukan` SET `tanggal`=CURRENT_TIMESTAMP,`transaksi`='" & ComboTransaksi.Text & "',`penjualan`='" & comboJual.Text & "',`jumlah`=" & NumJumlah.Value & ",`nominal`=" & NumNominal.Value & ",`modal`=" & (NumNominal.Value * 40 / 100) & ",`laba`=" & (NumNominal.Value * 30 / 100) & ",`promosi`=" & (NumNominal.Value * 10 / 100) & ",`asuransi`=" & (NumNominal.Value * 10 / 100) & ",`perpuluhan`=" & (NumNominal.Value * 10 / 100) & ",`keterangan`='" & txtKeterangan.Text & "',`id_user`='" & ModuleDB.pengguna & "' WHERE id = " & rowId
                Else
                    Sql = "UPDATE `pemasukan` SET `tanggal`='" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & " " & DateTimePicker1.Value.Hour & ":" & DateTimePicker1.Value.Minute & ":" & DateTimePicker1.Value.Second & "',`transaksi`='" & ComboTransaksi.Text & "',`penjualan`='" & comboJual.Text & "',`jumlah`=" & NumJumlah.Value & ",`nominal`=" & NumNominal.Value & ",`modal`=" & (NumNominal.Value * 40 / 100) & ",`laba`=" & (NumNominal.Value * 30 / 100) & ",`promosi`=" & (NumNominal.Value * 10 / 100) & ",`asuransi`=" & (NumNominal.Value * 10 / 100) & ",`perpuluhan`=" & (NumNominal.Value * 10 / 100) & ",`keterangan`='" & txtKeterangan.Text & "',`id_user`='" & ModuleDB.pengguna & "' WHERE id = " & rowId
                End If
            Else
                If DateTimePicker1.Value.Year.Equals(Now.Year) And DateTimePicker1.Value.Month.Equals(Now.Month) And DateTimePicker1.Value.Day.Equals(Now.Day) Then
                    Sql = "UPDATE `pemasukan` SET `tanggal`=CURRENT_TIMESTAMP,`transaksi`='" & ComboTransaksi.Text & "',`penjualan`='" & comboJual.Text & "',`jumlah`=" & NumJumlah.Value & ",`nominal`=" & NumNominal.Value & ",`modal`=" & (NumNominal.Value * 60 / 100) & ",`laba`=" & (NumNominal.Value * 20 / 100) & ",`promosi`=" & (NumNominal.Value * 5 / 100) & ",`asuransi`=" & (NumNominal.Value * 5 / 100) & ",`perpuluhan`=" & (NumNominal.Value * 10 / 100) & ",`keterangan`='" & txtKeterangan.Text & "',`id_user`='" & ModuleDB.pengguna & "' WHERE id = " & rowId
                Else
                    Sql = "UPDATE `pemasukan` SET `tanggal`='" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & " " & DateTimePicker1.Value.Hour & ":" & DateTimePicker1.Value.Minute & ":" & DateTimePicker1.Value.Second & "',`transaksi`='" & ComboTransaksi.Text & "',`penjualan`='" & comboJual.Text & "',`jumlah`=" & NumJumlah.Value & ",`nominal`=" & NumNominal.Value & ",`modal`=" & (NumNominal.Value * 60 / 100) & ",`laba`=" & (NumNominal.Value * 20 / 100) & ",`promosi`=" & (NumNominal.Value * 5 / 100) & ",`asuransi`=" & (NumNominal.Value * 5 / 100) & ",`perpuluhan`=" & (NumNominal.Value * 10 / 100) & ",`keterangan`='" & txtKeterangan.Text & "',`id_user`='" & ModuleDB.pengguna & "' WHERE id = " & rowId
                End If
            End If

            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridMasuk()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If

            If comboJual.Text = "Normal" Then
                Sql = "UPDATE `total` SET `tanggal`=CURRENT_TIMESTAMP,`modal`=(modal+" & (nominalAkhir * 40 / 100) & "),`laba`=(laba+" & (nominalAkhir * 30 / 100) & "),`promosi`=(promosi+" & (nominalAkhir * 10 / 100) & "),`asuransi`=(asuransi+" & (nominalAkhir * 10 / 100) & "),`perpuluhan`=(perpuluhan+" & (nominalAkhir * 10 / 100) & ") WHERE id = 1"
            Else
                Sql = "UPDATE `total` SET `tanggal`=CURRENT_TIMESTAMP,`modal`=(modal+" & (nominalAkhir * 60 / 100) & "),`laba`=(laba+" & (nominalAkhir * 20 / 100) & "),`promosi`=(promosi+" & (nominalAkhir * 5 / 100) & "),`asuransi`=(asuransi+" & (nominalAkhir * 5 / 100) & "),`perpuluhan`=(perpuluhan+" & (nominalAkhir * 10 / 100) & ") WHERE id = 1"
            End If

            'sql = "UPDATE `total` SET `tanggal`=CURRENT_TIMESTAMP,`modal`=(modal-" & (selisihNominal * 40 / 100) & "),`laba`=(laba-" & (selisihNominal * 30 / 100) & "),`promosi`=(promosi-" & (selisihNominal * 10 / 100) & "),`asuransi`=(asuransi-" & (selisihNominal * 10 / 100) & "),`perpuluhan`=(perpuluhan-" & (selisihNominal * 10 / 100) & ") WHERE id = 1"
            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridTotal()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If
            Me.Close()
        Else
            MsgBox("Mohon Lengkapi Semua Kolom", MsgBoxStyle.Information, "MAS Error")
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ComboTransaksi.Text = ""
        NumNominal.Value = 0
        NumJumlah.Value = 0
        txtKeterangan.Text = ""
    End Sub
End Class