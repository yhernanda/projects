﻿Imports MySql.Data.MySqlClient

Public Class FormDatabase
    Dim Pemasukan As Boolean = True

    Private Sub FormDatabase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RefreshGridMasuk()
        RefreshGridKeluar()
        RefreshGridTotal()
    End Sub

    Private Function ThousandSplitter(ByVal nominal As String)
        Dim temp1 As String
        Dim temp2 As String
        Dim index As Integer = nominal.Length
        While (index - 3) >= 1
            index -= 3
            temp1 = nominal.Substring(0, index)
            temp2 = nominal.Substring(index)
            If temp1 <> "-" Then
                temp1 += "."
            End If
            nominal = temp1 + temp2
        End While
        temp1 = "Rp."
        temp2 = ",-"
        nominal = temp1 + nominal + temp2
        Return nominal
    End Function

    Sub RefreshGridMasuk()
        Dim i
        i = 0
        DataGridViewMasuk.Rows.Clear()
        Dim sql As String = "select * from pemasukan group by tanggal desc"
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If
        If myCommand Is Nothing Then
            myCommand = New MySqlCommand(sql, myConn)
        Else
            myCommand.CommandText = sql
        End If
        myDataReader = myCommand.ExecuteReader
        If myDataReader.HasRows Then
            While myDataReader.Read()
                DataGridViewMasuk.Rows.Add()
                DataGridViewMasuk.Item(0, i).Value = myDataReader("tanggal")
                DataGridViewMasuk.Item(1, i).Value = myDataReader("transaksi")
                DataGridViewMasuk.Item(2, i).Value = myDataReader("penjualan")
                DataGridViewMasuk.Item(3, i).Value = myDataReader("jumlah")
                DataGridViewMasuk.Item(4, i).Value = ThousandSplitter(myDataReader("nominal"))
                DataGridViewMasuk.Item(5, i).Value = ThousandSplitter(myDataReader("modal"))
                DataGridViewMasuk.Item(6, i).Value = ThousandSplitter(myDataReader("laba"))
                DataGridViewMasuk.Item(7, i).Value = ThousandSplitter(myDataReader("promosi"))
                DataGridViewMasuk.Item(8, i).Value = ThousandSplitter(myDataReader("asuransi"))
                DataGridViewMasuk.Item(9, i).Value = ThousandSplitter(myDataReader("perpuluhan"))
                DataGridViewMasuk.Item(10, i).Value = myDataReader("keterangan")
                DataGridViewMasuk.Item(11, i).Value = myDataReader("id_user")
                DataGridViewMasuk.Item(12, i).Value = myDataReader("id")
                i = i + 1
            End While
        End If
        If myDataReader.IsClosed = False Then
            myDataReader.Close()
        End If
    End Sub

    Sub RefreshGridKeluar()
        Dim i
        i = 0
        DataGridViewKeluar.Rows.Clear()
        Dim sql As String = "select * from pengeluaran group by tanggal desc"
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If
        If myCommand Is Nothing Then
            myCommand = New MySqlCommand(sql, myConn)
        Else
            myCommand.CommandText = sql
        End If
        myDataReader = myCommand.ExecuteReader
        If myDataReader.HasRows Then
            While myDataReader.Read()
                DataGridViewKeluar.Rows.Add()
                DataGridViewKeluar.Item(0, i).Value = myDataReader("tanggal")
                DataGridViewKeluar.Item(1, i).Value = myDataReader("jenis")
                DataGridViewKeluar.Item(2, i).Value = ThousandSplitter(myDataReader("nominal"))
                DataGridViewKeluar.Item(3, i).Value = myDataReader("keterangan")
                DataGridViewKeluar.Item(4, i).Value = myDataReader("id_user")
                DataGridViewKeluar.Item(5, i).Value = myDataReader("id")
                i = i + 1
            End While
        End If
        If myDataReader.IsClosed = False Then
            myDataReader.Close()
        End If
    End Sub

    Sub RefreshGridTotal()
        Dim i
        i = 0
        DataGridView1.Rows.Clear()
        Dim sql As String = "select * from total"
        If myConn.State = ConnectionState.Closed Then
            myConn.Open()
        End If
        If myCommand Is Nothing Then
            myCommand = New MySqlCommand(sql, myConn)
        Else
            myCommand.CommandText = sql
        End If
        myDataReader = myCommand.ExecuteReader
        If myDataReader.HasRows Then
            While myDataReader.Read()
                DataGridView1.Rows.Add()
                DataGridView1.Item(0, i).Value = myDataReader("tanggal")
                DataGridView1.Item(1, i).Value = ThousandSplitter(myDataReader("modal"))
                DataGridView1.Item(2, i).Value = ThousandSplitter(myDataReader("laba"))
                DataGridView1.Item(3, i).Value = ThousandSplitter(myDataReader("promosi"))
                DataGridView1.Item(4, i).Value = ThousandSplitter(myDataReader("asuransi"))
                DataGridView1.Item(5, i).Value = ThousandSplitter(myDataReader("perpuluhan"))
                DataGridView1.Item(6, i).Value = myDataReader("id")
                i = i + 1
            End While
        End If
        If myDataReader.IsClosed = False Then
            myDataReader.Close()
        End If
    End Sub

    Private Sub btnTabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTabel.Click
        If Pemasukan = True Then
            PanelMasuk.Visible = False
            lblData.Text = "Pengeluaran"
            Pemasukan = False
            btnTabel.Text = "Tabel Pemasukan"
        Else
            PanelMasuk.Visible = True
            Pemasukan = True
            lblData.Text = "Pemasukan"
            btnTabel.Text = "Tabel Pengeluaran"
        End If
    End Sub

    Private Sub btnSaldo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaldo.Click
        FormLogSaldo.Show()
    End Sub

    Private Sub btnTambah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTambah.Click
        If Pemasukan = True Then
            FormTambah.Show()
        Else
            FormTmbhKeluar.Show()
        End If
    End Sub

    Private Sub FormDatabase_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        FormLogin.Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If (DataGridViewMasuk.SelectedRows.Count = 1 Or DataGridViewKeluar.SelectedRows.Count = 1) Then
            Dim id As String
            Dim sql As String
            If Pemasukan = True Then
                id = DataGridViewMasuk.SelectedCells.Item(12).Value.ToString
                sql = "select * from pemasukan where id = " & id
            Else
                id = DataGridViewKeluar.SelectedCells.Item(5).Value.ToString
                sql = "select * from pengeluaran where id = " & id
            End If
            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myDataReader = myCommand.ExecuteReader
            If myDataReader.HasRows Then
                myDataReader.Read()
                If Pemasukan = True Then
                    FormEdit.ComboTransaksi.Text = myDataReader("transaksi")
                    FormEdit.comboJual.Text = myDataReader("penjualan").ToString()
                    FormEdit.NumJumlah.Value = myDataReader("jumlah")
                    FormEdit.NumNominal.Value = myDataReader("nominal")
                    FormEdit.txtKeterangan.Text = myDataReader("keterangan")
                Else
                    FormEdtKeluar.ComboJenis.Text = myDataReader("jenis")
                    FormEdtKeluar.NumNominal.Value = myDataReader("nominal")
                    FormEdtKeluar.txtKeterangan.Text = myDataReader("keterangan")
                End If
                If myDataReader.IsClosed = False Then
                    myDataReader.Close()
                End If
            End If

            If Pemasukan = True Then
                FormEdit.rowId = id
                FormEdit.Show()
            Else
                FormEdtKeluar.rowId = id
                FormEdtKeluar.Show()
            End If
        End If
    End Sub

    Private Sub btnHapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHapus.Click
        If (DataGridViewMasuk.SelectedRows.Count = 1 Or DataGridViewKeluar.SelectedRows.Count = 1) Then
            Dim result As MsgBoxResult = MessageBox.Show("Hapus Data?", "Konfirmasi Hapus Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = vbYes Then
                Dim total As Integer
                Dim sql As String
                Dim id As String
                Dim jenisKeluar As String
                Dim penjualan As String
                If Pemasukan = True Then
                    id = DataGridViewMasuk.SelectedCells.Item(12).Value.ToString
                    sql = "select * from pemasukan where id = " & id
                Else
                    id = DataGridViewKeluar.SelectedCells.Item(5).Value.ToString
                    sql = "select * from pengeluaran where id = " & id
                End If
                If myConn.State = ConnectionState.Closed Then
                    myConn.Open()
                End If
                If myCommand Is Nothing Then
                    myCommand = New MySqlCommand(sql, myConn)
                Else
                    myCommand.CommandText = sql
                End If
                myDataReader = myCommand.ExecuteReader
                If myDataReader.HasRows Then
                    myDataReader.Read()
                    total = myDataReader("nominal")
                    If Pemasukan = False Then
                        jenisKeluar = myDataReader("jenis")
                    Else
                        penjualan = myDataReader("penjualan")
                    End If
                    If myDataReader.IsClosed = False Then
                        myDataReader.Close()
                    End If
                End If

                sql = "INSERT INTO `logsaldo`(`tanggal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`) VALUES ((select `tanggal` from total where id = 1), (select `modal` from total where id = 1), (select `laba` from total where id = 1), (select `promosi` from total where id = 1), (select `asuransi` from total where id = 1), (select `perpuluhan` from total where id = 1))"

                If myConn.State = ConnectionState.Closed Then
                    myConn.Open()
                End If
                If myCommand Is Nothing Then
                    myCommand = New MySqlCommand(sql, myConn)
                Else
                    myCommand.CommandText = sql
                End If
                myDataReader = myCommand.ExecuteReader()
                If myDataReader.IsClosed = False Then
                    myDataReader.Close()
                End If

                If Pemasukan = True And (penjualan = "Normal" Or penjualan = "") Then
                    sql = "UPDATE `total` SET `modal`=(modal-" & (total * 40 / 100) & "),`laba`=(laba-" & (total * 30 / 100) & "),`promosi`=(promosi-" & (total * 10 / 100) & "),`asuransi`=(asuransi-" & (total * 10 / 100) & "),`perpuluhan`=(perpuluhan-" & (total * 10 / 100) & ") WHERE id = 1"
                ElseIf Pemasukan = True And penjualan = "Anggota" Then
                    sql = "UPDATE `total` SET `modal`=(modal-" & (total * 60 / 100) & "),`laba`=(laba-" & (total * 20 / 100) & "),`promosi`=(promosi-" & (total * 5 / 100) & "),`asuransi`=(asuransi-" & (total * 5 / 100) & "),`perpuluhan`=(perpuluhan-" & (total * 10 / 100) & ") WHERE id = 1"
                Else
                    If jenisKeluar = "Lain-lain" Then
                        jenisKeluar = "laba"
                    End If
                    sql = "UPDATE `total` SET `" & jenisKeluar & "`=(" & jenisKeluar & "+" & total & ") WHERE id = 1"
                End If
                If myConn.State = ConnectionState.Closed Then
                    myConn.Open()
                End If
                If myCommand Is Nothing Then
                    myCommand = New MySqlCommand(sql, myConn)
                Else
                    myCommand.CommandText = sql
                End If
                myDataReader = myCommand.ExecuteReader()
                If myDataReader.IsClosed = False Then
                    myDataReader.Close()
                End If

                If Pemasukan = True Then
                    sql = "DELETE FROM `pemasukan` WHERE id = " & id
                Else
                    sql = "DELETE FROM `pengeluaran` WHERE id = " & id
                End If
                If myConn.State = ConnectionState.Closed Then
                    myConn.Open()
                End If
                If myCommand Is Nothing Then
                    myCommand = New MySqlCommand(sql, myConn)
                Else
                    myCommand.CommandText = sql
                End If
                myDataReader = myCommand.ExecuteReader()
                If myDataReader.IsClosed = False Then
                    myDataReader.Close()
                End If

                If Pemasukan = True Then
                    RefreshGridMasuk()
                Else
                    RefreshGridKeluar()
                End If
                RefreshGridTotal()
            End If
        End If
    End Sub

    Private Sub LogoutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LogoutToolStripMenuItem.Click
        Me.Hide()
        FormLogin.Show()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub
End Class