﻿Imports MySql.Data.MySqlClient

Public Class FormTambah

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If ComboTransaksi.Text <> "" And comboJual.Text <> "" And NumJumlah.Value <> 0 And NumNominal.Value <> 0 Then
            Dim sql As String
            If comboJual.Text = "Normal" Then
                If DateTimePicker1.Value.Year.Equals(Now.Year) And DateTimePicker1.Value.Month.Equals(Now.Month) And DateTimePicker1.Value.Day.Equals(Now.Day) Then
                    sql = "insert into `pemasukan`(`tanggal`, `transaksi`, `penjualan`, `jumlah`, `nominal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`, `keterangan`, `id_user`) VALUES (CURRENT_TIMESTAMP, '" & ComboTransaksi.Text & "', '" & comboJual.Text & "', " & NumJumlah.Value & ", " & NumNominal.Value & ", " & (NumNominal.Value * 40 / 100) & ", " & (NumNominal.Value * 30 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", '" & txtKeterangan.Text & "', '" & ModuleDB.pengguna & "')"
                Else
                    sql = "insert into `pemasukan`(`tanggal`, `transaksi`, `penjualan`, `jumlah`, `nominal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`, `keterangan`, `id_user`) VALUES ('" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & " " & DateTimePicker1.Value.Hour & ":" & DateTimePicker1.Value.Minute & ":" & DateTimePicker1.Value.Second & "', '" & ComboTransaksi.Text & "', '" & comboJual.Text & "', " & NumJumlah.Value & ", " & NumNominal.Value & ", " & (NumNominal.Value * 40 / 100) & ", " & (NumNominal.Value * 30 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", '" & txtKeterangan.Text & "', '" & ModuleDB.pengguna & "')"
                End If
            Else
                If DateTimePicker1.Value.Year.Equals(Now.Year) And DateTimePicker1.Value.Month.Equals(Now.Month) And DateTimePicker1.Value.Day.Equals(Now.Day) Then
                    sql = "insert into `pemasukan`(`tanggal`, `transaksi`, `penjualan`, `jumlah`, `nominal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`, `keterangan`, `id_user`) VALUES (CURRENT_TIMESTAMP, '" & ComboTransaksi.Text & "', '" & comboJual.Text & "', " & NumJumlah.Value & ", " & NumNominal.Value & ", " & (NumNominal.Value * 60 / 100) & ", " & (NumNominal.Value * 20 / 100) & ", " & (NumNominal.Value * 5 / 100) & ", " & (NumNominal.Value * 5 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", '" & txtKeterangan.Text & "', '" & ModuleDB.pengguna & "')"
                Else
                    sql = "insert into `pemasukan`(`tanggal`, `transaksi`, `penjualan`, `jumlah`, `nominal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`, `keterangan`, `id_user`) VALUES ('" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & " " & DateTimePicker1.Value.Hour & ":" & DateTimePicker1.Value.Minute & ":" & DateTimePicker1.Value.Second & "', '" & ComboTransaksi.Text & "', '" & comboJual.Text & "', " & NumJumlah.Value & ", " & NumNominal.Value & ", " & (NumNominal.Value * 60 / 100) & ", " & (NumNominal.Value * 20 / 100) & ", " & (NumNominal.Value * 5 / 100) & ", " & (NumNominal.Value * 5 / 100) & ", " & (NumNominal.Value * 10 / 100) & ", '" & txtKeterangan.Text & "', '" & ModuleDB.pengguna & "')"
                End If
            End If
            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridMasuk()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If

            sql = "INSERT INTO `logsaldo`(`tanggal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`) VALUES ((select `tanggal` from total where id = 1), (select `modal` from total where id = 1), (select `laba` from total where id = 1), (select `promosi` from total where id = 1), (select `asuransi` from total where id = 1), (select `perpuluhan` from total where id = 1))"

            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myDataReader = myCommand.ExecuteReader()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If

            If comboJual.Text = "Normal" Then
                sql = "UPDATE `total` SET `tanggal`=CURRENT_TIMESTAMP,`modal`=(modal+" & (NumNominal.Value * 40 / 100) & "),`laba`=(laba+" & (NumNominal.Value * 30 / 100) & "),`promosi`=(promosi+" & (NumNominal.Value * 10 / 100) & "),`asuransi`=(asuransi+" & (NumNominal.Value * 10 / 100) & "),`perpuluhan`=(perpuluhan+" & (NumNominal.Value * 10 / 100) & ") WHERE id = 1"
            Else
                sql = "UPDATE `total` SET `tanggal`=CURRENT_TIMESTAMP,`modal`=(modal+" & (NumNominal.Value * 60 / 100) & "),`laba`=(laba+" & (NumNominal.Value * 20 / 100) & "),`promosi`=(promosi+" & (NumNominal.Value * 5 / 100) & "),`asuransi`=(asuransi+" & (NumNominal.Value * 5 / 100) & "),`perpuluhan`=(perpuluhan+" & (NumNominal.Value * 10 / 100) & ") WHERE id = 1"
            End If

            'sql = "UPDATE `total` SET `modal`=(modal+" & (NumNominal.Value * 40 / 100) & "),`laba`=(laba+" & (NumNominal.Value * 30 / 100) & "),`promosi`=(promosi+" & (NumNominal.Value * 10 / 100) & "),`asuransi`=(asuransi+" & (NumNominal.Value * 10 / 100) & "),`perpuluhan`=(perpuluhan+" & (NumNominal.Value * 10 / 100) & ") WHERE id = 1"
            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridTotal()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If

            Me.Close()
        Else
            MsgBox("Mohon Lengkapi Semua Kolom", MsgBoxStyle.Information, "MAS Error")
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ComboTransaksi.Text = ""
        NumNominal.Value = 0
        NumJumlah.Value = 0
        txtKeterangan.Text = ""
    End Sub
End Class