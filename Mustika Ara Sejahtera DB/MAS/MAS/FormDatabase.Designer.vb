﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormDatabase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SistemToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TentangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblData = New System.Windows.Forms.Label()
        Me.PanelKeluar = New System.Windows.Forms.Panel()
        Me.PanelMasuk = New System.Windows.Forms.Panel()
        Me.DataGridViewMasuk = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DataGridViewKeluar = New System.Windows.Forms.DataGridView()
        Me.tanggalKel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jenis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nominalKel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.keteranganKel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.user = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdKel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.tanggalTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.modalTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.labaTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.promosiTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.asuransiTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.perpuluhanTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdTot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnTabel = New System.Windows.Forms.Button()
        Me.btnTambah = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnHapus = New System.Windows.Forms.Button()
        Me.btnSaldo = New System.Windows.Forms.Button()
        Me.tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.transaksi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.penjualan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jumlah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nominal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.modal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.laba = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.promosi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.asuransi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.perpuluhan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.keterangan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id_user = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MenuStrip1.SuspendLayout()
        Me.PanelKeluar.SuspendLayout()
        Me.PanelMasuk.SuspendLayout()
        CType(Me.DataGridViewMasuk, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridViewKeluar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SistemToolStripMenuItem, Me.TentangToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(943, 24)
        Me.MenuStrip1.TabIndex = 2
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SistemToolStripMenuItem
        '
        Me.SistemToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LogoutToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.SistemToolStripMenuItem.Name = "SistemToolStripMenuItem"
        Me.SistemToolStripMenuItem.Size = New System.Drawing.Size(54, 20)
        Me.SistemToolStripMenuItem.Text = "Sistem"
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'TentangToolStripMenuItem
        '
        Me.TentangToolStripMenuItem.Name = "TentangToolStripMenuItem"
        Me.TentangToolStripMenuItem.Size = New System.Drawing.Size(114, 20)
        Me.TentangToolStripMenuItem.Text = "Tentang Database"
        '
        'lblData
        '
        Me.lblData.AutoSize = True
        Me.lblData.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData.Location = New System.Drawing.Point(12, 35)
        Me.lblData.Name = "lblData"
        Me.lblData.Size = New System.Drawing.Size(148, 29)
        Me.lblData.TabIndex = 3
        Me.lblData.Text = "Pemasukan"
        '
        'PanelKeluar
        '
        Me.PanelKeluar.Controls.Add(Me.PanelMasuk)
        Me.PanelKeluar.Controls.Add(Me.DataGridViewKeluar)
        Me.PanelKeluar.Location = New System.Drawing.Point(-1, 78)
        Me.PanelKeluar.Name = "PanelKeluar"
        Me.PanelKeluar.Size = New System.Drawing.Size(942, 258)
        Me.PanelKeluar.TabIndex = 2
        '
        'PanelMasuk
        '
        Me.PanelMasuk.Controls.Add(Me.DataGridViewMasuk)
        Me.PanelMasuk.Controls.Add(Me.Panel1)
        Me.PanelMasuk.Location = New System.Drawing.Point(0, 0)
        Me.PanelMasuk.Name = "PanelMasuk"
        Me.PanelMasuk.Size = New System.Drawing.Size(942, 256)
        Me.PanelMasuk.TabIndex = 11
        '
        'DataGridViewMasuk
        '
        Me.DataGridViewMasuk.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridViewMasuk.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridViewMasuk.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.DataGridViewMasuk.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewMasuk.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridViewMasuk.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewMasuk.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.tanggal, Me.transaksi, Me.penjualan, Me.jumlah, Me.nominal, Me.modal, Me.laba, Me.promosi, Me.asuransi, Me.perpuluhan, Me.keterangan, Me.id_user, Me.id})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewMasuk.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewMasuk.Location = New System.Drawing.Point(3, 3)
        Me.DataGridViewMasuk.Name = "DataGridViewMasuk"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewMasuk.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewMasuk.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridViewMasuk.Size = New System.Drawing.Size(939, 250)
        Me.DataGridViewMasuk.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(4, 261)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(535, 57)
        Me.Panel1.TabIndex = 4
        '
        'DataGridViewKeluar
        '
        Me.DataGridViewKeluar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridViewKeluar.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewKeluar.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewKeluar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridViewKeluar.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.tanggalKel, Me.jenis, Me.nominalKel, Me.keteranganKel, Me.user, Me.IdKel})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewKeluar.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewKeluar.Location = New System.Drawing.Point(3, 3)
        Me.DataGridViewKeluar.Name = "DataGridViewKeluar"
        Me.DataGridViewKeluar.Size = New System.Drawing.Size(939, 250)
        Me.DataGridViewKeluar.TabIndex = 1
        '
        'tanggalKel
        '
        Me.tanggalKel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.tanggalKel.HeaderText = "Tanggal"
        Me.tanggalKel.Name = "tanggalKel"
        Me.tanggalKel.ReadOnly = True
        Me.tanggalKel.Width = 91
        '
        'jenis
        '
        Me.jenis.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.jenis.HeaderText = "Jenis"
        Me.jenis.Name = "jenis"
        Me.jenis.Width = 70
        '
        'nominalKel
        '
        Me.nominalKel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.nominalKel.HeaderText = "Nominal"
        Me.nominalKel.Name = "nominalKel"
        Me.nominalKel.Width = 90
        '
        'keteranganKel
        '
        Me.keteranganKel.HeaderText = "Keterangan"
        Me.keteranganKel.Name = "keteranganKel"
        '
        'user
        '
        Me.user.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.user.HeaderText = "User"
        Me.user.Name = "user"
        Me.user.Width = 66
        '
        'IdKel
        '
        Me.IdKel.HeaderText = "ID"
        Me.IdKel.Name = "IdKel"
        Me.IdKel.ReadOnly = True
        Me.IdKel.Visible = False
        '
        'Panel2
        '
        Me.Panel2.AutoSize = True
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Location = New System.Drawing.Point(16, 385)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(633, 50)
        Me.Panel2.TabIndex = 4
        '
        'DataGridView1
        '
        Me.DataGridView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.tanggalTot, Me.modalTot, Me.labaTot, Me.promosiTot, Me.asuransiTot, Me.perpuluhanTot, Me.IdTot})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridView1.Location = New System.Drawing.Point(2, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.DataGridView1.Size = New System.Drawing.Size(631, 46)
        Me.DataGridView1.TabIndex = 0
        '
        'tanggalTot
        '
        Me.tanggalTot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.tanggalTot.HeaderText = "Tanggal"
        Me.tanggalTot.Name = "tanggalTot"
        Me.tanggalTot.Width = 91
        '
        'modalTot
        '
        Me.modalTot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.modalTot.HeaderText = "Modal"
        Me.modalTot.Name = "modalTot"
        Me.modalTot.Width = 76
        '
        'labaTot
        '
        Me.labaTot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.labaTot.HeaderText = "Laba"
        Me.labaTot.Name = "labaTot"
        Me.labaTot.Width = 68
        '
        'promosiTot
        '
        Me.promosiTot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.promosiTot.HeaderText = "Promosi"
        Me.promosiTot.Name = "promosiTot"
        Me.promosiTot.Width = 90
        '
        'asuransiTot
        '
        Me.asuransiTot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.asuransiTot.HeaderText = "Asuransi"
        Me.asuransiTot.Name = "asuransiTot"
        Me.asuransiTot.Width = 93
        '
        'perpuluhanTot
        '
        Me.perpuluhanTot.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.perpuluhanTot.HeaderText = "Perpuluhan"
        Me.perpuluhanTot.Name = "perpuluhanTot"
        Me.perpuluhanTot.Width = 111
        '
        'IdTot
        '
        Me.IdTot.HeaderText = "ID"
        Me.IdTot.Name = "IdTot"
        Me.IdTot.ReadOnly = True
        Me.IdTot.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 350)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(148, 29)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Saldo Total"
        '
        'btnTabel
        '
        Me.btnTabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTabel.Location = New System.Drawing.Point(778, 31)
        Me.btnTabel.Name = "btnTabel"
        Me.btnTabel.Size = New System.Drawing.Size(153, 37)
        Me.btnTabel.TabIndex = 6
        Me.btnTabel.Text = "Tabel Pengeluaran"
        Me.btnTabel.UseVisualStyleBackColor = True
        '
        'btnTambah
        '
        Me.btnTambah.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTambah.Location = New System.Drawing.Point(663, 345)
        Me.btnTambah.Name = "btnTambah"
        Me.btnTambah.Size = New System.Drawing.Size(127, 37)
        Me.btnTambah.TabIndex = 7
        Me.btnTambah.Text = "Tambah Data"
        Me.btnTambah.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.Location = New System.Drawing.Point(798, 345)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(53, 37)
        Me.btnEdit.TabIndex = 8
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnHapus
        '
        Me.btnHapus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHapus.ForeColor = System.Drawing.Color.Red
        Me.btnHapus.Location = New System.Drawing.Point(858, 345)
        Me.btnHapus.Name = "btnHapus"
        Me.btnHapus.Size = New System.Drawing.Size(78, 37)
        Me.btnHapus.TabIndex = 9
        Me.btnHapus.Text = "Hapus"
        Me.btnHapus.UseVisualStyleBackColor = True
        '
        'btnSaldo
        '
        Me.btnSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaldo.Location = New System.Drawing.Point(663, 388)
        Me.btnSaldo.Name = "btnSaldo"
        Me.btnSaldo.Size = New System.Drawing.Size(127, 37)
        Me.btnSaldo.TabIndex = 10
        Me.btnSaldo.Text = "Log Saldo"
        Me.btnSaldo.UseVisualStyleBackColor = True
        '
        'tanggal
        '
        Me.tanggal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.tanggal.HeaderText = "Tanggal"
        Me.tanggal.Name = "tanggal"
        Me.tanggal.ReadOnly = True
        Me.tanggal.Width = 91
        '
        'transaksi
        '
        Me.transaksi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.transaksi.HeaderText = "Transaksi"
        Me.transaksi.Name = "transaksi"
        Me.transaksi.ReadOnly = True
        Me.transaksi.Width = 102
        '
        'penjualan
        '
        Me.penjualan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.penjualan.HeaderText = "Penjualan"
        Me.penjualan.Name = "penjualan"
        Me.penjualan.ReadOnly = True
        Me.penjualan.Width = 102
        '
        'jumlah
        '
        Me.jumlah.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        Me.jumlah.HeaderText = "Jumlah"
        Me.jumlah.Name = "jumlah"
        Me.jumlah.ReadOnly = True
        Me.jumlah.Width = 82
        '
        'nominal
        '
        Me.nominal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.nominal.HeaderText = "Nominal"
        Me.nominal.Name = "nominal"
        Me.nominal.ReadOnly = True
        Me.nominal.Width = 90
        '
        'modal
        '
        Me.modal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.modal.HeaderText = "Modal (40%)"
        Me.modal.Name = "modal"
        Me.modal.ReadOnly = True
        Me.modal.Width = 119
        '
        'laba
        '
        Me.laba.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.laba.HeaderText = "Laba (30%)"
        Me.laba.Name = "laba"
        Me.laba.ReadOnly = True
        Me.laba.Width = 111
        '
        'promosi
        '
        Me.promosi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.promosi.HeaderText = "Promosi (10%)"
        Me.promosi.Name = "promosi"
        Me.promosi.ReadOnly = True
        Me.promosi.Width = 133
        '
        'asuransi
        '
        Me.asuransi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.asuransi.HeaderText = "Asuransi (10%)"
        Me.asuransi.Name = "asuransi"
        Me.asuransi.ReadOnly = True
        Me.asuransi.Width = 136
        '
        'perpuluhan
        '
        Me.perpuluhan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.perpuluhan.HeaderText = "Perpuluhan (10%)"
        Me.perpuluhan.Name = "perpuluhan"
        Me.perpuluhan.ReadOnly = True
        Me.perpuluhan.Width = 154
        '
        'keterangan
        '
        Me.keterangan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.keterangan.DefaultCellStyle = DataGridViewCellStyle2
        Me.keterangan.HeaderText = "Keterangan"
        Me.keterangan.Name = "keterangan"
        Me.keterangan.ReadOnly = True
        Me.keterangan.Width = 112
        '
        'id_user
        '
        Me.id_user.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.id_user.HeaderText = "User"
        Me.id_user.Name = "id_user"
        Me.id_user.ReadOnly = True
        Me.id_user.Width = 66
        '
        'id
        '
        Me.id.HeaderText = "ID"
        Me.id.Name = "id"
        Me.id.ReadOnly = True
        Me.id.Visible = False
        '
        'FormDatabase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(943, 449)
        Me.Controls.Add(Me.btnSaldo)
        Me.Controls.Add(Me.btnHapus)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnTambah)
        Me.Controls.Add(Me.btnTabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.PanelKeluar)
        Me.Controls.Add(Me.lblData)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "FormDatabase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MAS Database Administrator"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.PanelKeluar.ResumeLayout(False)
        Me.PanelMasuk.ResumeLayout(False)
        CType(Me.DataGridViewMasuk, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridViewKeluar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents lblData As System.Windows.Forms.Label
    Friend WithEvents PanelKeluar As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewKeluar As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnTabel As System.Windows.Forms.Button
    Friend WithEvents btnTambah As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnHapus As System.Windows.Forms.Button
    Friend WithEvents SistemToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogoutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TentangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanelMasuk As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents tanggalKel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jenis As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nominalKel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents keteranganKel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents user As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdKel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tanggalTot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents modalTot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents labaTot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents promosiTot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents asuransiTot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents perpuluhanTot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdTot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewMasuk As System.Windows.Forms.DataGridView
    Friend WithEvents btnSaldo As System.Windows.Forms.Button
    Friend WithEvents tanggal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents transaksi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents penjualan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents jumlah As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nominal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents modal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents laba As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents promosi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents asuransi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents perpuluhan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents keterangan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents id_user As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents id As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
