﻿Imports MySql.Data.MySqlClient

Public Class FormTmbhKeluar

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ComboJenis.Text = ""
        NumNominal.Value = 0
        txtKeterangan.Text = ""
    End Sub

    Private Sub btnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSimpan.Click
        If ComboJenis.Text <> "" And NumNominal.Value <> 0 Then
            Dim sql As String
            If DateTimePicker1.Value.Year.Equals(Now.Year) And DateTimePicker1.Value.Month.Equals(Now.Month) And DateTimePicker1.Value.Day.Equals(Now.Day) Then
                sql = "insert into `pengeluaran`(`tanggal`, `jenis`, `nominal`, `keterangan`, `id_user`) VALUES (CURRENT_TIMESTAMP, '" & ComboJenis.Text & "', " & NumNominal.Value & ", '" & txtKeterangan.Text & "', '" & ModuleDB.pengguna & "')"
            Else
                sql = "insert into `pengeluaran`(`tanggal`, `jenis`, `nominal`, `keterangan`, `id_user`) VALUES ('" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & " " & DateTimePicker1.Value.Hour & ":" & DateTimePicker1.Value.Minute & ":" & DateTimePicker1.Value.Second & "', '" & ComboJenis.Text & "', " & NumNominal.Value & ", '" & txtKeterangan.Text & "', '" & ModuleDB.pengguna & "')"
            End If
            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridKeluar()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If

            sql = "INSERT INTO `logsaldo`(`tanggal`, `modal`, `laba`, `promosi`, `asuransi`, `perpuluhan`) VALUES ((select `tanggal` from total where id = 1), (select `modal` from total where id = 1), (select `laba` from total where id = 1), (select `promosi` from total where id = 1), (select `asuransi` from total where id = 1), (select `perpuluhan` from total where id = 1))"

            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myDataReader = myCommand.ExecuteReader()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If

            If (ComboJenis.Text = "Lain-lain") Then
                sql = "UPDATE `total` SET `laba`=(laba-" & NumNominal.Value & ") WHERE id = 1"
            Else
                sql = "UPDATE `total` SET `" & ComboJenis.Text.ToLower & "`=(" & ComboJenis.Text.ToLower & "-" & NumNominal.Value & ") WHERE id = 1"
            End If
            If myConn.State = ConnectionState.Closed Then
                myConn.Open()
            End If
            If myCommand Is Nothing Then
                myCommand = New MySqlCommand(sql, myConn)
            Else
                myCommand.CommandText = sql
            End If
            myCommand.ExecuteNonQuery()
            FormDatabase.RefreshGridTotal()
            If myDataReader.IsClosed = False Then
                myDataReader.Close()
            End If
            If myConn.State = ConnectionState.Open Then
                myConn.Close()
            End If

            Me.Close()
        Else
            MsgBox("Mohon Lengkapi Semua Kolom", MsgBoxStyle.Information, "MAS Error")
        End If
    End Sub
End Class