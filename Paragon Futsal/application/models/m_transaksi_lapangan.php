<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class m_transaksi_lapangan extends CI_Model{
	function __construct()
    {        
        parent::__construct();  
        $this->load->helper(array('html','url'));
    }

    function get_data_transaksi_lengkap(){
        $result = $this->db->query('SELECT n.*, c.nama FROM `nota_lapangan` n LEFT JOIN `customer` c ON n.id_customer = c.telp');        
        return $result->result_array();
    }

    function get_data_transaksi_home($enum, $start, $end){
        /*$result = $this->db->query('SELECT n.*, c.nama FROM `nota_lapangan` n LEFT JOIN `customer` c ON n.id_customer = c.telp WHERE n.lapangan+0 = '.$enum.' AND n.tgl_main BETWEEN "'.$start.'" AND "'.$end.'" AND n.status != "BATAL"');
        return $result->result_array();*/
        $result = $this->db->query('SELECT u.*, c.nama FROM
            (SELECT m.id_nota_lapangan as id,m.id_nota_dp as id_II,m.bayar_lunas as bayar,m.tgl_lunas as tanggal,m.diskon,m.override,m.total_bayar,m.id_customer,m.lapangan+0 as lapangan,m.tgl_main,m.jam_mulai,m.jam_selesai,m.bonus,m.keterangan,m.id_operator,m.status FROM `nota_lapangan_lunas` m 
            UNION ALL
            SELECT n.id_nota_lapangan as id,n.id_nota_lunas as id_II,n.bayar_dp as bayar,n.tgl_dp as tanggal,n.diskon,n.override,n.total_bayar,n.id_customer,n.lapangan+0 as lapangan,n.tgl_main,n.jam_mulai,n.jam_selesai,n.bonus,n.keterangan,n.id_operator,n.status FROM `nota_lapangan_dp` n) u LEFT JOIN `customer` c ON u.id_customer = c.telp
            WHERE u.lapangan = '.$enum.' AND u.tgl_main BETWEEN "'.$start.'" AND "'.$end.'" AND (u.status = "DP" OR u.status = "LUNAS")');        
        return $result->result_array();
    }

    function get_data_transaksi_rentang($enum, $start, $end){
        $result = $this->db->query('SELECT * FROM
            (SELECT m.id_nota_lapangan as id,m.id_nota_dp as id_II,m.bayar_lunas as bayar,m.tgl_lunas as tanggal,m.diskon,m.override,m.total_bayar,m.id_customer,m.lapangan,m.tgl_main,m.jam_mulai,m.jam_selesai,m.bonus,m.keterangan,m.id_operator,m.status,d.nama FROM `nota_lapangan_lunas` m LEFT JOIN `customer` d ON m.id_customer = d.telp
            UNION ALL
            SELECT n.id_nota_lapangan as id,n.id_nota_lunas as id_II,n.bayar_dp as bayar,n.tgl_dp as tanggal,n.diskon,n.override,n.total_bayar,n.id_customer,n.lapangan,n.tgl_main,n.jam_mulai,n.jam_selesai,n.bonus,n.keterangan,n.id_operator,n.status,c.nama FROM `nota_lapangan_dp` n LEFT JOIN `customer` c ON n.id_customer = c.telp) u
            WHERE u.lapangan = "'.$enum.'" AND u.tgl_main BETWEEN "'.$start.'" AND "'.$end.'"');
        return $result->result_array();
    }    

    function get_data_transaksi_today($enum){
        $result = $this->db->query('SELECT * FROM
            (SELECT m.id_nota_lapangan as id,m.id_nota_dp as id_II,m.bayar_lunas as bayar,m.tgl_lunas as tanggal,m.diskon,m.override,m.total_bayar,m.id_customer,m.lapangan,m.tgl_main,m.jam_mulai,m.jam_selesai,m.bonus,m.keterangan,m.id_operator,m.status,d.nama FROM `nota_lapangan_lunas` m LEFT JOIN `customer` d ON m.id_customer = d.telp
            UNION ALL
            SELECT n.id_nota_lapangan as id,n.id_nota_lunas as id_II,n.bayar_dp as bayar,n.tgl_dp as tanggal,n.diskon,n.override,n.total_bayar,n.id_customer,n.lapangan,n.tgl_main,n.jam_mulai,n.jam_selesai,n.bonus,n.keterangan,n.id_operator,n.status,c.nama FROM `nota_lapangan_dp` n LEFT JOIN `customer` c ON n.id_customer = c.telp) u
            WHERE u.lapangan = "'.$enum.'" AND u.tanggal = "'.date('Y-m-d').'"');
        return $result->result_array();
    }

    function search_nama_customer($nama = ""){
        $result = $this->db->query('SELECT * FROM `customer` WHERE nama LIKE '.$nama);
        return $result->result_array();
    }

    function simpan_customer($array = ""){
    	$result = $this->db->query('SELECT * FROM `customer` WHERE telp = "'.$array["telp"].'"');
    	if($result->num_rows() > 0){
            $result = $result->result_array()[0];
            $jml_jam = $result["jml_jam"] + $array["jml_jam"];
            /*echo "UPDATE `customer` SET `jml_jam`=".$jml_jam.",`jml_transaksi`=".($result["jml_transaksi"]+1).",`main_terakhir`='CURRENT_TIMESTAMP' WHERE `telp` = ".$array["telp"]; die;*/
            $result = $this->db->query("UPDATE `customer` SET `jml_jam`=".$jml_jam.",`jml_transaksi`=".($result["jml_transaksi"]+1).",`main_terakhir`='".$array["main_terakhir"]."' WHERE `telp` = ".$array["telp"]);
    	} else {
    		$this->db->insert('customer', $array);
    	}
    	return $result;
    }

    function simpan_edit_customer($array = ""){        
        $result = $this->db->query('SELECT * FROM `customer` WHERE telp = "'.$array["telp"].'"');
        if($result->num_rows() > 0){
            $result = $result->result_array()[0];
            $jml_jam = $result["jml_jam"] - $array["jml_jam"];
            /*echo "UPDATE `customer` SET `jml_jam`=".$jml_jam.",`jml_transaksi`=".($result["jml_transaksi"]+1).",`main_terakhir`='CURRENT_TIMESTAMP' WHERE `telp` = ".$array["telp"]; die;*/
            $result = $this->db->query("UPDATE `customer` SET `jml_jam`=".$jml_jam.",`main_terakhir`='".$array["main_terakhir"]."' WHERE `telp` = ".$array["telp"]);
        } else {
            $this->db->insert('customer', $array);
        }
        return $result;
    }

    function check_cashflow_today($tanggal = ""){
        $sql = "SELECT * FROM `cashflow`WHERE `tanggal`='".$tanggal."'";
        $result = $this->db->query($sql);
        if($result->num_rows() > 0){
            return TRUE;    
        } else {
            return FALSE;
        }
    }

    function create_id_nota($tipe = ''){
        $year=date('Y');
        $month=date('m');

        if($tipe == "DP")
            $sql = "SELECT SUBSTR(id_nota_lapangan, 10, 4)'id_nota' FROM `nota_lapangan_dp` 
              WHERE SUBSTR(id_nota_lapangan, 4, 4) = '$year' 
              AND SUBSTR(id_nota_lapangan, 8, 2) = '$month' 
              ORDER BY id_nota_lapangan 
              DESC LIMIT 1";
        else
            $sql = "SELECT SUBSTR(id_nota_lapangan, 13, 4)'id_nota' FROM `nota_lapangan_lunas` 
              WHERE SUBSTR(id_nota_lapangan, 7, 4) = '$year'
              AND SUBSTR(id_nota_lapangan, 11, 2) = '$month' 
              ORDER BY id_nota_lapangan 
              DESC LIMIT 1";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $id_nota = $query->row_array();
            $id_nota = intval($id_nota['id_nota']) + 1;

            if (strlen($id_nota) == '1') {
                $id_nota = '000' . $id_nota;
            } elseif (strlen($id_nota) == '2') {
                $id_nota = '00' . $id_nota;
            } elseif (strlen($id_nota) == '3') {
                $id_nota = '0' . $id_nota;
            } else {
                $id_nota = strlen($id_nota);
            }
            return $tipe . "-" . $year . $month . $id_nota;
        } else {
            return $tipe . "-" . $year . $month . '0001';
        }
    }

   	function simpan_transaksi($array = ""){
        /*$check_cashflow;
        if($array['status'] == "DP")            
            $check_cashflow = $this->check_cashflow_today($array['tgl_dp']);
        else
            $check_cashflow = $this->check_cashflow_today($array['tgl_lunas']);

        //Today's cashflow exist
        if($check_cashflow == TRUE){
            if($array['status'] == "DP")
                $sql="UPDATE `cashflow` SET `total_kredit`=`total_kredit`+".$array['bayar_dp'].",`saldo`=`saldo`+".$array['bayar_dp'];
            else
                $sql="UPDATE `cashflow` SET `total_kredit`=`total_kredit`+".$array['bayar_lunas'].",`saldo`=`saldo`+".$array['bayar_lunas'];                        
        }else{ //Today's cashflow not exist
            if($array['status'] == "DP")                
                $sql="INSERT INTO `cashflow`(`tanggal`, `total_kredit`, `total_debet`, `saldo`) SELECT '".$array['tgl_dp']."',".$array['bayar_dp'].",0,a.saldo+".$array['bayar_dp']." FROM `cashflow` a INNER JOIN (SELECT max(tanggal) as max_tgl FROM `cashflow`) b WHERE `tanggal`=b.max_tgl";
            else
                $sql="INSERT INTO `cashflow`(`tanggal`, `total_kredit`, `total_debet`, `saldo`) SELECT '".$array['tgl_lunas']."',".$array['bayar_lunas'].",0,a.saldo+".$array['bayar_lunas']." FROM `cashflow` a INNER JOIN (SELECT max(tanggal) as max_tgl FROM `cashflow`) b WHERE `tanggal`=b.max_tgl";                
        }
        $this->db->query($sql);*/
        if($array['status'] == "DP"){
            $array['id_nota_lapangan']=$this->create_id_nota("DP");
            $result = $this->db->insert('nota_lapangan_dp', $array);
        }               		   
        else{
            $array['id_nota_lapangan']=$this->create_id_nota("LUNAS");
            $result = $this->db->insert('nota_lapangan_lunas', $array);
        }            

        if($result){
            return TRUE;
        }else{
            return FALSE;
        }
   	}

    function cek_lapangan($lapangan){
        if($lapangan == 1)
            return "Rumput 1";
        elseif($lapangan == 2)
            return "Rumput 2";
        else
            return "AVA Court";
    }

    function simpan_edit_dp($array = ""){
        //print_r($array); die;
        $awal = "";
        $keterangan = "";
        $override = "";

        if($array['keterangan_old'] != "" || $array['keterangan_old'] != " ")
            $keterangan = $array['keterangan_old'];

        if($array['override'] != "")
            $override = $array['override'];

        for ($i=0; $i < 4; $i++) {
            if($keterangan == "")
                $awal = "";
            else
                $awal = " || ";

            if($i == 0 && $array['tgl_main_old'] != $array['tgl_main'])
                $keterangan .= $awal."Ganti Tgl Main: ".$array['tgl_main_old']." => ".$array['tgl_main'];
            if($i == 1 && $array['lapangan_old'] != $array['lapangan'])                
                $keterangan .= $awal."Ganti Lapangan: ".$this->cek_lapangan($array['lapangan_old'])." => ".$this->cek_lapangan($array['lapangan']);            
            if($i == 2 && ($array['jam_mulai_old'] != $array['jam_mulai'] || $array['jam_selesai_old'] != $array['jam_selesai']))
                $keterangan .= $awal."Ganti Jam Main: ".$array['jam_mulai_old'].":00-".$array['jam_selesai_old'].":00 => ".$array['jam_mulai'].":00-".$array['jam_selesai'].":00";
            if($i == 3 && $array['diskon_old'] != $array['diskon'])
                $keterangan .= $awal."Ganti Diskon: ".$array['diskon_old']." => ".$array['diskon'];
        }

        //echo $keterangan; die;        
            
        $sql="UPDATE `nota_lapangan_dp` SET `id_nota_lapangan`=concat(`id_nota_lapangan`,'-X'), `keterangan`='".$keterangan."', `status`='EDIT' WHERE `id_nota_lapangan`='".$array['id_nota_lapangan']."'";
        $result = $this->db->query($sql);

        $sql="INSERT INTO `nota_lapangan_dp`(`id_nota_lapangan`, `id_nota_lunas`, `bayar_dp`, `tgl_dp`, `diskon`, `override`, `total_bayar`, `id_customer`, `lapangan`, `tgl_main`, `jam_mulai`, `jam_selesai`, `bonus`, `keterangan`, `id_operator`, `status`) SELECT '".$array['id_nota_lapangan']."', `id_nota_lunas`, `bayar_dp`, `tgl_dp`, ".$array['diskon'].", '".$override."', ".$array['total_bayar'].", `id_customer`, '".$this->cek_lapangan($array['lapangan'])."', '".$array['tgl_main']."', ".$array['jam_mulai'].", ".$array['jam_selesai'].", `bonus`, '".$array['keterangan']."', `id_operator`, 'DP' FROM `nota_lapangan_dp` WHERE `id_nota_lapangan`='".$array['id_nota_lapangan']."-X'";
        $result = $this->db->query($sql);

        if($result){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function simpan_pelunasan_dp($array = ""){
        $id_nota_lunas = $this->create_id_nota('LUNAS');
        $sql = "UPDATE `nota_lapangan_dp` SET `id_nota_lunas`='".$id_nota_lunas."',`status`='PELUNASAN' WHERE `id_nota_lapangan`='".$array["id_nota_dp"]."'";        
        $result = $this->db->query($sql);
        
        if($result){
            $array['id_nota_lapangan']=$id_nota_lunas;
            $result = $this->db->insert('nota_lapangan_lunas', $array);
            if($result)
                return TRUE;
        } else {
            return FALSE;
        }
    }

    function check_data_transaksi($array = ""){
        $id = "";
        if($array["id_nota_lapangan"] != "")
            $id = "u.`id_nota_lapangan` != ".$array["id_nota_lapangan"]." AND ";
        $sql = "SELECT * FROM
                (SELECT `lapangan`+0 as lapangan, `tgl_main`, `jam_mulai`, `jam_selesai`, `status` from `nota_lapangan_dp`
                UNION ALL
                SELECT `lapangan`+0 as lapangan, `tgl_main`, `jam_mulai`, `jam_selesai`, `status` from `nota_lapangan_lunas`) u
                WHERE
                ".$id."u.`status` != 'EDIT' AND u.`status` != 'BATAL' AND u.`lapangan` = ".$array["lapangan"]." AND u.`tgl_main` = '".$array["tgl_main"]."' AND
                ((".$array["jam_mulai"]." < u.`jam_selesai`  AND ".$array["jam_selesai"]." > u.`jam_mulai`) OR
                (u.`jam_mulai` = ".$array["jam_mulai"]." AND u.`jam_selesai` = ".$array["jam_selesai"].")) LIMIT 1";
        $result = $this->db->query($sql);
        //return $result->result_array();
        if($result->num_rows() > 0){
            return $result->result_array();    
        } else {
            return FALSE;
        }
    }

    function get_enum_lapangan(){
        $result = $this->db->query("SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'nota_lapangan' AND COLUMN_NAME = 'lapangan'");
        $row = $result->result_array()[0];
        $enumList = explode(",", str_replace("'", "", substr($row['COLUMN_TYPE'], 5, (strlen($row['COLUMN_TYPE'])-6))));
        return $enumList;
    }

    function decimalHours($time)
    {
        $hms = explode(":", $time);
        return ($hms[0] + ($hms[1]/60));
    }

    function batal_transaksi($id, $id_customer, $jenis){        
        $sql = "UPDATE `nota_lapangan_".strtolower($jenis)."` SET `status` = 'BATAL' WHERE `id_nota_lapangan` = '".$id."'";
        $res = $this->db->query($sql);

        $sql = "SELECT `jam_mulai`, `jam_selesai` FROM `nota_lapangan_".strtolower($jenis)."` WHERE `id_nota_lapangan` = '".$id."'";
        //$sql = "SELECT `bayar_lunas`, `status`, `jam_mulai`, `jam_selesai` FROM `nota_lapangan` WHERE `id_nota_lapangan` = ".$id;

        $res = $this->db->query($sql);
        $res = $res->result_array();

        /*if($res[0]['status'] == "DP")
            $sql="UPDATE `cashflow` SET `total_kredit`=`total_kredit`-".$array['bayar_dp'].",`saldo`=`saldo`+".$array['bayar_dp'];
        else
            $sql="UPDATE `cashflow` SET `total_kredit`=`total_kredit`-".$array['bayar_lunas'].",`saldo`=`saldo`-".$array['bayar_lunas'];*/

        $res = $this->decimalHours($res[0]['jam_selesai'])-$this->decimalHours($res[0]['jam_mulai']);

        $sql = "UPDATE `customer` SET `jml_transaksi`=`jml_transaksi`-1, `jml_jam`=`jml_jam`-$res, `main_terakhir`=(SELECT max(tgl_main) FROM `nota_lapangan` WHERE `status`!='BATAL' AND `id_customer`='".$id_customer."') WHERE `telp` = ".$id_customer;
        $res = $this->db->query($sql);
        if ($res) {            
            return true;
        }else{
            return false;
        }
    }

    function get_list_customer(){
        $sql = "SELECT nama, telp as id_customer, `e-mail` as email, saldo, password FROM customer";
        $query = $this->db->query($sql);
        
        return $query->result();    
    }
}
?>
