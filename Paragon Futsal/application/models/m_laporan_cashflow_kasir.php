<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class m_laporan_cashflow_kasir extends CI_Model{

	var $table_pickup = 'cash_pickup';
	var $table_inject = 'cash_inject';
	var $table_deposit_bank = 'cash_deposit';
	var $table_user = 'user';


	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function get_data_saldo($tgl) {
		$result = $this->db->query("SELECT max(tanggal), total_kredit, total_debet, saldo FROM `cashflow` WHERE tanggal < '".$tgl."'");
		return $result->result_array();
	}

	function get_saldo_kemarin($tgl) {		
		$result = $this->db->query("SELECT * FROM `tutup_buku` WHERE SUBSTR(`tanggal`,1,7) = '".substr($tgl, 0, 7)."'");
		$month_ini = "";
		$month_end = "";
		$sql = "";

		if($result->num_rows() == 0){
			$month_ini = date('Y-m-d',strtotime('first day of last month', strtotime($tgl)));
			$month_end = date('Y-m-d',strtotime('last day of last month', strtotime($tgl)));

			$sql = "INSERT INTO `tutup_buku` (`tanggal`, `total_kredit`, `total_debet`, `saldo`)
				SELECT '".$tgl."', sum(u.kredit) as 'total_kredit', sum(u.debet) as 'total_debet', (s.saldo + sum(u.kredit) - sum(u.debet)) as 'saldo' FROM (
				SELECT tgl_dp as tanggal, id_nota_lapangan as keterangan, bayar_dp kredit, '' as debet, status FROM `nota_lapangan_dp`
					UNION ALL
                SELECT tgl_lunas as tanggal, id_nota_lapangan as keterangan, bayar_lunas kredit, '' as debet, status FROM `nota_lapangan_lunas`
					UNION ALL
				SELECT tgl_deposit as tanggal, concat('DEPOSIT-', id_nota_deposit) as keterangan, jml_deposit as kredit, '' as debet, 'DEPOSIT' as status FROM `nota_deposit`
					UNION ALL
				SELECT DATE(tgl_nota) as tanggal, concat('PARKIR-', id_nota) as keterangan, jml as kredit, '' as debet, 'PARKIR' as status FROM `nota_parkir`
					UNION ALL
				SELECT DATE(tgl_jual) as tanggal, concat('PENJUALAN-', id_nota) as keterangan, (harga_jual*jml) as kredit, '' as debet, 'PENJUALAN' as status FROM `nota_penjualan_barang`
					UNION ALL
				SELECT DATE(tanggal) as tanggal, concat('BAYAR-', nota) as keterangan, '' as kredit, grosstotal as debet, 'BAYAR' as status FROM `pembeliannota`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('PICKUP-', id_nota) as keterangan, '' as kredit, jml as debet, 'PICKUP' as status FROM `cash_pickup`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('INJECT-', id_inject) as keterangan, jml as kredit, '' as debet, 'INJECT' as status FROM `cash_inject`) u, (SELECT `tanggal`, saldo FROM `tutup_buku` WHERE SUBSTR(`tanggal`,1,7) = '".date('Y-m',strtotime('previous month', strtotime($tgl)))."') s
				WHERE u.tanggal BETWEEN '".$month_ini."' AND '".$month_end."' AND u.status != 'EDIT'";

        	$result = $this->db->query($sql);
        }

        $month_ini = date('Y-m',strtotime($tgl))."-01";        
        if($tgl === $month_ini){
        	$sql = "SELECT 0 as 'kredit', 0 as 'debet', `saldo` FROM `tutup_buku` WHERE SUBSTR(`tanggal`,1,7) = '".substr($tgl, 0, 7)."'";        	
        }
        else{
        	$month_end = date('Y-m-d',strtotime('previous day', strtotime($tgl)));        
        	$sql = "SELECT '".$month_end."' as 'tanggal', sum(u.kredit) as 'kredit', sum(u.debet) as 'debet', (s.saldo + sum(u.kredit) - sum(u.debet)) as 'saldo' FROM (
        		SELECT tgl_dp as tanggal, id_nota_lapangan as keterangan, bayar_dp kredit, '' as debet, status FROM `nota_lapangan_dp`
					UNION ALL
                SELECT tgl_lunas as tanggal, id_nota_lapangan as keterangan, bayar_lunas kredit, '' as debet, status FROM `nota_lapangan_lunas`
					UNION ALL
				SELECT tgl_deposit as tanggal, concat('DEPOSIT-', id_nota_deposit) as keterangan, jml_deposit as kredit, '' as debet, 'DEPOSIT' as status FROM `nota_deposit`
					UNION ALL
				SELECT DATE(tgl_nota) as tanggal, concat('PARKIR-', id_nota) as keterangan, jml as kredit, '' as debet, 'PARKIR' as status FROM `nota_parkir`
					UNION ALL
				SELECT DATE(tgl_jual) as tanggal, concat('PENJUALAN-', id_nota) as keterangan, (harga_jual*jml) as kredit, '' as debet, 'PENJUALAN' as status FROM `nota_penjualan_barang`
					UNION ALL
				SELECT DATE(tanggal) as tanggal, concat('BAYAR-', nota) as keterangan, '' as kredit, grosstotal as debet, 'BAYAR' as status FROM `pembeliannota`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('PICKUP-', id_nota) as keterangan, '' as kredit, jml as debet, 'PICKUP' as status FROM `cash_pickup`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('INJECT-', id_inject) as keterangan, jml as kredit, '' as debet, 'INJECT' as status FROM `cash_inject`) u, (SELECT `tanggal`, saldo FROM `tutup_buku` WHERE SUBSTR(`tanggal`,1,7) = '".date('Y-m',strtotime($tgl))."') s
				WHERE u.tanggal BETWEEN '".$month_ini."' AND '".$month_end."' AND u.status != 'EDIT'";	        
        }
        $result = $this->db->query($sql);       	
        return $result->result_array();
	}

	function get_data_today($tgl) {
		$kemarin = $this->get_saldo_kemarin($tgl);		
		$sekarang = $this->db->query("SELECT sum(u.kredit) as 'kredit', sum(u.debet) as 'debet', (sum(u.kredit) - sum(u.debet)) as 'saldo' FROM (
        		SELECT tgl_dp as tanggal, id_nota_lapangan as keterangan, bayar_dp kredit, '' as debet, status FROM `nota_lapangan_dp`
					UNION ALL
                SELECT tgl_lunas as tanggal, id_nota_lapangan as keterangan, bayar_lunas kredit, '' as debet, status FROM `nota_lapangan_lunas`
					UNION ALL
				SELECT tgl_deposit as tanggal, concat('DEPOSIT-', id_nota_deposit) as keterangan, jml_deposit as kredit, '' as debet, 'DEPOSIT' as status FROM `nota_deposit`
					UNION ALL
				SELECT DATE(tgl_nota) as tanggal, concat('PARKIR-', id_nota) as keterangan, jml as kredit, '' as debet, 'PARKIR' as status FROM `nota_parkir`
					UNION ALL
				SELECT DATE(tgl_jual) as tanggal, concat('PENJUALAN-', id_nota) as keterangan, (harga_jual*jml) as kredit, '' as debet, 'PENJUALAN' as status FROM `nota_penjualan_barang`
					UNION ALL
				SELECT DATE(tanggal) as tanggal, concat('BAYAR-', nota) as keterangan, '' as kredit, grosstotal as debet, 'BAYAR' as status FROM `pembeliannota`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('PICKUP-', id_nota) as keterangan, '' as kredit, jml as debet, 'PICKUP' as status FROM `cash_pickup`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('INJECT-', id_inject) as keterangan, jml as kredit, '' as debet, 'INJECT' as status FROM `cash_inject`) u
				WHERE u.tanggal = '".$tgl."' AND u.status != 'EDIT'");
		
		$transaksi = $this->db->query("SELECT * FROM
				(SELECT tgl_dp as tanggal, id_nota_lapangan as keterangan, bayar_dp kredit, '' as debet, status FROM `nota_lapangan_dp`
					UNION ALL
                SELECT tgl_lunas as tanggal, id_nota_lapangan as keterangan, bayar_lunas kredit, '' as debet, status FROM `nota_lapangan_lunas`
					UNION ALL
				SELECT tgl_deposit as tanggal, concat('DEPOSIT-', id_nota_deposit) as keterangan, jml_deposit as kredit, '' as debet, 'DEPOSIT' as status FROM `nota_deposit`
					UNION ALL
				SELECT DATE(tgl_nota) as tanggal, concat('PARKIR-', id_nota) as keterangan, jml as kredit, '' as debet, 'PARKIR' as status FROM `nota_parkir`
					UNION ALL
				SELECT DATE(tgl_jual) as tanggal, concat('PENJUALAN-', id_nota) as keterangan, (harga_jual*jml) as kredit, '' as debet, 'PENJUALAN' as status FROM `nota_penjualan_barang`
					UNION ALL
				SELECT DATE(tanggal) as tanggal, concat('BAYAR-', nota) as keterangan, '' as kredit, grosstotal as debet, 'BAYAR' as status FROM `pembeliannota`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('PICKUP-', id_nota) as keterangan, '' as kredit, jml as debet, 'PICKUP' as status FROM `cash_pickup`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('INJECT-', id_inject) as keterangan, jml as kredit, '' as debet, 'INJECT' as status FROM `cash_inject`) u
				WHERE u.tanggal = '".$tgl."' AND u.status != 'EDIT'");
		//echo $tgl." || "; print_r($transaksi->result_array()); die;
		$result['tanggal'] = $tgl;
		$result['transaksi'] = $transaksi->result_array();
		$result['saldo_kemarin'] = $kemarin;
		$result['saldo_sekarang'] = $sekarang->result_array();

    	return $result;
	}

	function get_data_all($start, $end) {
		$result = $this->db->query("SELECT * FROM
				(SELECT IF(status='DP', tgl_dp, tgl_lunas) as tanggal, concat(status, '-', id_nota_lapangan) as keterangan, IF(status='DP', bayar_dp, bayar_lunas) as kredit, '' as debet, status FROM `nota_lapangan`
					UNION ALL
				SELECT tgl_deposit as tanggal, concat('DEPOSIT-', id_nota_deposit) as keterangan, jml_deposit as kredit, '' as debet, 'DEPOSIT' as status FROM `nota_deposit`
					UNION ALL
				SELECT DATE(tgl_nota) as tanggal, concat('PARKIR-', id_nota) as keterangan, jml as kredit, '' as debet, 'PARKIR' as status FROM `nota_parkir`
					UNION ALL
				SELECT DATE(tgl_jual) as tanggal, concat('PENJUALAN-', id_nota) as keterangan, (harga_jual*jml) as kredit, '' as debet, 'PENJUALAN' as status FROM `nota_penjualan_barang`
					UNION ALL
				SELECT DATE(tanggal) as tanggal, concat('BAYAR-', nota) as keterangan, '' as kredit, grosstotal as debet, 'BAYAR' as status FROM `pembeliannota`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('PICKUP-', id_nota) as keterangan, '' as kredit, jml as debet, 'PICKUP' as status FROM `cash_pickup`
					UNION ALL
				SELECT DATE(tgl_waktu) as tanggal, concat('INJECT-', id_inject) as keterangan, jml as kredit, '' as debet, 'INJECT' as status FROM `cash_inject`) u
				WHERE u.tanggal BETWEEN '".$start."' AND '".$end."' AND u.status != 'EDIT'");    	
    	return $result->result_array();
	}
}