<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporancashflowkasir extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('m_laporan_cashflow_kasir','laporan_cashflow_kasir');
	}

	function index() {
		if (!isset($this->session->userdata('user_data')['username'])) {
			redirect('login');
		}		
		
		$this->load->helper('url');
		$tgl = date('Y-m-d');
		if(!empty($this->session->userdata('data_cashflow'))){
			$tgl = $this->session->userdata('data_cashflow');
			$this->session->unset_userdata('data_cashflow');
		}

    	$data=$this->laporan_cashflow_kasir->get_data_today($tgl);        
    	
		$this->load->view('laporancashflowkasir', $data);		
	}

	function get_tgl() {
		if (!isset($this->session->userdata('user_data')['username'])) {
			redirect('login');
		}
		$this->load->helper('url');		
                
        if(!empty($this->input->post('tgl_cashflow'))){
        	//$tgl=$this->input->post('tgl_cashflow');        	
        	$this->session->set_userdata('data_cashflow', $this->input->post('tgl_cashflow'));
        	redirect('laporancashflowkasir');
        }
        
        //$data=$this->laporan_cashflow_kasir->get_data_today($tgl);
        //$data=$this->laporan_cashflow_kasir->get_data_all(date('Y-m')."-01", date('Y-m-t', strtotime(date('Y-m-d'))));
        //$_SESSION['flash_data']=$data;
        //$_SESSION['flash_tgl']=$tgl_real;
        // /print_r($_SESSION); die;
		//redirect('laporancashflowkasir');
	}
}