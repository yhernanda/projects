<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tlapangan extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_transaksi_lapangan');
		$this->load->model('m_override_pengguna', 'override_pengguna');
	}

	public function index()
	{
		if (!isset($this->session->userdata('user_data')['username'])) {
			redirect('login');
		}
		
		$array['enum_lapangan'] = $this->m_transaksi_lapangan->get_enum_lapangan();		
		$this->session->unset_userdata('override');
		$this->load->view('tlapangan', $array);
	}

	public function ajax_override_pengguna()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        // early validation
        if($username == '') {
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'required';
            $data['status'] = FALSE;
        }

        if($password == '') {
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'required';
            $data['status'] = FALSE;
        }


        if($data['status'] === FALSE) {
            echo json_encode($data);
        }
        else {
            echo json_encode($this->override_pengguna->user_check($username, $password));
        }
    }

	public function get_data_transaksi_rentang($lapangan, $start, $end){		
		if (!isset($this->session->userdata('user_data')['username'])) {
			redirect('login');
		}
		if($lapangan > 2)
			$lapangan = "AVA Court";
		elseif($lapangan > 1)
			$lapangan = "Rumput 2";
		else
			$lapangan = "Rumput 1";

		if($start == "-" && $end == "-")
			$array = $this->m_transaksi_lapangan->get_data_transaksi_today($lapangan);
		else
			$array = $this->m_transaksi_lapangan->get_data_transaksi_rentang($lapangan, $start, $end);
		$data['data'] = array();
		$count=0;
		foreach ($array as $row) {
			$act="-";
			$status=$row['status'];
			if($status == "DP" || $status == "LUNAS"){				
                $act = "<a href='".base_url()."tlapangan/batal_transaksi/".$row['id']."/".$row['id_customer']."/".$status."' style='cursor:pointer;' class='batal_transaksi btn btn-danger btn-xs' id='tool' data-toggle='tooltip' data-placement='top' title='Batalkan Transaksi'><i class='fa fa-times'></i></a>";
            }

            if($row['status'] == "DP"){
            	$status = "<a class='pelunasan_dp' data-toggle='modal' data-target='#edit_transaksi' style='cursor:pointer;'>".$row['status']."</a>";
            }

			$e = array(
					"id" => $row['id'],
					"id_II" => $row['id_II'],
					"bayar" => $row['bayar'],
					"tanggal" => $row['tanggal'],					
					"diskon" => $row['diskon'],					
					"total_bayar" => $row['total_bayar'],
					"lapangan" => $row['lapangan'],					
					"tgl_main" => $row['tgl_main'],
					"jam_mulai" => $row['jam_mulai'],
					"jam_selesai" => $row['jam_selesai'],
					"nama" => $row['nama'],
					"telp" => $row['id_customer'],
					"bonus" => $row['bonus'],
					"keterangan" => $row['keterangan'],
					"override" => $row['override'],
					"id_operator" => $row['id_operator'],					
					"status" => $status,
					"act" => $act);						
			$data['data'][$count] = $e;
			$count++;
		}
		header('Content-Type: application/json');
		echo json_encode($data);
		/*print_r($array); die;
		$array['nota'] = $array;
		$array['enum_lapangan'] = $this->m_transaksi_lapangan->get_enum_lapangan();
		$this->session->unset_userdata('override');
		$this->load->view('tlapangan', $array);*/
	}

	public function decimalHours($time)
	{
	    $hms = explode(":", $time);
	    return ($hms[0] + ($hms[1]/60));
	}

	public function get_master_customer(){
		$result = $this->m_transaksi_lapangan->get_list_customer();
		header('Content-Type: application/json');
    	echo json_encode($result);
	}

	public function search_nama_customer($nama){
		$result = $this->m_transaksi_lapangan->search_nama_customer($nama);
        $tamp = array();
        foreach ($result as $key => $value) {
            array_push($tamp, $value["nama"]);
        }
        $temp = array(
            "query" => "Unit",
            "suggestions" => $tamp);
        return json_encode($temp);
	}

	//Ancur
	public function simpan_transaksi_lunas(){
		if ($this->session->userdata('user_data') == "") {
			redirect('home');
		}	

		$jml_jam = $this->input->post('jam_selesai') - $this->input->post('jam_mulai');

		$array_customer = array(
			"nama" => $this->input->post('nama'),
			"telp" => $this->input->post('telp'),
			"jml_jam" => $jml_jam,
			"jml_transaksi" => 1,
			"main_terakhir" => $this->input->post('tgl_main'));

		$query = $this->m_transaksi_lapangan->simpan_customer($array_customer);

		$diskon = 0;
		$override = "";
		$total_bayar = $this->input->post('total_bayar');
		$bayar_lunas = $this->input->post('bayar_lunas');

		if(null != $this->input->post('override') || "" != $this->input->post('override')){
			$override = substr($this->input->post('override_view'), 4);
			$diskon = $this->input->post('diskon');
		} else{
			if($this->input->post('diskon') != 0){
				$total_bayar += $this->input->post('diskon');
				$bayar_lunas += $this->input->post('diskon');
			}
			$diskon = 0;
		}

		$array_nota = array(
			"bayar_lunas" => $this->input->post('bayar_lunas'),
			"tgl_lunas" => $this->input->post('tgl_lunas'),
			"lapangan" => $this->input->post('lapangan'),
			"tgl_main" => $this->input->post('tgl_main'),
			"jam_mulai" => $this->input->post('jam_mulai'),
			"jam_selesai" => $this->input->post('jam_selesai'),
			"total_bayar" => $total_bayar,
			"id_customer" => $this->input->post('telp'),
			"diskon" => $diskon,
			"bonus" => $this->input->post('bonus'),
			"keterangan" => $this->input->post('keterangan'),
			"override" => $override,
			"id_operator" => $this->session->userdata('user_data')['user_id']." (".$this->session->userdata('user_data')['name'].")",
			"status" => "LUNAS"
		);		
	
		if($query){
			$query = $this->m_transaksi_lapangan->simpan_transaksi($array_nota);
		}else{
			redirect('tlapangan');
		}
		redirect('tlapangan');
	}

	public function simpan_transaksi_dp(){
		//print_r($this->input->post()); die;
		if ($this->session->userdata('user_data') == "") {
			redirect('home');
		}					

		//$jml_jam = $this->decimalHours($this->input->post('jam_selesai')) - $this->decimalHours($this->input->post('jam_mulai'));
		$jml_jam = $this->input->post('jam_selesai') - $this->input->post('jam_mulai');

		$array_customer = array(
			"nama" => $this->input->post('nama'),
			"telp" => $this->input->post('telp'),
			"jml_jam" => $jml_jam,
			"jml_transaksi" => 1,
			"main_terakhir" => $this->input->post('tgl_main'));

		$query = $this->m_transaksi_lapangan->simpan_customer($array_customer);

		$diskon = 0;
		$override = "";
		$total_bayar = $this->input->post('total_bayar');
		
		if(null != $this->input->post('override') || "" != $this->input->post('override')){
			$override = substr($this->input->post('override_view'), 4);
			$diskon = $this->input->post('diskon');
		} else{
			if($this->input->post('diskon') != 0){
				$total_bayar += $this->input->post('diskon');				
			}
			$diskon = 0;
		}

		$array_nota = array(
			"bayar_dp" => $this->input->post('bayar_dp'),
			"tgl_dp" => $this->input->post('tgl_dp'),
			"lapangan" => $this->input->post('lapangan'),
			"tgl_main" => $this->input->post('tgl_main'),
			"jam_mulai" => $this->input->post('jam_mulai'),
			"jam_selesai" => $this->input->post('jam_selesai'),
			"total_bayar" => $total_bayar,
			"id_customer" => $this->input->post('telp'),
			"diskon" => $diskon,
			"bonus" => $this->input->post('bonus'),
			"keterangan" => $this->input->post('keterangan'),
			"override" => $override,
			"id_operator" => $this->session->userdata('user_data')['user_id']." (".$this->session->userdata('user_data')['name'].")",
			"status" => "DP"
		);

		//print_r($array_nota); die;
	
		if($query){
			$query = $this->m_transaksi_lapangan->simpan_transaksi($array_nota);
		}else{
			redirect('tlapangan');
		}
		redirect('tlapangan');
	}

	public function simpan_pelunasan_dp(){
		if ($this->session->userdata('user_data') == "") {
			redirect('home');
		}

		//print_r($this->input->post()); die;

		//$jml_jam = $this->decimalHours($this->input->post('jam_selesai')) - $this->decimalHours($this->input->post('jam_mulai'));
		//print_r($this->input->post()); die;
		$diskon = 0;
		$override = "";
		$total_bayar = $this->input->post('total_bayar');
		$bayar_lunas = $this->input->post('bayar_lunas');

		if(null != $this->input->post('override') || "" != $this->input->post('override')){
			$override = substr($this->input->post('override_view'), 4);
			$diskon = $this->input->post('diskon');
		} else{
			if($this->input->post('diskon') > $this->input->post('diskon_asli')){
				//$total_bayar += $this->input->post('diskon')-$this->input->post('diskon_asli');
				$bayar_lunas += $this->input->post('diskon')-$this->input->post('diskon_asli');
				$diskon = 0;
			} elseif ($this->input->post('diskon') == $this->input->post('diskon_asli')) {
				$diskon = $this->input->post('diskon');
			}			
		}

		$array_nota = array(
			"id_nota_dp" => $this->input->post('id_nota_dp'),
			"bayar_lunas" => $this->input->post('bayar_lunas'),
			"tgl_lunas" => $this->input->post('tgl_lunas'),
			"lapangan" => $this->input->post('lapangan'),
			"tgl_main" => $this->input->post('tgl_main'),
			"jam_mulai" => $this->input->post('jam_mulai'),
			"jam_selesai" => $this->input->post('jam_selesai'),
			"total_bayar" => $total_bayar,
			"id_customer" => $this->input->post('telp'),
			"diskon" => $diskon,
			"bonus" => $this->input->post('bonus'),
			"keterangan" => $this->input->post('keterangan'),
			"override" => $override,
			"id_operator" => $this->session->userdata('user_data')['user_id']." (".$this->session->userdata('user_data')['name'].")",
			"status" => "LUNAS"
		);	

		//print_r($array_nota); die;

		$query = $this->m_transaksi_lapangan->simpan_pelunasan_dp($array_nota);
		
		redirect('tlapangan');
	}

	public function simpan_edit_dp(){
		if ($this->session->userdata('user_data') == "") {
			redirect('home');
		}		

		$jml_jam_old = $this->input->post('jam_selesai_old') - $this->input->post('jam_mulai_old');
		$jml_jam = $this->input->post('jam_selesai') - $this->input->post('jam_mulai');
		$jml_jam = $jml_jam_old - $jml_jam;

		$array_customer = array(
			"nama" => $this->input->post('nama'),
			"telp" => $this->input->post('telp'),
			"jml_jam" => $jml_jam,
			"main_terakhir" => $this->input->post('tgl_main'));

		//$query = $this->m_transaksi_lapangan->simpan_edit_customer($array_customer);

		$diskon = "";
		$override = "";
		$keterangan_old = "";
		
		if(null != $this->input->post('override') || "" != $this->input->post('override')){
			$override = substr($this->input->post('override_view'), 4);			
		}
		if(null != $this->input->post('keterangan_old')){
			$keterangan_old = $this->input->post('keterangan_old');
		}

		$array_nota = array(
			"id_nota_lapangan" => $this->input->post('id_nota_lapangan'),
			"bayar_dp" => $this->input->post('bayar_dp'),
			"tgl_dp" => $this->input->post('tgl_dp'),
			"lapangan_old" => $this->input->post('lapangan_old'),
			"lapangan" => $this->input->post('lapangan'),
			"tgl_main_old" => $this->input->post('tgl_main_old'),
			"jam_mulai_old" => $this->input->post('jam_mulai_old'),
			"jam_selesai_old" => $this->input->post('jam_selesai_old'),
			"tgl_main" => $this->input->post('tgl_main'),
			"jam_mulai" => $this->input->post('jam_mulai'),
			"jam_selesai" => $this->input->post('jam_selesai'),
			"total_bayar" => $this->input->post('total_bayar'),
			"id_customer" => $this->input->post('telp'),
			"diskon_old" => $this->input->post('diskon_old'),
			"diskon" => $this->input->post('diskon'),
			"bonus" => $this->input->post('bonus'),
			"keterangan" => $this->input->post('keterangan'),
			"keterangan_old" => $keterangan_old,
			"override" => $override,
			"id_operator" => $this->session->userdata('user_data')['user_id'],
			"status" => "DP"
		);

		//print_r($array_nota); die;
		$this->m_transaksi_lapangan->simpan_edit_dp($array_nota);

		if($query){
			$query = $this->m_transaksi_lapangan->simpan_edit_dp($array_nota);
		}else{

		}
		redirect('tlapangan');
	}

	public function batal_transaksi($id, $id_customer, $jenis){
		if ($this->session->userdata('user_data') == "") {
			redirect('home');
		}
		
		$result = $this->m_transaksi_lapangan->batal_transaksi($id, $id_customer, $jenis);
		if($result == true){
			redirect('tlapangan');
		}else{
			return false;
		}
	}
}
?>

