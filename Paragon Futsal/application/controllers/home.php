<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event {}
class Home extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('m_transaksi_lapangan');
		$this->load->model('m_override_pengguna', 'override_pengguna');
	}

	public function index()
	{
		if (isset($this->session->userdata('user_data')['username'])) {
			$enum['enum_lapangan'] = $this->m_transaksi_lapangan->get_enum_lapangan();
			$this->load->view('home', $enum);
		}else{
			redirect('login');
		}
	}

	public function ajax_override_pengguna()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        // early validation
        if($username == '') {
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'required';
            $data['status'] = FALSE;
        }

        if($password == '') {
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'required';
            $data['status'] = FALSE;
        }


        if($data['status'] === FALSE) {
            echo json_encode($data);
        }
        else {
            echo json_encode($this->override_pengguna->user_check($username, $password));
        }
    }

	public function get_data_transaksi($enum="", $start, $end)
	{
		$array = $this->m_transaksi_lapangan->get_data_transaksi_home($enum, $start, $end);			
		$events = array();
		foreach ($array as $row) {
			$jam_mulai=$row['jam_mulai'].":00:00";
			$jam_selesai=$row['jam_selesai'].":00:00";
			if($row['jam_mulai'] < 10)
				$jam_mulai = "0".$row['jam_mulai'].":00:00";
			if($row['jam_selesai'] < 10)
				$jam_selesai = "0".$row['jam_selesai'].":00:00";			
               
			$e = new Event();
			if($row['status'] == "DP"){				
				$e->backColor = "#0868ac";
            	$e->barColor = "#084081";
            	$e->borderColor = "#084081";
            }
			$e->id = $row['id'];
			$e->text = $row['nama'];
			$e->start = $row['tgl_main'].'T'.$jam_mulai;
			$e->end = $row['tgl_main'].'T'.$jam_selesai;
			$e->bubbleHtml = $row['nama'];
			$e->tag = array(
					"id" => $row['id'],
					"id_II" => $row['id_II'],
					"bayar" => $row['bayar'],
					"tanggal" => $row['tanggal'],					
					"diskon" => $row['diskon'],					
					"total_bayar" => $row['total_bayar'],
					"lapangan" => $row['lapangan'],					
					"tgl_main" => $row['tgl_main'],
					"jam_mulai" => $row['jam_mulai'],
					"jam_selesai" => $row['jam_selesai'],
					"nama" => $row['nama'],
					"telp" => $row['id_customer'],
					"bonus" => $row['bonus'],
					"keterangan" => $row['keterangan'],
					"override" => $row['override'],
					"id_operator" => $row['id_operator'],					
					"status" => $row['status']);
			$events[] = $e;
		}
		header('Content-Type: application/json');
		echo json_encode($events);
	}

	public function check_data_transaksi()
	{
		$id_nota = "";
		if(isset($_POST['id_nota_lapangan']))
			$id_nota = $_POST['id_nota_lapangan'];
		$array = array(
				'tgl_main' => date('Y-m-d', strtotime($_POST['tgl_main'])),
				'jam_mulai' => $_POST['jam_mulai'],
				'jam_selesai' => $_POST['jam_selesai'],
				'lapangan' => $_POST['lapangan'],
				'id_nota_lapangan' => $id_nota);
		$result = $this->m_transaksi_lapangan->check_data_transaksi($array);

		if($result != FALSE){
			$result[0]['status'] = "FOUND";
		} else {
			$result = array("0" => array('status' => "CLEAR"));	
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}	
}