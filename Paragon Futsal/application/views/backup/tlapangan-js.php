<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>


<script type="text/javascript">
	$(document).ready(function (){		
		var override_id;
		var mode_temp;
		var mode_id;
		var now = new Date();
		var table;
		populate_table(false);

		//SECTION AUTO COMPLETE
		var idcustomer = [];
	    var nama = [];
	    var email = [];
	    var saldo = [];
	    $('.kode').focus(function(event){
	        var $input = $(event.target);	        
	        if(idcustomer.length == 0){
	            $.ajax({
	                type:'POST',
	                url:'<?php echo base_url();?>tlapangan/get_master_customer',
	                success:function(data){
	                    for(var i = 0; i<data.length; i++){
	                        idcustomer.push(data[i]['id_customer']);
	                        nama.push(data[i]['nama']);
	                        email.push(data[i]['email']);	                        
	                    }
	                }
	            });
	        }

	        $input.typeahead({source:idcustomer, 
	        autoSelect: true}); 

	        $input.change(function() {        	
	            var current = $input.typeahead("getActive");
	            var index = idcustomer.indexOf(current);

	            $(event.target).closest('form').find('input.nama_customer').val(nama[index]);
	            //$('#email_customer').val(email[index]);	            
	        });

	    });


	    //SECTION HITUNG
		//Hitung Total Bayar dari Jam Main
		function calculate_total(){			    		
	        var day = $('#tgl_main'+mode_id).val();	                	        
	        var lapangan = $('#lapangan'+mode_id).val();
	        var jam_mulai = $('#jam_mulai'+mode_id).val();
	        var jam_selesai = $('#jam_selesai'+mode_id).val();
	        var mode_mulai;
	        var mode_selesai;
	        var total_bayar = 0;
	        day = day.split("-");	        
	        day = new Date(day[2], day[1]-1, day[0]);	        
	        day = day.getDay();
	        
	        //check_jadwal_main();
	        //alert("lap:"+lapangan+" day:"+day+" jm:"+jam_mulai);

	        if(lapangan === 3 || day === 0 || day === 6){
	            if(jam_mulai < 16)
	                mode_mulai=1;
	            else
	                mode_mulai=2;

	            if(jam_selesai <= 16)
	                mode_selesai=1;
	            else
	                mode_selesai=2;

	            if(mode_mulai === mode_selesai){
	                total_bayar = jam_selesai - jam_mulai;
	                if(mode_mulai === 1 && lapangan === 3)
	                    total_bayar = total_bayar * 100000;
	                else if(mode_mulai === 1 && lapangan < 3)
	                    total_bayar = total_bayar * 90000;
	                else
	                    total_bayar = total_bayar * 135000;
	            } else {
	                total_bayar = (jam_selesai - 16) * 135000;
	                if(lapangan < 3)
	                    total_bayar += (16 - jam_mulai) * 90000;
	                else
	                    total_bayar += (16 - jam_mulai) * 100000;
	            }
	        }else{
	            if(jam_mulai < 12)
	                mode_mulai=1;
	            else if(jam_mulai < 16)
	                mode_mulai=2;
	            else
	                mode_mulai=3;

	            if(jam_selesai <= 12)
	                mode_selesai=1;
	            else if(jam_selesai <= 16)
	                mode_selesai=2;
	            else
	                mode_selesai=3;

	            if(mode_mulai === mode_selesai){
	                total_bayar = jam_selesai - jam_mulai;
	                if(mode_mulai === 1)
	                    total_bayar = total_bayar * 50000;
	                else if(mode_mulai === 2)
	                    total_bayar = total_bayar * 90000;
	                else
	                    total_bayar = total_bayar * 135000;
	            } else {
	                if(mode_selesai === 2)
	                    total_bayar = (jam_selesai - 12) * 90000;
	                else
	                    total_bayar = (jam_selesai - 16) * 135000;

	                if(mode_mulai === 1)
	                    total_bayar += (12 - jam_mulai) * 50000;
	                else
	                    total_bayar += (16 - jam_mulai) * 90000;

	                if(mode_selesai -  mode_mulai > 1)
	                    total_bayar += 4 * 90000;
	            }
	        }
	        alert('#total_bayar'+mode_id);

	        $('#total_bayar'+mode_id).val(total_bayar);
	        $('#total_bayar'+mode_id).closest('form').find('input.krg_lunas').val(total_bayar);
	    }


		$(".jam_main").change(function (){			
	        calculate_total();
	    })

		//Ambil ajax data nota transaksi
		function populate_table(today){			
			var startD = new Date(now.getFullYear(), $('#jadwal_bulan').val()-1, 1).toString("yyyy-MM-dd");
	        var endD = new Date(now.getFullYear(), $('#jadwal_bulan').val(), 0).toString("yyyy-MM-dd");
			var url = "<?php echo base_url();?>tlapangan/get_data_transaksi_rentang/"+$('#jadwal_lapangan').val()+"/-/-";	        

			if(today === false){
				url = "<?php echo base_url();?>tlapangan/get_data_transaksi_rentang/"+$('#jadwal_lapangan').val()+"/"+startD+"/"+endD;
			}
			
			if ($.fn.DataTable.isDataTable( '#tabel_nota_lapangan' ) ) {										
	    		table.ajax.url(url).load();
	    	} else {
	    		table = $('#tabel_nota_lapangan').DataTable( {
			        "ajax": url,
			        "columns": [			        
			            { "data": "id", "class": "id" },
			            { "data": "id_II", "class": "id_II" },
			            { "data": "tanggal", "class": "tanggal" },
			            { "data": "bayar", "class": "bayar" },			            
			            { "data": "diskon", "class": "diskon" },
			            { "data": "total_bayar", "class": "total_bayar" },
			            { "data": "lapangan", "class": "lapangan" },
			            { "data": "tgl_main", "class": "tgl_main" },
			            { "data": "jam_mulai", "class": "jam_mulai" },
			            { "data": "jam_selesai", "class": "jam_selesai" },
			            { "data": "nama", "class": "nama" },
			            { "data": "telp", "class": "telp" },
			            { "data": "bonus", "class": "bonus" },
			            { "data": "keterangan", "class": "keterangan" },
			            { "data": "override", "class": "override" },		            
			            { "data": "id_operator", "class": "id_operator" },
			            { "data": "status", "class": "status" },
			            { "data": "act", "class": "act" }		            		            
			        ]
			    });
	    	}	  
		}

		$(document).on('change', '#jadwal_bulan', function(event){	            	
	    	populate_table(false);	    	  	
	    })

	    $(document).on('change', '#jadwal_lapangan', function(event){	        
	        populate_table(false);
	    })

	    $(document).on('click', '#transaksi_hari_ini', function(event){
	    	populate_table(true);
	    })

		//Hitung Kurang Bayar dari Jumlah diskon || NOT DONE YET
		$('#diskon_bayar_dp').change(function (){			
			/*
			CHECK SESSION LAMA
			check_session("check", function(data){
				if(data == "true"){
					$('#kurang_bayar').val(parseInt($('#kurang_bayar').val())-parseInt($('#diskon_bayar_dp').val()));
					$('#total_bayar').val(parseInt($('#total_bayar').val())-parseInt($('#diskon_bayar_dp').val()));
				} else{
					$('#diskon_bayar_dp').val("");
					$('#diskon_bayar_dp').attr("disabled", "disabled");
				}
			});*/

			if($('[name="override"]').val() != ""){
				$('#kurang_bayar_dp').val(parseInt($('#kurang_bayar_dp').val())-parseInt($('#diskon_bayar_dp').val()));
				$('#total_bayar_dp').val(parseInt($('#total_bayar_dp').val())-parseInt($('#diskon_bayar_dp').val()));
			} else{
				$('#diskon_bayar_dp').val("");
				$('#diskon_bayar_dp').attr("disabled", "disabled");
			}
		})

		//Hitung Kurang Bayar dari Jumlah diskon
		$('#diskon_bayar_pelunasan_dp').change(function (){			
			if($('[name="override"]').val() != ""){
				//$('#total_bayar_pelunasan_dp').val(parseInt($('#total_bayar_pelunasan_dp').val())-parseInt($('#diskon_bayar_pelunasan_dp').val()));
				//$('#bayar_lunas_pelunasan_dp').val(parseInt($('#bayar_lunas_pelunasan_dp').val())-parseInt($('#diskon_bayar_pelunasan_dp').val()))
				hitung_pelunasan();
			} else{
				$('#diskon_bayar_pelunasan_dp').val("");
				$('#diskon_bayar_pelunasan_dp').attr("disabled", "disabled");
			}
		})

		function hitung_pelunasan(){
			var mulai =  $("#jam_mulai_pelunasan_dp").val();
			var selesai = $('#jam_selesai_pelunasan_dp').val();
			var total = parseInt(selesai) - parseInt(mulai);
			total = total * 50000;
			$('#total_bayar_pelunasan_dp').val(total-parseInt($('#diskon_bayar_pelunasan_dp').val()));
			$('#bayar_lunas_pelunasan_dp').val($('#total_bayar_pelunasan_dp').val() - $('#bayar_pelunasan_dp').val());
		}
		//SECTION HITUNG END

		$('.btn-submit').click(function (){
			$('.submit').removeAttr("disabled");
		})

		//Hitung Kurang Bayar dari Bayaran DP
		/*$('#bayar_dp').change(function (){
			$("#kurang_bayar").val(parseInt($('#total_bayar').val())-parseInt($('#bayar_dp').val()));
		})*/

		$('.bayar').change(function(event){        
	        calculate_total();
	        var total = parseInt($(event.target).closest('form').find('input.total_bayar').val());
	        var disc = 0;
	        if($(event.target).closest('form').find('input.disc_bayar').val() != "")
	            disc = parseInt($(event.target).closest('form').find('input.disc_bayar').val());        
	        //Kurang buat dp, lunas buat bayar lunas
	        var krg_lunas = $(event.target).closest('form').find('input.krg_lunas');
	        //alert(total+" "+disc+" "+krg_lunas);
	        if($(event.target).attr('tag') === "dp" || $(event.target).attr('tag') === "pelunasan"){
	            //alert("dp");
	            var dp = parseInt($(event.target).closest('form').find('input.bayar_dp').val());            
	            krg_lunas.val(total - dp - disc);            
	        } else if($(event.target).attr('tag') === "lunas"){            
	            //alert("lunas");
	            krg_lunas.val(total - disc);
	        }        
	    })

		$(document).on('click', '.pelunasan_dp', function(event){			
			$('#tab-edit-pelunasan-dp').click();		
			set_value_event_click(event, "_pelunasan_dp");			
		})

		//to tell the submit button which tab is active. Save the value to form global variable
		$(document).on('click', '.modal-tab', function(event){			
			if(mode_id)
            	mode_temp = mode_id;
        
	        mode_id = $(event.target).attr('name');
	        //alert(mode_temp);	        
	        
	        if(mode_temp){
	        	$('.bayar').val("");         
	            $("#jam_mulai"+mode_id).val($("#jam_mulai"+mode_temp).val());
	            $('#jam_selesai'+mode_id).val($('#jam_selesai'+mode_temp).val());
	        	calculate_total();
	        }	        
		})

		//When the main submit of transaction modal input clicked
		$(document).on('click', '#tambah_jadwal', function(){			
	        $('#submit'+mode_id).click();        
	        //$('.active form.form-tambah').submit();
	    })

		//#edit_jadwal is an save button for multiple tabs, when it clicked, it will submit the active one		
		$(document).on('click', '#edit_jadwal', function(){
	        $('.active form.form-edit').submit();
	    })

		//what to do when form is just submitted
		$(document).on('submit', '.form-edit', function(){						
			$(".submit").removeAttr("disabled");
		})

		//when batal_transaksi button clicked
		$(document).on('click', '.batal_transaksi', function(event){
			e.preventDefault();
				
			var d = confirm('Hapus user?');
			if (d == true){				
				var id = $(event.target).closest('tr').children('td.id_nota_lapangan').text();
				$.ajax({
					url: "<?php echo base_url() ?>tlapangan/batal_transaksi/"+id,
	                type: "POST",
	                success: function(data){

	                },
					error: function  (data) {
						alert('terjadi kesalahan, gagal');
						return false;
					}	
				})		
			}else{
				return false;
			}
			$(this).closest('tr').fadeOut(function () {
				$(this).remove();
			})							
		})

		function set_value_event_click(args, mode){			
	        $('#id'+mode).val($(args.target).closest('tr').children('td.id').text());
	        $('#tgl'+mode).val($(args.target).closest('tr').children('td.tanggal').text());
	        $('#tgl_lunas'+mode).datetimepicker('setDate', new Date());
	        $('#nama'+mode).val($(args.target).closest('tr').children('td.nama').text());
	        $('#telp'+mode).val($(args.target).closest('tr').children('td.telp').text());
	        $('#lapangan'+mode).val($('#jadwal_lapangan').val());
	        $('#tgl_main'+mode).val($(args.target).closest('tr').children('td.tgl_main').text());
	        $('#jam_mulai_old').val($(args.target).closest('tr').children('td.jam_mulai').text());
	        $('#jam_selesai_old').val($(args.target).closest('tr').children('td.jam_selesai').text());
	        $('#jam_mulai'+mode).val($(args.target).closest('tr').children('td.jam_mulai').text());
	        $('#jam_selesai'+mode).val($(args.target).closest('tr').children('td.jam_selesai').text());
	        $('#total_bayar'+mode).val($(args.target).closest('tr').children('td.total_bayar').text());
	        $('#bayar'+mode).val($(args.target).closest('tr').children('td.bayar').text());
	        $('#diskon_bayar'+mode).val($(args.target).closest('tr').children('td.diskon').text());
	        $('#diskon_asli'+mode).val($(args.target).closest('tr').children('td.diskon').text());
	        $('#kurang_bayar'+mode).val(parseInt($('#total_bayar'+mode).val()) - parseInt($('#bayar'+mode).val()) - parseInt($('#diskon_bayar'+mode).val()));
	        $('#bayar_lunas'+mode).val(parseInt($('#total_bayar'+mode).val()) - parseInt($('#bayar'+mode).val()) - parseInt($('#diskon_bayar'+mode).val()));
	        $('#bonus'+mode).val($(args.target).closest('tr').children('td.bonus').text());
	        $('#keterangan'+mode).val($(args.target).closest('tr').children('td.keterangan').text());
	        $('#batal_main').attr('href', "<?php echo site_url('tlapangan/batal_transaksi/')?>/"+$(args.target).closest('tr').children('td.id').text()+"/"+$(args.target).closest('tr').children('td.telp').text());
	        $('#edit_transaksi').modal('show');
	        $('#tab-edit-pelunasan-dp').click();
	        //alert()
	    }		

	    $(document).on('click', '#btn_input_transaksi', function(){
	    	$('#tab-dp').click();
	    })
		
        // OVERRIDE PENGGUNA        
        $(document).on('click', '.override_pengguna', function override_pengguna(event) {
        	//alert($(event.target).attr("name"));
        	override_id = $(event.target).attr("name");
            $('#override-form')[0].reset(); // reset form on modals
            $('#override-form .form-group').removeClass('has-error'); // clear error class
            $('#override-form .help-block').empty(); // clear error string
            
            $('#modal-override').modal('show'); // show bootstrap modal
            $('#modal-override .modal-title').text('Override Pengguna'); // Set Title to Bootstrap modal title
        });

        $(document).on('click', '#reset_override', function reset_override() {
            $('#override-form')[0].reset();

            $('#override-form .form-group').removeClass('has-error'); // clear error class
            $('#override-form .help-block').empty(); // clear error string
        })

        $(document).on('click', '#save_override', function save_override(event)
        {
            $('#btnSaveOverride').text('Saving...'); //change button text
            $('#btnSaveOverride').attr('disabled',true); //set button disable
         
            // ajax adding data to database
            $.ajax({
                url : "<?php echo site_url('tlapangan/ajax_override_pengguna')?>",
                type: "POST",
                data: $('#override-form').serialize(),
                dataType: "JSON",
                success: function(data)
                {         
                    if(data.status) //if success close modal and update input main-form
                    {
                        $('#modal-override').modal('hide');
                        
                        // update input text in main-form                        
                        $('[name="override_view'+mode_id+'"]').val('ID: ' + data.user_id + ' (' + data.username + ')');
                        $('[name="override"]').val(data.user_id);
                        $('#diskon_bayar_'+override_id).removeAttr('disabled');
                    }
                    else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            
                            $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent TWICE to select div form-group class and add has-error class
                            $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
         
                    $('#btnSaveOverride').text('Save'); //change button text
                    $('#btnSaveOverride').attr('disabled',false); //set button enable
         
         
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error override data');
                    $('#btnSaveOverride').text('Save'); //change button text
                    $('#btnSaveOverride').attr('disabled',false); //set button enable
         
                }
            });
        })


		/*
		$(document).on('submit', '#override_dp', function(event){
			event.preventDefault();
			var formData = new FormData($(this)[0]);
			override(formData, function(data){
				if(data == "true"){
					$('#diskon_bayar_dp').removeAttr("disabled");
					$('#modal_override_dp').modal('toggle');
				} else{
					$('#override_dp_notfound').removeClass("hidden");
				}
			});
		})

		$(document).on('submit', '#transaksi_dp', function(event){
			$('#diskon_bayar_ovr').attr("id", "diskon_bayar_dp");
		})

		function override(formData, callback){
			$.ajax({
				type:"POST",
				data:formData,
				async:true,
				url:"<?php echo base_url() ?>login/override_validation/",
				success: function(data){
					//callback check data, karena ajax gabisa check data krn asynchronous
					if(data && callback){
						callback(data);
					}
				},
		        cache: false,
		        contentType: false,
		        processData: false
			});
		}

		function check_session(action, callback){
			$.ajax({
				type:"POST",
				data: {action: action},
				async: true,
				url:"<?php echo base_url() ?>login/check_session/",
				success: function(data){
					if(data && callback){
						callback(data);
					}
				}
			});
		}

	


		$('#nama_customer').change(function(){
			var nama = $('#nama_customer').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>tlapangan/search_nama_customer/"+nama,
				success: function (data){

				}
			})
		})

		$('#telp_customer').change(function(){

		})
		/*
		$('#btn-override').click(function(e){
			e.preventDefault();
			var data;
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>login/override_validation/"+data,
				success: function (data) {
					$('#t_body_pelamar').empty();
					for (var i = data.length - 1; i >= 0; i--) {
						$('#t_body_pelamar').append(
							'<tr>'+
								'<td>'+data[i]["np"]+'</td>'+
								'<td>'+data[i]["nama"]+'</td>'+
								'<td><input type="radio" class="select_pelamar" id="'+data[i]['np']+'" name="select_pelamar" value="'+data[i]['np']+'"/></td>'+
							'</tr>'
						);

					};
				},
				error: function (data) {
					console.log(data);
					alert('gagal');
				}
			})
		})*/
	})
</script>