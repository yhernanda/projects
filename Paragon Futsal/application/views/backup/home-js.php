<script type="text/javascript"> 
    var main;
    var bayar;
    var override_id;
    var mode_temp = "";
    var mode_id = ""; //_edit, _lunas, _edit_dp, _edit_lunas

    var idcustomer = [];
    var nama = [];
    var email = [];
    var saldo = [];
    $('.kode').focus(function(event){
        var $input = $(event.target);           
        if(idcustomer.length === 0){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>tlapangan/get_master_customer',
                success:function(data){
                    for(var i = 0; i<data.length; i++){
                        idcustomer.push(data[i]['id_customer']);
                        nama.push(data[i]['nama']);
                        email.push(data[i]['email']);                           
                    }
                }
            });
        }

        $input.typeahead({source:idcustomer, 
        autoSelect: true}); 

        $input.change(function() {          
            var current = $input.typeahead("getActive");
            var index = idcustomer.indexOf(current);

            if($input.val() == current)
                $(event.target).closest('form').find('input.nama_customer').val(nama[index]);
            //$('#email_customer').val(email[index]);               
        });

    });    

    function calculate_total(){        
        check_jadwal_main();
        var day = new Date($('#tgl_main'+mode_id).val()).getDay();                              
        var lapangan = $('#lapangan'+mode_id).val();
        var jam_mulai = $('#jam_mulai'+mode_id).val();
        var jam_selesai = $('#jam_selesai'+mode_id).val();
        var mode_mulai;
        var mode_selesai;
        var total_bayar = 0;
        var jml_minum = 0;       

        if(jam_selesai > 14){
            if(jam_mulai >= 14)
                jml_minum = jam_selesai - jam_mulai;
            else
                jml_minum = jam_selesai - 14;

            if(jml_minum > 1)
                jml_minum = jml_minum * 5;
        }   

        if(jam_selesai != ""){
            if(lapangan === 3 || day === 0 || day === 6){
                if(jam_mulai < 16)
                    mode_mulai=1;
                else
                    mode_mulai=2;

                if(jam_selesai <= 16)
                    mode_selesai=1;
                else
                    mode_selesai=2;

                if(mode_mulai === mode_selesai){
                    total_bayar = jam_selesai - jam_mulai;
                    if(mode_mulai === 1 && lapangan === 3)
                        total_bayar = total_bayar * 100000;
                    else if(mode_mulai === 1 && lapangan < 3)
                        total_bayar = total_bayar * 90000;
                    else
                        total_bayar = total_bayar * 135000;
                } else {
                    total_bayar = (jam_selesai - 16) * 135000;
                    if(lapangan < 3)
                        total_bayar += (16 - jam_mulai) * 90000;
                    else
                        total_bayar += (16 - jam_mulai) * 100000;
                }
            }else{
                if(jam_mulai < 12)
                    mode_mulai=1;
                else if(jam_mulai < 16)
                    mode_mulai=2;
                else
                    mode_mulai=3;

                if(jam_selesai <= 12)
                    mode_selesai=1;
                else if(jam_selesai <= 16)
                    mode_selesai=2;
                else
                    mode_selesai=3;

                if(mode_mulai === mode_selesai){
                    total_bayar = jam_selesai - jam_mulai;
                    if(mode_mulai === 1)
                        total_bayar = total_bayar * 50000;
                    else if(mode_mulai === 2)
                        total_bayar = total_bayar * 90000;
                    else
                        total_bayar = total_bayar * 135000;
                } else {
                    if(mode_selesai === 2)
                        total_bayar = (jam_selesai - 12) * 90000;
                    else
                        total_bayar = (jam_selesai - 16) * 135000;

                    if(mode_mulai === 1)
                        total_bayar += (12 - jam_mulai) * 50000;
                    else
                        total_bayar += (16 - jam_mulai) * 90000;

                    if(mode_selesai -  mode_mulai > 1)
                        total_bayar += 4 * 90000;
                }
            }
            //alert(mode_mulai+" "+mode_selesai);

            $('#total_bayar'+mode_id).val(total_bayar);
            if(mode_id === "_lunas")
                $('#total_bayar'+mode_id).closest('form').find('input.krg_lunas').val(total_bayar - $('#diskon_bayar'+mode_id).val());
            else    
                $('#total_bayar'+mode_id).closest('form').find('input.krg_lunas').val(total_bayar - $('#diskon_bayar'+mode_id).val() - $('#bayar'+mode_id).val());

            if(jml_minum > 1)                   
                $('#bonus'+mode_id).val(jml_minum+" Club 600mL");
            else if($('#bonus'+mode_id).val() != "" || $('#bonus'+mode_id).val() != " ")
                $('#bonus'+mode_id).val("");
        }
    }

    //SECTION HITUNG
    //Calculate total play cost
    /*function calculate_total(){
        if($("#jam_mulai"+mode_id).val() != "" && $('#jam_selesai'+mode_id).val() != ""){
            check_jadwal_main();

            if(cek_range_main($('#jam_mulai')))


            var harga = 50000;
            var mulai =  $("#jam_mulai"+mode_id).val();
            mulai = mulai.split(":");
            mulai = parseInt(mulai[0])+(parseInt(mulai[1])/60);
            var selesai =  $("#jam_selesai"+mode_id).val();
            selesai = selesai.split(":");
            selesai = parseInt(selesai[0])+(parseInt(selesai[1])/60);
            var total = parseFloat(selesai) - parseFloat(mulai);
            
            total = total * 50000;
            $('#total_bayar'+mode_id).val(total);
            $('#total_bayar'+mode_id).closest('form').find('input.krg_lunas').val(total);
            /*if($('#bayar'+mode_id).val() != ""){
                $("#kurang_bayar"+mode_id).val(parseInt($('#total_bayar'+mode_id).val())-parseInt($('#bayar'+mode_id).val()));
                $('#bayar_lunas').val()
            }
        }
    }*/

    $('.bayar').change(function(event){        
        calculate_total();        
    })

    //Hitung Kurang Bayar dari Bayaran DP
    /*$('.bayar_dp').change(function (){
        //alert("#kurang_bayar"+mode_id);
        $("#kurang_bayar"+mode_id).val(parseInt($('#total_bayar'+mode_id).val())-parseInt($('#bayar'+mode_id).val()));
    })

    $('.diskon_bayar_dp').change(function (){
        if($('[name="override'+mode_id+'"]').val() != ""){
            $('#kurang_bayar'+mode_id).val(parseInt($('#kurang_bayar'+mode_id).val())-parseInt($('#diskon_bayar'+mode_id).val()));
            $('#total_bayar'+mode_id).val(parseInt($('#total_bayar'+mode_id).val())-parseInt($('#diskon_bayar'+mode_id).val()));
        } else{
            $('#diskon_bayar'+mode_id).val("");
            $('#diskon_bayar'+mode_id).attr("disabled", "disabled");
        }
    })

    //Hitung Kurang Bayar dari Jumlah diskon
    $('#diskon_bayar_lunas').change(function (){
        if($('[name="override"]').val() != ""){
            $('#total_bayar_lunas').val(parseInt($('#total_bayar_lunas').val())-parseInt($('#diskon_bayar_lunas').val()));
        } else{
            $('#diskon_bayar_lunas').val("");
            $('#diskon_bayar_lunas').attr("disabled", "disabled");
        }
    })*/
    //SECTION HITUNG END
        

        /* DAY PILOT EVENT CALENDAR */
        var now = new Date();
        var dp = new DayPilot.Calendar("daypilot");
        dp.businessBeginsHour = 6;
        dp.businessEndsHour = 24;
        dp.heightSpec = "BusinessHoursNoScroll";     
        dp.viewType = "Days";        
        dp.crosshairType = "Full";        
        dp.days = new Date(now.getFullYear(), now.getMonth()+1, 0).getDate(),
        dp.headerDateFormat = "dd";      
        dp.theme = "calendar_green";
        dp.startDate = new Date(now.getFullYear(), now.getMonth(), 1).toString("yyyy-MM-dd");        
        dp.timeHeaderCellDuration = 60;
        dp.cellDuration = 60;
        dp.cellHeight = 30;       
        dp.bubble = new DayPilot.Bubble();                   
       
        //creating event (click on empty space in the calendar)        
        dp.onBeforeCellRender = function(args) {
          if (args.cell.start.getDatePart().getTime() < new DayPilot.Date().getDatePart().getTime()) {
            args.cell.backColor = "#e0f3db";
          }
        };

        dp.onTimeRangeSelected = function (args) {
            mulai = new Date(args.start);
            selesai = new Date(args.end);
            //Set Jam Main & Tgl Main di modal            
            $('#jam_mulai_dp').val(mulai.getUTCHours());
            $('#jam_mulai_lunas').val(mulai.getUTCHours());
            $('#jam_selesai_dp').val(selesai.getUTCHours());
            $('#jam_selesai_lunas').val(selesai.getUTCHours());
            $('#tgl_main_dp').val(mulai.getFullYear() + "-" + ("0" + (mulai.getMonth() + 1)).slice(-2) + "-" + ("0" + mulai.getDate()).slice(-2));
            $('#tgl_main_lunas').val(mulai.getFullYear() + "-" + ("0" + (mulai.getMonth() + 1)).slice(-2) + "-" + ("0" + mulai.getDate()).slice(-2));
            
            //Set Tanggal Bayar (Hari Ini) di modal
            bayar = new Date();
            $('#tgl_dp').val(bayar.getFullYear() + "-" + ("0" + (bayar.getMonth() + 1)).slice(-2) + "-" + ("0" + bayar.getDate()).slice(-2));
            $('#tgl_lunas').val(bayar.getFullYear() + "-" + ("0" + (bayar.getMonth() + 1)).slice(-2) + "-" + ("0" + bayar.getDate()).slice(-2));
            $('#input_transaksi').modal('show');            
            $('#tab-lunas').click();
            calculate_total();
            $('#tab-dp').click();
            calculate_total();

            /*var name = prompt("New event name:", "Event");
            if (!name) return;
            var e = new DayPilot.Event({
                start: args.start,
                end: args.end,
                ifd: DayPilot.guid(), //Customable
                resource: args.resource,
                text: name
            });
            dp.events.add(e);*/

            /*$.post("backend_create.php", 
                    {
                        start: args.start.toString(),
                        end: args.end.toString(),
                        name: name
                    }, 
                    function() {
                        console.log("Created.");
                    });*/
            dp.clearSelection();
        };

        dp.onEventClick = function(args) {               
            if(args.e.data.tag.status === "DP"){
                //alert("aaa");
                $('#tab-edit-pelunasan-dp').click();
                $('#tab-edit-lunas').removeAttr("data-toggle");
                $('#tab-edit-lunas').css("cursor", "not-allowed");
                set_value_event_click(args, "_pelunasan_dp");                       
            }            
        };

        dp.init();
        $("div:contains('DEMO')").children('div div div div div div').css( "background-color", "grey" );        
        $("div:contains('DEMO')").css("color", "grey");


    //On Jadwal_bulan change
    $(document).on('change', '#jadwal_bulan', function(event){
        var startD = new Date(now.getFullYear(), $(event.target).val()-1, 1).toString("yyyy-MM-dd");
        var endD = new Date(now.getFullYear(), $(event.target).val(), 0).toString("yyyy-MM-dd");
        dp.days = new Date(now.getFullYear(), $(event.target).val(), 0).getDate();
        //-1 coz it looks like the months are started from 0 instead of 1
        dp.startDate = startD;        
        dp.init();
        get_data_transaksi($('#jadwal_lapangan').val(), startD, endD);
    })     

    //calendar
    $(document).on('click', '.fc-widget-content', function(event){
        alert($(event.target).closest('div.fc-time-grid').find('td.fc-day').attr('data-date'));
    })

    //check whether there are any overlapping schedules in the database or not
    function check_schedule_overlap(data){
        $.ajax({
            url: "<?php echo site_url('home/check_data_transaksi/')?>",
            type: "post",
            success: function(data){
                        
            }
        })    
    }

    //load transactions data into fullcalendar
    function arrange_schedule(data){
        //console.log(data);
        dp.events.list = [];
        dp.update();
        for (var i = Object.keys(data).length - 1; i >= 0; i--) {
            var e = new DayPilot.Event(data[i]);                     
            dp.events.add(e);
        }
    }

    //ajax getting transactions data from database
    function get_data_transaksi(lapangan, start, end){
        console.log("<?php echo site_url('home/get_data_transaksi')?>/"+lapangan+"/"+start+"/"+end);
        $.ajax({
            url: "<?php echo site_url('home/get_data_transaksi')?>/"+lapangan+"/"+start+"/"+end,
            type: "post",
            success: function(data){
                arrange_schedule(data);
            }
        })
    }

    //Check field's schedule from database, for conflicts or overlaps
    function check_jadwal_main(){
        if(mode_id != "_pelunasan_dp"){
            if(mode_id === "_edit_dp" || mode_id === "_edit_lunas"){
                var dataObject =  {
                    tgl_main: $('#tgl_main'+mode_id).val(),
                    jam_mulai: $('#jam_mulai'+mode_id).val(),
                    jam_selesai: $('#jam_selesai'+mode_id).val(),
                    lapangan:  $('#lapangan'+mode_id).val(),
                    id_nota_lapangan: $('#id'+mode_id).val()
                };
                alert($('#id'+mode_id).val());
            } else{
                var dataObject =  {
                    tgl_main: $('#tgl_main'+mode_id).val(),
                    jam_mulai: $('#jam_mulai'+mode_id).val(),
                    jam_selesai: $('#jam_selesai'+mode_id).val(),
                    lapangan:  $('#lapangan'+mode_id).val()
                };    
            }
            
            console.log(dataObject);
            $.ajax({
                url: "<?php echo site_url('home/check_data_transaksi')?>",
                type: "POST",
                data: dataObject,
                cache: false,
                success: function(response){
                    //console.log(response);
                    if(response[0]['status'] != "CLEAR"){
                        alert("Lapangan "+response[0]['lapangan']+" sudah terpakai! \nPada tanggal "+response[0]['tgl_main']+" antara pukul "+(response[0]['jam_mulai']).substr(0,5)+" s/d "+(response[0]['jam_selesai']).substr(0,5)+". \nSilahkan ganti lapangan/jam/hari lain.");                                                
                        reset_input();
                        $('#jam_mulai'+mode_id).focus();
                        return false;
                    }
                }
            });
            return true;
        }        
    }

    //Get schedule from selected court
    $('#jadwal_lapangan').change(function(event){        
        $('#lapangan_dp').val($(event.target).val());
        $('#lapangan_lunas').val($(event.target).val());
        get_data_transaksi($(event.target).val(), new Date(now.getFullYear(), now.getMonth(), 1).toString("yyyy-MM-dd"), new Date(now.getFullYear(), now.getMonth()+1, 0).toString("yyyy-MM-dd"));
    })

    //Hitung jumlah bayar berdasar jam
    $(".jam_main").change(function (){
        calculate_total();
    })

    //Reset the current active transaction form
    $(document).on('click', '#reset_transaksi', function reset_dp(){
        $('#transaksi'+mode_id)[0].reset();

        $('#transaksi'+mode_id+' .form-group').removeClass('has-error'); // clear error class
        $('#transaksi'+mode_id+' .help-block').empty(); // clear error string
    })

    function reset_input() {
        $('#reset_dp').closest('form').find("input[type=text], input[type=number], textarea").val("");
        $('#reset_lunas').closest('form').find("input[type=text], input[type=number], textarea").val("");
        $('.override_dp, .override_lunas').attr("disabled", "disabled");
    }

    $('#input_transaksi').on('hidden.bs.modal', function () {         
        reset_input();
    })

    $('#edit_transaksi').on('hidden.bs.modal', function () {            
        $('#reset_edit_dp').closest('form').find("input[type=text], input[type=number], textarea").val("");
        $('#reset_pelunasan_dp').closest('form').find("input[type=text], input[type=number], textarea").val("");
        $('.override_edit_dp, .override_pelunasan_dp').attr("disabled", "disabled");
    })

    // OVERRIDE PENGGUNA
    $(document).on('click', '.override_pengguna', function override_pengguna(event) {
        //alert($(event.target).attr("name"));
        $('#override-form')[0].reset(); // reset form on modals
        $('#override-form .form-group').removeClass('has-error'); // clear error class
        $('#override-form .help-block').empty(); // clear error string
        
        $('#modal-override').modal('show'); // show bootstrap modal
        $('#modal-override .modal-title').text('Override Pengguna'); // Set Title to Bootstrap modal title
    });

    $(document).on('click', '#reset_override', function reset_override() {
        $('#override-form')[0].reset();

        $('#override-form .form-group').removeClass('has-error'); // clear error class
        $('#override-form .help-block').empty(); // clear error string
    })

    $(document).on('click', '#save_override', function save_override(event)
    {
        $('#btnSaveOverride').text('Saving...'); //change button text
        $('#btnSaveOverride').attr('disabled',true); //set button disable
     
        // ajax adding data to database
        $.ajax({
            url : "<?php echo site_url('home/ajax_override_pengguna')?>",
            type: "POST",
            data: $('#override-form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
     
                if(data.status) //if success close modal and update input main-form
                {
                    //alert(mode_id);
                    $('#modal-override').modal('hide');
                    
                    // update input text in main-form
                    $('#override_view'+mode_id).val('ID: ' + data.user_id + ' (' + data.username + ')');
                    $('#override'+mode_id).val(data.user_id);
                    $('#override'+mode_id).closest('form').find('[name="diskon"]').removeAttr('disabled');                    
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++)
                    {
                        
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent TWICE to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
     
                $('#btnSaveOverride').text('Save'); //change button text
                $('#btnSaveOverride').attr('disabled',false); //set button enable
     
     
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error override data');
                $('#btnSaveOverride').text('Save'); //change button text
                $('#btnSaveOverride').attr('disabled',false); //set button enable
     
            }
        });
    })

    $(document).on('click', '#tambah_jadwal', function(){
        $('#submit'+mode_id).click();        
        //$('.active form.form-tambah').submit();
    })

    $(document).on('click', '#edit_jadwal', function(){
        $('.active form.form-edit').submit();
    })

    //what to do when form is just submitted
    $(document).on('submit', '.form-edit', function(){                      
        $(".submit").removeAttr("disabled");
    })

    $(document).on('submit', '.form-tambah', function(){                        
        $(".submit").removeAttr("disabled");
    })

    //CHANGE TAB/MODE ID
    $(document).on('click', '.modal-tab', function(event){
        mode_id = $(event.target).attr('name');
    })

    $(document).ready(function() {
        get_data_transaksi(1, new Date(now.getFullYear(), now.getMonth(), 1).toString("yyyy-MM-dd"), new Date(now.getFullYear(), now.getMonth()+1, 0).toString("yyyy-MM-dd"));
        $('#daypilot').append($('#daypilot div:first-child').html());
    });

    function set_value_event_click(args, mode){
        for (var i = 1; i < 3; i++) {
            if(i === 2)
                mode = "_edit_dp";
            
            $('#id'+mode).val(args.e.data.id);
            $('#tgl'+mode).val(args.e.data.tag.tanggal);
            $('#tgl_lunas'+mode).datetimepicker('setDate', new Date());
            $('#nama'+mode).val(args.e.data.text);
            $('#telp'+mode).val(args.e.data.tag.telp);
            $('#lapangan_old').val($('#jadwal_lapangan').val());
            $('#lapangan'+mode).val($('#jadwal_lapangan').val());
            $('#tgl_main_old').val(args.e.data.tag.tgl_main);
            $('#tgl_main'+mode).val(args.e.data.tag.tgl_main);
            $('#jam_mulai_old').val(args.e.data.tag.jam_mulai);
            $('#jam_selesai_old').val(args.e.data.tag.jam_selesai);
            $('#jam_mulai'+mode).val(args.e.data.tag.jam_mulai);
            $('#jam_selesai'+mode).val(args.e.data.tag.jam_selesai);
            $('#total_bayar'+mode).val(args.e.data.tag.total_bayar);
            $('#bayar'+mode).val(args.e.data.tag.bayar);
            $('#diskon_bayar_old').val(args.e.data.tag.diskon);
            $('#diskon_bayar'+mode).val(args.e.data.tag.diskon);
            $('#kurang_bayar'+mode).val(parseInt(args.e.data.tag.total_bayar) - parseInt(args.e.data.tag.bayar) - parseInt(args.e.data.tag.diskon));
            $('#bayar_lunas'+mode).val(parseInt(args.e.data.tag.total_bayar) - parseInt(args.e.data.tag.bayar) - parseInt(args.e.data.tag.diskon));
            $('#bonus'+mode).val(args.e.data.tag.bonus);
            $('#keterangan_old').val(args.e.data.tag.keterangan);
            $('#keterangan'+mode).val(args.e.data.tag.keterangan);
            $('#batal_main').attr('href', "<?php base_url(); ?>tlapangan/batal_transaksi/"+args.e.data.id+"/"+args.e.data.tag.telp+"/"+args.e.data.tag.status);            
        }
        $('#edit_transaksi').modal('show');
        $('#tab-edit-pelunasan-dp').click();
        /*$('#id'+mode).val($(args.target).closest('tr').children('td.id').text());
        $('#tgl'+mode).val($(args.target).closest('tr').children('td.tanggal').text());
        $('#tgl_lunas'+mode).datetimepicker('setDate', new Date());
        $('#nama'+mode).val($(args.target).closest('tr').children('td.nama').text());
        $('#telp'+mode).val($(args.target).closest('tr').children('td.telp').text());
        $('#lapangan_old').val($('#jadwal_lapangan').val());
        $('#lapangan'+mode).val($('#jadwal_lapangan').val());
        $('#tgl_main'+mode).val($(args.target).closest('tr').children('td.tgl_main').text());
        $('#tgl_main_old').val($(args.target).closest('tr').children('td.tgl_main').text());
        $('#jam_mulai_old').val($(args.target).closest('tr').children('td.jam_mulai').text());
        $('#jam_selesai_old').val($(args.target).closest('tr').children('td.jam_selesai').text());
        $('#jam_mulai'+mode).val($(args.target).closest('tr').children('td.jam_mulai').text());
        $('#jam_selesai'+mode).val($(args.target).closest('tr').children('td.jam_selesai').text());
        $('#total_bayar'+mode).val($(args.target).closest('tr').children('td.total_bayar').text());
        $('#bayar'+mode).val($(args.target).closest('tr').children('td.bayar').text());
        $('#diskon_bayar_old').val($(args.target).closest('tr').children('td.diskon').text());
        $('#diskon_bayar'+mode).val($(args.target).closest('tr').children('td.diskon').text());         
        $('#kurang_bayar'+mode).val(parseInt($('#total_bayar'+mode).val()) - parseInt($('#bayar'+mode).val()) - parseInt($('#diskon_bayar'+mode).val()));
        $('#bayar_lunas'+mode).val(parseInt($('#total_bayar'+mode).val()) - parseInt($('#bayar'+mode).val()) - parseInt($('#diskon_bayar'+mode).val()));
        $('#bonus'+mode).val($(args.target).closest('tr').children('td.bonus').text());
        $('#keterangan_old').val($(args.target).closest('tr').children('td.keterangan').text());
        $('#keterangan'+mode).val($(args.target).closest('tr').children('td.keterangan').text());
        $('#batal_main').attr('href', (args.target).closest('tr').children('td.act').children('a.batal_transaksi').attr('href'));
        //alert((args.target).closest('tr').children('td.act').children('a.batal_transaksi').attr('href'));
        $('#edit_transaksi').modal('show');
        $('#tab-edit-pelunasan-dp').click();*/
    }

    function get_datenow(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        return yyyy+'-'+mm+'-'+dd;
    }
</script>