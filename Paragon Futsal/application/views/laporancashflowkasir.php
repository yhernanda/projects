<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include "head.php"; ?>
    <title>Cashflow Kasir - Paragon Futsal</title>

</head>
<body>
    <?php
        include "prenavbar.php"; 
        /*print_r($this->session->userdata('data_cashflow')); die;
        if(!empty($this->session->userdata('data_cashflow'))){
            $tanggal = $this->session->userdata('data_cashflow')['tanggal'];
            $transaksi = $this->session->userdata('data_cashflow')['transaksi'];
            $saldo_kemarin = $this->session->userdata('data_cashflow')['saldo_kemarin'];
            $saldo_sekarang = $this->session->userdata('data_cashflow')['saldo_sekarang'];
        }*/
    ?>
    </div>
    <!-- LOGO HEADER END-->
    <section class="menu-section">
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <div id="main_nav" class="right">
                        <ul>
                            <li><a href="home"><i class="fa fa-home">&nbsp;</i>Home</a></li>
                            <li>
                                <a href=""><i class="fa fa-dollar">&nbsp;</i>Transaksi <i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="tlapangan">Lapangan</a></li>
                                    <li><a href="tdeposit">Deposit</a></li>
                                    <li><a href="">F&B dan Barang &nbsp;<i class="fa fa-angle-right"></i></a>
                                        <ul>
                                            <li><a href="barangpenjualan">Penjualan</a></li>
                                            <li><a href="barangpembelian">Pembelian</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="parkir">Parkir</a></li>
                                    <li><a href="sewatempat">Sewa Tempat</a></li>
                                    <li><a href="">Cash &nbsp;<i class="fa fa-angle-right" style="float: right;padding-top: 7px;"></i></a>
                                    <ul>
                                        <li><a href="tpickup">Cash Pick Up</a></li>
                                        <li><a href="">Cash Inject</a></li>
                                        <li><a href="">Cash Deposit (Bank)</a></li>
                                    </ul>
                                    </li>
                                    <li><a href="#">Pengeluaran</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="" class="menu-top-active"><i class="fa fa-file-text">&nbsp;</i>laporan <i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="laporancashflowkasir">Cashflow Kasir</a></li>
                                    <li><a href="laporancashflowpib">Cashflow Pickup/Inject/Bank</a></li>
                                    <li><a href="#">Lembur</a></li>
                                    <li><a href="#">Stok Barang</a></li>
                                    <li><a href="laporancustomer">Customers</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href=""><i class="fa fa-database">&nbsp;</i>data <i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="databarang">Data F&B dan Barang</a></li>
                                    <li><a href="datauser">Data Account User</a></li>
                                    <li><a href="#">Database Management</a></li>
                                </ul>
                            </li>
                            <li><a href="login/logout"><i class="fa fa-sign-out">&nbsp;</i>LOGOUT</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
     <!-- MENU SECTION END-->
    <div class="content-wrapper">
        <div class="isi">
            <div class="row pad-botm">
                <div class="col-md-12">
                    <h4 class="header-line">Laporan Cashflow Kasir </h4>
                    <div style="margin: 0 auto;">
                        <form id="form_cashflow" action="<?php echo base_url();?>laporancashflowkasir/get_tgl" method="post" class="form-submit">
                            <h4 style="float:left; vertical-align: middle;">Tanggal : &nbsp</h4>
                            <input type="text" class="form-control datepicker" style="float:left; width: 10%;" value="<?php if(!empty($tanggal)) echo $tanggal; else echo date("Y-m-d");?>" id="tgl_cashflow" name="tgl_cashflow">
                            <button type="submit" class="hidden" id="submit-tgl">submit</button>
                        </form>                        
                        <a href="laporancashflowkasir" type="button" class="btn btn-success" style="float:right; width: 10%;">Transaksi Hari Ini</a>                        
                    </div>                                        
                    <!-- modal -->                    
                </div>
            </div>
             
            <div class="row" style="margin-top: -20px;">
                <div class="col-md-12">
                    <table class="table table-responsive table-striped data-table table-bordered table-hover">
                        <thead>
                            <tr class="success" align="center">                                
                                <th width="10%">Tanggal</th>                                
                                <th>Keterangan</th>
                                <th>Kredit</th>
                                <th>Debet</th>
                                <th>Saldo</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style='vertical-align:middle'><?php if(!empty($saldo_kemarin[0]['tanggal'])) echo date('d-m-Y', strtotime($saldo_kemarin[0]['tanggal'])); else echo '-';?></td>
                                <td style='vertical-align:middle'>Saldo Kemarin</td>
                                <td style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_kemarin)) echo "Rp.".number_format($saldo_kemarin[0]['kredit']);?></td>
                                <td style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_kemarin)) echo "Rp.".number_format($saldo_kemarin[0]['debet']);?></td>
                                <td style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_kemarin)) echo "Rp.".number_format($saldo_kemarin[0]['saldo']);?></td>
                            </tr>
                            <?php                                                                
                                $dp=0;$lunas=0;$dpst=0;$parkir=0;$sewa=0;$jualanfnb=0;$pengeluaran=0;$bayarfnb=0;$cashpickup=0;$cashinject=0;

                                function cetak($value=""){
                                    $kredit=''; $debet=''; $saldo='';
                                    if(array_key_exists('kredit', $value))
                                        $kredit=$value['kredit'];
                                    if(array_key_exists('debet', $value))
                                        $debet=$value['debet'];
                                    if(array_key_exists('saldo', $value))
                                        $saldo=$value['saldo'];

                                    $print = "<tr>                                                                                
                                        <td style='vertical-align:middle'>".date('d-m-Y', strtotime($value['tanggal']))."</td>
                                        <td style='text-align: left; vertical-align:middle'>".$value['keterangan']."</td>
                                        <td style='text-align: right; vertical-align:middle'>Rp.".number_format((int)$kredit)."</td>
                                        <td style='text-align: right; vertical-align:middle'>Rp.".number_format((int)$debet)."</td>
                                        <td style='text-align: right; vertical-align:middle'>Rp.".number_format((int)$saldo)."</td>
                                        </tr>";

                                    echo $print;
                                    $kredit=''; $debet=''; $saldo='';
                                }
                                
                                
                                if(!empty($transaksi)){
                                    foreach ($transaksi as $key => $value) {
                                        cetak($value);
                                        if($value['status']=="DP" || $value['status']=="PELUNASAN")
                                            $dp+=$value['kredit'];
                                        elseif($value['status']=="LUNAS")
                                            $lunas+=$value['kredit'];

                                        if($value['status']=="DEPOSIT")
                                            $dpst+=$value['kredit'];

                                        if($value['status']=="PARKIR")
                                            $parkir+=$value['kredit'];
                                        
                                        if($value['status']=="PENJUALAN")
                                            $jualanfnb+=$value['kredit'];

                                        if($value['status']=="BAYAR")
                                            $bayarfnb+=$value['debet'];

                                        if($value['status']=="PICKUP")
                                            $cashpickup+=$value['debet'];

                                        if($value['status']=="INJECT")
                                            $cashinject+=$value['kredit'];
                                    }
                                }
                                                                                                
                            ?>
                            <tr>                                
                                <td class="info" colspan="2" style='vertical-align:middle'>Total</td>
                                <td class="warning" style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_sekarang) && !empty($saldo_kemarin)) echo "Rp.".number_format($saldo_sekarang[0]['kredit']+$saldo_kemarin[0]['kredit']);?></td>
                                <td class="warning" style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_sekarang) && !empty($saldo_kemarin)) echo "Rp.".number_format($saldo_sekarang[0]['debet']+$saldo_kemarin[0]['debet']);?></td>
                                <td class="warning" style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_sekarang) && !empty($saldo_kemarin)) echo "Rp.".number_format($saldo_sekarang[0]['saldo']+$saldo_kemarin[0]['saldo']);?></td>
                            </tr>                            
                            <tr>                                
                                <td class="success" colspan="5" style='vertical-align:middle'>REKAP LAPORAN PER HARI</td>                                
                            </tr>
                            <tr>                                
                                <td style='vertical-align:middle'><?php if(!empty($saldo_kemarin[0]['tanggal'])) echo date('d-m-Y', strtotime($saldo_kemarin[0]['tanggal'])); else echo '-';?></td>
                                <td style='vertical-align:middle'>Saldo Kemarin</td>
                                <td style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_kemarin)) echo "Rp.".number_format($saldo_kemarin[0]['kredit']);?></td>
                                <td style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_kemarin)) echo "Rp.".number_format($saldo_kemarin[0]['debet']);?></td>
                                <td style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_kemarin)) echo "Rp.".number_format($saldo_kemarin[0]['saldo']);?></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total DP</td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($dp); ?></td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total LUNAS</td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($lunas); ?></td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"></td>
                            </tr>                            
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total DEPOSIT</td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($dpst); ?></td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total PARKIR</td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($parkir); ?></td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total SEWA TEMPAT</td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($sewa); ?></td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total PENJUALAN FNB</td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($jualanfnb); ?></td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total PENGELUARAN</td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($pengeluaran); ?></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total BAYAR FNB</td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($bayarfnb); ?></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total CASH PICKUP</td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($cashpickup); ?></td>
                                <td style="text-align: right;"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left;">Total CASH INJECT</td>
                                <td style="text-align: right;"><?php echo "Rp.".number_format($cashinject); ?></td>
                                <td style="text-align: right;"></td>
                                <td style="text-align: right;"></td>
                            </tr>
                        </tbody>                            
                        <thead>
                            <tr>                                
                                <td class="info" colspan="2" style='vertical-align:middle'>Total</td>
                                <td class="warning" style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_sekarang) && !empty($saldo_kemarin)) echo "Rp.".number_format($saldo_sekarang[0]['kredit']+$saldo_kemarin[0]['kredit']);?></td>
                                <td class="warning" style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_sekarang) && !empty($saldo_kemarin)) echo "Rp.".number_format($saldo_sekarang[0]['debet']+$saldo_kemarin[0]['debet']);?></td>
                                <td class="warning" style='text-align: right; vertical-align:middle'><?php if(!empty($saldo_sekarang) && !empty($saldo_kemarin)) echo "Rp.".number_format($saldo_sekarang[0]['saldo']+$saldo_kemarin[0]['saldo']);?></td>
                            </tr>
                        </thead>
                    </table>           
                </div>
            </div>
        </div>
    </div>      
    
    <?php include "footer-lapangan.php"; ?>
    <?php include "laporancashflowkasir-js.php"; ?>
    <script type="text/javascript">
        window.alert = function(){};
    </script>
</body>
</html>