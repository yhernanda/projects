<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.autocomplete.js"></script>


<script type="text/javascript">
	$(document).ready(function (){		
		var override_id;
		var mode_temp;
		var mode_id;
		var now = new Date();
		var table;
		var lastToday = false;
		populate_table(false);
		
		//SECTION AUTO COMPLETE
		var idcustomer = [];
	    var nama = [];
	    var email = [];
	    var saldo = [];
	    var kodenama = [];
	    $('.kode').focus(function(event){
	        var $input = $(event.target);           
	        if(idcustomer.length !== 0){
	            idcustomer = [];
	            nama = [];
	            email = [];
	            saldo = [];
	            kodenama = [];
	        }

	        $.ajax({
	            type:'POST',
	            url:'<?php echo base_url();?>tlapangan/get_master_customer',
	            success:function(data){
	                for(var i = 0; i<data.length; i++){
	                    idcustomer.push(data[i]['id_customer']);
	                    nama.push(data[i]['nama']);
	                    email.push(data[i]['email']); 
	                    kodenama.push(data[i]['id_customer']+" - "+data[i]['nama']);                          
	                }
	            }
	        });

	        $input.typeahead({source:kodenama}); 

	        $input.change(function() {          
	            var current = $input.typeahead("getActive");
	            var index = kodenama.indexOf(current);

	            if($input.val() == current){
	                $(event.target).val(idcustomer[index]);
	                $(event.target).closest('form').find('input.nama_customer').val(nama[index]);
	            }
	            //$('#email_customer').val(email[index]);               
	        });

	    });
	    
	    $('.nama_customer').focus(function(event){
	        var $input = $(event.target);           
	        if(idcustomer.length !== 0){
	            idcustomer = [];
	            nama = [];
	            email = [];
	            saldo = [];
	            kodenama = [];
	        }

	        $.ajax({
	            type:'POST',
	            url:'<?php echo base_url();?>tlapangan/get_master_customer',
	            success:function(data){
	                for(var i = 0; i<data.length; i++){
	                    idcustomer.push(data[i]['id_customer']);
	                    nama.push(data[i]['nama']);
	                    email.push(data[i]['email']); 
	                    kodenama.push(data[i]['nama']+" - "+data[i]['id_customer']);                          
	                }
	            }
	        });

	        $input.typeahead({source:kodenama}); 

	        $input.change(function() {          
	            var current = $input.typeahead("getActive");
	            var index = kodenama.indexOf(current);

	            if($input.val() == current){
	                $(event.target).val(nama[index]);
	                $(event.target).closest('form').find('input.kode').val(idcustomer[index]);
	            }
	            //$('#email_customer').val(email[index]);               
	        });

	    });  


	    //SECTION HITUNG
		//Hitung Total Bayar dari Jam Main
		function calculate_total(){
			check_jadwal_main();			
	        var day = new Date($('#tgl_main'+mode_id).val()).getDay();	                	        
	        var lapangan = $('#lapangan'+mode_id).val();
	        var jam_mulai = parseInt($('#jam_mulai'+mode_id).val());
	        var jam_selesai = parseInt($('#jam_selesai'+mode_id).val());
	        var jml_minum = 0;

	        if(jam_selesai < jam_mulai){
	            $('#jam_mulai'+mode_id).val(jam_selesai);
	            $('#jam_selesai'+mode_id).val(jam_mulai);
	            jam_mulai = $('#jam_mulai'+mode_id).val();
	            jam_selesai = $('#jam_selesai'+mode_id).val();
	        }

	        if(jam_selesai > 16){
				if(jam_mulai >= 16)
					jml_minum = jam_selesai - jam_mulai;
				else
					jml_minum = jam_selesai - 16;

				if(jml_minum > 1)
					jml_minum = jml_minum * 5;
			}
	        	
	        var mode_mulai;
	        var mode_selesai;
	        var total_bayar = 0;	        
	        
	        //check_jadwal_main();
	        //alert("lap:"+lapangan+" day:"+day+" jm:"+jam_mulai);
	        if(jam_selesai != ""){
	        	if(lapangan === 3 || day === 0 || day === 6){
		            if(jam_mulai < 16)
		                mode_mulai=1;
		            else
		                mode_mulai=2;

		            if(jam_selesai <= 16)
		                mode_selesai=1;
		            else
		                mode_selesai=2;

		            if(mode_mulai === mode_selesai){
		                total_bayar = jam_selesai - jam_mulai;
		                if(mode_mulai === 1 && lapangan === 3)
		                    total_bayar = total_bayar * 100000;
		                else if(mode_mulai === 1 && lapangan < 3)
		                    total_bayar = total_bayar * 90000;
		                else
		                    total_bayar = total_bayar * 135000;
		            } else {
		                total_bayar = (jam_selesai - 16) * 135000;
		                if(lapangan < 3)
		                    total_bayar += (16 - jam_mulai) * 90000;
		                else
		                    total_bayar += (16 - jam_mulai) * 100000;
		            }
		        }else{
		            if(jam_mulai < 12)
		                mode_mulai=1;
		            else if(jam_mulai < 16)
		                mode_mulai=2;
		            else
		                mode_mulai=3;

		            if(jam_selesai <= 12)
		                mode_selesai=1;
		            else if(jam_selesai <= 16)
		                mode_selesai=2;
		            else
		                mode_selesai=3;

		            if(mode_mulai === mode_selesai){
		                total_bayar = jam_selesai - jam_mulai;
		                if(mode_mulai === 1)
		                    total_bayar = total_bayar * 50000;
		                else if(mode_mulai === 2)
		                    total_bayar = total_bayar * 90000;
		                else
		                    total_bayar = total_bayar * 135000;
		            } else {
		                if(mode_selesai === 2)
		                    total_bayar = (jam_selesai - 12) * 90000;
		                else
		                    total_bayar = (jam_selesai - 16) * 135000;

		                if(mode_mulai === 1)
		                    total_bayar += (12 - jam_mulai) * 50000;
		                else
		                    total_bayar += (16 - jam_mulai) * 90000;

		                if(mode_selesai -  mode_mulai > 1)
		                    total_bayar += 4 * 90000;
		            }
		        }		        

		        $('#total_bayar'+mode_id).val(total_bayar);
		        if(mode_id === "_lunas")
		        	$('#total_bayar'+mode_id).closest('form').find('input.krg_lunas').val(total_bayar - $('#diskon_bayar'+mode_id).val());
		        else    
		        	$('#total_bayar'+mode_id).closest('form').find('input.krg_lunas').val(total_bayar - $('#diskon_bayar'+mode_id).val() - $('#bayar'+mode_id).val());

		        //alert(jml_minum);
		        if(jml_minum > 1)		        	
		        	$('#bonus'+mode_id).val(jml_minum+" Club 600mL");
		        else if($('#bonus'+mode_id).val() != "" || $('#bonus'+mode_id).val() != " ")
		        	$('#bonus'+mode_id).val("");
	        }	        
	    }


		$(".jam_main").change(function (){			
	        calculate_total();
	    })

	    function reset_input() {
		    $('#reset_dp').closest('form').find("input[type=text], input[type=number], textarea").val("");
		    $('#reset_lunas').closest('form').find("input[type=text], input[type=number], textarea").val("");
		    $('.override_dp, .override_lunas').attr("disabled", "disabled");
		}

	    function check_jadwal_main(){
	        if(mode_id != "_pelunasan_dp"){
	            if(mode_id === "_edit_dp" || mode_id === "_edit_lunas"){
	                var dataObject =  {
	                    tgl_main: $('#tgl_main'+mode_id).val(),
	                    jam_mulai: $('#jam_mulai'+mode_id).val(),
	                    jam_selesai: $('#jam_selesai'+mode_id).val(),
	                    lapangan:  $('#lapangan'+mode_id).val(),
	                    id_nota_lapangan: $('#id'+mode_id).val()
	                };
	            } else{
	                var dataObject =  {
	                    tgl_main: $('#tgl_main'+mode_id).val(),
	                    jam_mulai: $('#jam_mulai'+mode_id).val(),
	                    jam_selesai: $('#jam_selesai'+mode_id).val(),
	                    lapangan:  $('#lapangan'+mode_id).val()
	                };    
	            }
	            
	            console.log(dataObject);
	            $.ajax({
	                url: "<?php echo site_url('home/check_data_transaksi')?>",
	                type: "POST",
	                data: dataObject,
	                cache: false,
	                success: function(response){
	                    //console.log(response);
	                    if(response[0]['status'] != "CLEAR"){
	                        alert("Lapangan "+response[0]['lapangan']+" sudah terpakai! \nPada tanggal "+response[0]['tgl_main']+" antara pukul "+(response[0]['jam_mulai']).substr(0,5)+" s/d "+(response[0]['jam_selesai']).substr(0,5)+". \nSilahkan ganti lapangan/jam/hari lain.");                                                
	                        reset_input();
	                        $('#jam_mulai'+mode_id).focus();
	                        return false;
	                    }
	                }
	            });
	            return true;
	        }        
	    }

		//Ambil ajax data nota transaksi
		function populate_table(today){			
			var startD = new Date(now.getFullYear(), $('#jadwal_bulan').val()-1, 1).toString("yyyy-MM-dd");
	        var endD = new Date(now.getFullYear(), $('#jadwal_bulan').val(), 0).toString("yyyy-MM-dd");
			var url = "<?php echo base_url();?>tlapangan/get_data_transaksi_rentang/"+$('#jadwal_lapangan').val()+"/-/-";	        
			lastToday = today;

			if(today === false){
				url = "<?php echo base_url();?>tlapangan/get_data_transaksi_rentang/"+$('#jadwal_lapangan').val()+"/"+startD+"/"+endD;
			}
			
			if ($.fn.DataTable.isDataTable( '#tabel_nota_lapangan' ) ) {										
	    		table.ajax.url(url).load();
	    		//var table = $( '#tabel_nota_lapangan' ).DataTable();
	    		//alert(table.column(':contains(EDIT)'));
	    	} else {
	    		table = $('#tabel_nota_lapangan').DataTable( {
	    			"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
						$('td:contains(EDIT)',nRow).closest("tr").css("color","orange");
						$('td:contains(BATAL)',nRow).closest("tr").css("color","red");
						if(document.getElementById('checkbox_edit').checked)
	    					$('td:contains(EDIT)',nRow).closest("tr").removeClass("hidden");
	    				else
	    					$('td:contains(EDIT)',nRow).closest("tr").addClass("hidden");
	    				if(document.getElementById('checkbox_batal').checked)
	    					$('td:contains(BATAL)',nRow).closest("tr").removeClass("hidden");
	    				else
	    					$('td:contains(BATAL)',nRow).closest("tr").addClass("hidden");
					},
			        "ajax": url,
			        "columns": [			        
			            { "data": "id", "class": "id" },
			            { "data": "id_II", "class": "id_II" },
			            { "data": "tanggal", "class": "tanggal" },
			            { "data": "bayar", "class": "bayar" },			            
			            { "data": "diskon", "class": "diskon" },
			            { "data": "total_bayar", "class": "total_bayar" },
			            { "data": "lapangan", "class": "lapangan" },
			            { "data": "tgl_main", "class": "tgl_main" },
			            { "data": "jam_mulai", "class": "jam_mulai" },
			            { "data": "jam_selesai", "class": "jam_selesai" },
			            { "data": "nama", "class": "nama" },
			            { "data": "telp", "class": "telp" },
			            { "data": "bonus", "class": "bonus" },
			            { "data": "keterangan", "class": "keterangan" },
			            { "data": "override", "class": "override" },		            
			            { "data": "id_operator", "class": "id_operator" },
			            { "data": "status", "class": "status" },
			            { "data": "act", "class": "act" }		            		            
			        ]
			    });				
	    	}	   	    				
		}


		$(document).on('change', '#jadwal_bulan', function(event){	            	
	    	populate_table(false);
	    });	    

	    $(document).on('change', '#jadwal_lapangan', function(event){	        
	        populate_table(false);
	    });

	    $(document).on('click', '#transaksi_hari_ini', function(event){
	    	populate_table(true);
	    });

	    $(document).on('change', '#checkbox_edit', function(event){	            	
	    	populate_table(lastToday);	    	
	    });

	    $(document).on('change', '#checkbox_batal', function(event){	            	
	    	populate_table(lastToday);
	    });
	    
		//Hitung Kurang Bayar dari Jumlah diskon || NOT DONE YET
		$('#diskon_bayar_dp').change(function (){						
			if($('[name="override"]').val() != ""){
				$('#kurang_bayar_dp').val(parseInt($('#kurang_bayar_dp').val())-parseInt($('#diskon_bayar_dp').val()));
				$('#total_bayar_dp').val(parseInt($('#total_bayar_dp').val())-parseInt($('#diskon_bayar_dp').val()));
			} else{
				$('#diskon_bayar_dp').val("");
				$('#diskon_bayar_dp').attr("disabled", "disabled");
			}
		})
		
		$('.bayar').change(function(event){			       
	        calculate_total();	            
	    })

		$(document).on('click', '.pelunasan_dp', function(event){						
			set_value_event_click(event, "_pelunasan_dp");
			//set_value_event_click(event, "_edit_dp");			
		})

		//to tell the submit button which tab is active. Save the value to form global variable
		$(document).on('click', '.modal-tab', function(event){			
			mode_id = $(event.target).attr('name');			     
		})

		//When the main submit of transaction modal input clicked
		$(document).on('click', '#tambah_jadwal', function(){			
	        $('#submit'+mode_id).click();        
	        //$('.active form.form-tambah').submit();
	    })

		//#edit_jadwal is an save button for multiple tabs, when it clicked, it will submit the active one		
		$(document).on('click', '#edit_jadwal', function(){
	        $('.active form.form-edit').submit();
	    })

		//what to do when form is just submitted
		$(document).on('submit', '.form-edit', function(){						
			$(".submit").removeAttr("disabled");
		})

		$(document).on('submit', '.form-tambah', function(){						
			$(".submit").removeAttr("disabled");
		})

		//when batal_transaksi button clicked
		$(document).on('click', '.batal_transaksi', function(event){
			e.preventDefault();
				
			var d = confirm('Hapus user?');
			if (d == true){				
				var id = $(event.target).closest('tr').children('td.id_nota_lapangan').text();
				$.ajax({
					url: "<?php echo base_url() ?>tlapangan/batal_transaksi/"+id,
	                type: "POST",
	                success: function(data){

	                },
					error: function  (data) {
						alert('terjadi kesalahan, gagal');
						return false;
					}	
				})		
			}else{
				return false;
			}
			$(this).closest('tr').fadeOut(function () {
				$(this).remove();
			})							
		})

		function set_value_event_click(args, mode){		
			for (var i = 1; i < 3; i++) {
				if(i === 2)
					mode = "_edit_dp";

		        $('#id'+mode).val($(args.target).closest('tr').children('td.id').text());
		        $('#tgl'+mode).val($(args.target).closest('tr').children('td.tanggal').text());
		        $('#tgl_lunas'+mode).datetimepicker('setDate', new Date());
		        $('#nama'+mode).val($(args.target).closest('tr').children('td.nama').text());
		        $('#telp'+mode).val($(args.target).closest('tr').children('td.telp').text());
		        $('#lapangan_old').val($('#jadwal_lapangan').val());
		        $('#lapangan'+mode).val($('#jadwal_lapangan').val());
		        $('#tgl_main'+mode).val($(args.target).closest('tr').children('td.tgl_main').text());
		        $('#tgl_main_old').val($(args.target).closest('tr').children('td.tgl_main').text());
		        $('#jam_mulai_old').val($(args.target).closest('tr').children('td.jam_mulai').text());
		        $('#jam_selesai_old').val($(args.target).closest('tr').children('td.jam_selesai').text());
		        $('#jam_mulai'+mode).val($(args.target).closest('tr').children('td.jam_mulai').text());
		        $('#jam_selesai'+mode).val($(args.target).closest('tr').children('td.jam_selesai').text());
		        $('#total_bayar'+mode).val($(args.target).closest('tr').children('td.total_bayar').text());
		        $('#bayar'+mode).val($(args.target).closest('tr').children('td.bayar').text());
		        $('#diskon_bayar_old').val($(args.target).closest('tr').children('td.diskon').text());
		        $('#diskon_bayar'+mode).val($(args.target).closest('tr').children('td.diskon').text());	        
		        $('#kurang_bayar'+mode).val(parseInt($('#total_bayar'+mode).val()) - parseInt($('#bayar'+mode).val()) - parseInt($('#diskon_bayar'+mode).val()));
		        $('#bayar_lunas'+mode).val(parseInt($('#total_bayar'+mode).val()) - parseInt($('#bayar'+mode).val()) - parseInt($('#diskon_bayar'+mode).val()));
		        $('#bonus'+mode).val($(args.target).closest('tr').children('td.bonus').text());
		        $('#keterangan_old').val($(args.target).closest('tr').children('td.keterangan').text());
		        $('#keterangan'+mode).val($(args.target).closest('tr').children('td.keterangan').text());
		        $('#batal_main').attr('href', $(args.target).closest('tr').children('td.act').children('a.batal_transaksi').attr('href'));
	       	}	        
	        $('#edit_transaksi').modal('show');
	        $('#tab-edit-pelunasan-dp').click();	        
	    }		

	    $(document).on('click', '#btn_input_transaksi', function(){
	    	$('#tab-dp').click();	    		    	
	    	$('#lapangan_dp').val($('#jadwal_lapangan').val());
	    	$('#lapangan_lunas').val($('#jadwal_lapangan').val());
	    })

	    $('#input_transaksi').on('hidden.bs.modal', function () {	    	
	    	$('#reset_dp').closest('form').find("input[type=text], input[type=number], textarea").val("");
	    	$('#reset_lunas').closest('form').find("input[type=text], input[type=number], textarea").val("");
	    	$('.override_dp, .override_lunas').attr("disabled", "disabled");
	    })

	    $('#edit_transaksi').on('hidden.bs.modal', function () {	    	
	    	$('#reset_edit_dp').closest('form').find("input[type=text], input[type=number], textarea").val("");
	    	$('#reset_pelunasan_dp').closest('form').find("input[type=text], input[type=number], textarea").val("");
	    	$('.override_edit_dp, .override_pelunasan_dp').attr("disabled", "disabled");
	    })
		
        // OVERRIDE PENGGUNA        
        $(document).on('click', '.override_pengguna', function override_pengguna(event) {
        	//alert($(event.target).attr("name"));
        	override_id = $(event.target).attr("name");
            $('#override-form')[0].reset(); // reset form on modals
            $('#override-form .form-group').removeClass('has-error'); // clear error class
            $('#override-form .help-block').empty(); // clear error string
            
            $('#modal-override').modal('show'); // show bootstrap modal
            $('#modal-override .modal-title').text('Override Pengguna'); // Set Title to Bootstrap modal title
        });

        $(document).on('click', '#reset_override', function reset_override() {
            $('#override-form')[0].reset();

            $('#override-form .form-group').removeClass('has-error'); // clear error class
            $('#override-form .help-block').empty(); // clear error string
        })

        $(document).on('click', '#save_override', function save_override(event)
        {
            $('#btnSaveOverride').text('Saving...'); //change button text
            $('#btnSaveOverride').attr('disabled',true); //set button disable
         
            // ajax adding data to database
            $.ajax({
                url : "<?php echo site_url('tlapangan/ajax_override_pengguna')?>",
                type: "POST",
                data: $('#override-form').serialize(),
                dataType: "JSON",
                success: function(data)
                {         
                    if(data.status) //if success close modal and update input main-form
                    {
                        $('#modal-override').modal('hide');                                               
                        $('#override_view'+mode_id).val('ID: ' + data.user_id + ' (' + data.username + ')');
                        $('#override'+mode_id).val(data.user_id);
                        $('#override'+mode_id).closest('form').find('[name="diskon"]').removeAttr('disabled');
                    }
                    else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            
                            $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent TWICE to select div form-group class and add has-error class
                            $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
         
                    $('#btnSaveOverride').text('Save'); //change button text
                    $('#btnSaveOverride').attr('disabled',false); //set button enable
         
         
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error override data');
                    $('#btnSaveOverride').text('Save'); //change button text
                    $('#btnSaveOverride').attr('disabled',false); //set button enable
         
                }
            });
        })		
	})
</script>