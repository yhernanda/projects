﻿Public Class Subtitle

    Private Sub Subtitle_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.SetBounds(370, 500, FormPlayer.Player.Size.Width, CInt(FormPlayer.Player.Size.Height / 5))
        lblSubs.SetBounds(CInt((Me.Width / 2) - (lblSubs.Width / 2)), lblSubs.Location.Y, lblSubs.Width, lblSubs.Height)
        Me.Visible = True
    End Sub

    Private Sub lblSubs_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblSubs.SizeChanged
        lblSubs.SetBounds(CInt((Me.Width / 2) - (lblSubs.Width / 2)), lblSubs.Location.Y, lblSubs.Width, lblSubs.Height)
    End Sub
End Class